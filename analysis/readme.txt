
Files info
----------

bjack.idb is the IDA Database of Bombjack Arcade game
It is based on bjack.z80, main CPU code
bjack.z80 is 09_j01b.bin + 10_l01b.bin + 11_m01b.bin + 12_n01b.bin +13.1r

bjack.asm is an export of IDA Database, but may be outdated
 
bjack_audio.idb is the IDA Database of Bombjack Arcade Sound engineIt is based on 01_h03t.bin, audio CPU code

assets folder contains extrat of all Bombjack assets (char, sprite, map, music)

bjrep folder is original Bombjack reverse-engineering project, by André Majorel



Game info
---------
1/ Level platform
Pointer table at 0x4c0b (code) or 0x0c0b (11_m01b.bin) =>0x7c74
Level Def Call adress at 0x7c74 (code) or 0x1c74 (12_n01b.bin) + 2bytes*level_number
If the Call = 0x32 0x7D 0xC9, empty level (every 5)
If the Call = 0x32 0x7D 0xCD and 0xCD 0xCF 0x00, read Level Def adress (L1: 0x7300)

Read Line 1 adress
Until Line 1 adress, read Line X adress

For each line
Read posX (byte) and posY (byte)
Read chartype (byte) and palcolor (byte)
chartype is one of the following:
0x60 (start of horizontal platform)
0x61 (end of horizontal platform)
0x62 (horizontal platform)
0x63 (horizontal platform, after a 0x62)
0x64 (Up of a vertical platform)
0x65 (Down of a vertical platform)
0x66 (vertical platform)
0x67 (vertical platform, after a 0x66)
0x68 (Up-left corner)
0x69 (Up-right corner)
0x6A (Down-left corner)
0x6B (Down-right corner)
0x6C (Tri to right) |-
0x6D (Tri to left)  -|
0x6E (Tri to down)  T
0x6F (Tri to up)    _L
...see the 8x8 char to dump them

If chartype=0xFF and palcolor = 0xFF, end of line, process next one if exists
if chartype!=0xFF and palcolor = 0xFF, add chartype to current posX, skip posX and posY, continue (used when more than one platform in one line)

The border isn't coded here, somewhere else... :)

2/ Level Background
(FUTURE : find palette info)

All is in 02_p04t.bin, at 90∞
If you mod it, MAME doesn't reflect the change, why ? It doesn't load it!
Perhaps try it on a TRUE BombJack but since it works on MAME, i think it re-creates it at the fly.

Each background is 16x16 char idx + 16x16 palette index.
So each of the 256 first bytes is an index in the background chars data (06_l08t.bin, 07_n08t.bin & 08_r08t.bin)
And each of the 256 next bytes is the pal to use.
Yes...each char can use a different pal. Great! No ?!

I think the first and last column/row (all the FF) are ignored but not sure....


Hacks/cheats
------------
- change checksums at C356h
- Change the values at 5AE7 to up the number of credit by coin
- Change the values at 5B1C to up the number of lives by credit
- something special with DSW1 : 5 lifes, 1Coin->1Credit, 1Coin->1Credit, Sound on ?
- change the level/bomb design number at DE38h
- change the collide data of platform at 1C6Ah
- change the platform pos at 7300h
- change the bomb design at C828h
- change the graph of the bomb (access to hidden one) by using D8BCh data!



ROMs info
---------
Code :
09_j01b.bin
10_l01b.bin
11_m01b.bin
12_n01b.bin
13.1r

Sound :
01_h03t.bin

Chars :
03_e08t.bin
04_h08t.bin
05_k08t.bin

Background :
06_l08t.bin
07_n08t.bin
08_r08t.bin

Sprites :
16_m07b.bin
15_l07b.bin
14_j07b.bin

Background maps :
02_p04t.bin