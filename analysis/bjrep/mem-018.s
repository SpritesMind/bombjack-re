; Commented disassembly and dump of the ROMs of Bombjack.
; http://www.teaser.fr/~amajorel/bjrep/
; 
; The assembly code and data is copyright Tehkan 1984. The
; comments and C-like pseudo-code are copyright Andr� Majorel 2001
; and available under the terms of the GNU General Public License
; version 2.
; 
; This file was made using as a base the output of z80dasm, Marcel
; de Kogel's Z80 disassembler. The memory map and hardware
; information comes from Mame's Bombjack drivers, written by Mirko
; Buffoni.
; 
; Bombjack uses a character set that is mostly ASCII except that:
; - in strings, 3Ah (:) looks like "(c)"
; - in strings, 3Bh (;) looks like "(p)"
; - in strings, 3Eh3Ch (><) looks like "Pts"

    ld     a,$00           ; 0000 3E 00
    ld     ($b000),a       ; 0002 32 00 B0
    ld     a,$00           ; 0005 3E 00
    ld     ($9e00),a       ; 0007 32 00 9E
    ld     a,$01           ; 000A 3E 01
    ld     ($b800),a       ; 000C 32 00 B8
    ld     sp,$9000        ; 000F 31 00 90
    jp     $0120           ; 0012 C3 20 01

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0015
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0025
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0035
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0045
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0055
    hex    00                                              ; 0065

    push   af              ; 0066 F5
    ld     a,$00           ; 0067 3E 00
    ld     ($b000),a       ; 0069 32 00 B0
    pop    af              ; 006C F1
    jp     $01c0           ; 006D C3 C0 01
    nop                    ; 0070 00
    djnz   $0073           ; 0071 10 00
    jr     nz,$0075        ; 0073 20 00
    jr     nc,$0077        ; 0075 30 00
    ld     d,b             ; 0077 50
    nop                    ; 0078 00
    ld     b,b             ; 0079 40
    add    a,b             ; 007A 80
    adc    a,a             ; 007B 8F
    nop                    ; 007C 00
    adc    a,a             ; 007D 8F
    add    a,b             ; 007E 80
    adc    a,(hl)          ; 007F 8E
    nop                    ; 0080 00
    adc    a,(hl)          ; 0081 8E
    add    a,b             ; 0082 80
    adc    a,l             ; 0083 8D
    dec    b               ; 0084 05
    nop                    ; 0085 00
    jr     nz,$00a8        ; 0086 20 20
    jr     nz,$00aa        ; 0088 20 20
    ld     b,b             ; 008A 40
    rst    38h             ; 008B FF
    rst    38h             ; 008C FF
    rst    38h             ; 008D FF
    rst    38h             ; 008E FF
    rst    38h             ; 008F FF
    dec    b               ; 0090 05
    nop                    ; 0091 00
    nop                    ; 0092 00
    nop                    ; 0093 00
    nop                    ; 0094 00
    nop                    ; 0095 00
    jr     nz,$0097        ; 0096 20 FF
    rst    38h             ; 0098 FF
    rst    38h             ; 0099 FF
    rst    38h             ; 009A FF
    rst    38h             ; 009B FF
    rst    38h             ; 009C FF
    dec    b               ; 009D 05
    rst    38h             ; 009E FF
    ld     b,b             ; 009F 40
    ld     b,b             ; 00A0 40
    ld     b,b             ; 00A1 40
    ld     b,b             ; 00A2 40
    nop                    ; 00A3 00
    rst    38h             ; 00A4 FF
    rst    38h             ; 00A5 FF
    rst    38h             ; 00A6 FF
    rst    38h             ; 00A7 FF
    rst    38h             ; 00A8 FF
    rst    38h             ; 00A9 FF

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 00AA
    hex    00 00 00 00 00 00                               ; 00BA

; Jump table

    jp     $01ad           ; 00C0 C3 AD 01
    jp     $0718           ; 00C3 C3 18 07
    jp     $0192           ; 00C6 C3 92 01
    jp     $0864           ; 00C9 C3 64 08
    jp     $0850           ; 00CC C3 50 08
    jp     $0b1d           ; 00CF C3 1D 0B
    jp     $0b41           ; 00D2 C3 41 0B
    jp     $0a21           ; 00D5 C3 21 0A
    jp     $09fd           ; 00D8 C3 FD 09
    jp     $0a82           ; 00DB C3 82 0A
    jp     $0a98           ; 00DE C3 98 0A
    jp     $09ee           ; 00E1 C3 EE 09
    jp     $0bac           ; 00E4 C3 AC 0B
    jp     $0618           ; 00E7 C3 18 06
    jp     $0bb0           ; 00EA C3 B0 0B
    jp     $0be4           ; 00ED C3 E4 0B
    jp     $0bf2           ; 00F0 C3 F2 0B
    jp     $0c01           ; 00F3 C3 01 0C
    jp     $0c42           ; 00F6 C3 42 0C
    jp     $0c4c           ; 00F9 C3 4C 0C
    jp     $600f           ; 00FC C3 0F 60
    jp     $ffff           ; 00FF C3 FF FF

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0102
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00       ; 0112

    call   $03c7           ; 0120 CD C7 03
    call   $0720           ; 0123 CD 20 07
    ld     sp,$9000        ; 0126 31 00 90
    ld     hl,$8000        ; 0129 21 00 80
    ld     bc,$0800        ; 012C 01 00 08
    call   $0850           ; 012F CD 50 08
    ld     hl,$8800        ; 0132 21 00 88
    ld     bc,$07f6        ; 0135 01 F6 07
    call   $0850           ; 0138 CD 50 08
    ld     hl,$9000        ; 013B 21 00 90
    ld     bc,$03ff        ; 013E 01 FF 03
    call   $085a           ; 0141 CD 5A 08
    ld     hl,$9400        ; 0144 21 00 94
    ld     bc,$03ff        ; 0147 01 FF 03
    call   $0850           ; 014A CD 50 08
    ld     hl,$9820        ; 014D 21 20 98
    ld     bc,$0060        ; 0150 01 60 00
    call   $0850           ; 0153 CD 50 08
    ld     hl,$9c00        ; 0156 21 00 9C
    ld     bc,$0100        ; 0159 01 00 01
    call   $0850           ; 015C CD 50 08
    ld     hl,$9a00        ; 015F 21 00 9A
    ld     bc,$00c8        ; 0162 01 C8 00
    call   $0850           ; 0165 CD 50 08
    ld     a,$01           ; 0168 3E 01
    ld     ($8700),a       ; 016A 32 00 87
    ld     a,$01           ; 016D 3E 01
    ld     ($8701),a       ; 016F 32 01 87
    ld     a,($b004)       ; 0172 3A 04 B0
    ld     ($8013),a       ; 0175 32 13 80
    ld     a,($b005)       ; 0178 3A 05 B0
    ld     ($8014),a       ; 017B 32 14 80
    call   $02b9           ; 017E CD B9 02
    call   $0908           ; 0181 CD 08 09
    call   $0618           ; 0184 CD 18 06
    ld     a,$01           ; 0187 3E 01
    ld     ($808c),a       ; 0189 32 8C 80
    ld     hl,$0001        ; 018C 21 01 00
    ld     ($8022),hl      ; 018F 22 22 80

; 0192h - ?
;
; Vector: 00C6h

    ld     a,$00           ; 0192 3E 00		; (0xb000) = 0
    ld     ($b000),a       ; 0194 32 00 B0
    ld     a,$ff           ; 0197 3E FF		; (0x8002) = 0
    ld     ($8002),a       ; 0199 32 02 80
    ld     sp,$9000        ; 019C 31 00 90	; sp = 0x9000
    ld     a,$00           ; 019F 3E 00		; (0x8003) = 0
    ld     ($8003),a       ; 01A1 32 03 80
    ld     hl,$0084        ; 01A4 21 84 00	; ?
    call   $0368           ; 01A7 CD 68 03
    jp     $0205           ; 01AA C3 05 02

    ld     a,$00           ; 01AD 3E 00
    ld     ($b000),a       ; 01AF 32 00 B0
    ld     a,$00           ; 01B2 3E 00
    ld     ($8003),a       ; 01B4 32 03 80
    ld     hl,$0090        ; 01B7 21 90 00
    call   $0368           ; 01BA CD 68 03
    jp     $0205           ; 01BD C3 05 02
    push   af              ; 01C0 F5
    push   bc              ; 01C1 C5
    push   de              ; 01C2 D5
    push   hl              ; 01C3 E5
    push   ix              ; 01C4 DD E5
    push   iy              ; 01C6 FD E5
    call   $029f           ; 01C8 CD 9F 02
    ld     sp,$9000        ; 01CB 31 00 90
    nop                    ; 01CE 00
    nop                    ; 01CF 00
    nop                    ; 01D0 00
    call   $0958           ; 01D1 CD 58 09
    call   $09ab           ; 01D4 CD AB 09
    call   $601b           ; 01D7 CD 1B 60
    call   $6012           ; 01DA CD 12 60
    call   $0382           ; 01DD CD 82 03
    call   $062f           ; 01E0 CD 2F 06
    call   $5fd9           ; 01E3 CD D9 5F
    call   $02bd           ; 01E6 CD BD 02
    call   $0864           ; 01E9 CD 64 08
    call   $09e2           ; 01EC CD E2 09
    ld     a,($8077)       ; 01EF 3A 77 80
    or     a               ; 01F2 B7
    jr     nz,$01fa        ; 01F3 20 05
    call   $0365           ; 01F5 CD 65 03
    jr     $0205           ; 01F8 18 0B
    ld     a,$00           ; 01FA 3E 00
    ld     ($8003),a       ; 01FC 32 03 80
    ld     hl,$0084        ; 01FF 21 84 00
    call   $0368           ; 0202 CD 68 03

; 0205h - ?

    ld     a,($8003)       ; 0205 3A 03 80	; if ((0x8003) == 0) {
    or     a               ; 0208 B7
    jp     nz,$0247        ; 0209 C2 47 02
    ld     a,$ff           ; 020C 3E FF		; (0x8002) = 0xff
    ld     ($8002),a       ; 020E 32 02 80
    ld     a,$00           ; 0211 3E 00		; (0xb000) = 0
    ld     ($b000),a       ; 0213 32 00 B0
    ld     a,$01           ; 0216 3E 01		; (0xb000) = 11
    ld     ($b000),a       ; 0218 32 00 B0
    ld     b,$05           ; 021B 06 05		; ?
    ld     c,$00           ; 021D 0E 00
    ld     hl,$800e        ; 021F 21 0E 80
    ld     a,(hl)          ; 0222 7E
    and    $30             ; 0223 E6 30
    jp     nz,$022e        ; 0225 C2 2E 02
    ld     a,(hl)          ; 0228 7E
    and    $c0             ; 0229 E6 C0
    jp     nz,$0235        ; 022B C2 35 02
    inc    hl              ; 022E 23
    inc    c               ; 022F 0C
    djnz   $0222           ; 0230 10 F0
    jp     $0216           ; 0232 C3 16 02
    ld     a,$00           ; 0235 3E 00
    ld     ($b000),a       ; 0237 32 00 B0
    ld     ($8002),a       ; 023A 32 02 80
    ld     a,c             ; 023D 79
    ld     ($8000),a       ; 023E 32 00 80
    ld     a,(hl)          ; 0241 7E
    and    $80             ; 0242 E6 80
    jp     z,$0268         ; 0244 CA 68 02
    ld     hl,($8000)      ; 0247 2A 00 80	; } else
    add    hl,hl           ; 024A 29
    ld     de,$8004        ; 024B 11 04 80
    add    hl,de           ; 024E 19
    ld     e,(hl)          ; 024F 5E
    inc    hl              ; 0250 23
    ld     d,(hl)          ; 0251 56
    ex     de,hl           ; 0252 EB
    ld     sp,hl           ; 0253 F9
    pop    iy              ; 0254 FD E1
    pop    ix              ; 0256 DD E1
    pop    hl              ; 0258 E1
    pop    de              ; 0259 D1
    pop    bc              ; 025A C1
    ld     a,$00           ; 025B 3E 00
    ld     ($b000),a       ; 025D 32 00 B0
    ld     a,$01           ; 0260 3E 01
    ld     ($b000),a       ; 0262 32 00 B0
    pop    af              ; 0265 F1
    retn                   ; 0266 ED 45

    ld     (hl),$80        ; 0268 36 80
    ld     hl,($8000)      ; 026A 2A 00 80
    add    hl,hl           ; 026D 29
    ld     de,$007a        ; 026E 11 7A 00
    add    hl,de           ; 0271 19
    ld     e,(hl)          ; 0272 5E
    inc    hl              ; 0273 23
    ld     d,(hl)          ; 0274 56
    ex     de,hl           ; 0275 EB
    ld     sp,hl           ; 0276 F9
    ld     hl,($8000)      ; 0277 2A 00 80
    add    hl,hl           ; 027A 29
    ld     de,$0070        ; 027B 11 70 00
    add    hl,de           ; 027E 19
    ld     e,(hl)          ; 027F 5E
    inc    hl              ; 0280 23
    ld     d,(hl)          ; 0281 56
    ex     de,hl           ; 0282 EB
    ld     a,$00           ; 0283 3E 00
    ld     ($b000),a       ; 0285 32 00 B0
    ld     a,$01           ; 0288 3E 01
    ld     ($b000),a       ; 028A 32 00 B0
    jp     (hl)            ; 028D E9
    ld     a,$00           ; 028E 3E 00
    ld     ($b000),a       ; 0290 32 00 B0
    ld     hl,($8000)      ; 0293 2A 00 80
    ld     de,$800e        ; 0296 11 0E 80
    add    hl,de           ; 0299 19
    res    7,(hl)          ; 029A CB BE
    jp     $0205           ; 029C C3 05 02
    ld     a,($8002)       ; 029F 3A 02 80
    cp     $ff             ; 02A2 FE FF
    ret    z               ; 02A4 C8
    ld     hl,($8000)      ; 02A5 2A 00 80
    add    hl,hl           ; 02A8 29
    ld     de,$8004        ; 02A9 11 04 80
    add    hl,de           ; 02AC 19
    ex     de,hl           ; 02AD EB
    ld     hl,$0000        ; 02AE 21 00 00
    add    hl,sp           ; 02B1 39
    inc    hl              ; 02B2 23
    inc    hl              ; 02B3 23
    ex     de,hl           ; 02B4 EB
    ld     (hl),e          ; 02B5 73
    inc    hl              ; 02B6 23
    ld     (hl),d          ; 02B7 72
    ret                    ; 02B8 C9
    call   $5fd0           ; 02B9 CD D0 5F
    ret                    ; 02BC C9
    ld     a,($b002)       ; 02BD 3A 02 B0
    and    $01             ; 02C0 E6 01
    jp     nz,$02d0        ; 02C2 C2 D0 02
    ld     ix,$8015        ; 02C5 DD 21 15 80
    ld     h,$00           ; 02C9 26 00
    call   $02ee           ; 02CB CD EE 02
    jr     $02d5           ; 02CE 18 05
    ld     a,$01           ; 02D0 3E 01
    ld     ($8015),a       ; 02D2 32 15 80
    ld     a,($b002)       ; 02D5 3A 02 B0
    and    $02             ; 02D8 E6 02
    jp     nz,$02e8        ; 02DA C2 E8 02
    ld     ix,$8082        ; 02DD DD 21 82 80
    ld     h,$01           ; 02E1 26 01
    call   $02ee           ; 02E3 CD EE 02
    jr     $02ed           ; 02E6 18 05
    ld     a,$01           ; 02E8 3E 01
    ld     ($8082),a       ; 02EA 32 82 80
    ret                    ; 02ED C9
    ld     a,(ix+$00)      ; 02EE DD 7E 00
    cp     $00             ; 02F1 FE 00
    jr     z,$0364         ; 02F3 28 6F
    ld     a,($8017)       ; 02F5 3A 17 80
    ld     ($8090),a       ; 02F8 32 90 80
    ld     a,h             ; 02FB 7C
    cp     $00             ; 02FC FE 00
    jr     z,$0317         ; 02FE 28 17
    ld     a,($8084)       ; 0300 3A 84 80
    cp     $ff             ; 0303 FE FF
    jr     nz,$0317        ; 0305 20 10
    ld     a,($8085)       ; 0307 3A 85 80
    inc    a               ; 030A 3C
    ld     ($8085),a       ; 030B 32 85 80
    cp     $02             ; 030E FE 02
    jr     c,$0360         ; 0310 38 4E
    ld     a,$00           ; 0312 3E 00
    ld     ($8085),a       ; 0314 32 85 80
    ld     b,(ix+$01)      ; 0317 DD 46 01
    ld     a,($8017)       ; 031A 3A 17 80
    cp     $99             ; 031D FE 99
    jp     nc,$0333        ; 031F D2 33 03
    ld     a,($8017)       ; 0322 3A 17 80
    add    a,$01           ; 0325 C6 01
    daa                    ; 0327 27
    ld     ($8017),a       ; 0328 32 17 80
    dec    b               ; 032B 05
    ld     a,b             ; 032C 78
    cp     $00             ; 032D FE 00
    jr     nz,$031a        ; 032F 20 E9
    jr     $033a           ; 0331 18 07
    ld     a,($8087)       ; 0333 3A 87 80
    add    a,b             ; 0336 80
    ld     ($8087),a       ; 0337 32 87 80
    ld     (ix+$00),$00    ; 033A DD 36 00 00
    ld     a,($8070)       ; 033E 3A 70 80
    or     a               ; 0341 B7
    ret    nz              ; 0342 C0
    call   $00f0           ; 0343 CD F0 00
    ld     a,$10           ; 0346 3E 10
    call   $00fc           ; 0348 CD FC 00
    ld     a,($8081)       ; 034B 3A 81 80
    or     a               ; 034E B7
    jr     z,$0358         ; 034F 28 07
    ld     a,($8090)       ; 0351 3A 90 80
    cp     $01             ; 0354 FE 01
    jr     nz,$0364        ; 0356 20 0C
    ld     a,$00           ; 0358 3E 00
    ld     ($8003),a       ; 035A 32 03 80
    jp     $00c6           ; 035D C3 C6 00
    ld     (ix+$00),$00    ; 0360 DD 36 00 00
    ret                    ; 0364 C9
    ld     hl,$009d        ; 0365 21 9D 00

; 0368h - ?

    ld     de,$800e        ; 0368 11 0E 80
    ld     b,(hl)          ; 036B 46
    inc    hl              ; 036C 23
    ld     a,(hl)          ; 036D 7E
    cp     $ff             ; 036E FE FF
    jr     z,$037a         ; 0370 28 08
    inc    hl              ; 0372 23
    ld     a,(hl)          ; 0373 7E
    ld     (de),a          ; 0374 12
    inc    de              ; 0375 13
    djnz   $0372           ; 0376 10 FA
    jr     $0381           ; 0378 18 07
    inc    hl              ; 037A 23
    ld     a,(de)          ; 037B 1A
    or     (hl)            ; 037C B6
    ld     (de),a          ; 037D 12
    inc    de              ; 037E 13
    djnz   $037a           ; 037F 10 F9
    ret                    ; 0381 C9

    push   af              ; 0382 F5
    push   bc              ; 0383 C5
    push   hl              ; 0384 E5
    ld     hl,$8018        ; 0385 21 18 80
    ld     a,($8027)       ; 0388 3A 27 80
    ld     b,a             ; 038B 47
    ld     a,($8070)       ; 038C 3A 70 80
    or     a               ; 038F B7
    jp     nz,$0398        ; 0390 C2 98 03
    ld     a,($807a)       ; 0393 3A 7A 80
    jr     $03a2           ; 0396 18 0A
    ld     a,($b000)       ; 0398 3A 00 B0
    bit    0,b             ; 039B CB 40
    jr     z,$03a2         ; 039D 28 03
    jp     $0fe0           ; 039F C3 E0 0F
    ld     bc,$0500        ; 03A2 01 00 05
    rra                    ; 03A5 1F
    jr     nc,$03bd        ; 03A6 30 15
    rl     (hl)            ; 03A8 CB 16
    push   af              ; 03AA F5
    ld     a,(hl)          ; 03AB 7E
    cp     $03             ; 03AC FE 03
    jr     z,$03b7         ; 03AE 28 07
    pop    af              ; 03B0 F1
    inc    hl              ; 03B1 23
    jr     nc,$03bf        ; 03B2 30 0B
    inc    (hl)            ; 03B4 34
    jr     $03c0           ; 03B5 18 09
    pop    af              ; 03B7 F1
    inc    hl              ; 03B8 23
    ld     (hl),$ff        ; 03B9 36 FF
    jr     $03c0           ; 03BB 18 03
    ld     (hl),c          ; 03BD 71
    inc    hl              ; 03BE 23
    ld     (hl),c          ; 03BF 71
    inc    hl              ; 03C0 23
    djnz   $03a5           ; 03C1 10 E2
    pop    hl              ; 03C3 E1
    pop    bc              ; 03C4 C1
    pop    af              ; 03C5 F1
    ret                    ; 03C6 C9
    ld     a,$00           ; 03C7 3E 00
    ld     ($808f),a       ; 03C9 32 8F 80
    call   $0618           ; 03CC CD 18 06
    ld     a,$01           ; 03CF 3E 01
    ld     ($808c),a       ; 03D1 32 8C 80
    call   $09ab           ; 03D4 CD AB 09
    ld     hl,$9820        ; 03D7 21 20 98
    ld     bc,$0060        ; 03DA 01 60 00
    call   $00cc           ; 03DD CD CC 00
    ld     hl,$9000        ; 03E0 21 00 90
    ld     bc,$0400        ; 03E3 01 00 04
    call   $00cc           ; 03E6 CD CC 00
    ld     hl,$9400        ; 03E9 21 00 94
    ld     bc,$0400        ; 03EC 01 00 04
    call   $00cc           ; 03EF CD CC 00
    ld     hl,$c662        ; 03F2 21 62 C6
    ld     de,$9cc0        ; 03F5 11 C0 9C
    ld     bc,$0040        ; 03F8 01 40 00
    ldir                   ; 03FB ED B0
    call   $00cf           ; 03FD CD CF 00
    cp     b               ; 0400 B8
    push   bc              ; 0401 C5
    ld     d,$cd           ; 0402 16 CD
    rst    08h             ; 0404 CF
    nop                    ; 0405 00
    and    d               ; 0406 A2
    add    a,$08           ; 0407 C6 08
    ld     iy,$c356        ; 0409 FD 21 56 C3
    ld     l,(iy+$00)      ; 040D FD 6E 00
    ld     h,(iy+$01)      ; 0410 FD 66 01
    xor    a               ; 0413 AF
    ld     c,a             ; 0414 4F
    ld     d,a             ; 0415 57
    ld     e,a             ; 0416 5F
    ld     b,$08           ; 0417 06 08
    ld     a,(hl)          ; 0419 7E
    rrca                   ; 041A 0F
    jr     nc,$0424        ; 041B 30 07
    inc    e               ; 041D 1C
    jr     nz,$0424        ; 041E 20 04
    inc    d               ; 0420 14
    jr     nz,$0424        ; 0421 20 01
    inc    c               ; 0423 0C
    jp     $1f10           ; 0424 C3 10 1F
    and    $10             ; 0427 E6 10
    jp     nz,$0539        ; 0429 C2 39 05
    djnz   $041a           ; 042C 10 EC
    inc    hl              ; 042E 23
    ld     a,(iy+$02)      ; 042F FD 7E 02
    cp     l               ; 0432 BD
    jr     nz,$0417        ; 0433 20 E2
    ld     a,(iy+$03)      ; 0435 FD 7E 03
    cp     h               ; 0438 BC
    jr     nz,$0417        ; 0439 20 DC
    call   $056c           ; 043B CD 6C 05
    ld     a,(iy+$0c)      ; 043E FD 7E 0C
    and    (iy+$0d)        ; 0441 FD A6 0D
    cp     $ff             ; 0444 FE FF
    jr     z,$044f         ; 0446 28 07
    ld     de,$000c        ; 0448 11 0C 00
    add    iy,de           ; 044B FD 19
    jr     $040d           ; 044D 18 BE
    ld     iy,$c394        ; 044F FD 21 94 C3
    ld     l,(iy+$00)      ; 0453 FD 6E 00
    ld     h,(iy+$01)      ; 0456 FD 66 01
    jp     $1f20           ; 0459 C3 20 1F
    and    $10             ; 045C E6 10
    jp     nz,$0539        ; 045E C2 39 05
    ld     a,$ff           ; 0461 3E FF
    xor    (hl)            ; 0463 AE
    ld     (hl),a          ; 0464 77
    inc    hl              ; 0465 23
    ld     a,l             ; 0466 7D
    cp     (iy+$02)        ; 0467 FD BE 02
    jr     nz,$0459        ; 046A 20 ED
    ld     a,h             ; 046C 7C
    cp     (iy+$03)        ; 046D FD BE 03
    jr     nz,$0459        ; 0470 20 E7
    ld     l,(iy+$00)      ; 0472 FD 6E 00
    ld     h,(iy+$01)      ; 0475 FD 66 01
    ld     de,$0000        ; 0478 11 00 00
    exx                    ; 047B D9
    ld     de,$0000        ; 047C 11 00 00
    exx                    ; 047F D9
    ld     b,$08           ; 0480 06 08
    ld     a,(hl)          ; 0482 7E
    rrca                   ; 0483 0F
    jr     nc,$0489        ; 0484 30 03
    inc    de              ; 0486 13
    jr     $048c           ; 0487 18 03
    exx                    ; 0489 D9
    inc    de              ; 048A 13
    exx                    ; 048B D9
    djnz   $0483           ; 048C 10 F5
    inc    hl              ; 048E 23
    ld     a,(iy+$02)      ; 048F FD 7E 02
    cp     l               ; 0492 BD
    jr     nz,$0480        ; 0493 20 EB
    jp     $3f20           ; 0495 C3 20 3F
    cp     h               ; 0498 BC
    jr     nz,$0480        ; 0499 20 E5
    ld     l,(iy+$00)      ; 049B FD 6E 00
    ld     h,(iy+$01)      ; 049E FD 66 01
    jp     $1f30           ; 04A1 C3 30 1F
    and    $10             ; 04A4 E6 10
    jp     nz,$0539        ; 04A6 C2 39 05
    ld     a,$ff           ; 04A9 3E FF
    xor    (hl)            ; 04AB AE
    ld     (hl),a          ; 04AC 77
    inc    hl              ; 04AD 23
    ld     a,l             ; 04AE 7D
    cp     (iy+$02)        ; 04AF FD BE 02
    jr     nz,$04a1        ; 04B2 20 ED
    ld     a,h             ; 04B4 7C
    cp     (iy+$03)        ; 04B5 FD BE 03
    jr     nz,$04a1        ; 04B8 20 E7
    ld     l,(iy+$00)      ; 04BA FD 6E 00
    ld     h,(iy+$01)      ; 04BD FD 66 01
    ld     b,$08           ; 04C0 06 08
    ld     a,(hl)          ; 04C2 7E
    rrca                   ; 04C3 0F
    jr     nc,$04c9        ; 04C4 30 03
    inc    de              ; 04C6 13
    jr     $04cc           ; 04C7 18 03
    exx                    ; 04C9 D9
    inc    de              ; 04CA 13
    exx                    ; 04CB D9
    djnz   $04c3           ; 04CC 10 F5
    inc    hl              ; 04CE 23
    ld     a,(iy+$02)      ; 04CF FD 7E 02
    cp     l               ; 04D2 BD
    jr     nz,$04c0        ; 04D3 20 EB
    jp     $3f30           ; 04D5 C3 30 3F
    cp     h               ; 04D8 BC
    jr     nz,$04c0        ; 04D9 20 E5
    ld     c,(iy+$04)      ; 04DB FD 4E 04
    ld     b,(iy+$05)      ; 04DE FD 46 05
    exx                    ; 04E1 D9
    ld     a,e             ; 04E2 7B
    exx                    ; 04E3 D9
    cp     e               ; 04E4 BB
    jr     nz,$04ed        ; 04E5 20 06
    exx                    ; 04E7 D9
    ld     a,d             ; 04E8 7A
    exx                    ; 04E9 D9
    cp     d               ; 04EA BA
    jr     z,$04f3         ; 04EB 28 06
    ld     c,(iy+$06)      ; 04ED FD 4E 06
    ld     b,(iy+$07)      ; 04F0 FD 46 07
    push   bc              ; 04F3 C5
    pop    ix              ; 04F4 DD E1
    jp     $1fe0           ; 04F6 C3 E0 1F
    jp     $1fc0           ; 04F9 C3 C0 1F
    add    iy,de           ; 04FC FD 19
    ld     a,(iy+$00)      ; 04FE FD 7E 00
    and    (iy+$01)        ; 0501 FD A6 01
    cp     $ff             ; 0504 FE FF
    jp     nz,$0453        ; 0506 C2 53 04
    ld     hl,$846c        ; 0509 21 6C 84
    ld     bc,$0028        ; 050C 01 28 00
    call   $00cc           ; 050F CD CC 00
    call   $058b           ; 0512 CD 8B 05
    ld     a,$01           ; 0515 3E 01
    ld     ($8015),a       ; 0517 32 15 80
    ld     b,$04           ; 051A 06 04
    jp     $1f40           ; 051C C3 40 1F
    and    $10             ; 051F E6 10
    jp     nz,$0539        ; 0521 C2 39 05
    ld     hl,$ffff        ; 0524 21 FF FF
    jp     $3f40           ; 0527 C3 40 3F
    jr     nz,$0527        ; 052A 20 FB
    djnz   $051c           ; 052C 10 EE
    ret                    ; 052E C9
    push   af              ; 052F F5
    ld     a,$ff           ; 0530 3E FF
    dec    a               ; 0532 3D
    cp     $00             ; 0533 FE 00
    jr     nz,$0532        ; 0535 20 FB
    pop    af              ; 0537 F1
    ret                    ; 0538 C9
    ld     hl,$846c        ; 0539 21 6C 84
    ld     bc,$0028        ; 053C 01 28 00
    call   $00cc           ; 053F CD CC 00
    ld     ix,$c648        ; 0542 DD 21 48 C6
    call   $00d2           ; 0546 CD D2 00
    jp     $1f50           ; 0549 C3 50 1F
    and    $10             ; 054C E6 10
    jp     nz,$0556        ; 054E C2 56 05
    ld     a,$00           ; 0551 3E 00
    ld     ($8015),a       ; 0553 32 15 80
    ld     a,($8015)       ; 0556 3A 15 80
    or     a               ; 0559 B7
    jp     nz,$0565        ; 055A C2 65 05
    ld     a,($b000)       ; 055D 3A 00 B0
    and    $10             ; 0560 E6 10
    jp     nz,$056b        ; 0562 C2 6B 05
    call   $058b           ; 0565 CD 8B 05
    jp     $0549           ; 0568 C3 49 05
    ret                    ; 056B C9
    ld     l,(iy+$04)      ; 056C FD 6E 04
    ld     h,(iy+$05)      ; 056F FD 66 05
    or     a               ; 0572 B7
    ld     c,(iy+$06)      ; 0573 FD 4E 06
    ld     b,(iy+$07)      ; 0576 FD 46 07
    sbc    hl,de           ; 0579 ED 52
    jp     z,$0584         ; 057B CA 84 05
    ld     c,(iy+$08)      ; 057E FD 4E 08
    ld     b,(iy+$09)      ; 0581 FD 46 09
    push   bc              ; 0584 C5
    pop    ix              ; 0585 DD E1
    call   $00d2           ; 0587 CD D2 00
    ret                    ; 058A C9
    ld     ix,$8481        ; 058B DD 21 81 84
    ld     iy,$b000        ; 058F FD 21 00 B0
    ld     de,$0014        ; 0593 11 14 00
    ld     hl,$c5e4        ; 0596 21 E4 C5
    ld     b,$05           ; 0599 06 05
    push   bc              ; 059B C5
    push   de              ; 059C D5
    push   hl              ; 059D E5
    push   ix              ; 059E DD E5
    ld     de,$846c        ; 05A0 11 6C 84
    ld     bc,$0014        ; 05A3 01 14 00
    ldir                   ; 05A6 ED B0
    jp     $1ff7           ; 05A8 C3 F7 1F
    or     a               ; 05AB B7
    jp     z,$05b9         ; 05AC CA B9 05
    call   $05ce           ; 05AF CD CE 05
    ld     ix,$846c        ; 05B2 DD 21 6C 84
    call   $00d2           ; 05B6 CD D2 00
    pop    ix              ; 05B9 DD E1
    pop    hl              ; 05BB E1
    pop    de              ; 05BC D1
    pop    bc              ; 05BD C1
    ld     a,b             ; 05BE 78
    cp     $03             ; 05BF FE 03
    jp     nz,$05c6        ; 05C1 C2 C6 05
    inc    iy              ; 05C4 FD 23
    inc    iy              ; 05C6 FD 23
    add    hl,de           ; 05C8 19
    inc    ix              ; 05C9 DD 23
    djnz   $059b           ; 05CB 10 CE
    ret                    ; 05CD C9
    push   bc              ; 05CE C5
    push   ix              ; 05CF DD E5
    ld     ix,$846e        ; 05D1 DD 21 6E 84
    push   iy              ; 05D5 FD E5
    pop    bc              ; 05D7 C1
    ld     a,c             ; 05D8 79
    cp     $02             ; 05D9 FE 02
    jp     nz,$05e3        ; 05DB C2 E3 05
    ld     b,$04           ; 05DE 06 04
    jp     $05e5           ; 05E0 C3 E5 05
    ld     b,$08           ; 05E3 06 08
    ld     a,(iy+$00)      ; 05E5 FD 7E 00
    or     a               ; 05E8 B7
    rra                    ; 05E9 1F
    jp     nc,$05f4        ; 05EA D2 F4 05
    ld     (ix+$00),$31    ; 05ED DD 36 00 31
    jp     $05f8           ; 05F1 C3 F8 05
    jp     $1f80           ; 05F4 C3 80 1F
    nop                    ; 05F7 00
    inc    ix              ; 05F8 DD 23
    inc    ix              ; 05FA DD 23
    djnz   $05e8           ; 05FC 10 EA
    pop    ix              ; 05FE DD E1
    pop    bc              ; 0600 C1
    ret                    ; 0601 C9
    ld     a,(ix+$00)      ; 0602 DD 7E 00
    cp     (iy+$00)        ; 0605 FD BE 00
    jp     z,$0616         ; 0608 CA 16 06
    ld     a,(iy+$00)      ; 060B FD 7E 00
    ld     (ix+$00),a      ; 060E DD 77 00
    ld     a,$01           ; 0611 3E 01
    jp     $0617           ; 0613 C3 17 06
    xor    a               ; 0616 AF
    ret                    ; 0617 C9
    ld     de,$8c00        ; 0618 11 00 8C
    ld     hl,$d500        ; 061B 21 00 D5
    ld     bc,$0100        ; 061E 01 00 01
    ldir                   ; 0621 ED B0
    ld     de,$8800        ; 0623 11 00 88
    ld     hl,$0f1a        ; 0626 21 1A 0F
    ld     bc,$0070        ; 0629 01 70 00
    ldir                   ; 062C ED B0
    ret                    ; 062E C9
    push   bc              ; 062F C5
    push   de              ; 0630 D5
    push   hl              ; 0631 E5
    push   ix              ; 0632 DD E5
    push   iy              ; 0634 FD E5
    ld     ix,$8800        ; 0636 DD 21 00 88
    ld     a,(ix+$00)      ; 063A DD 7E 00
    cp     $ff             ; 063D FE FF
    jp     z,$06a8         ; 063F CA A8 06
    bit    7,(ix+$00)      ; 0642 DD CB 00 7E
    jp     nz,$0695        ; 0646 C2 95 06
    ld     a,(ix+$03)      ; 0649 DD 7E 03
    cp     (ix+$07)        ; 064C DD BE 07
    jr     z,$0654         ; 064F 28 03
    jp     nc,$069d        ; 0651 D2 9D 06
    ld     (ix+$07),$00    ; 0654 DD 36 07 00
    ld     h,(ix+$02)      ; 0658 DD 66 02
    ld     l,(ix+$01)      ; 065B DD 6E 01
    ld     d,$00           ; 065E 16 00
    ld     e,(ix+$08)      ; 0660 DD 5E 08
    add    hl,de           ; 0663 19
    push   hl              ; 0664 E5
    inc    hl              ; 0665 23
    inc    hl              ; 0666 23
    ld     c,$00           ; 0667 0E 00
    inc    hl              ; 0669 23
    ld     a,(hl)          ; 066A 7E
    cp     $ff             ; 066B FE FF
    jr     z,$0674         ; 066D 28 05
    ld     c,(ix+$08)      ; 066F DD 4E 08
    inc    c               ; 0672 0C
    inc    c               ; 0673 0C
    ld     (ix+$08),c      ; 0674 DD 71 08
    pop    hl              ; 0677 E1
    ld     iy,$0000        ; 0678 FD 21 00 00
    ld     de,$0010        ; 067C 11 10 00
    ld     a,(ix+$00)      ; 067F DD 7E 00
    and    $7f             ; 0682 E6 7F
    cp     $00             ; 0684 FE 00
    jr     z,$068d         ; 0686 28 05
    add    iy,de           ; 0688 FD 19
    dec    a               ; 068A 3D
    jr     $0684           ; 068B 18 F7
    ld     de,$8c00        ; 068D 11 00 8C
    add    iy,de           ; 0690 FD 19
    call   $06b0           ; 0692 CD B0 06
    ld     de,$000a        ; 0695 11 0A 00
    add    ix,de           ; 0698 DD 19
    jp     $063a           ; 069A C3 3A 06
    inc    (ix+$07)        ; 069D DD 34 07
    ld     de,$000a        ; 06A0 11 0A 00
    add    ix,de           ; 06A3 DD 19
    jp     $063a           ; 06A5 C3 3A 06
    pop    iy              ; 06A8 FD E1
    pop    ix              ; 06AA DD E1
    pop    hl              ; 06AC E1
    pop    de              ; 06AD D1
    pop    bc              ; 06AE C1
    ret                    ; 06AF C9
    push   bc              ; 06B0 C5
    push   de              ; 06B1 D5
    push   hl              ; 06B2 E5
    push   ix              ; 06B3 DD E5
    push   iy              ; 06B5 FD E5
    ld     a,(hl)          ; 06B7 7E
    ld     ($8872),a       ; 06B8 32 72 88
    inc    hl              ; 06BB 23
    ld     a,(hl)          ; 06BC 7E
    ld     ($8873),a       ; 06BD 32 73 88
    ld     a,(ix+$05)      ; 06C0 DD 7E 05
    add    a,a             ; 06C3 87
    ld     de,$0000        ; 06C4 11 00 00
    ld     e,a             ; 06C7 5F
    add    iy,de           ; 06C8 FD 19
    push   iy              ; 06CA FD E5
    ld     c,(ix+$06)      ; 06CC DD 4E 06
    dec    c               ; 06CF 0D
    ld     de,$0002        ; 06D0 11 02 00
    ld     a,(ix+$04)      ; 06D3 DD 7E 04
    cp     $01             ; 06D6 FE 01
    jr     z,$06dd         ; 06D8 28 03
    ld     de,$fffe        ; 06DA 11 FE FF
    ld     hl,$8874        ; 06DD 21 74 88
    ld     a,c             ; 06E0 79
    cp     $00             ; 06E1 FE 00
    jr     z,$06f4         ; 06E3 28 0F
    ld     a,(iy+$00)      ; 06E5 FD 7E 00
    ld     (hl),a          ; 06E8 77
    inc    hl              ; 06E9 23
    ld     a,(iy+$01)      ; 06EA FD 7E 01
    ld     (hl),a          ; 06ED 77
    inc    hl              ; 06EE 23
    add    iy,de           ; 06EF FD 19
    dec    c               ; 06F1 0D
    jr     $06e0           ; 06F2 18 EC
    pop    iy              ; 06F4 FD E1
    ld     hl,$8872        ; 06F6 21 72 88
    ld     c,(ix+$06)      ; 06F9 DD 4E 06
    ld     a,c             ; 06FC 79
    cp     $00             ; 06FD FE 00
    jr     z,$0710         ; 06FF 28 0F
    ld     a,(hl)          ; 0701 7E
    ld     (iy+$00),a      ; 0702 FD 77 00
    inc    hl              ; 0705 23
    ld     a,(hl)          ; 0706 7E
    ld     (iy+$01),a      ; 0707 FD 77 01
    inc    hl              ; 070A 23
    add    iy,de           ; 070B FD 19
    dec    c               ; 070D 0D
    jr     $06fc           ; 070E 18 EC
    pop    iy              ; 0710 FD E1
    pop    ix              ; 0712 DD E1
    pop    hl              ; 0714 E1
    pop    de              ; 0715 D1
    pop    bc              ; 0716 C1
    ret                    ; 0717 C9
    ld     a,($8070)       ; 0718 3A 70 80
    or     a               ; 071B B7
    jp     nz,$028e        ; 071C C2 8E 02
    ret                    ; 071F C9
    ld     ix,$9000        ; 0720 DD 21 00 90
    ld     iy,$9400        ; 0724 FD 21 00 94
    ld     bc,$0000        ; 0728 01 00 00
    ld     a,b             ; 072B 78
    cp     $10             ; 072C FE 10
    jr     z,$076a         ; 072E 28 3A
    ld     a,c             ; 0730 79
    cp     $10             ; 0731 FE 10
    jr     nz,$073f        ; 0733 20 0A
    inc    b               ; 0735 04
    ld     c,$00           ; 0736 0E 00
    ld     de,$0020        ; 0738 11 20 00
    add    ix,de           ; 073B DD 19
    add    iy,de           ; 073D FD 19
    ld     (ix+$00),$5c    ; 073F DD 36 00 5C
    ld     (ix+$01),$5d    ; 0743 DD 36 01 5D
    ld     (ix+$20),$5e    ; 0747 DD 36 20 5E
    ld     (ix+$21),$5f    ; 074B DD 36 21 5F
    ld     (iy+$00),$05    ; 074F FD 36 00 05
    ld     (iy+$01),$05    ; 0753 FD 36 01 05
    ld     (iy+$20),$05    ; 0757 FD 36 20 05
    ld     (iy+$21),$05    ; 075B FD 36 21 05
    inc    ix              ; 075F DD 23
    inc    ix              ; 0761 DD 23
    inc    iy              ; 0763 FD 23
    inc    iy              ; 0765 FD 23
    inc    c               ; 0767 0C
    jr     $072b           ; 0768 18 C1
    ld     b,$00           ; 076A 06 00
    ld     hl,$ffff        ; 076C 21 FF FF
    jp     $1f90           ; 076F C3 90 1F
    bit    4,a             ; 0772 CB 67
    jr     nz,$0782        ; 0774 20 0C
    dec    hl              ; 0776 2B
    ld     a,h             ; 0777 7C
    or     l               ; 0778 B5
    jr     nz,$076f        ; 0779 20 F4
    inc    b               ; 077B 04
    ld     a,b             ; 077C 78
    cp     $03             ; 077D FE 03
    jr     c,$076c         ; 077F 38 EB
    ret                    ; 0781 C9
    ld     d,$ff           ; 0782 16 FF
    dec    d               ; 0784 15
    ld     a,d             ; 0785 7A
    jr     nz,$0784        ; 0786 20 FC
    jp     $1fa0           ; 0788 C3 A0 1F
    bit    4,a             ; 078B CB 67
    jr     z,$0782         ; 078D 28 F3
    ld     d,$ff           ; 078F 16 FF
    dec    d               ; 0791 15
    ld     a,d             ; 0792 7A
    jr     nz,$0791        ; 0793 20 FC
    jp     $1fb0           ; 0795 C3 B0 1F
    bit    4,a             ; 0798 CB 67
    jr     nz,$078f        ; 079A 20 F3
    inc    b               ; 079C 04
    ld     a,b             ; 079D 78
    cp     $03             ; 079E FE 03
    jr     c,$0782         ; 07A0 38 E0
    ret                    ; 07A2 C9

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 07A3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 07B3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 07C3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 07D3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 07E3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 07F3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0803
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0813
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0823
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 0833
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00          ; 0843

; 0850h - zero a block
;
; memset (hl, 0, bc);
;
; Vector: 00CCh

    ld     (hl),$00        ; 0850 36 00
    inc    hl              ; 0852 23
    dec    bc              ; 0853 0B
    ld     a,c             ; 0854 79
    or     b               ; 0855 B0
    jp     nz,$0850        ; 0856 C2 50 08
    ret                    ; 0859 C9

    ld     (hl),$00        ; 085A 36 00
    inc    hl              ; 085C 23
    dec    bc              ; 085D 0B
    ld     a,c             ; 085E 79
    or     b               ; 085F B0
    jp     nz,$085a        ; 0860 C2 5A 08
    ret                    ; 0863 C9
    push   de              ; 0864 D5
    push   hl              ; 0865 E5
    ld     hl,($8024)      ; 0866 2A 24 80
    ld     e,l             ; 0869 5D
    ld     d,h             ; 086A 54
    add    hl,de           ; 086B 19
    add    hl,de           ; 086C 19
    push   hl              ; 086D E5
    ld     hl,($8022)      ; 086E 2A 22 80
    ld     e,l             ; 0871 5D
    ld     d,h             ; 0872 54
    sla    e               ; 0873 CB 23
    rl     d               ; 0875 CB 12
    add    hl,de           ; 0877 19
    ld     ($8022),hl      ; 0878 22 22 80
    pop    hl              ; 087B E1
    adc    hl,de           ; 087C ED 5A
    ld     ($8024),hl      ; 087E 22 24 80
    pop    hl              ; 0881 E1
    pop    de              ; 0882 D1
    ret                    ; 0883 C9
    rlca                   ; 0884 07
    ld     de,$0554        ; 0885 11 54 05
    ld     l,$05           ; 0888 2E 05
    ld     d,h             ; 088A 54
    dec    b               ; 088B 05
    rst    38h             ; 088C FF
    rst    38h             ; 088D FF
    add    hl,bc           ; 088E 09
    ld     de,$0545        ; 088F 11 45 05
    ld     l,$05           ; 0892 2E 05
    ld     b,l             ; 0894 45
    dec    b               ; 0895 05
    rst    38h             ; 0896 FF
    rst    38h             ; 0897 FF
    dec    bc              ; 0898 0B
    ld     de,$0548        ; 0899 11 48 05
    ld     l,$05           ; 089C 2E 05
    ld     c,b             ; 089E 48
    dec    b               ; 089F 05
    rst    38h             ; 08A0 FF
    rst    38h             ; 08A1 FF
    dec    c               ; 08A2 0D
    ld     de,$054b        ; 08A3 11 4B 05
    ld     l,$05           ; 08A6 2E 05
    ld     c,e             ; 08A8 4B
    dec    b               ; 08A9 05
    rst    38h             ; 08AA FF
    rst    38h             ; 08AB FF
    rrca                   ; 08AC 0F
    ld     de,$0541        ; 08AD 11 41 05
    ld     l,$05           ; 08B0 2E 05
    ld     b,c             ; 08B2 41
    dec    b               ; 08B3 05
    rst    38h             ; 08B4 FF
    rst    38h             ; 08B5 FF
    ld     de,$4e11        ; 08B6 11 11 4E
    dec    b               ; 08B9 05
    ld     l,$05           ; 08BA 2E 05
    ld     c,(hl)          ; 08BC 4E
    dec    b               ; 08BD 05
    rst    38h             ; 08BE FF
    rst    38h             ; 08BF FF
    inc    de              ; 08C0 13
    ld     de,$054c        ; 08C1 11 4C 05
    ld     l,$05           ; 08C4 2E 05
    ld     c,h             ; 08C6 4C
    dec    b               ; 08C7 05
    rst    38h             ; 08C8 FF
    rst    38h             ; 08C9 FF
    dec    d               ; 08CA 15
    ld     de,$0554        ; 08CB 11 54 05
    ld     l,$05           ; 08CE 2E 05
    ld     d,h             ; 08D0 54
    dec    b               ; 08D1 05
    rst    38h             ; 08D2 FF
    rst    38h             ; 08D3 FF
    rla                    ; 08D4 17
    ld     de,$0544        ; 08D5 11 44 05
    ld     l,$05           ; 08D8 2E 05
    ld     b,h             ; 08DA 44
    dec    b               ; 08DB 05
    rst    38h             ; 08DC FF
    rst    38h             ; 08DD FF
    add    hl,de           ; 08DE 19
    ld     de,$052e        ; 08DF 11 2E 05
    ld     l,$05           ; 08E2 2E 05
    ld     l,$05           ; 08E4 2E 05
    rst    38h             ; 08E6 FF
    rst    38h             ; 08E7 FF
    nop                    ; 08E8 00
    nop                    ; 08E9 00
    ld     bc,$0000        ; 08EA 01 00 00
    nop                    ; 08ED 00
    djnz   $08f0           ; 08EE 10 00
    nop                    ; 08F0 00
    nop                    ; 08F1 00
    inc    bc              ; 08F2 03
    nop                    ; 08F3 00
    nop                    ; 08F4 00
    nop                    ; 08F5 00
    dec    b               ; 08F6 05
    nop                    ; 08F7 00
    nop                    ; 08F8 00
    nop                    ; 08F9 00
    djnz   $08fc           ; 08FA 10 00
    nop                    ; 08FC 00
    nop                    ; 08FD 00
    dec    b               ; 08FE 05
    nop                    ; 08FF 00
    nop                    ; 0900 00
    nop                    ; 0901 00
    djnz   $0904           ; 0902 10 00
    nop                    ; 0904 00
    nop                    ; 0905 00
    dec    b               ; 0906 05
    nop                    ; 0907 00
    ld     hl,$b000        ; 0908 21 00 B0
    ld     b,$10           ; 090B 06 10
    ld     (hl),$00        ; 090D 36 00
    inc    hl              ; 090F 23
    djnz   $090d           ; 0910 10 FB
    ld     hl,$0884        ; 0912 21 84 08
    ld     de,$8132        ; 0915 11 32 81
    ld     bc,$0064        ; 0918 01 64 00
    ldir                   ; 091B ED B0
    ld     de,$8100        ; 091D 11 00 81
    ld     b,$0a           ; 0920 06 0A
    ld     c,$04           ; 0922 0E 04
    call   $0945           ; 0924 CD 45 09
    ldi                    ; 0927 ED A0
    ld     a,c             ; 0929 79
    or     a               ; 092A B7
    jr     nz,$0927        ; 092B 20 FA
    djnz   $0922           ; 092D 10 F3
    call   $0945           ; 092F CD 45 09
    ld     de,$80e2        ; 0932 11 E2 80
    ld     bc,$0004        ; 0935 01 04 00
    ldir                   ; 0938 ED B0
    ld     hl,$8128        ; 093A 21 28 81
    ld     b,$0a           ; 093D 06 0A
    ld     (hl),$01        ; 093F 36 01
    inc    hl              ; 0941 23
    djnz   $093f           ; 0942 10 FB
    ret                    ; 0944 C9
    push   bc              ; 0945 C5
    push   de              ; 0946 D5
    ld     a,($8014)       ; 0947 3A 14 80
    and    $07             ; 094A E6 07
    ld     hl,$08e8        ; 094C 21 E8 08
    ld     bc,$0004        ; 094F 01 04 00
    call   $00f6           ; 0952 CD F6 00
    pop    de              ; 0955 D1
    pop    bc              ; 0956 C1
    ret                    ; 0957 C9
    ld     ix,$9878        ; 0958 DD 21 78 98
    ld     iy,$9820        ; 095C FD 21 20 98
    ld     hl,$8500        ; 0960 21 00 85
    ld     b,$0c           ; 0963 06 0C
    ld     c,$03           ; 0965 0E 03
    ld     a,b             ; 0967 78
    cp     $00             ; 0968 FE 00
    jr     z,$097d         ; 096A 28 11
    inc    hl              ; 096C 23
    ld     a,(hl)          ; 096D 7E
    dec    hl              ; 096E 2B
    bit    5,a             ; 096F CB 6F
    jr     nz,$0978        ; 0971 20 05
    call   $0982           ; 0973 CD 82 09
    jr     $0967           ; 0976 18 EF
    call   $0996           ; 0978 CD 96 09
    jr     $0967           ; 097B 18 EA
    ld     a,c             ; 097D 79
    ld     ($9a00),a       ; 097E 32 00 9A
    ret                    ; 0981 C9
    dec    b               ; 0982 05
    push   bc              ; 0983 C5
    push   de              ; 0984 D5
    push   ix              ; 0985 DD E5
    pop    de              ; 0987 D1
    ld     bc,$0008        ; 0988 01 08 00
    ldir                   ; 098B ED B0
    ld     de,$fff8        ; 098D 11 F8 FF
    or     a               ; 0990 B7
    add    ix,de           ; 0991 DD 19
    pop    de              ; 0993 D1
    pop    bc              ; 0994 C1
    ret                    ; 0995 C9
    inc    c               ; 0996 0C
    dec    b               ; 0997 05
    push   bc              ; 0998 C5
    push   de              ; 0999 D5
    push   iy              ; 099A FD E5
    pop    de              ; 099C D1
    ld     bc,$0008        ; 099D 01 08 00
    ldir                   ; 09A0 ED B0
    ld     de,$0008        ; 09A2 11 08 00
    or     a               ; 09A5 B7
    add    iy,de           ; 09A6 FD 19
    pop    de              ; 09A8 D1
    pop    bc              ; 09A9 C1
    ret                    ; 09AA C9
    ld     hl,$8c40        ; 09AB 21 40 8C
    ld     de,$9c40        ; 09AE 11 40 9C
    ld     bc,$0010        ; 09B1 01 10 00
    ldir                   ; 09B4 ED B0
    ld     hl,$8ca0        ; 09B6 21 A0 8C
    ld     de,$9ca0        ; 09B9 11 A0 9C
    ld     bc,$0060        ; 09BC 01 60 00
    ldir                   ; 09BF ED B0
    ld     a,($808c)       ; 09C1 3A 8C 80
    or     a               ; 09C4 B7
    ret    z               ; 09C5 C8
    cp     $02             ; 09C6 FE 02
    jr     z,$09dc         ; 09C8 28 12
    ld     a,$00           ; 09CA 3E 00
    ld     ($808c),a       ; 09CC 32 8C 80
    ld     hl,$8c00        ; 09CF 21 00 8C
    ld     de,$9c00        ; 09D2 11 00 9C
    ld     bc,$00a0        ; 09D5 01 A0 00
    ldir                   ; 09D8 ED B0
    jr     $09e1           ; 09DA 18 05
    ld     a,$00           ; 09DC 3E 00
    ld     ($808c),a       ; 09DE 32 8C 80
    ret                    ; 09E1 C9
    ld     hl,($806e)      ; 09E2 2A 6E 80
    ld     a,l             ; 09E5 7D
    or     h               ; 09E6 B4
    jr     z,$09ed         ; 09E7 28 04
    dec    hl              ; 09E9 2B
    ld     ($806e),hl      ; 09EA 22 6E 80
    ret                    ; 09ED C9
    push   af              ; 09EE F5
    ld     ($806e),bc      ; 09EF ED 43 6E 80
    ld     bc,($806e)      ; 09F3 ED 4B 6E 80
    ld     a,c             ; 09F7 79
    or     b               ; 09F8 B0
    jr     nz,$09f3        ; 09F9 20 F8
    pop    af              ; 09FB F1
    ret                    ; 09FC C9
    push   af              ; 09FD F5
    push   bc              ; 09FE C5
    push   de              ; 09FF D5
    push   hl              ; 0A00 E5
    push   ix              ; 0A01 DD E5
    push   iy              ; 0A03 FD E5
    call   $0b8a           ; 0A05 CD 8A 0B
    push   bc              ; 0A08 C5
    pop    hl              ; 0A09 E1
    ld     c,(hl)          ; 0A0A 4E
    inc    hl              ; 0A0B 23
    ld     b,(hl)          ; 0A0C 46
    inc    hl              ; 0A0D 23
    push   bc              ; 0A0E C5
    pop    ix              ; 0A0F DD E1
    call   $0a21           ; 0A11 CD 21 0A
    dec    a               ; 0A14 3D
    jp     nz,$0a0a        ; 0A15 C2 0A 0A
    pop    iy              ; 0A18 FD E1
    pop    ix              ; 0A1A DD E1
    pop    hl              ; 0A1C E1
    pop    de              ; 0A1D D1
    pop    bc              ; 0A1E C1
    pop    af              ; 0A1F F1
    ret                    ; 0A20 C9
    push   af              ; 0A21 F5
    push   bc              ; 0A22 C5
    push   de              ; 0A23 D5
    push   hl              ; 0A24 E5
    push   ix              ; 0A25 DD E5
    push   iy              ; 0A27 FD E5
    call   $0a82           ; 0A29 CD 82 0A
    push   hl              ; 0A2C E5
    ld     a,(ix+$05)      ; 0A2D DD 7E 05
    dec    a               ; 0A30 3D
    sra    a               ; 0A31 CB 2F
    ld     d,$00           ; 0A33 16 00
    ld     e,a             ; 0A35 5F
    ld     l,(ix+$02)      ; 0A36 DD 6E 02
    ld     h,(ix+$03)      ; 0A39 DD 66 03
    add    hl,de           ; 0A3C 19
    push   hl              ; 0A3D E5
    pop    de              ; 0A3E D1
    pop    hl              ; 0A3F E1
    ld     c,$00           ; 0A40 0E 00
    ld     b,(ix+$05)      ; 0A42 DD 46 05
    ld     a,b             ; 0A45 78
    dec    a               ; 0A46 3D
    jr     nz,$0a4b        ; 0A47 20 02
    ld     c,$0f           ; 0A49 0E 0F
    call   $0a59           ; 0A4B CD 59 0A
    djnz   $0a45           ; 0A4E 10 F5
    pop    iy              ; 0A50 FD E1
    pop    ix              ; 0A52 DD E1
    pop    hl              ; 0A54 E1
    pop    de              ; 0A55 D1
    pop    bc              ; 0A56 C1
    pop    af              ; 0A57 F1
    ret                    ; 0A58 C9
    ld     a,(de)          ; 0A59 1A
    bit    0,b             ; 0A5A CB 40
    jp     nz,$0a67        ; 0A5C C2 67 0A
    and    $f0             ; 0A5F E6 F0
    rrca                   ; 0A61 0F
    rrca                   ; 0A62 0F
    rrca                   ; 0A63 0F
    rrca                   ; 0A64 0F
    jr     $0a69           ; 0A65 18 02
    and    $0f             ; 0A67 E6 0F
    call   $0b07           ; 0A69 CD 07 0B
    ld     (hl),a          ; 0A6C 77
    push   de              ; 0A6D D5
    ld     de,$0400        ; 0A6E 11 00 04
    add    hl,de           ; 0A71 19
    ld     a,(ix+$04)      ; 0A72 DD 7E 04
    ld     (hl),a          ; 0A75 77
    ld     de,$fbe0        ; 0A76 11 E0 FB
    add    hl,de           ; 0A79 19
    pop    de              ; 0A7A D1
    bit    0,b             ; 0A7B CB 40
    jp     z,$0a81         ; 0A7D CA 81 0A
    dec    de              ; 0A80 1B
    ret                    ; 0A81 C9
    push   af              ; 0A82 F5
    ld     b,(ix+$01)      ; 0A83 DD 46 01
    ld     hl,$9400        ; 0A86 21 00 94
    ld     de,$ffe0        ; 0A89 11 E0 FF
    inc    b               ; 0A8C 04
    add    hl,de           ; 0A8D 19
    djnz   $0a8d           ; 0A8E 10 FD
    ld     b,$00           ; 0A90 06 00
    ld     c,(ix+$00)      ; 0A92 DD 4E 00
    add    hl,bc           ; 0A95 09
    pop    af              ; 0A96 F1
    ret                    ; 0A97 C9
    push   af              ; 0A98 F5
    push   bc              ; 0A99 C5
    push   de              ; 0A9A D5
    push   iy              ; 0A9B FD E5
    srl    b               ; 0A9D CB 38
    srl    b               ; 0A9F CB 38
    srl    b               ; 0AA1 CB 38
    srl    c               ; 0AA3 CB 39
    srl    c               ; 0AA5 CB 39
    srl    c               ; 0AA7 CB 39
    ld     iy,$0ac7        ; 0AA9 FD 21 C7 0A
    ld     d,$00           ; 0AAD 16 00
    sla    b               ; 0AAF CB 20
    ld     e,b             ; 0AB1 58
    add    iy,de           ; 0AB2 FD 19
    ld     e,(iy+$00)      ; 0AB4 FD 5E 00
    ld     d,(iy+$01)      ; 0AB7 FD 56 01
    ld     hl,$9400        ; 0ABA 21 00 94
    add    hl,de           ; 0ABD 19
    ld     b,$00           ; 0ABE 06 00
    add    hl,bc           ; 0AC0 09
    pop    iy              ; 0AC1 FD E1
    pop    de              ; 0AC3 D1
    pop    bc              ; 0AC4 C1
    pop    af              ; 0AC5 F1
    ret                    ; 0AC6 C9
    ret    po              ; 0AC7 E0
    rst    38h             ; 0AC8 FF
    ret    nz              ; 0AC9 C0
    rst    38h             ; 0ACA FF
    and    b               ; 0ACB A0
    rst    38h             ; 0ACC FF
    add    a,b             ; 0ACD 80
    rst    38h             ; 0ACE FF
    ld     h,b             ; 0ACF 60
    rst    38h             ; 0AD0 FF
    ld     b,b             ; 0AD1 40
    rst    38h             ; 0AD2 FF
    jr     nz,$0ad4        ; 0AD3 20 FF
    nop                    ; 0AD5 00
    rst    38h             ; 0AD6 FF
    ret    po              ; 0AD7 E0
    cp     $c0             ; 0AD8 FE C0
    cp     $a0             ; 0ADA FE A0
    cp     $80             ; 0ADC FE 80
    cp     $60             ; 0ADE FE 60
    cp     $40             ; 0AE0 FE 40
    cp     $20             ; 0AE2 FE 20
    cp     $00             ; 0AE4 FE 00
    cp     $e0             ; 0AE6 FE E0
    db     $fd             ; 0AE8 FD
    ret    nz              ; 0AE9 C0
    db     $fd             ; 0AEA FD
    and    b               ; 0AEB A0
    db     $fd             ; 0AEC FD
    add    a,b             ; 0AED 80
    ld     iyh,b           ; 0AEE FD 60
    db     $fd             ; 0AF0 FD
    ld     b,b             ; 0AF1 40
    db     $fd             ; 0AF2 FD
    jr     nz,$0af2        ; 0AF3 20 FD
    nop                    ; 0AF5 00
    db     $fd             ; 0AF6 FD
    ret    po              ; 0AF7 E0
    call   m,$fcc0         ; 0AF8 FC C0 FC
    and    b               ; 0AFB A0
    call   m,$fc80         ; 0AFC FC 80 FC
    ld     h,b             ; 0AFF 60
    call   m,$fc40         ; 0B00 FC 40 FC
    jr     nz,$0b01        ; 0B03 20 FC
    nop                    ; 0B05 00
    call   m,$20b7         ; 0B06 FC B7 20
    rlca                   ; 0B09 07
    cp     c               ; 0B0A B9
    jr     nz,$0b11        ; 0B0B 20 04
    ld     a,$00           ; 0B0D 3E 00
    jr     $0b1c           ; 0B0F 18 0B
    ld     c,$0f           ; 0B11 0E 0F
    add    a,$30           ; 0B13 C6 30
    cp     $3a             ; 0B15 FE 3A
    jp     c,$0b1c         ; 0B17 DA 1C 0B
    add    a,$07           ; 0B1A C6 07
    ret                    ; 0B1C C9

; 0B1Dh - pulls stuff from the stack ?
;
; Called: 7C74h's routines
;
; Vector: 00CFh

    push   af              ; 0B1D F5
    push   bc              ; 0B1E C5
    push   de              ; 0B1F D5
    push   hl              ; 0B20 E5
    push   ix              ; 0B21 DD E5
    push   iy              ; 0B23 FD E5
    call   $0b8a           ; 0B25 CD 8A 0B
    push   bc              ; 0B28 C5 		; short *bc;
    pop    hl              ; 0B29 E1 		; do {
    ld     c,(hl)          ; 0B2A 4E 		;   ix = *bc++;
    inc    hl              ; 0B2B 23 		;   call 0B41h;
    ld     b,(hl)          ; 0B2C 46 		; } while (a != 0);
    inc    hl              ; 0B2D 23 	
    push   bc              ; 0B2E C5
    pop    ix              ; 0B2F DD E1
    call   $0b41           ; 0B31 CD 41 0B
    dec    a               ; 0B34 3D
    jp     nz,$0b2a        ; 0B35 C2 2A 0B
    pop    iy              ; 0B38 FD E1
    pop    ix              ; 0B3A DD E1
    pop    hl              ; 0B3C E1
    pop    de              ; 0B3D D1
    pop    bc              ; 0B3E C1
    pop    af              ; 0B3F F1
    ret                    ; 0B40 C9

; 0B41h - ?
;
; Called: 0B1Dh

    push   af              ; 0B41 F5
    push   bc              ; 0B42 C5
    push   de              ; 0B43 D5
    push   hl              ; 0B44 E5
    push   ix              ; 0B45 DD E5
    push   iy              ; 0B47 FD E5
    call   $0a82           ; 0B49 CD 82 0A
    inc    ix              ; 0B4C DD 23
    inc    ix              ; 0B4E DD 23
    ld     e,(ix+$00)      ; 0B50 DD 5E 00
    ld     d,(ix+$01)      ; 0B53 DD 56 01
    ld     a,$ff           ; 0B56 3E FF
    cp     d               ; 0B58 BA
    jp     z,$0b65         ; 0B59 CA 65 0B
    call   $5fd3           ; 0B5C CD D3 5F
    call   $0b9f           ; 0B5F CD 9F 0B
    jp     $0b4c           ; 0B62 C3 4C 0B
    cp     e               ; 0B65 BB
    jp     z,$0b81         ; 0B66 CA 81 0B
    ld     a,e             ; 0B69 7B
    inc    ix              ; 0B6A DD 23
    inc    ix              ; 0B6C DD 23
    ld     e,(ix+$00)      ; 0B6E DD 5E 00
    ld     d,(ix+$01)      ; 0B71 DD 56 01
    call   $5fd3           ; 0B74 CD D3 5F
    call   $0b9f           ; 0B77 CD 9F 0B
    dec    a               ; 0B7A 3D
    jp     nz,$0b77        ; 0B7B C2 77 0B
    jp     $0b4c           ; 0B7E C3 4C 0B
    pop    iy              ; 0B81 FD E1
    pop    ix              ; 0B83 DD E1
    pop    hl              ; 0B85 E1
    pop    de              ; 0B86 D1
    pop    bc              ; 0B87 C1
    pop    af              ; 0B88 F1
    ret                    ; 0B89 C9

; 0B8Ah - ?
;
; Called: 0B1Dh

    ld     hl,$000e        ; 0B8A 21 0E 00
    add    hl,sp           ; 0B8D 39 		; de = address in stack[-14]
    ld     e,(hl)          ; 0B8E 5E
    inc    hl              ; 0B8F 23
    ld     d,(hl)          ; 0B90 56
    push   hl              ; 0B91 E5
    ex     de,hl           ; 0B92 EB 		; hl = de
    ld     c,(hl)          ; 0B93 4E
    inc    hl              ; 0B94 23
    ld     b,(hl)          ; 0B95 46
    inc    hl              ; 0B96 23
    ld     a,(hl)          ; 0B97 7E
    inc    hl              ; 0B98 23
    ex     de,hl           ; 0B99 EB
    pop    hl              ; 0B9A E1
    ld     (hl),d          ; 0B9B 72
    dec    hl              ; 0B9C 2B
    ld     (hl),e          ; 0B9D 73
    ret                    ; 0B9E C9
    ld     (hl),e          ; 0B9F 73
    push   bc              ; 0BA0 C5
    ld     bc,$0400        ; 0BA1 01 00 04
    add    hl,bc           ; 0BA4 09
    ld     (hl),d          ; 0BA5 72
    ld     bc,$fbe0        ; 0BA6 01 E0 FB
    add    hl,bc           ; 0BA9 09
    pop    bc              ; 0BAA C1
    ret                    ; 0BAB C9

; 0BACh - ?
;
; Vector: 00E4h

    call   $601e           ; 0BAC CD 1E 60
    ret                    ; 0BAF C9

; OBB0h - indirect jump
;
; Vector: 00EAh
;
; Input:
; - hl contains the address of a table of words
; - a is an index into that table
;
; Jumps to hl[a]

    add    a,a             ; 0BB0 87
    ld     d,$00           ; 0BB1 16 00
    ld     e,a             ; 0BB3 5F
    add    hl,de           ; 0BB4 19
    ld     e,(hl)          ; 0BB5 5E
    inc    hl              ; 0BB6 23
    ld     d,(hl)          ; 0BB7 56
    ex     de,hl           ; 0BB8 EB
    jp     (hl)            ; 0BB9 E9

; 0BBAh - ?

    push   bc              ; 0BBA C5
    push   de              ; 0BBB D5
    push   hl              ; 0BBC E5
    push   ix              ; 0BBD DD E5
    ld     ix,$9400        ; 0BBF DD 21 00 94
    ld     hl,$9000        ; 0BC3 21 00 90
    ld     e,a             ; 0BC6 5F
    ld     d,$00           ; 0BC7 16 00
    add    hl,de           ; 0BC9 19
    add    ix,de           ; 0BCA DD 19
    ld     de,$0020        ; 0BCC 11 20 00
    ld     b,$20           ; 0BCF 06 20
    ld     (hl),$00        ; 0BD1 36 00
    ld     (ix+$00),$0a    ; 0BD3 DD 36 00 0A
    add    ix,de           ; 0BD7 DD 19
    add    hl,de           ; 0BD9 19
    dec    b               ; 0BDA 05
    jp     nz,$0bd1        ; 0BDB C2 D1 0B
    pop    ix              ; 0BDE DD E1
    pop    hl              ; 0BE0 E1
    pop    de              ; 0BE1 D1
    pop    bc              ; 0BE2 C1
    ret                    ; 0BE3 C9
    push   af              ; 0BE4 F5
    push   bc              ; 0BE5 C5
    ld     a,b             ; 0BE6 78
    call   $0bba           ; 0BE7 CD BA 0B
    inc    a               ; 0BEA 3C
    dec    c               ; 0BEB 0D
    jp     nz,$0be7        ; 0BEC C2 E7 0B
    pop    bc              ; 0BEF C1
    pop    af              ; 0BF0 F1
    ret                    ; 0BF1 C9
    ld     ix,$c812        ; 0BF2 DD 21 12 C8
    call   $00d2           ; 0BF6 CD D2 00
    ld     ix,$c822        ; 0BF9 DD 21 22 C8
    call   $00d5           ; 0BFD CD D5 00
    ret                    ; 0C00 C9
    push   de              ; 0C01 D5
    push   hl              ; 0C02 E5
    push   ix              ; 0C03 DD E5
    push   iy              ; 0C05 FD E5
    ld     de,$0f8a        ; 0C07 11 8A 0F
    add    hl,hl           ; 0C0A 29
    add    hl,de           ; 0C0B 19
    ld     e,(hl)          ; 0C0C 5E
    inc    hl              ; 0C0D 23
    ld     d,(hl)          ; 0C0E 56
    ld     h,$00           ; 0C0F 26 00
    ld     l,a             ; 0C11 6F
    add    hl,de           ; 0C12 19
    ld     a,(hl)          ; 0C13 7E
    ld     (ix+$02),a      ; 0C14 DD 77 02
    ld     (ix+$03),$05    ; 0C17 DD 36 03 05
    ld     a,($81ac)       ; 0C1B 3A AC 81
    ld     d,$00           ; 0C1E 16 00
    ld     e,a             ; 0C20 5F
    ld     hl,$0fa8        ; 0C21 21 A8 0F
    add    hl,de           ; 0C24 19
    ld     a,(hl)          ; 0C25 7E
    ld     (iy+$04),a      ; 0C26 FD 77 04
    ld     (iy+$05),$05    ; 0C29 FD 36 05 05
    ld     a,(ix+$04)      ; 0C2D DD 7E 04
    add    a,$10           ; 0C30 C6 10
    ld     (iy+$06),a      ; 0C32 FD 77 06
    ld     a,(ix+$05)      ; 0C35 DD 7E 05
    ld     (iy+$07),a      ; 0C38 FD 77 07
    pop    iy              ; 0C3B FD E1
    pop    ix              ; 0C3D DD E1
    pop    hl              ; 0C3F E1
    pop    de              ; 0C40 D1
    ret                    ; 0C41 C9

; 0C42h - hl += a * bc
;
; Vector: 00F6h

    or     a               ; 0C42 B7
    jp     z,$0c4b         ; 0C43 CA 4B 0C
    add    hl,bc           ; 0C46 09
    dec    a               ; 0C47 3D
    jp     nz,$0c46        ; 0C48 C2 46 0C
    ret                    ; 0C4B C9

    or     a               ; 0C4C B7
    jp     z,$0c55         ; 0C4D CA 55 0C
    add    hl,bc           ; 0C50 09
    dec    a               ; 0C51 3D
    jp     nz,$0c50        ; 0C52 C2 50 0C
    ld     e,(hl)          ; 0C55 5E
    inc    hl              ; 0C56 23
    ld     d,(hl)          ; 0C57 56
    ex     de,hl           ; 0C58 EB
    ret                    ; 0C59 C9
    ld     a,($b000)       ; 0C5A 3A 00 B0
    and    $20             ; 0C5D E6 20
    jp     z,$0c92         ; 0C5F CA 92 0C
    ld     a,($b002)       ; 0C62 3A 02 B0
    and    $04             ; 0C65 E6 04
    jp     nz,$0c62        ; 0C67 C2 62 0C
    ld     a,($b000)       ; 0C6A 3A 00 B0
    and    $20             ; 0C6D E6 20
    jp     z,$0c92         ; 0C6F CA 92 0C
    ld     a,($b002)       ; 0C72 3A 02 B0
    and    $04             ; 0C75 E6 04
    jp     nz,$0c7d        ; 0C77 C2 7D 0C
    jp     $0c6a           ; 0C7A C3 6A 0C
    ld     b,$ff           ; 0C7D 06 FF
    ld     a,$1c           ; 0C7F 3E 1C
    dec    a               ; 0C81 3D
    jp     nz,$0c81        ; 0C82 C2 81 0C
    djnz   $0c7f           ; 0C85 10 F8
    ld     a,($b002)       ; 0C87 3A 02 B0
    and    $04             ; 0C8A E6 04
    jp     nz,$0c92        ; 0C8C C2 92 0C
    jp     $0c6a           ; 0C8F C3 6A 0C
    ret                    ; 0C92 C9

; 0C93h - zeros

; 0D00h - data ?

    rst    38h             ; 0D00 FF
    nop                    ; 0D01 00
    rst    38h             ; 0D02 FF
    ld     (bc),a          ; 0D03 02
    rst    38h             ; 0D04 FF
    inc    b               ; 0D05 04
    rst    38h             ; 0D06 FF
    ld     b,$ff           ; 0D07 06 FF
    ex     af,af'          ; 0D09 08
    rst    38h             ; 0D0A FF
    ld     a,(bc)          ; 0D0B 0A
    rst    38h             ; 0D0C FF
    inc    c               ; 0D0D 0C
    rst    38h             ; 0D0E FF
    rrca                   ; 0D0F 0F
    call   z,$aa0f         ; 0D10 CC 0F AA
    rrca                   ; 0D13 0F
    adc    a,b             ; 0D14 88
    rrca                   ; 0D15 0F
    ld     h,(hl)          ; 0D16 66
    rrca                   ; 0D17 0F
    ld     b,h             ; 0D18 44
    rrca                   ; 0D19 0F
    ld     ($000f),hl      ; 0D1A 22 0F 00
    rrca                   ; 0D1D 0F
    ld     (bc),a          ; 0D1E 02
    rrca                   ; 0D1F 0F
    inc    b               ; 0D20 04
    rrca                   ; 0D21 0F
    ld     b,$0f           ; 0D22 06 0F
    ex     af,af'          ; 0D24 08
    rrca                   ; 0D25 0F
    ld     a,(bc)          ; 0D26 0A
    rrca                   ; 0D27 0F
    inc    c               ; 0D28 0C
    rrca                   ; 0D29 0F
    rrca                   ; 0D2A 0F
    rrca                   ; 0D2B 0F
    rrca                   ; 0D2C 0F
    inc    c               ; 0D2D 0C
    rrca                   ; 0D2E 0F
    ld     a,(bc)          ; 0D2F 0A
    rrca                   ; 0D30 0F
    ex     af,af'          ; 0D31 08
    rrca                   ; 0D32 0F
    ld     b,$0f           ; 0D33 06 0F
    inc    b               ; 0D35 04
    rrca                   ; 0D36 0F
    ld     (bc),a          ; 0D37 02
    rrca                   ; 0D38 0F
    nop                    ; 0D39 00
    cpl                    ; 0D3A 2F
    nop                    ; 0D3B 00
    ld     c,a             ; 0D3C 4F
    nop                    ; 0D3D 00
    ld     l,a             ; 0D3E 6F
    nop                    ; 0D3F 00
    adc    a,a             ; 0D40 8F
    nop                    ; 0D41 00
    xor    a               ; 0D42 AF
    nop                    ; 0D43 00
    rst    08h             ; 0D44 CF
    nop                    ; 0D45 00
    rst    38h             ; 0D46 FF
    rst    38h             ; 0D47 FF
    rrca                   ; 0D48 0F
    nop                    ; 0D49 00
    cpl                    ; 0D4A 2F
    nop                    ; 0D4B 00
    ld     c,a             ; 0D4C 4F
    nop                    ; 0D4D 00
    ld     l,a             ; 0D4E 6F
    nop                    ; 0D4F 00
    adc    a,a             ; 0D50 8F
    nop                    ; 0D51 00
    xor    a               ; 0D52 AF
    nop                    ; 0D53 00
    rst    08h             ; 0D54 CF
    nop                    ; 0D55 00
    rst    38h             ; 0D56 FF
    nop                    ; 0D57 00
    call   m,$fa00         ; 0D58 FC 00 FA
    nop                    ; 0D5B 00
    ret    m               ; 0D5C F8
    nop                    ; 0D5D 00
    or     $00             ; 0D5E F6 00
    call   p,$f200         ; 0D60 F4 00 F2
    nop                    ; 0D63 00
    ret    p               ; 0D64 F0
    nop                    ; 0D65 00
    ret    p               ; 0D66 F0
    ld     (bc),a          ; 0D67 02
    ret    p               ; 0D68 F0
    inc    b               ; 0D69 04
    ret    p               ; 0D6A F0
    ld     b,$f0           ; 0D6B 06 F0
    ex     af,af'          ; 0D6D 08
    ret    p               ; 0D6E F0
    ld     a,(bc)          ; 0D6F 0A
    ret    p               ; 0D70 F0
    inc    c               ; 0D71 0C
    ret    p               ; 0D72 F0
    rrca                   ; 0D73 0F
    ret    nz              ; 0D74 C0
    rrca                   ; 0D75 0F
    and    b               ; 0D76 A0
    rrca                   ; 0D77 0F
    add    a,b             ; 0D78 80
    rrca                   ; 0D79 0F
    ld     h,b             ; 0D7A 60
    rrca                   ; 0D7B 0F
    ld     b,b             ; 0D7C 40
    rrca                   ; 0D7D 0F
    jr     nz,$0d8f        ; 0D7E 20 0F
    nop                    ; 0D80 00
    rrca                   ; 0D81 0F
    ld     (bc),a          ; 0D82 02
    rrca                   ; 0D83 0F
    inc    b               ; 0D84 04
    rrca                   ; 0D85 0F
    ld     b,$0f           ; 0D86 06 0F
    ex     af,af'          ; 0D88 08
    rrca                   ; 0D89 0F
    ld     a,(bc)          ; 0D8A 0A
    rrca                   ; 0D8B 0F
    inc    c               ; 0D8C 0C
    rrca                   ; 0D8D 0F
    rrca                   ; 0D8E 0F
    rrca                   ; 0D8F 0F
    rrca                   ; 0D90 0F
    inc    c               ; 0D91 0C
    rrca                   ; 0D92 0F
    ld     a,(bc)          ; 0D93 0A
    rrca                   ; 0D94 0F
    ex     af,af'          ; 0D95 08
    rrca                   ; 0D96 0F
    ld     b,$0f           ; 0D97 06 0F
    inc    b               ; 0D99 04
    rrca                   ; 0D9A 0F
    ld     (bc),a          ; 0D9B 02
    rst    38h             ; 0D9C FF
    rst    38h             ; 0D9D FF
    nop                    ; 0D9E 00
    rrca                   ; 0D9F 0F
    ld     ($440f),hl      ; 0DA0 22 0F 44
    rrca                   ; 0DA3 0F
    ld     h,(hl)          ; 0DA4 66
    rrca                   ; 0DA5 0F
    adc    a,b             ; 0DA6 88
    rrca                   ; 0DA7 0F
    xor    d               ; 0DA8 AA
    rrca                   ; 0DA9 0F
    call   z,$ee0f         ; 0DAA CC 0F EE
    rrca                   ; 0DAD 0F
    rst    38h             ; 0DAE FF
    rst    38h             ; 0DAF FF
    rrca                   ; 0DB0 0F
    nop                    ; 0DB1 00
    cpl                    ; 0DB2 2F
    ld     (bc),a          ; 0DB3 02
    ld     c,a             ; 0DB4 4F
    inc    b               ; 0DB5 04
    ld     l,a             ; 0DB6 6F
    ld     b,$8f           ; 0DB7 06 8F
    ex     af,af'          ; 0DB9 08
    xor    a               ; 0DBA AF
    ld     a,(bc)          ; 0DBB 0A
    rst    08h             ; 0DBC CF
    inc    c               ; 0DBD 0C
    rst    28h             ; 0DBE EF
    ld     c,$ff           ; 0DBF 0E FF
    rst    38h             ; 0DC1 FF
    rrca                   ; 0DC2 0F
    rrca                   ; 0DC3 0F
    cpl                    ; 0DC4 2F
    rrca                   ; 0DC5 0F
    ld     c,a             ; 0DC6 4F
    rrca                   ; 0DC7 0F
    ld     l,a             ; 0DC8 6F
    rrca                   ; 0DC9 0F
    adc    a,a             ; 0DCA 8F
    rrca                   ; 0DCB 0F
    xor    a               ; 0DCC AF
    rrca                   ; 0DCD 0F
    rst    08h             ; 0DCE CF
    rrca                   ; 0DCF 0F
    rst    28h             ; 0DD0 EF
    rrca                   ; 0DD1 0F
    rst    38h             ; 0DD2 FF
    rst    38h             ; 0DD3 FF
    ret    p               ; 0DD4 F0
    nop                    ; 0DD5 00
    jp     p,$f402         ; 0DD6 F2 02 F4
    inc    b               ; 0DD9 04
    or     $06             ; 0DDA F6 06
    ret    m               ; 0DDC F8
    ex     af,af'          ; 0DDD 08
    jp     m,$fc0a         ; 0DDE FA 0A FC
    inc    c               ; 0DE1 0C
    cp     $0e             ; 0DE2 FE 0E
    rst    38h             ; 0DE4 FF
    rst    38h             ; 0DE5 FF
    ret    p               ; 0DE6 F0
    rrca                   ; 0DE7 0F
    jp     p,$f40f         ; 0DE8 F2 0F F4
    rrca                   ; 0DEB 0F
    or     $0f             ; 0DEC F6 0F
    ret    m               ; 0DEE F8
    rrca                   ; 0DEF 0F
    jp     m,$fc0f         ; 0DF0 FA 0F FC
    rrca                   ; 0DF3 0F
    cp     $0f             ; 0DF4 FE 0F
    rst    38h             ; 0DF6 FF
    rst    38h             ; 0DF7 FF
    rst    38h             ; 0DF8 FF
    nop                    ; 0DF9 00
    rst    38h             ; 0DFA FF
    ld     (bc),a          ; 0DFB 02
    rst    38h             ; 0DFC FF
    inc    b               ; 0DFD 04
    rst    38h             ; 0DFE FF
    ld     b,$ff           ; 0DFF 06 FF
    ex     af,af'          ; 0E01 08
    rst    38h             ; 0E02 FF
    ld     a,(bc)          ; 0E03 0A
    rst    38h             ; 0E04 FF
    inc    c               ; 0E05 0C
    rst    38h             ; 0E06 FF
    ld     c,$ff           ; 0E07 0E FF
    rst    38h             ; 0E09 FF
    rst    38h             ; 0E0A FF
    rrca                   ; 0E0B 0F
    xor    $0e             ; 0E0C EE 0E
    db     $dd             ; 0E0E DD
    dec    c               ; 0E0F 0D
    call   z,$bb0c         ; 0E10 CC 0C BB
    dec    bc              ; 0E13 0B
    xor    d               ; 0E14 AA
    ld     a,(bc)          ; 0E15 0A
    sbc    a,c             ; 0E16 99
    add    hl,bc           ; 0E17 09
    adc    a,b             ; 0E18 88
    ex     af,af'          ; 0E19 08
    rst    38h             ; 0E1A FF
    rst    38h             ; 0E1B FF
    rrca                   ; 0E1C 0F
    nop                    ; 0E1D 00
    nop                    ; 0E1E 00
    nop                    ; 0E1F 00
    sbc    a,a             ; 0E20 9F
    nop                    ; 0E21 00
    nop                    ; 0E22 00
    nop                    ; 0E23 00
    rst    38h             ; 0E24 FF
    nop                    ; 0E25 00
    nop                    ; 0E26 00
    nop                    ; 0E27 00
    rst    38h             ; 0E28 FF
    rrca                   ; 0E29 0F
    nop                    ; 0E2A 00
    nop                    ; 0E2B 00
    rrca                   ; 0E2C 0F
    nop                    ; 0E2D 00
    nop                    ; 0E2E 00
    nop                    ; 0E2F 00
    sbc    a,a             ; 0E30 9F
    nop                    ; 0E31 00
    nop                    ; 0E32 00
    nop                    ; 0E33 00
    rst    38h             ; 0E34 FF
    nop                    ; 0E35 00
    nop                    ; 0E36 00
    nop                    ; 0E37 00
    rst    38h             ; 0E38 FF
    rrca                   ; 0E39 0F
    nop                    ; 0E3A 00
    nop                    ; 0E3B 00
    rrca                   ; 0E3C 0F
    nop                    ; 0E3D 00
    nop                    ; 0E3E 00
    nop                    ; 0E3F 00
    sbc    a,a             ; 0E40 9F
    nop                    ; 0E41 00
    nop                    ; 0E42 00
    nop                    ; 0E43 00
    rst    38h             ; 0E44 FF
    rst    38h             ; 0E45 FF
    rst    38h             ; 0E46 FF
    rrca                   ; 0E47 0F
    nop                    ; 0E48 00
    nop                    ; 0E49 00
    nop                    ; 0E4A 00
    rrca                   ; 0E4B 0F
    nop                    ; 0E4C 00
    nop                    ; 0E4D 00
    rst    38h             ; 0E4E FF
    nop                    ; 0E4F 00
    nop                    ; 0E50 00
    nop                    ; 0E51 00
    sbc    a,a             ; 0E52 9F
    nop                    ; 0E53 00
    nop                    ; 0E54 00
    nop                    ; 0E55 00
    rrca                   ; 0E56 0F
    nop                    ; 0E57 00
    nop                    ; 0E58 00
    nop                    ; 0E59 00
    rst    38h             ; 0E5A FF
    rst    38h             ; 0E5B FF
    rst    38h             ; 0E5C FF
    rst    38h             ; 0E5D FF
    rst    38h             ; 0E5E FF
    rst    38h             ; 0E5F FF
    rst    38h             ; 0E60 FF
    rst    38h             ; 0E61 FF
    rst    38h             ; 0E62 FF
    rst    38h             ; 0E63 FF
    rst    38h             ; 0E64 FF
    rst    38h             ; 0E65 FF
    rst    38h             ; 0E66 FF
    rst    38h             ; 0E67 FF
    rst    38h             ; 0E68 FF
    rst    38h             ; 0E69 FF
    rst    38h             ; 0E6A FF
    rst    38h             ; 0E6B FF
    rst    38h             ; 0E6C FF
    rst    38h             ; 0E6D FF
    rst    38h             ; 0E6E FF
    rst    38h             ; 0E6F FF
    ex     af,af'          ; 0E70 08
    nop                    ; 0E71 00
    rst    38h             ; 0E72 FF
    rst    38h             ; 0E73 FF
    rrca                   ; 0E74 0F
    rrca                   ; 0E75 0F
    nop                    ; 0E76 00
    nop                    ; 0E77 00
    rrca                   ; 0E78 0F
    nop                    ; 0E79 00
    nop                    ; 0E7A 00
    nop                    ; 0E7B 00
    rst    38h             ; 0E7C FF
    rst    38h             ; 0E7D FF
    rst    38h             ; 0E7E FF
    rst    38h             ; 0E7F FF
    rst    38h             ; 0E80 FF
    rst    38h             ; 0E81 FF
    rst    38h             ; 0E82 FF
    rst    38h             ; 0E83 FF
    rst    38h             ; 0E84 FF
    rst    38h             ; 0E85 FF
    rst    38h             ; 0E86 FF
    rst    38h             ; 0E87 FF
    rst    38h             ; 0E88 FF
    rst    38h             ; 0E89 FF
    rst    38h             ; 0E8A FF
    rst    38h             ; 0E8B FF
    rst    38h             ; 0E8C FF
    rst    38h             ; 0E8D FF
    rst    38h             ; 0E8E FF
    rst    38h             ; 0E8F FF
    rst    38h             ; 0E90 FF
    rst    38h             ; 0E91 FF
    rst    38h             ; 0E92 FF
    rst    38h             ; 0E93 FF
    rst    38h             ; 0E94 FF
    rst    38h             ; 0E95 FF
    rst    38h             ; 0E96 FF
    rst    38h             ; 0E97 FF
    rst    38h             ; 0E98 FF
    rst    38h             ; 0E99 FF
    inc    c               ; 0E9A 0C
    nop                    ; 0E9B 00
    rst    38h             ; 0E9C FF
    rst    38h             ; 0E9D FF
    nop                    ; 0E9E 00
    nop                    ; 0E9F 00
    rst    38h             ; 0EA0 FF
    rst    38h             ; 0EA1 FF
    rst    38h             ; 0EA2 FF
    nop                    ; 0EA3 00
    nop                    ; 0EA4 00
    nop                    ; 0EA5 00
    nop                    ; 0EA6 00
    nop                    ; 0EA7 00
    nop                    ; 0EA8 00
    nop                    ; 0EA9 00
    nop                    ; 0EAA 00
    nop                    ; 0EAB 00
    nop                    ; 0EAC 00
    nop                    ; 0EAD 00
    nop                    ; 0EAE 00
    nop                    ; 0EAF 00
    rst    38h             ; 0EB0 FF
    rst    38h             ; 0EB1 FF
    dec    c               ; 0EB2 0D
    nop                    ; 0EB3 00
    dec    bc              ; 0EB4 0B
    nop                    ; 0EB5 00
    add    hl,bc           ; 0EB6 09
    nop                    ; 0EB7 00
    rlca                   ; 0EB8 07
    nop                    ; 0EB9 00
    dec    b               ; 0EBA 05
    nop                    ; 0EBB 00
    inc    bc              ; 0EBC 03
    nop                    ; 0EBD 00
    ld     bc,$0000        ; 0EBE 01 00 00
    nop                    ; 0EC1 00
    nop                    ; 0EC2 00
    rst    38h             ; 0EC3 FF
    call   z,$8800         ; 0EC4 CC 00 88
    nop                    ; 0EC7 00
    nop                    ; 0EC8 00
    nop                    ; 0EC9 00
    nop                    ; 0ECA 00
    nop                    ; 0ECB 00
    nop                    ; 0ECC 00
    nop                    ; 0ECD 00
    nop                    ; 0ECE 00
    nop                    ; 0ECF 00
    rst    38h             ; 0ED0 FF
    rrca                   ; 0ED1 0F
    rst    38h             ; 0ED2 FF
    rst    38h             ; 0ED3 FF
    nop                    ; 0ED4 00
    rrca                   ; 0ED5 0F
    ld     ($440f),hl      ; 0ED6 22 0F 44
    rrca                   ; 0ED9 0F
    ld     h,(hl)          ; 0EDA 66
    rrca                   ; 0EDB 0F
    adc    a,b             ; 0EDC 88
    rrca                   ; 0EDD 0F
    xor    d               ; 0EDE AA
    rrca                   ; 0EDF 0F
    call   z,$ff0f         ; 0EE0 CC 0F FF
    rrca                   ; 0EE3 0F
    rst    38h             ; 0EE4 FF
    rst    38h             ; 0EE5 FF
    rst    38h             ; 0EE6 FF
    nop                    ; 0EE7 00
    rst    38h             ; 0EE8 FF
    ld     (bc),a          ; 0EE9 02
    rst    38h             ; 0EEA FF
    inc    b               ; 0EEB 04
    rst    38h             ; 0EEC FF
    ld     b,$ff           ; 0EED 06 FF
    ex     af,af'          ; 0EEF 08
    rst    38h             ; 0EF0 FF
    ld     a,(bc)          ; 0EF1 0A
    rst    38h             ; 0EF2 FF
    inc    c               ; 0EF3 0C
    rst    38h             ; 0EF4 FF
    rrca                   ; 0EF5 0F
    rst    38h             ; 0EF6 FF
    rst    38h             ; 0EF7 FF
    rrca                   ; 0EF8 0F
    nop                    ; 0EF9 00
    cpl                    ; 0EFA 2F
    ld     (bc),a          ; 0EFB 02
    ld     c,a             ; 0EFC 4F
    inc    b               ; 0EFD 04
    ld     l,a             ; 0EFE 6F
    ld     b,$8f           ; 0EFF 06 8F
    ex     af,af'          ; 0F01 08
    xor    a               ; 0F02 AF
    ld     a,(bc)          ; 0F03 0A
    rst    08h             ; 0F04 CF
    inc    c               ; 0F05 0C
    rst    38h             ; 0F06 FF
    rrca                   ; 0F07 0F
    rst    38h             ; 0F08 FF
    rst    38h             ; 0F09 FF
    sbc    a,c             ; 0F0A 99
    nop                    ; 0F0B 00
    xor    d               ; 0F0C AA
    nop                    ; 0F0D 00
    cp     e               ; 0F0E BB
    nop                    ; 0F0F 00
    call   z,$dd00         ; 0F10 CC 00 DD
    nop                    ; 0F13 00
    xor    $00             ; 0F14 EE 00
    rst    38h             ; 0F16 FF
    nop                    ; 0F17 00
    rst    38h             ; 0F18 FF
    rst    38h             ; 0F19 FF
    add    a,h             ; 0F1A 84
    ret    nc              ; 0F1B D0
    rrca                   ; 0F1C 0F
    inc    bc              ; 0F1D 03
    ld     bc,$0305        ; 0F1E 01 05 03
    ld     (bc),a          ; 0F21 02
    nop                    ; 0F22 00
    nop                    ; 0F23 00
    adc    a,d             ; 0F24 8A
    nop                    ; 0F25 00
    nop                    ; 0F26 00
    inc    b               ; 0F27 04
    ld     bc,$0601        ; 0F28 01 01 06
    nop                    ; 0F2B 00
    nop                    ; 0F2C 00
    nop                    ; 0F2D 00
    adc    a,e             ; 0F2E 8B
    ld     a,(bc)          ; 0F2F 0A
    rrca                   ; 0F30 0F
    ld     bc,$0707        ; 0F31 01 07 07
    rlca                   ; 0F34 07
    ld     bc,$0000        ; 0F35 01 00 00
    inc    c               ; 0F38 0C
    call   nz,$020e        ; 0F39 C4 0E 02
    ld     bc,$0107        ; 0F3C 01 07 01
    nop                    ; 0F3F 00
    nop                    ; 0F40 00
    nop                    ; 0F41 00
    dec    c               ; 0F42 0D
    inc    e               ; 0F43 1C
    ld     c,$01           ; 0F44 0E 01
    ld     bc,$0101        ; 0F46 01 01 01
    ld     (bc),a          ; 0F49 02
    nop                    ; 0F4A 00
    nop                    ; 0F4B 00
    dec    c               ; 0F4C 0D
    ld     b,(hl)          ; 0F4D 46
    ld     c,$01           ; 0F4E 0E 01
    ld     bc,$0102        ; 0F50 01 02 01
    ld     (bc),a          ; 0F53 02
    nop                    ; 0F54 00
    nop                    ; 0F55 00
    dec    c               ; 0F56 0D
    ld     (hl),b          ; 0F57 70
    ld     c,$01           ; 0F58 0E 01
    ld     bc,$0103        ; 0F5A 01 03 01
    ld     (bc),a          ; 0F5D 02
    nop                    ; 0F5E 00
    nop                    ; 0F5F 00
    dec    c               ; 0F60 0D
    sbc    a,d             ; 0F61 9A
    ld     c,$04           ; 0F62 0E 04
    ld     bc,$0104        ; 0F64 01 04 01
    ld     (bc),a          ; 0F67 02
    nop                    ; 0F68 00
    nop                    ; 0F69 00
    ld     c,$48           ; 0F6A 0E 48
    dec    c               ; 0F6C 0D
    ld     (bc),a          ; 0F6D 02
    rlca                   ; 0F6E 07
    rlca                   ; 0F6F 07
    rlca                   ; 0F70 07
    inc    b               ; 0F71 04
    nop                    ; 0F72 00
    nop                    ; 0F73 00
    rrca                   ; 0F74 0F
    nop                    ; 0F75 00
    dec    c               ; 0F76 0D
    ld     (bc),a          ; 0F77 02
    rlca                   ; 0F78 07
    rlca                   ; 0F79 07
    rlca                   ; 0F7A 07
    ld     (bc),a          ; 0F7B 02
    nop                    ; 0F7C 00
    nop                    ; 0F7D 00
    inc    b               ; 0F7E 04
    cp     e               ; 0F7F BB
    rrca                   ; 0F80 0F
    inc    bc              ; 0F81 03
    ld     bc,$0101        ; 0F82 01 01 01
    inc    bc              ; 0F85 03
    nop                    ; 0F86 00
    nop                    ; 0F87 00
    rst    38h             ; 0F88 FF
    rst    38h             ; 0F89 FF
    sub    d               ; 0F8A 92
    rrca                   ; 0F8B 0F
    sbc    a,d             ; 0F8C 9A
    rrca                   ; 0F8D 0F
    sbc    a,l             ; 0F8E 9D
    rrca                   ; 0F8F 0F
    sbc    a,a             ; 0F90 9F
    rrca                   ; 0F91 0F
    nop                    ; 0F92 00
    ld     (hl),b          ; 0F93 70
    ld     (hl),c          ; 0F94 71
    ld     (hl),d          ; 0F95 72
    ld     (hl),h          ; 0F96 74
    halt                   ; 0F97 76
    ld     a,b             ; 0F98 78
    ld     a,d             ; 0F99 7A
    ld     (hl),a          ; 0F9A 77
    ld     a,c             ; 0F9B 79
    ld     a,e             ; 0F9C 7B
    ld     (hl),c          ; 0F9D 71
    ld     (hl),e          ; 0F9E 73
    ld     (hl),b          ; 0F9F 70
    ld     (hl),c          ; 0FA0 71
    ld     (hl),d          ; 0FA1 72
    ld     (hl),h          ; 0FA2 74
    halt                   ; 0FA3 76
    ld     a,b             ; 0FA4 78
    ld     a,d             ; 0FA5 7A
    ld     a,d             ; 0FA6 7A
    ld     a,d             ; 0FA7 7A
    nop                    ; 0FA8 00
    ld     a,h             ; 0FA9 7C
    ld     a,l             ; 0FAA 7D
    ld     a,(hl)          ; 0FAB 7E
    ld     a,a             ; 0FAC 7F
    rst    38h             ; 0FAD FF
    nop                    ; 0FAE 00
    ei                     ; 0FAF FB
    nop                    ; 0FB0 00
    ret    m               ; 0FB1 F8
    nop                    ; 0FB2 00
    rst    38h             ; 0FB3 FF
    rst    38h             ; 0FB4 FF
    rst    38h             ; 0FB5 FF
    rst    38h             ; 0FB6 FF
    rst    38h             ; 0FB7 FF
    rst    38h             ; 0FB8 FF
    rst    38h             ; 0FB9 FF
    rst    38h             ; 0FBA FF
    rrca                   ; 0FBB 0F
    nop                    ; 0FBC 00
    inc    c               ; 0FBD 0C
    nop                    ; 0FBE 00
    ex     af,af'          ; 0FBF 08
    nop                    ; 0FC0 00
    inc    b               ; 0FC1 04
    nop                    ; 0FC2 00
    nop                    ; 0FC3 00
    nop                    ; 0FC4 00
    inc    b               ; 0FC5 04
    nop                    ; 0FC6 00
    ex     af,af'          ; 0FC7 08
    nop                    ; 0FC8 00
    inc    c               ; 0FC9 0C
    nop                    ; 0FCA 00
    rst    38h             ; 0FCB FF
    rst    38h             ; 0FCC FF
    rst    38h             ; 0FCD FF
    rst    38h             ; 0FCE FF
    nop                    ; 0FCF 00
    or     b               ; 0FD0 B0
    rrca                   ; 0FD1 0F
    add    a,b             ; 0FD2 80
    rrca                   ; 0FD3 0F
    ld     d,b             ; 0FD4 50
    rrca                   ; 0FD5 0F
    rst    38h             ; 0FD6 FF
    rst    38h             ; 0FD7 FF
    rst    38h             ; 0FD8 FF
    rst    38h             ; 0FD9 FF
    rst    38h             ; 0FDA FF
    rst    38h             ; 0FDB FF
    rst    38h             ; 0FDC FF
    rst    38h             ; 0FDD FF
    nop                    ; 0FDE 00
    nop                    ; 0FDF 00

; ?

    ld     a,($8013)       ; 0FE0 3A 13 80
    bit    6,a             ; 0FE3 CB 77
    jp     nz,$0fee        ; 0FE5 C2 EE 0F
    ld     a,($b001)       ; 0FE8 3A 01 B0
    jp     $03a2           ; 0FEB C3 A2 03
    ld     a,($b000)       ; 0FEE 3A 00 B0
    jp     $03a2           ; 0FF1 C3 A2 03
    nop                    ; 0FF4 00
    nop                    ; 0FF5 00
    nop                    ; 0FF6 00
    nop                    ; 0FF7 00
 
; ?

    ld     a,($b003)       ; 0FF8 3A 03 B0
    ld     a,($b000)       ; 0FFB 3A 00 B0
    ret                    ; 0FFE C9
    nop                    ; 0FFF 00

; 1000h - code

    ld     a,($808e)       ; 1000 3A 8E 80
    cp     $00             ; 1003 FE 00
    jr     z,$1012         ; 1005 28 0B
    ld     a,($8028)       ; 1007 3A 28 80
    res    2,a             ; 100A CB 97
    ld     ($8028),a       ; 100C 32 28 80
    jp     $00c3           ; 100F C3 C3 00
    ld     ix,$8028        ; 1012 DD 21 28 80
    bit    2,(ix+$00)      ; 1016 DD CB 00 56
    jp     nz,$12dd        ; 101A C2 DD 12
    bit    7,(ix+$00)      ; 101D DD CB 00 7E
    jr     z,$1065         ; 1021 28 42
    ld     (ix+$02),$00    ; 1023 DD 36 02 00
    ld     hl,($8036)      ; 1027 2A 36 80
    dec    hl              ; 102A 2B
    ld     ($8036),hl      ; 102B 22 36 80
    ld     a,h             ; 102E 7C
    or     l               ; 102F B5
    jr     nz,$1046        ; 1030 20 14
    call   $2ff9           ; 1032 CD F9 2F
    call   $2fe6           ; 1035 CD E6 2F
    res    7,(ix+$00)      ; 1038 DD CB 00 BE
    res    6,(ix+$00)      ; 103C DD CB 00 B6
    ld     (ix+$02),$00    ; 1040 DD 36 02 00
    jr     $1065           ; 1044 18 1F
    ld     a,h             ; 1046 7C
    or     a               ; 1047 B7
    jr     nz,$1065        ; 1048 20 1B
    ld     a,l             ; 104A 7D
    cp     $39             ; 104B FE 39
    jr     nc,$1065        ; 104D 30 16
    and    $03             ; 104F E6 03
    cp     $00             ; 1051 FE 00
    jr     nz,$1065        ; 1053 20 10
    bit    6,(ix+$00)      ; 1055 DD CB 00 76
    jr     z,$1061         ; 1059 28 06
    res    6,(ix+$00)      ; 105B DD CB 00 B6
    jr     $1065           ; 105F 18 04
    set    6,(ix+$00)      ; 1061 DD CB 00 F6
    ld     a,(ix+$19)      ; 1065 DD 7E 19
    inc    (ix+$23)        ; 1068 DD 34 23
    bit    3,a             ; 106B CB 5F
    jr     nz,$1073        ; 106D 20 04
    ld     (ix+$23),$00    ; 106F DD 36 23 00
    and    $0a             ; 1073 E6 0A
    jr     z,$1081         ; 1075 28 0A
    ld     a,(ix+$0b)      ; 1077 DD 7E 0B
    cp     $ff             ; 107A FE FF
    jr     z,$1081         ; 107C 28 03
    jp     $1272           ; 107E C3 72 12
    ld     a,($803f)       ; 1081 3A 3F 80
    cp     $00             ; 1084 FE 00
    jr     z,$109f         ; 1086 28 17
    ld     a,($8020)       ; 1088 3A 20 80
    cp     $03             ; 108B FE 03
    jp     nz,$12ce        ; 108D C2 CE 12
    ld     (ix+$17),$00    ; 1090 DD 36 17 00
    ld     (ix+$0b),$00    ; 1094 DD 36 0B 00
    ld     (ix+$1c),$00    ; 1098 DD 36 1C 00
    jp     $1272           ; 109C C3 72 12
    bit    1,(ix+$00)      ; 109F DD CB 00 4E
    jp     nz,$10d2        ; 10A3 C2 D2 10
    call   $1add           ; 10A6 CD DD 1A
    ld     a,h             ; 10A9 7C
    or     l               ; 10AA B5
    jr     nz,$10cc        ; 10AB 20 1F
    ld     de,($8040)      ; 10AD ED 5B 40 80
    ld     a,d             ; 10B1 7A
    or     e               ; 10B2 B3
    jr     z,$10cc         ; 10B3 28 17
    bit    3,d             ; 10B5 CB 5A
    jr     z,$10be         ; 10B7 28 05
    ld     a,$11           ; 10B9 3E 11
    call   $1bfd           ; 10BB CD FD 1B
    ld     bc,$0010        ; 10BE 01 10 00
    call   $00e4           ; 10C1 CD E4 00
    ld     a,($8453)       ; 10C4 3A 53 84
    or     $c0             ; 10C7 F6 C0
    ld     ($8453),a       ; 10C9 32 53 84
    ld     (ix+$18),h      ; 10CC DD 74 18
    ld     (ix+$19),l      ; 10CF DD 75 19
    call   $1870           ; 10D2 CD 70 18
    call   $17b7           ; 10D5 CD B7 17
    ld     a,(ix+$12)      ; 10D8 DD 7E 12
    cp     $ff             ; 10DB FE FF
    jp     nz,$1225        ; 10DD C2 25 12
    ld     hl,($8042)      ; 10E0 2A 42 80
    ld     a,h             ; 10E3 7C
    or     l               ; 10E4 B5
    jr     nz,$10f1        ; 10E5 20 0A
    ld     hl,($8031)      ; 10E7 2A 31 80
    ld     a,h             ; 10EA 7C
    or     l               ; 10EB B5
    jr     nz,$10f1        ; 10EC 20 03
    jp     $1138           ; 10EE C3 38 11
    bit    0,(ix+$00)      ; 10F1 DD CB 00 46
    jp     nz,$1152        ; 10F5 C2 52 11
    ld     a,($8020)       ; 10F8 3A 20 80
    cp     $03             ; 10FB FE 03
    jp     nz,$111d        ; 10FD C2 1D 11
    call   $1773           ; 1100 CD 73 17
    ld     a,$12           ; 1103 3E 12
    call   $1bfd           ; 1105 CD FD 1B
    ld     (ix+$0b),$00    ; 1108 DD 36 0B 00
    ld     (ix+$1c),$00    ; 110C DD 36 1C 00
    bit    3,(ix+$19)      ; 1110 DD CB 19 5E
    jr     z,$111d         ; 1114 28 07
    set    0,(ix+$00)      ; 1116 DD CB 00 C6
    jp     $1272           ; 111A C3 72 12
    ld     a,($8041)       ; 111D 3A 41 80
    bit    3,a             ; 1120 CB 5F
    jr     z,$1135         ; 1122 28 11
    bit    0,(ix+$00)      ; 1124 DD CB 00 46
    jr     nz,$1135        ; 1128 20 0B
    call   $1773           ; 112A CD 73 17
    ld     (ix+$17),$08    ; 112D DD 36 17 08
    set    0,(ix+$00)      ; 1131 DD CB 00 C6
    jp     $11a7           ; 1135 C3 A7 11
    ld     a,($8020)       ; 1138 3A 20 80
    cp     $03             ; 113B FE 03
    jr     nz,$114f        ; 113D 20 10
    ld     a,$12           ; 113F 3E 12
    call   $1bfd           ; 1141 CD FD 1B
    ld     (ix+$0b),$00    ; 1144 DD 36 0B 00
    ld     (ix+$1c),$00    ; 1148 DD 36 1C 00
    jp     $1272           ; 114C C3 72 12
    jp     $11a7           ; 114F C3 A7 11
    ld     a,($8020)       ; 1152 3A 20 80
    cp     $03             ; 1155 FE 03
    jp     nz,$1177        ; 1157 C2 77 11
    call   $1773           ; 115A CD 73 17
    ld     a,$12           ; 115D 3E 12
    call   $1bfd           ; 115F CD FD 1B
    ld     (ix+$0b),$00    ; 1162 DD 36 0B 00
    ld     (ix+$1c),$00    ; 1166 DD 36 1C 00
    bit    1,(ix+$19)      ; 116A DD CB 19 4E
    jr     z,$1177         ; 116E 28 07
    res    0,(ix+$00)      ; 1170 DD CB 00 86
    jp     $1272           ; 1174 C3 72 12
    ld     a,($8041)       ; 1177 3A 41 80
    bit    1,a             ; 117A CB 4F
    jr     z,$118f         ; 117C 28 11
    bit    0,(ix+$00)      ; 117E DD CB 00 46
    jr     z,$118f         ; 1182 28 0B
    call   $1773           ; 1184 CD 73 17
    ld     (ix+$17),$00    ; 1187 DD 36 17 00
    res    0,(ix+$00)      ; 118B DD CB 00 86
    ld     hl,($8042)      ; 118F 2A 42 80
    bit    7,h             ; 1192 CB 7C
    jr     nz,$119a        ; 1194 20 04
    ld     a,h             ; 1196 7C
    or     l               ; 1197 B5
    jr     nz,$11a4        ; 1198 20 0A
    ld     hl,$0000        ; 119A 21 00 00
    ld     ($8042),hl      ; 119D 22 42 80
    res    0,(ix+$00)      ; 11A0 DD CB 00 86
    jp     $11a7           ; 11A4 C3 A7 11
    ld     a,($8041)       ; 11A7 3A 41 80
    bit    3,a             ; 11AA CB 5F
    jr     z,$11c0         ; 11AC 28 12
    ld     hl,($8042)      ; 11AE 2A 42 80
    ld     a,h             ; 11B1 7C
    or     l               ; 11B2 B5
    jr     nz,$11c0        ; 11B3 20 0B
    ld     hl,$0000        ; 11B5 21 00 00
    ld     ($8031),hl      ; 11B8 22 31 80
    ld     ($8038),hl      ; 11BB 22 38 80
    jr     $11de           ; 11BE 18 1E
    call   $18e3           ; 11C0 CD E3 18
    bit    0,(ix+$00)      ; 11C3 DD CB 00 46
    jr     nz,$11d5        ; 11C7 20 0C
    ld     hl,($805a)      ; 11C9 2A 5A 80
    ld     de,$000a        ; 11CC 11 0A 00
    add    hl,de           ; 11CF 19
    ld     ($8031),hl      ; 11D0 22 31 80
    jr     $11de           ; 11D3 18 09
    ld     hl,($805a)      ; 11D5 2A 5A 80
    call   $18d9           ; 11D8 CD D9 18
    ld     ($8031),hl      ; 11DB 22 31 80
    call   $189a           ; 11DE CD 9A 18
    ld     hl,($8042)      ; 11E1 2A 42 80
    ld     de,($8031)      ; 11E4 ED 5B 31 80
    ld     a,($801c)       ; 11E8 3A 1C 80
    cp     $00             ; 11EB FE 00
    jr     nz,$11f8        ; 11ED 20 09
    ld     a,($801e)       ; 11EF 3A 1E 80
    cp     $00             ; 11F2 FE 00
    jr     nz,$11ff        ; 11F4 20 09
    jr     $1204           ; 11F6 18 0C
    or     a               ; 11F8 B7
    sra    d               ; 11F9 CB 2A
    rr     e               ; 11FB CB 1B
    jr     $1204           ; 11FD 18 05
    or     a               ; 11FF B7
    sla    e               ; 1200 CB 23
    rl     d               ; 1202 CB 12
    add    hl,de           ; 1204 19
    bit    7,h             ; 1205 CB 7C
    jr     nz,$1212        ; 1207 20 09
    ld     a,h             ; 1209 7C
    or     l               ; 120A B5
    jr     z,$1212         ; 120B 28 05
    ld     ($8042),hl      ; 120D 22 42 80
    jr     $1222           ; 1210 18 10
    bit    0,(ix+$00)      ; 1212 DD CB 00 46
    jr     z,$1222         ; 1216 28 0A
    ld     hl,$0000        ; 1218 21 00 00
    ld     ($8042),hl      ; 121B 22 42 80
    res    0,(ix+$00)      ; 121E DD CB 00 86
    jp     $1272           ; 1222 C3 72 12
    bit    0,(ix+$00)      ; 1225 DD CB 00 46
    jp     nz,$124f        ; 1229 C2 4F 12
    ld     a,($8020)       ; 122C 3A 20 80
    cp     $03             ; 122F FE 03
    jr     nz,$1239        ; 1231 20 06
    call   $1773           ; 1233 CD 73 17
    call   $170d           ; 1236 CD 0D 17
    ld     a,($8020)       ; 1239 3A 20 80
    cp     $ff             ; 123C FE FF
    jr     nz,$1243        ; 123E 20 03
    call   $1733           ; 1240 CD 33 17
    ld     a,($8020)       ; 1243 3A 20 80
    or     a               ; 1246 B7
    jr     nz,$124c        ; 1247 20 03
    call   $1753           ; 1249 CD 53 17
    jp     $111d           ; 124C C3 1D 11
    ld     a,($8020)       ; 124F 3A 20 80
    cp     $03             ; 1252 FE 03
    jr     nz,$125c        ; 1254 20 06
    call   $1773           ; 1256 CD 73 17
    call   $170d           ; 1259 CD 0D 17
    ld     a,($8020)       ; 125C 3A 20 80
    cp     $ff             ; 125F FE FF
    jr     nz,$1266        ; 1261 20 03
    call   $1733           ; 1263 CD 33 17
    ld     a,($8020)       ; 1266 3A 20 80
    or     a               ; 1269 B7
    jr     nz,$126f        ; 126A 20 03
    call   $1753           ; 126C CD 53 17
    jp     $1177           ; 126F C3 77 11
    res    3,(ix+$00)      ; 1272 DD CB 00 9E
    call   $158e           ; 1276 CD 8E 15
    bit    3,(ix+$00)      ; 1279 DD CB 00 5E
    jr     nz,$1282        ; 127D 20 03
    call   $14b4           ; 127F CD B4 14
    call   $1951           ; 1282 CD 51 19
    res    1,(ix+$00)      ; 1285 DD CB 00 8E
    ld     a,($8502)       ; 1289 3A 02 85
    cp     (ix+$03)        ; 128C DD BE 03
    jr     nz,$129d        ; 128F 20 0C
    ld     a,($8503)       ; 1291 3A 03 85
    cp     (ix+$04)        ; 1294 DD BE 04
    jr     nz,$129d        ; 1297 20 04
    set    1,(ix+$00)      ; 1299 DD CB 00 CE
    bit    7,(ix+$00)      ; 129D DD CB 00 7E
    jr     z,$12a7         ; 12A1 28 04
    ld     (ix+$02),$0e    ; 12A3 DD 36 02 0E
    call   $16f4           ; 12A7 CD F4 16
    ld     a,($803a)       ; 12AA 3A 3A 80
    cp     $ff             ; 12AD FE FF
    jr     z,$12cb         ; 12AF 28 1A
    cp     $00             ; 12B1 FE 00
    jr     nz,$12c4        ; 12B3 20 0F
    ld     a,($8035)       ; 12B5 3A 35 80
    inc    a               ; 12B8 3C
    ld     ($8035),a       ; 12B9 32 35 80
    call   $17b7           ; 12BC CD B7 17
    call   $17fc           ; 12BF CD FC 17
    jr     $12cb           ; 12C2 18 07
    ld     a,($803a)       ; 12C4 3A 3A 80
    dec    a               ; 12C7 3D
    ld     ($803a),a       ; 12C8 32 3A 80
    jp     $00c3           ; 12CB C3 C3 00
    ld     a,$19           ; 12CE 3E 19
    ld     ($8500),a       ; 12D0 32 00 85
    ld     a,($803f)       ; 12D3 3A 3F 80
    dec    a               ; 12D6 3D
    ld     ($803f),a       ; 12D7 32 3F 80
    jp     $00c3           ; 12DA C3 C3 00
    ld     a,($8095)       ; 12DD 3A 95 80
    cp     $ff             ; 12E0 FE FF
    jr     z,$12ea         ; 12E2 28 06
    call   $1370           ; 12E4 CD 70 13
    jp     $136d           ; 12E7 C3 6D 13
    ld     ix,$805e        ; 12EA DD 21 5E 80
    ld     iy,$8500        ; 12EE FD 21 00 85
    ld     a,($805f)       ; 12F2 3A 5F 80
    cp     $ff             ; 12F5 FE FF
    jr     z,$1325         ; 12F7 28 2C
    ld     a,$01           ; 12F9 3E 01
    call   $00fc           ; 12FB CD FC 00
    ld     a,$14           ; 12FE 3E 14
    call   $00fc           ; 1300 CD FC 00
    ld     (ix+$00),$00    ; 1303 DD 36 00 00
    ld     (ix+$01),$ff    ; 1307 DD 36 01 FF
    ld     (iy+$00),$a4    ; 130B FD 36 00 A4
    ld     (iy+$01),$20    ; 130F FD 36 01 20
    ld     a,(iy+$02)      ; 1313 FD 7E 02
    sub    $08             ; 1316 D6 08
    ld     (iy+$02),a      ; 1318 FD 77 02
    ld     a,(iy+$03)      ; 131B FD 7E 03
    sub    $08             ; 131E D6 08
    ld     (iy+$03),a      ; 1320 FD 77 03
    jr     $136d           ; 1323 18 48
    ld     a,(ix+$00)      ; 1325 DD 7E 00
    cp     $40             ; 1328 FE 40
    jp     nc,$1341        ; 132A D2 41 13
    inc    (ix+$00)        ; 132D DD 34 00
    ld     a,(ix+$00)      ; 1330 DD 7E 00
    cp     $40             ; 1333 FE 40
    jp     nc,$1341        ; 1335 D2 41 13
    and    $0f             ; 1338 E6 0F
    jr     nz,$136d        ; 133A 20 31
    inc    (iy+$00)        ; 133C FD 34 00
    jr     $136d           ; 133F 18 2C
    ld     hl,$0000        ; 1341 21 00 00
    ld     ($8500),hl      ; 1344 22 00 85
    ld     a,($8088)       ; 1347 3A 88 80
    cp     $00             ; 134A FE 00
    jr     nz,$136d        ; 134C 20 1F
    ld     a,($808e)       ; 134E 3A 8E 80
    cp     $00             ; 1351 FE 00
    jr     nz,$136d        ; 1353 20 18
    ld     a,$00           ; 1355 3E 00
    ld     ($8095),a       ; 1357 32 95 80
    ld     a,($8070)       ; 135A 3A 70 80
    cp     $00             ; 135D FE 00
    jr     nz,$1368        ; 135F 20 07
    ld     a,$01           ; 1361 3E 01
    ld     ($8078),a       ; 1363 32 78 80
    jr     $136d           ; 1366 18 05
    ld     a,$01           ; 1368 3E 01
    ld     ($8077),a       ; 136A 32 77 80
    jp     $00c3           ; 136D C3 C3 00
    ld     ix,$8095        ; 1370 DD 21 95 80
    ld     a,(ix+$00)      ; 1374 DD 7E 00
    and    $0f             ; 1377 E6 0F
    cp     $0f             ; 1379 FE 0F
    jr     z,$1387         ; 137B 28 0A
    cp     $07             ; 137D FE 07
    jr     z,$1387         ; 137F 28 06
    call   $142d           ; 1381 CD 2D 14
    jp     $142c           ; 1384 C3 2C 14
    ld     hl,$0000        ; 1387 21 00 00
    ld     ($8031),hl      ; 138A 22 31 80
    call   $1add           ; 138D CD DD 1A
    bit    3,l             ; 1390 CB 5D
    jr     z,$13a0         ; 1392 28 0C
    ld     (ix+$00),$ff    ; 1394 DD 36 00 FF
    ld     a,$a7           ; 1398 3E A7
    call   $00fc           ; 139A CD FC 00
    jp     $142c           ; 139D C3 2C 14
    ld     a,(ix+$00)      ; 13A0 DD 7E 00
    cp     $0f             ; 13A3 FE 0F
    jr     z,$13ce         ; 13A5 28 27
    ld     (ix+$00),$0f    ; 13A7 DD 36 00 0F
    ld     hl,($14aa)      ; 13AB 2A AA 14
    ld     (ix+$01),l      ; 13AE DD 75 01
    ld     (ix+$02),h      ; 13B1 DD 74 02
    ld     (ix+$03),$00    ; 13B4 DD 36 03 00
    ld     a,($8503)       ; 13B8 3A 03 85
    ld     (ix+$04),a      ; 13BB DD 77 04
    ld     (ix+$05),$00    ; 13BE DD 36 05 00
    ld     (ix+$06),$00    ; 13C2 DD 36 06 00
    ld     (ix+$07),$00    ; 13C6 DD 36 07 00
    ld     (ix+$08),$00    ; 13CA DD 36 08 00
    ld     hl,($809a)      ; 13CE 2A 9A 80
    ld     de,$000a        ; 13D1 11 0A 00
    add    hl,de           ; 13D4 19
    ld     ($809a),hl      ; 13D5 22 9A 80
    ld     de,($8098)      ; 13D8 ED 5B 98 80
    add    hl,de           ; 13DC 19
    ld     ($8098),hl      ; 13DD 22 98 80
    inc    (ix+$07)        ; 13E0 DD 34 07
    ld     a,(ix+$07)      ; 13E3 DD 7E 07
    cp     $05             ; 13E6 FE 05
    jr     c,$141a         ; 13E8 38 30
    ld     (ix+$07),$00    ; 13EA DD 36 07 00
    ld     iy,$14aa        ; 13EE FD 21 AA 14
    ld     d,$00           ; 13F2 16 00
    ld     e,(ix+$08)      ; 13F4 DD 5E 08
    add    iy,de           ; 13F7 FD 19
    ld     a,(iy+$00)      ; 13F9 FD 7E 00
    cp     $ff             ; 13FC FE FF
    jr     nz,$1408        ; 13FE 20 08
    ld     iy,$14aa        ; 1400 FD 21 AA 14
    ld     (ix+$08),$00    ; 1404 DD 36 08 00
    inc    (ix+$08)        ; 1408 DD 34 08
    inc    (ix+$08)        ; 140B DD 34 08
    ld     a,(iy+$00)      ; 140E FD 7E 00
    ld     (ix+$01),a      ; 1411 DD 77 01
    ld     a,(iy+$01)      ; 1414 FD 7E 01
    ld     (ix+$02),a      ; 1417 DD 77 02
    ld     a,($8096)       ; 141A 3A 96 80
    ld     ($8500),a       ; 141D 32 00 85
    ld     a,($8097)       ; 1420 3A 97 80
    ld     ($8501),a       ; 1423 32 01 85
    ld     a,($8099)       ; 1426 3A 99 80
    ld     ($8503),a       ; 1429 32 03 85
    ret                    ; 142C C9
    ld     a,(ix+$00)      ; 142D DD 7E 00
    cp     $03             ; 1430 FE 03
    jr     z,$1453         ; 1432 28 1F
    ld     a,$01           ; 1434 3E 01
    ld     ($808d),a       ; 1436 32 8D 80
    ld     (ix+$00),$03    ; 1439 DD 36 00 03
    ld     (ix+$07),$00    ; 143D DD 36 07 00
    ld     (ix+$08),$00    ; 1441 DD 36 08 00
    ld     (ix+$09),$00    ; 1445 DD 36 09 00
    ld     a,$01           ; 1449 3E 01
    call   $00fc           ; 144B CD FC 00
    ld     a,$27           ; 144E 3E 27
    call   $00fc           ; 1450 CD FC 00
    ld     a,(ix+$07)      ; 1453 DD 7E 07
    cp     $3c             ; 1456 FE 3C
    jr     c,$1461         ; 1458 38 07
    ld     (ix+$00),$07    ; 145A DD 36 00 07
    jp     $149f           ; 145E C3 9F 14
    ld     a,(ix+$09)      ; 1461 DD 7E 09
    cp     $03             ; 1464 FE 03
    jr     c,$1493         ; 1466 38 2B
    ld     (ix+$09),$00    ; 1468 DD 36 09 00
    ld     iy,$14a0        ; 146C FD 21 A0 14
    ld     d,$00           ; 1470 16 00
    ld     e,(ix+$08)      ; 1472 DD 5E 08
    or     a               ; 1475 B7
    add    iy,de           ; 1476 FD 19
    ld     a,(iy+$00)      ; 1478 FD 7E 00
    cp     $ff             ; 147B FE FF
    jr     nz,$1487        ; 147D 20 08
    ld     iy,$14a0        ; 147F FD 21 A0 14
    ld     (ix+$08),$00    ; 1483 DD 36 08 00
    ld     a,(iy+$00)      ; 1487 FD 7E 00
    ld     ($8500),a       ; 148A 32 00 85
    ld     a,(iy+$01)      ; 148D FD 7E 01
    ld     ($8501),a       ; 1490 32 01 85
    inc    (ix+$07)        ; 1493 DD 34 07
    inc    (ix+$08)        ; 1496 DD 34 08
    inc    (ix+$08)        ; 1499 DD 34 08
    inc    (ix+$09)        ; 149C DD 34 09
    ret                    ; 149F C9

; 14A0h - data ?

    jr     z,$1462         ; 14A0 28 C0
    jr     z,$1424         ; 14A2 28 80
    jr     z,$14a6         ; 14A4 28 00
    jr     z,$14e8         ; 14A6 28 40
    rst    38h             ; 14A8 FF
    rst    38h             ; 14A9 FF
    ld     c,b             ; 14AA 48
    nop                    ; 14AB 00
    ld     c,c             ; 14AC 49
    nop                    ; 14AD 00
    ld     c,d             ; 14AE 4A
    nop                    ; 14AF 00
    ld     c,e             ; 14B0 4B
    nop                    ; 14B1 00
    rst    38h             ; 14B2 FF
    rst    38h             ; 14B3 FF

; 14B4h - ?

    push   hl              ; 14B4 E5
    push   de              ; 14B5 D5
    ld     hl,($802f)      ; 14B6 2A 2F 80
    ld     de,($8031)      ; 14B9 ED 5B 31 80
    ld     a,d             ; 14BD 7A
    or     e               ; 14BE B3
    jr     nz,$1509        ; 14BF 20 48
    ld     a,($8041)       ; 14C1 3A 41 80
    bit    3,a             ; 14C4 CB 5F
    jr     z,$1509         ; 14C6 28 41
    ld     a,h             ; 14C8 7C
    or     l               ; 14C9 B5
    jr     nz,$14dd        ; 14CA 20 11
    ld     a,($8029)       ; 14CC 3A 29 80
    cp     $1b             ; 14CF FE 1B
    jr     c,$14d9         ; 14D1 38 06
    cp     $23             ; 14D3 FE 23
    jr     nc,$14d9        ; 14D5 30 02
    jr     $1533           ; 14D7 18 5A
    ld     a,$18           ; 14D9 3E 18
    jr     $1533           ; 14DB 18 56
    bit    7,h             ; 14DD CB 7C
    jr     nz,$14f5        ; 14DF 20 14
    inc    (ix+$22)        ; 14E1 DD 34 22
    ld     a,($804a)       ; 14E4 3A 4A 80
    cp     $04             ; 14E7 FE 04
    jp     c,$154b         ; 14E9 DA 4B 15
    ld     (ix+$22),$00    ; 14EC DD 36 22 00
    call   $154e           ; 14F0 CD 4E 15
    jr     $1540           ; 14F3 18 4B
    inc    (ix+$22)        ; 14F5 DD 34 22
    ld     a,($804a)       ; 14F8 3A 4A 80
    cp     $04             ; 14FB FE 04
    jp     c,$154b         ; 14FD DA 4B 15
    ld     (ix+$22),$00    ; 1500 DD 36 22 00
    call   $156e           ; 1504 CD 6E 15
    jr     $1540           ; 1507 18 37
    bit    7,d             ; 1509 CB 7A
    jr     z,$1521         ; 150B 28 14
    ld     a,h             ; 150D 7C
    or     l               ; 150E B5
    jr     nz,$1515        ; 150F 20 04
    ld     a,$23           ; 1511 3E 23
    jr     $1533           ; 1513 18 1E
    bit    7,h             ; 1515 CB 7C
    jr     nz,$151d        ; 1517 20 04
    ld     a,$25           ; 1519 3E 25
    jr     $1533           ; 151B 18 16
    ld     a,$27           ; 151D 3E 27
    jr     $1533           ; 151F 18 12
    ld     a,h             ; 1521 7C
    or     l               ; 1522 B5
    jr     nz,$1529        ; 1523 20 04
    ld     a,$24           ; 1525 3E 24
    jr     $1533           ; 1527 18 0A
    bit    7,h             ; 1529 CB 7C
    jr     nz,$1531        ; 152B 20 04
    ld     a,$26           ; 152D 3E 26
    jr     $1533           ; 152F 18 02
    ld     a,$28           ; 1531 3E 28
    ld     ($8029),a       ; 1533 32 29 80
    ld     ($8500),a       ; 1536 32 00 85
    ld     a,$93           ; 1539 3E 93
    call   $1bfd           ; 153B CD FD 1B
    jr     $154b           ; 153E 18 0B
    ld     ($8029),a       ; 1540 32 29 80
    ld     ($8500),a       ; 1543 32 00 85
    ld     a,$13           ; 1546 3E 13
    call   $1bfd           ; 1548 CD FD 1B
    pop    de              ; 154B D1
    pop    hl              ; 154C E1
    ret                    ; 154D C9

    ld     a,($8029)       ; 154E 3A 29 80
    cp     $1b             ; 1551 FE 1B
    jp     p,$155a         ; 1553 F2 5A 15
    ld     a,$1b           ; 1556 3E 1B
    jr     $156d           ; 1558 18 13
    cp     $1e             ; 155A FE 1E
    jr     z,$1564         ; 155C 28 06
    jr     c,$1564         ; 155E 38 04
    ld     a,$1b           ; 1560 3E 1B
    jr     $156d           ; 1562 18 09
    inc    a               ; 1564 3C
    cp     $1e             ; 1565 FE 1E
    jr     z,$156d         ; 1567 28 04
    jr     c,$156d         ; 1569 38 02
    ld     a,$1b           ; 156B 3E 1B
    ret                    ; 156D C9
    ld     a,($8029)       ; 156E 3A 29 80
    cp     $1f             ; 1571 FE 1F
    jp     p,$157a         ; 1573 F2 7A 15
    ld     a,$1f           ; 1576 3E 1F
    jr     $158d           ; 1578 18 13
    cp     $22             ; 157A FE 22
    jr     z,$1584         ; 157C 28 06
    jr     c,$1584         ; 157E 38 04
    ld     a,$1f           ; 1580 3E 1F
    jr     $158d           ; 1582 18 09
    inc    a               ; 1584 3C
    cp     $22             ; 1585 FE 22
    jr     z,$158d         ; 1587 28 04
    jr     c,$158d         ; 1589 38 02
    ld     a,$1f           ; 158B 3E 1F
    ret                    ; 158D C9
    push   bc              ; 158E C5
    push   de              ; 158F D5
    push   hl              ; 1590 E5
    push   ix              ; 1591 DD E5
    ld     ix,$8028        ; 1593 DD 21 28 80
    ld     a,(ix+$0b)      ; 1597 DD 7E 0B
    cp     $ff             ; 159A FE FF
    jp     z,$16ee         ; 159C CA EE 16
    ld     a,(ix+$1c)      ; 159F DD 7E 1C
    cp     $02             ; 15A2 FE 02
    jp     z,$1678         ; 15A4 CA 78 16
    cp     $01             ; 15A7 FE 01
    jp     z,$15bc         ; 15A9 CA BC 15
    ld     (ix+$1d),$28    ; 15AC DD 36 1D 28
    ld     (ix+$1e),$00    ; 15B0 DD 36 1E 00
    ld     (ix+$1f),$00    ; 15B4 DD 36 1F 00
    ld     (ix+$1c),$01    ; 15B8 DD 36 1C 01
    ld     a,($804b)       ; 15BC 3A 4B 80
    cp     $01             ; 15BF FE 01
    jr     nc,$15ed        ; 15C1 30 2A
    ld     a,($8018)       ; 15C3 3A 18 80
    cp     $00             ; 15C6 FE 00
    jr     nz,$15d1        ; 15C8 20 07
    ld     a,($801a)       ; 15CA 3A 1A 80
    cp     $00             ; 15CD FE 00
    jr     z,$15ed         ; 15CF 28 1C
    ld     (ix+$1d),$00    ; 15D1 DD 36 1D 00
    ld     (ix+$1e),$00    ; 15D5 DD 36 1E 00
    ld     (ix+$1f),$00    ; 15D9 DD 36 1F 00
    ld     (ix+$1c),$02    ; 15DD DD 36 1C 02
    ld     hl,$0000        ; 15E1 21 00 00
    ld     ($8042),hl      ; 15E4 22 42 80
    ld     ($8031),hl      ; 15E7 22 31 80
    jp     $16ee           ; 15EA C3 EE 16
    set    3,(ix+$00)      ; 15ED DD CB 00 DE
    ld     a,(ix+$0b)      ; 15F1 DD 7E 0B
    cp     $10             ; 15F4 FE 10
    jp     z,$163a         ; 15F6 CA 3A 16
    jp     c,$163a         ; 15F9 DA 3A 16
    ld     (ix+$1d),$00    ; 15FC DD 36 1D 00
    ld     (ix+$1e),$28    ; 1600 DD 36 1E 28
    cp     $20             ; 1604 FE 20
    jp     z,$1653         ; 1606 CA 53 16
    jp     c,$1653         ; 1609 DA 53 16
    ld     (ix+$1e),$04    ; 160C DD 36 1E 04
    ld     (ix+$1c),$02    ; 1610 DD 36 1C 02
    ld     a,(ix+$19)      ; 1614 DD 7E 19
    and    $0a             ; 1617 E6 0A
    jr     z,$1627         ; 1619 28 0C
    ld     hl,$0000        ; 161B 21 00 00
    ld     ($8042),hl      ; 161E 22 42 80
    ld     ($8031),hl      ; 1621 22 31 80
    jp     $166c           ; 1624 C3 6C 16
    ld     a,(ix+$19)      ; 1627 DD 7E 19
    cp     $00             ; 162A FE 00
    jr     z,$1634         ; 162C 28 06
    call   $1798           ; 162E CD 98 17
    jp     $166c           ; 1631 C3 6C 16
    call   $1773           ; 1634 CD 73 17
    jp     $166c           ; 1637 C3 6C 16
    ld     (ix+$1d),$00    ; 163A DD 36 1D 00
    ld     (ix+$1e),$04    ; 163E DD 36 1E 04
    ld     (ix+$1f),$02    ; 1642 DD 36 1F 02
    ld     bc,$7fff        ; 1646 01 FF 7F
    call   $177a           ; 1649 CD 7A 17
    ld     (ix+$1c),$02    ; 164C DD 36 1C 02
    jp     $166c           ; 1650 C3 6C 16
    ld     a,($8020)       ; 1653 3A 20 80
    or     a               ; 1656 B7
    jp     nz,$166c        ; 1657 C2 6C 16
    ld     (ix+$1e),$04    ; 165A DD 36 1E 04
    ld     (ix+$1f),$02    ; 165E DD 36 1F 02
    ld     bc,$1a00        ; 1662 01 00 1A
    call   $177a           ; 1665 CD 7A 17
    ld     (ix+$1c),$02    ; 1668 DD 36 1C 02
    ld     a,(ix+$0b)      ; 166C DD 7E 0B
    cp     $50             ; 166F FE 50
    jp     nc,$1678        ; 1671 D2 78 16
    inc    a               ; 1674 3C
    ld     (ix+$0b),a      ; 1675 DD 77 0B
    ld     a,(ix+$1d)      ; 1678 DD 7E 1D
    cp     $00             ; 167B FE 00
    jp     z,$168f         ; 167D CA 8F 16
    ld     (ix+$0c),$01    ; 1680 DD 36 0C 01
    ld     (ix+$0d),$00    ; 1684 DD 36 0D 00
    dec    a               ; 1688 3D
    ld     (ix+$1d),a      ; 1689 DD 77 1D
    jp     $16c9           ; 168C C3 C9 16
    ld     a,(ix+$1e)      ; 168F DD 7E 1E
    cp     $00             ; 1692 FE 00
    jp     z,$16a6         ; 1694 CA A6 16
    ld     (ix+$0c),$01    ; 1697 DD 36 0C 01
    ld     (ix+$0d),$01    ; 169B DD 36 0D 01
    dec    a               ; 169F 3D
    ld     (ix+$1e),a      ; 16A0 DD 77 1E
    jp     $16c9           ; 16A3 C3 C9 16
    ld     a,(ix+$1f)      ; 16A6 DD 7E 1F
    cp     $00             ; 16A9 FE 00
    jp     z,$16bd         ; 16AB CA BD 16
    ld     (ix+$0c),$01    ; 16AE DD 36 0C 01
    ld     (ix+$0d),$02    ; 16B2 DD 36 0D 02
    dec    a               ; 16B6 3D
    ld     (ix+$1f),a      ; 16B7 DD 77 1F
    jp     $16c9           ; 16BA C3 C9 16
    ld     (ix+$0c),$00    ; 16BD DD 36 0C 00
    ld     (ix+$0d),$00    ; 16C1 DD 36 0D 00
    ld     (ix+$0b),$ff    ; 16C5 DD 36 0B FF
    call   $17b7           ; 16C9 CD B7 17
    call   $17fc           ; 16CC CD FC 17
    ld     a,($8020)       ; 16CF 3A 20 80
    cp     $03             ; 16D2 FE 03
    jp     nz,$16ee        ; 16D4 C2 EE 16
    ld     a,$12           ; 16D7 3E 12
    call   $1bfd           ; 16D9 CD FD 1B
    ld     (ix+$0b),$00    ; 16DC DD 36 0B 00
    ld     (ix+$1c),$00    ; 16E0 DD 36 1C 00
    ld     a,(ix+$19)      ; 16E4 DD 7E 19
    and    $0a             ; 16E7 E6 0A
    jr     z,$16ee         ; 16E9 28 03
    call   $1773           ; 16EB CD 73 17
    pop    ix              ; 16EE DD E1
    pop    hl              ; 16F0 E1
    pop    de              ; 16F1 D1
    pop    bc              ; 16F2 C1
    ret                    ; 16F3 C9
    ld     a,($8029)       ; 16F4 3A 29 80
    ld     ($8500),a       ; 16F7 32 00 85
    ld     a,($802a)       ; 16FA 3A 2A 80
    ld     ($8501),a       ; 16FD 32 01 85
    ld     a,($802b)       ; 1700 3A 2B 80
    ld     ($8502),a       ; 1703 32 02 85
    ld     a,($802c)       ; 1706 3A 2C 80
    ld     ($8503),a       ; 1709 32 03 85
    ret                    ; 170C C9
    ld     a,($803b)       ; 170D 3A 3B 80
    bit    7,a             ; 1710 CB 7F
    jr     z,$1722         ; 1712 28 0E
    ld     a,($803c)       ; 1714 3A 3C 80
    ld     ($8034),a       ; 1717 32 34 80
    ld     a,($803d)       ; 171A 3A 3D 80
    ld     ($8035),a       ; 171D 32 35 80
    jr     $172c           ; 1720 18 0A
    bit    2,a             ; 1722 CB 57
    jr     z,$1732         ; 1724 28 0C
    ld     a,($803e)       ; 1726 3A 3E 80
    ld     ($8035),a       ; 1729 32 35 80
    call   $17b7           ; 172C CD B7 17
    call   $17fc           ; 172F CD FC 17
    ret                    ; 1732 C9
    ld     a,($803b)       ; 1733 3A 3B 80
    bit    6,a             ; 1736 CB 77
    jr     z,$1742         ; 1738 28 08
    ld     a,($803d)       ; 173A 3A 3D 80
    ld     ($8034),a       ; 173D 32 34 80
    jr     $174c           ; 1740 18 0A
    bit    1,a             ; 1742 CB 4F
    jr     z,$1752         ; 1744 28 0C
    ld     a,($803d)       ; 1746 3A 3D 80
    ld     ($8035),a       ; 1749 32 35 80
    call   $17b7           ; 174C CD B7 17
    call   $17fc           ; 174F CD FC 17
    ret                    ; 1752 C9
    ld     a,($803b)       ; 1753 3A 3B 80
    bit    5,a             ; 1756 CB 6F
    jr     z,$1762         ; 1758 28 08
    ld     a,($803c)       ; 175A 3A 3C 80
    ld     ($8034),a       ; 175D 32 34 80
    jr     $176c           ; 1760 18 0A
    bit    0,a             ; 1762 CB 47
    jr     z,$1772         ; 1764 28 0C
    ld     a,($803c)       ; 1766 3A 3C 80
    ld     ($8035),a       ; 1769 32 35 80
    call   $17b7           ; 176C CD B7 17
    call   $17fc           ; 176F CD FC 17
    ret                    ; 1772 C9
    ld     hl,$0000        ; 1773 21 00 00
    ld     ($8042),hl      ; 1776 22 42 80
    ret                    ; 1779 C9
    push   hl              ; 177A E5
    push   bc              ; 177B C5
    ld     a,($8041)       ; 177C 3A 41 80
    and    $0a             ; 177F E6 0A
    jr     z,$1795         ; 1781 28 12
    ld     hl,($8042)      ; 1783 2A 42 80
    add    hl,bc           ; 1786 09
    bit    7,h             ; 1787 CB 7C
    jr     z,$178e         ; 1789 28 03
    ld     hl,$7fff        ; 178B 21 FF 7F
    ld     ($8042),hl      ; 178E 22 42 80
    set    0,(ix+$00)      ; 1791 DD CB 00 C6
    pop    bc              ; 1795 C1
    pop    hl              ; 1796 E1
    ret                    ; 1797 C9
    push   hl              ; 1798 E5
    push   de              ; 1799 D5
    ld     hl,$0000        ; 179A 21 00 00
    ld     a,($8043)       ; 179D 3A 43 80
    ld     l,a             ; 17A0 6F
    call   $17dd           ; 17A1 CD DD 17
    ld     h,l             ; 17A4 65
    ld     l,(ix+$1a)      ; 17A5 DD 6E 1A
    ld     a,h             ; 17A8 7C
    or     a               ; 17A9 B7
    cp     $08             ; 17AA FE 08
    jr     nc,$17b1        ; 17AC 30 03
    ld     hl,$0000        ; 17AE 21 00 00
    ld     ($8042),hl      ; 17B1 22 42 80
    pop    de              ; 17B4 D1
    pop    hl              ; 17B5 E1
    ret                    ; 17B6 C9
    ld     hl,$1e63        ; 17B7 21 63 1E
    ld     a,(ix+$0c)      ; 17BA DD 7E 0C
    add    a,a             ; 17BD 87
    ld     e,a             ; 17BE 5F
    ld     d,$00           ; 17BF 16 00
    add    hl,de           ; 17C1 19
    ld     e,(hl)          ; 17C2 5E
    inc    hl              ; 17C3 23
    ld     d,(hl)          ; 17C4 56
    push   de              ; 17C5 D5
    pop    iy              ; 17C6 FD E1
    ld     c,(ix+$0d)      ; 17C8 DD 4E 0D
    ld     b,$00           ; 17CB 06 00
    ld     e,$07           ; 17CD 1E 07
    ld     d,$00           ; 17CF 16 00
    ld     a,c             ; 17D1 79
    or     b               ; 17D2 B0
    jp     z,$17dc         ; 17D3 CA DC 17
    add    iy,de           ; 17D6 FD 19
    dec    bc              ; 17D8 0B
    jp     $17d1           ; 17D9 C3 D1 17
    ret                    ; 17DC C9
    push   de              ; 17DD D5
    ld     d,h             ; 17DE 54
    ld     e,l             ; 17DF 5D
    or     a               ; 17E0 B7
    rl     l               ; 17E1 CB 15
    rl     h               ; 17E3 CB 14
    or     a               ; 17E5 B7
    rl     l               ; 17E6 CB 15
    rl     h               ; 17E8 CB 14
    add    hl,de           ; 17EA 19
    or     a               ; 17EB B7
    rr     h               ; 17EC CB 1C
    rr     l               ; 17EE CB 1D
    or     a               ; 17F0 B7
    rr     h               ; 17F1 CB 1C
    rr     l               ; 17F3 CB 1D
    or     a               ; 17F5 B7
    rr     h               ; 17F6 CB 1C
    rr     l               ; 17F8 CB 1D
    pop    de              ; 17FA D1
    ret                    ; 17FB C9
    ld     a,(iy+$00)      ; 17FC FD 7E 00
    ld     (ix+$01),a      ; 17FF DD 77 01
    ld     a,(iy+$01)      ; 1802 FD 7E 01
    ld     (ix+$02),a      ; 1805 DD 77 02
    ld     a,(iy+$02)      ; 1808 FD 7E 02
    ld     (ix+$12),a      ; 180B DD 77 12
    ld     a,(iy+$03)      ; 180E FD 7E 03
    ld     (ix+$13),a      ; 1811 DD 77 13
    ld     a,(iy+$04)      ; 1814 FD 7E 04
    ld     (ix+$14),a      ; 1817 DD 77 14
    ld     a,(iy+$05)      ; 181A FD 7E 05
    ld     (ix+$15),a      ; 181D DD 77 15
    ld     a,(iy+$06)      ; 1820 FD 7E 06
    ld     (ix+$16),a      ; 1823 DD 77 16
    ret                    ; 1826 C9
    push   hl              ; 1827 E5
    push   de              ; 1828 D5
    push   bc              ; 1829 C5
    ld     a,(ix+$19)      ; 182A DD 7E 19
    bit    1,a             ; 182D CB 4F
    jp     z,$186c         ; 182F CA 6C 18
    ld     a,(ix+$01)      ; 1832 DD 7E 01
    cp     b               ; 1835 B8
    jp     z,$186c         ; 1836 CA 6C 18
    ld     de,$0002        ; 1839 11 02 00
    jp     nc,$1842        ; 183C D2 42 18
    ld     de,$fffe        ; 183F 11 FE FF
    push   de              ; 1842 D5
    ld     de,$0000        ; 1843 11 00 00
    ld     hl,$0000        ; 1846 21 00 00
    ld     l,(ix+$01)      ; 1849 DD 6E 01
    ld     e,b             ; 184C 58
    or     a               ; 184D B7
    sbc    hl,de           ; 184E ED 52
    call   $18c7           ; 1850 CD C7 18
    pop    de              ; 1853 D1
    ld     a,l             ; 1854 7D
    ld     hl,$0000        ; 1855 21 00 00
    cp     $01             ; 1858 FE 01
    jr     z,$1862         ; 185A 28 06
    jr     c,$1862         ; 185C 38 04
    add    hl,de           ; 185E 19
    dec    a               ; 185F 3D
    jr     $1858           ; 1860 18 F6
    ld     de,$0000        ; 1862 11 00 00
    ld     e,(ix+$04)      ; 1865 DD 5E 04
    add    hl,de           ; 1868 19
    ld     (ix+$04),l      ; 1869 DD 75 04
    pop    bc              ; 186C C1
    pop    de              ; 186D D1
    pop    hl              ; 186E E1
    ret                    ; 186F C9
    ld     hl,$0000        ; 1870 21 00 00
    ld     ($802f),hl      ; 1873 22 2F 80
    ld     a,($8018)       ; 1876 3A 18 80
    and    $03             ; 1879 E6 03
    cp     $03             ; 187B FE 03
    jp     nz,$1889        ; 187D C2 89 18
    ld     hl,($1ea2)      ; 1880 2A A2 1E
    ld     ($802f),hl      ; 1883 22 2F 80
    jp     $1899           ; 1886 C3 99 18
    ld     a,($801a)       ; 1889 3A 1A 80
    and    $03             ; 188C E6 03
    cp     $03             ; 188E FE 03
    jp     nz,$1899        ; 1890 C2 99 18
    ld     hl,($1ea6)      ; 1893 2A A6 1E
    ld     ($802f),hl      ; 1896 22 2F 80
    ret                    ; 1899 C9
    push   de              ; 189A D5
    push   hl              ; 189B E5
    ld     a,($803f)       ; 189C 3A 3F 80
    cp     $00             ; 189F FE 00
    jp     nz,$18c4        ; 18A1 C2 C4 18
    ld     hl,($802f)      ; 18A4 2A 2F 80
    ld     e,(ix+$05)      ; 18A7 DD 5E 05
    ld     d,(ix+$03)      ; 18AA DD 56 03
    add    hl,de           ; 18AD 19
    ld     (ix+$05),l      ; 18AE DD 75 05
    ld     (ix+$03),h      ; 18B1 DD 74 03
    ld     hl,($8031)      ; 18B4 2A 31 80
    ld     e,(ix+$06)      ; 18B7 DD 5E 06
    ld     d,(ix+$04)      ; 18BA DD 56 04
    add    hl,de           ; 18BD 19
    ld     (ix+$06),l      ; 18BE DD 75 06
    ld     (ix+$04),h      ; 18C1 DD 74 04
    pop    hl              ; 18C4 E1
    pop    de              ; 18C5 D1
    ret                    ; 18C6 C9

; 18C7h - ?

    push   de              ; 18C7 D5
    ex     de,hl           ; 18C8 EB
    or     a               ; 18C9 B7
    bit    7,d             ; 18CA CB 7A
    jr     nz,$18d1        ; 18CC 20 03
    ex     de,hl           ; 18CE EB
    jr     $18d7           ; 18CF 18 06
    ld     hl,$0000        ; 18D1 21 00 00
    or     a               ; 18D4 B7
    sbc    hl,de           ; 18D5 ED 52
    pop    de              ; 18D7 D1
    ret                    ; 18D8 C9
    push   de              ; 18D9 D5
    ex     de,hl           ; 18DA EB
    ld     hl,$0000        ; 18DB 21 00 00
    or     a               ; 18DE B7
    sbc    hl,de           ; 18DF ED 52
    pop    de              ; 18E1 D1
    ret                    ; 18E2 C9
    push   hl              ; 18E3 E5
    push   ix              ; 18E4 DD E5
    push   de              ; 18E6 D5
    ld     hl,$0000        ; 18E7 21 00 00
    ld     ($805a),hl      ; 18EA 22 5A 80
    ld     ($805c),hl      ; 18ED 22 5C 80
    ld     ix,$1c18        ; 18F0 DD 21 18 1C
    ld     hl,($8042)      ; 18F4 2A 42 80
    ld     d,(ix+$01)      ; 18F7 DD 56 01
    ld     e,(ix+$00)      ; 18FA DD 5E 00
    or     a               ; 18FD B7
    sbc    hl,de           ; 18FE ED 52
    jp     z,$1922         ; 1900 CA 22 19
    bit    7,h             ; 1903 CB 7C
    jp     nz,$1922        ; 1905 C2 22 19
    ld     h,(ix+$01)      ; 1908 DD 66 01
    ld     l,(ix+$00)      ; 190B DD 6E 00
    ld     ($805c),hl      ; 190E 22 5C 80
    ld     h,(ix+$03)      ; 1911 DD 66 03
    ld     l,(ix+$02)      ; 1914 DD 6E 02
    ld     ($805a),hl      ; 1917 22 5A 80
    ld     de,$0004        ; 191A 11 04 00
    add    ix,de           ; 191D DD 19
    jp     $18f4           ; 191F C3 F4 18
    ld     hl,($8042)      ; 1922 2A 42 80
    ld     de,($805c)      ; 1925 ED 5B 5C 80
    or     a               ; 1929 B7
    sbc    hl,de           ; 192A ED 52
    jp     z,$194c         ; 192C CA 4C 19
    bit    7,h             ; 192F CB 7C
    jp     nz,$194c        ; 1931 C2 4C 19
    ld     hl,($805a)      ; 1934 2A 5A 80
    ld     de,$000a        ; 1937 11 0A 00
    add    hl,de           ; 193A 19
    ld     ($805a),hl      ; 193B 22 5A 80
    ld     hl,($805c)      ; 193E 2A 5C 80
    ld     de,($805a)      ; 1941 ED 5B 5A 80
    add    hl,de           ; 1945 19
    ld     ($805c),hl      ; 1946 22 5C 80
    jp     $1922           ; 1949 C3 22 19
    pop    de              ; 194C D1
    pop    ix              ; 194D DD E1
    pop    hl              ; 194F E1
    ret                    ; 1950 C9

; 1951h - ?

    push   ix              ; 1951 DD E5
    push   iy              ; 1953 FD E5
    push   hl              ; 1955 E5
    push   de              ; 1956 D5
    ld     ix,$1c6a        ; 1957 DD 21 6A 1C
    ld     a,($81a1)       ; 195B 3A A1 81
    add    a,a             ; 195E 87
    ld     de,$0000        ; 195F 11 00 00
    ld     e,a             ; 1962 5F
    add    ix,de           ; 1963 DD 19
    ld     l,(ix+$00)      ; 1965 DD 6E 00
    ld     h,(ix+$01)      ; 1968 DD 66 01
    push   hl              ; 196B E5
    pop    ix              ; 196C DD E1
    ld     iy,$1e43        ; 196E FD 21 43 1E
    ld     a,($8029)       ; 1972 3A 29 80
    cp     $19             ; 1975 FE 19
    jr     nz,$197f        ; 1977 20 06
    ld     iy,$1e4d        ; 1979 FD 21 4D 1E
    jr     $1987           ; 197D 18 08
    cp     $1a             ; 197F FE 1A
    jr     nz,$1987        ; 1981 20 04
    ld     iy,$1e57        ; 1983 FD 21 57 1E
    ld     a,(ix+$00)      ; 1987 DD 7E 00
    cp     $ff             ; 198A FE FF
    jp     z,$1ac7         ; 198C CA C7 1A
    ld     hl,$0000        ; 198F 21 00 00
    ld     de,$0000        ; 1992 11 00 00
    ld     l,(ix+$00)      ; 1995 DD 6E 00
    ld     b,(iy+$03)      ; 1998 FD 46 03
    ld     c,(iy+$02)      ; 199B FD 4E 02
    add    hl,bc           ; 199E 09
    ld     a,($802b)       ; 199F 3A 2B 80
    ld     e,a             ; 19A2 5F
    or     a               ; 19A3 B7
    sbc    hl,de           ; 19A4 ED 52
    jp     z,$1abf         ; 19A6 CA BF 1A
    jp     nc,$1abf        ; 19A9 D2 BF 1A
    call   $18c7           ; 19AC CD C7 18
    ld     ($805e),hl      ; 19AF 22 5E 80
    ld     hl,$0000        ; 19B2 21 00 00
    ld     l,(ix+$01)      ; 19B5 DD 6E 01
    ld     b,(iy+$05)      ; 19B8 FD 46 05
    ld     c,(iy+$04)      ; 19BB FD 4E 04
    add    hl,bc           ; 19BE 09
    or     a               ; 19BF B7
    sbc    hl,de           ; 19C0 ED 52
    jp     z,$1abf         ; 19C2 CA BF 1A
    jp     c,$1abf         ; 19C5 DA BF 1A
    ld     ($8060),hl      ; 19C8 22 60 80
    ld     hl,$0000        ; 19CB 21 00 00
    ld     l,(ix+$02)      ; 19CE DD 6E 02
    ld     b,(iy+$07)      ; 19D1 FD 46 07
    ld     c,(iy+$06)      ; 19D4 FD 4E 06
    add    hl,bc           ; 19D7 09
    ld     de,$0000        ; 19D8 11 00 00
    ld     a,($802c)       ; 19DB 3A 2C 80
    ld     e,a             ; 19DE 5F
    or     a               ; 19DF B7
    sbc    hl,de           ; 19E0 ED 52
    jp     z,$1abf         ; 19E2 CA BF 1A
    jp     nc,$1abf        ; 19E5 D2 BF 1A
    call   $18c7           ; 19E8 CD C7 18
    ld     ($8062),hl      ; 19EB 22 62 80
    ld     hl,$0000        ; 19EE 21 00 00
    ld     l,(ix+$03)      ; 19F1 DD 6E 03
    ld     b,(iy+$09)      ; 19F4 FD 46 09
    ld     c,(iy+$08)      ; 19F7 FD 4E 08
    add    hl,bc           ; 19FA 09
    or     a               ; 19FB B7
    sbc    hl,de           ; 19FC ED 52
    jp     z,$1abf         ; 19FE CA BF 1A
    jp     c,$1abf         ; 1A01 DA BF 1A
    ld     ($8064),hl      ; 1A04 22 64 80
    ld     hl,($805e)      ; 1A07 2A 5E 80
    ld     de,($8060)      ; 1A0A ED 5B 60 80
    ld     b,h             ; 1A0E 44
    ld     c,l             ; 1A0F 4D
    or     a               ; 1A10 B7
    sbc    hl,de           ; 1A11 ED 52
    jr     c,$1a1f         ; 1A13 38 0A
    ld     b,d             ; 1A15 42
    ld     c,e             ; 1A16 4B
    ld     hl,$0000        ; 1A17 21 00 00
    ld     ($805e),hl      ; 1A1A 22 5E 80
    jr     $1a25           ; 1A1D 18 06
    ld     hl,$0000        ; 1A1F 21 00 00
    ld     ($8060),hl      ; 1A22 22 60 80
    ld     hl,($8062)      ; 1A25 2A 62 80
    ld     de,($8064)      ; 1A28 ED 5B 64 80
    or     a               ; 1A2C B7
    sbc    hl,de           ; 1A2D ED 52
    jr     c,$1a39         ; 1A2F 38 08
    ld     hl,$0000        ; 1A31 21 00 00
    ld     ($8062),hl      ; 1A34 22 62 80
    jr     $1a43           ; 1A37 18 0A
    ld     de,($8062)      ; 1A39 ED 5B 62 80
    ld     hl,$0000        ; 1A3D 21 00 00
    ld     ($8064),hl      ; 1A40 22 64 80
    ld     h,b             ; 1A43 60
    ld     l,c             ; 1A44 69
    or     a               ; 1A45 B7
    sbc    hl,de           ; 1A46 ED 52
    jr     c,$1a55         ; 1A48 38 0B
    ld     hl,$0000        ; 1A4A 21 00 00
    ld     ($805e),hl      ; 1A4D 22 5E 80
    ld     ($8060),hl      ; 1A50 22 60 80
    jr     $1a5e           ; 1A53 18 09
    ld     hl,$0000        ; 1A55 21 00 00
    ld     ($8062),hl      ; 1A58 22 62 80
    ld     ($8064),hl      ; 1A5B 22 64 80
    ld     hl,($805e)      ; 1A5E 2A 5E 80
    ld     a,h             ; 1A61 7C
    or     l               ; 1A62 B5
    jr     z,$1a78         ; 1A63 28 13
    ld     hl,$0000        ; 1A65 21 00 00
    ld     l,(ix+$00)      ; 1A68 DD 6E 00
    ld     d,(iy+$03)      ; 1A6B FD 56 03
    ld     e,(iy+$02)      ; 1A6E FD 5E 02
    add    hl,de           ; 1A71 19
    ld     a,l             ; 1A72 7D
    ld     ($802b),a       ; 1A73 32 2B 80
    jr     $1ad6           ; 1A76 18 5E
    ld     hl,($8060)      ; 1A78 2A 60 80
    ld     a,h             ; 1A7B 7C
    or     l               ; 1A7C B5
    jr     z,$1a92         ; 1A7D 28 13
    ld     hl,$0000        ; 1A7F 21 00 00
    ld     l,(ix+$01)      ; 1A82 DD 6E 01
    ld     d,(iy+$05)      ; 1A85 FD 56 05
    ld     e,(iy+$04)      ; 1A88 FD 5E 04
    add    hl,de           ; 1A8B 19
    ld     a,l             ; 1A8C 7D
    ld     ($802b),a       ; 1A8D 32 2B 80
    jr     $1ad6           ; 1A90 18 44
    ld     hl,($8062)      ; 1A92 2A 62 80
    ld     a,h             ; 1A95 7C
    or     l               ; 1A96 B5
    jr     z,$1aac         ; 1A97 28 13
    ld     hl,$0000        ; 1A99 21 00 00
    ld     l,(ix+$02)      ; 1A9C DD 6E 02
    ld     d,(iy+$07)      ; 1A9F FD 56 07
    ld     e,(iy+$06)      ; 1AA2 FD 5E 06
    add    hl,de           ; 1AA5 19
    ld     a,l             ; 1AA6 7D
    ld     ($802c),a       ; 1AA7 32 2C 80
    jr     $1ad6           ; 1AAA 18 2A
    ld     hl,$0000        ; 1AAC 21 00 00
    ld     l,(ix+$03)      ; 1AAF DD 6E 03
    ld     d,(iy+$09)      ; 1AB2 FD 56 09
    ld     e,(iy+$08)      ; 1AB5 FD 5E 08
    add    hl,de           ; 1AB8 19
    ld     a,l             ; 1AB9 7D
    ld     ($802c),a       ; 1ABA 32 2C 80
    jr     $1ad6           ; 1ABD 18 17
    ld     de,$0004        ; 1ABF 11 04 00
    add    ix,de           ; 1AC2 DD 19
    jp     $1987           ; 1AC4 C3 87 19
    ld     a,(ix+$01)      ; 1AC7 DD 7E 01
    cp     $ff             ; 1ACA FE FF
    jp     z,$1ad6         ; 1ACC CA D6 1A
    ld     ix,$1e31        ; 1ACF DD 21 31 1E
    jp     $1987           ; 1AD3 C3 87 19
    pop    de              ; 1AD6 D1
    pop    hl              ; 1AD7 E1
    pop    iy              ; 1AD8 FD E1
    pop    ix              ; 1ADA DD E1
    ret                    ; 1ADC C9
    push   bc              ; 1ADD C5
    push   de              ; 1ADE D5
    ld     de,$0000        ; 1ADF 11 00 00
    ld     a,($8502)       ; 1AE2 3A 02 85
    add    a,$04           ; 1AE5 C6 04
    ld     b,a             ; 1AE7 47
    ld     a,($8503)       ; 1AE8 3A 03 85
    add    a,$00           ; 1AEB C6 00
    ld     c,a             ; 1AED 4F
    call   $00de           ; 1AEE CD DE 00
    ld     a,(hl)          ; 1AF1 7E
    call   $1baf           ; 1AF2 CD AF 1B
    cp     $00             ; 1AF5 FE 00
    jr     z,$1afb         ; 1AF7 28 02
    set    1,e             ; 1AF9 CB CB
    ld     a,($8502)       ; 1AFB 3A 02 85
    add    a,$0c           ; 1AFE C6 0C
    ld     b,a             ; 1B00 47
    ld     a,($8503)       ; 1B01 3A 03 85
    add    a,$00           ; 1B04 C6 00
    ld     c,a             ; 1B06 4F
    call   $00de           ; 1B07 CD DE 00
    ld     a,(hl)          ; 1B0A 7E
    call   $1baf           ; 1B0B CD AF 1B
    cp     $00             ; 1B0E FE 00
    jr     z,$1b14         ; 1B10 28 02
    set    1,e             ; 1B12 CB CB
    ld     a,($8502)       ; 1B14 3A 02 85
    add    a,$04           ; 1B17 C6 04
    ld     b,a             ; 1B19 47
    ld     a,($8503)       ; 1B1A 3A 03 85
    add    a,$0f           ; 1B1D C6 0F
    ld     c,a             ; 1B1F 4F
    call   $00de           ; 1B20 CD DE 00
    ld     a,(hl)          ; 1B23 7E
    call   $1baf           ; 1B24 CD AF 1B
    cp     $00             ; 1B27 FE 00
    jr     z,$1b2d         ; 1B29 28 02
    set    3,e             ; 1B2B CB DB
    ld     a,($8502)       ; 1B2D 3A 02 85
    add    a,$0c           ; 1B30 C6 0C
    ld     b,a             ; 1B32 47
    ld     a,($8503)       ; 1B33 3A 03 85
    add    a,$0f           ; 1B36 C6 0F
    ld     c,a             ; 1B38 4F
    call   $00de           ; 1B39 CD DE 00
    ld     a,(hl)          ; 1B3C 7E
    call   $1baf           ; 1B3D CD AF 1B
    cp     $00             ; 1B40 FE 00
    jr     z,$1b46         ; 1B42 28 02
    set    3,e             ; 1B44 CB DB
    ld     a,($8502)       ; 1B46 3A 02 85
    add    a,$02           ; 1B49 C6 02
    ld     b,a             ; 1B4B 47
    ld     a,($8503)       ; 1B4C 3A 03 85
    add    a,$04           ; 1B4F C6 04
    ld     c,a             ; 1B51 4F
    call   $00de           ; 1B52 CD DE 00
    ld     a,(hl)          ; 1B55 7E
    call   $1bd8           ; 1B56 CD D8 1B
    cp     $00             ; 1B59 FE 00
    jr     z,$1b5f         ; 1B5B 28 02
    set    0,e             ; 1B5D CB C3
    ld     a,($8502)       ; 1B5F 3A 02 85
    add    a,$02           ; 1B62 C6 02
    ld     b,a             ; 1B64 47
    ld     a,($8503)       ; 1B65 3A 03 85
    add    a,$0c           ; 1B68 C6 0C
    ld     c,a             ; 1B6A 4F
    call   $00de           ; 1B6B CD DE 00
    ld     a,(hl)          ; 1B6E 7E
    call   $1bd8           ; 1B6F CD D8 1B
    cp     $00             ; 1B72 FE 00
    jr     z,$1b78         ; 1B74 28 02
    set    0,e             ; 1B76 CB C3
    ld     a,($8502)       ; 1B78 3A 02 85
    add    a,$0d           ; 1B7B C6 0D
    ld     b,a             ; 1B7D 47
    ld     a,($8503)       ; 1B7E 3A 03 85
    add    a,$04           ; 1B81 C6 04
    ld     c,a             ; 1B83 4F
    call   $00de           ; 1B84 CD DE 00
    ld     a,(hl)          ; 1B87 7E
    call   $1bd8           ; 1B88 CD D8 1B
    cp     $00             ; 1B8B FE 00
    jr     z,$1b91         ; 1B8D 28 02
    set    0,e             ; 1B8F CB C3
    ld     a,($8502)       ; 1B91 3A 02 85
    add    a,$0d           ; 1B94 C6 0D
    ld     b,a             ; 1B96 47
    ld     a,($8503)       ; 1B97 3A 03 85
    add    a,$0c           ; 1B9A C6 0C
    ld     c,a             ; 1B9C 4F
    call   $00de           ; 1B9D CD DE 00
    ld     a,(hl)          ; 1BA0 7E
    call   $1bd8           ; 1BA1 CD D8 1B
    cp     $00             ; 1BA4 FE 00
    jr     z,$1baa         ; 1BA6 28 02
    set    0,e             ; 1BA8 CB C3
    ld     h,d             ; 1BAA 62
    ld     l,e             ; 1BAB 6B
    pop    de              ; 1BAC D1
    pop    bc              ; 1BAD C1
    ret                    ; 1BAE C9
    cp     $60             ; 1BAF FE 60
    jr     c,$1bd5         ; 1BB1 38 22
    cp     $80             ; 1BB3 FE 80
    jr     c,$1bbf         ; 1BB5 38 08
    cp     $a0             ; 1BB7 FE A0
    jr     c,$1bd5         ; 1BB9 38 1A
    cp     $c0             ; 1BBB FE C0
    jr     nc,$1bd5        ; 1BBD 30 16
    and    $0f             ; 1BBF E6 0F
    cp     $06             ; 1BC1 FE 06
    jr     c,$1bd1         ; 1BC3 38 0C
    cp     $08             ; 1BC5 FE 08
    jr     c,$1bd5         ; 1BC7 38 0C
    cp     $0c             ; 1BC9 FE 0C
    jr     c,$1bd1         ; 1BCB 38 04
    cp     $0e             ; 1BCD FE 0E
    jr     c,$1bd5         ; 1BCF 38 04
    ld     a,$01           ; 1BD1 3E 01
    jr     $1bd7           ; 1BD3 18 02
    ld     a,$00           ; 1BD5 3E 00
    ret                    ; 1BD7 C9
    cp     $60             ; 1BD8 FE 60
    jr     c,$1bfa         ; 1BDA 38 1E
    cp     $80             ; 1BDC FE 80
    jr     c,$1be8         ; 1BDE 38 08
    cp     $a0             ; 1BE0 FE A0
    jr     c,$1bfa         ; 1BE2 38 16
    cp     $c0             ; 1BE4 FE C0
    jr     nc,$1bfa        ; 1BE6 30 12
    and    $0f             ; 1BE8 E6 0F
    cp     $02             ; 1BEA FE 02
    jr     c,$1bf6         ; 1BEC 38 08
    cp     $04             ; 1BEE FE 04
    jr     c,$1bfa         ; 1BF0 38 08
    cp     $0e             ; 1BF2 FE 0E
    jr     nc,$1bfa        ; 1BF4 30 04
    ld     a,$01           ; 1BF6 3E 01
    jr     $1bfc           ; 1BF8 18 02
    ld     a,$00           ; 1BFA 3E 00
    ret                    ; 1BFC C9
    push   bc              ; 1BFD C5
    cp     $13             ; 1BFE FE 13
    jr     z,$1c08         ; 1C00 28 06
    cp     $93             ; 1C02 FE 93
    jr     z,$1c08         ; 1C04 28 02
    jr     $1c10           ; 1C06 18 08
    ld     b,a             ; 1C08 47
    ld     a,($8089)       ; 1C09 3A 89 80
    cp     b               ; 1C0C B8
    jr     z,$1c16         ; 1C0D 28 07
    ld     a,b             ; 1C0F 78
    ld     ($8089),a       ; 1C10 32 89 80
    call   $00fc           ; 1C13 CD FC 00
    pop    bc              ; 1C16 C1
    ret                    ; 1C17 C9

; 1C18h - data ?

    sub    (hl)            ; 1C18 96
    nop                    ; 1C19 00
    ld     ($c200),a       ; 1C1A 32 00 C2
    ld     bc,$005a        ; 1C1D 01 5A 00
    adc    a,(hl)          ; 1C20 8E
    inc    bc              ; 1C21 03
    add    a,d             ; 1C22 82
    nop                    ; 1C23 00
    jp     m,$aa05         ; 1C24 FA 05 AA
    nop                    ; 1C27 00
    ld     b,$09           ; 1C28 06 09
    jp     nc,$b200        ; 1C2A D2 00 B2
    inc    c               ; 1C2D 0C
    jp     m,$fe00         ; 1C2E FA 00 FE
    djnz   $1c55           ; 1C31 10 22
    ld     bc,$15ea        ; 1C33 01 EA 15
    ld     c,d             ; 1C36 4A
    ld     bc,$1b76        ; 1C37 01 76 1B
    ld     (hl),d          ; 1C3A 72
    ld     bc,$21a2        ; 1C3B 01 A2 21
    sbc    a,d             ; 1C3E 9A
    ld     bc,$286e        ; 1C3F 01 6E 28
    jp     nz,$da01        ; 1C42 C2 01 DA
    cpl                    ; 1C45 2F
    jp     pe,$e601        ; 1C46 EA 01 E6
    scf                    ; 1C49 37
    ld     (de),a          ; 1C4A 12
    ld     (bc),a          ; 1C4B 02
    sub    d               ; 1C4C 92
    ld     b,b             ; 1C4D 40
    ld     a,($de02)       ; 1C4E 3A 02 DE
    ld     c,c             ; 1C51 49
    ld     h,d             ; 1C52 62
    ld     (bc),a          ; 1C53 02
    jp     z,$8a53         ; 1C54 CA 53 8A
    ld     (bc),a          ; 1C57 02
    ld     d,(hl)          ; 1C58 56
    ld     e,(hl)          ; 1C59 5E
    or     d               ; 1C5A B2
    ld     (bc),a          ; 1C5B 02
    add    a,d             ; 1C5C 82
    ld     l,c             ; 1C5D 69
    jp     c,$4e02         ; 1C5E DA 02 4E
    ld     (hl),l          ; 1C61 75
    ld     (bc),a          ; 1C62 02
    inc    bc              ; 1C63 03
    cp     d               ; 1C64 BA
    add    a,c             ; 1C65 81
    ld     hl,($ff03)      ; 1C66 2A 03 FF
    rst    38h             ; 1C69 FF
    adc    a,h             ; 1C6A 8C
    inc    e               ; 1C6B 1C
    and    c               ; 1C6C A1
    inc    e               ; 1C6D 1C
    or     d               ; 1C6E B2
    inc    e               ; 1C6F 1C
    rst    00h             ; 1C70 C7
    inc    e               ; 1C71 1C
    jp     (hl)            ; 1C72 E9
    inc    e               ; 1C73 1C
    jp     pe,$131c        ; 1C74 EA 1C 13
    dec    e               ; 1C77 1D
    inc    l               ; 1C78 2C
    dec    e               ; 1C79 1D
    dec    a               ; 1C7A 3D
    dec    e               ; 1C7B 1D
    ld     d,d             ; 1C7C 52
    dec    e               ; 1C7D 1D
    ld     h,e             ; 1C7E 63
    dec    e               ; 1C7F 1D
    sub    h               ; 1C80 94
    dec    e               ; 1C81 1D
    xor    l               ; 1C82 AD
    dec    e               ; 1C83 1D
    add    a,$1d           ; 1C84 C6 1D
    rst    20h             ; 1C86 E7
    dec    e               ; 1C87 1D
    nop                    ; 1C88 00
    ld     e,$ff           ; 1C89 1E FF
    rst    38h             ; 1C8B FF
    ld     a,e             ; 1C8C 7B
    cp     l               ; 1C8D BD
    ld     sp,$3346        ; 1C8E 31 46 33
    ld     e,l             ; 1C91 5D
    ld     c,c             ; 1C92 49
    ld     e,(hl)          ; 1C93 5E
    ld     h,e             ; 1C94 63
    sbc    a,l             ; 1C95 9D
    sub    c               ; 1C96 91
    and    (hl)            ; 1C97 A6
    dec    de              ; 1C98 1B
    ld     b,l             ; 1C99 45
    xor    c               ; 1C9A A9
    cp     (hl)            ; 1C9B BE
    add    a,e             ; 1C9C 83
    push   de              ; 1C9D D5
    pop    bc              ; 1C9E C1
    sub    $ff             ; 1C9F D6 FF
    ld     d,e             ; 1CA1 53
    sbc    a,l             ; 1CA2 9D
    ld     sp,$3346        ; 1CA3 31 46 33
    ld     b,l             ; 1CA6 45
    ld     d,c             ; 1CA7 51
    sbc    a,(hl)          ; 1CA8 9E
    xor    e               ; 1CA9 AB
    cp     l               ; 1CAA BD
    ld     d,c             ; 1CAB 51
    sbc    a,(hl)          ; 1CAC 9E
    ld     d,e             ; 1CAD 53
    sbc    a,l             ; 1CAE 9D
    xor    c               ; 1CAF A9
    cp     (hl)            ; 1CB0 BE
    rst    38h             ; 1CB1 FF
    ld     l,e             ; 1CB2 6B
    sbc    a,l             ; 1CB3 9D
    ld     sp,$0b46        ; 1CB4 31 46 0B
    ld     d,l             ; 1CB7 55
    ld     c,c             ; 1CB8 49
    ld     e,(hl)          ; 1CB9 5E
    sbc    a,e             ; 1CBA 9B
    call   $7661           ; 1CBB CD 61 76
    sub    e               ; 1CBE 93
    and    l               ; 1CBF A5
    ld     h,c             ; 1CC0 61
    and    (hl)            ; 1CC1 A6
    inc    sp              ; 1CC2 33
    and    l               ; 1CC3 A5
    sub    c               ; 1CC4 91
    and    (hl)            ; 1CC5 A6
    rst    38h             ; 1CC6 FF
    ld     c,e             ; 1CC7 4B
    ld     e,l             ; 1CC8 5D
    ld     hl,$935e        ; 1CC9 21 5E 93
    and    l               ; 1CCC A5
    ld     hl,$235e        ; 1CCD 21 5E 23
    ld     e,l             ; 1CD0 5D
    ld     c,c             ; 1CD1 49
    ld     e,(hl)          ; 1CD2 5E
    sub    e               ; 1CD3 93
    call   $5e49           ; 1CD4 CD 49 5E
    inc    hl              ; 1CD7 23
    ld     d,l             ; 1CD8 55
    sub    c               ; 1CD9 91
    and    (hl)            ; 1CDA A6
    sbc    a,e             ; 1CDB 9B
    call   $a691           ; 1CDC CD 91 A6
    ld     c,e             ; 1CDF 4B
    ld     e,l             ; 1CE0 5D
    sub    c               ; 1CE1 91
    adc    a,$93           ; 1CE2 CE 93
    and    l               ; 1CE4 A5
    sub    c               ; 1CE5 91
    adc    a,$ff           ; 1CE6 CE FF
    nop                    ; 1CE8 00
    rst    38h             ; 1CE9 FF
    dec    de              ; 1CEA 1B
    ld     b,l             ; 1CEB 45
    ld     sp,$6346        ; 1CEC 31 46 63
    adc    a,l             ; 1CEF 8D
    ld     sp,$ab46        ; 1CF0 31 46 AB
    push   de              ; 1CF3 D5
    ld     sp,$3b46        ; 1CF4 31 46 3B
    ld     l,l             ; 1CF7 6D
    ld     h,c             ; 1CF8 61
    halt                   ; 1CF9 76
    add    a,e             ; 1CFA 83
    or     l               ; 1CFB B5
    ld     h,c             ; 1CFC 61
    halt                   ; 1CFD 76
    dec    de              ; 1CFE 1B
    ld     b,l             ; 1CFF 45
    sub    c               ; 1D00 91
    and    (hl)            ; 1D01 A6
    ld     h,e             ; 1D02 63
    adc    a,l             ; 1D03 8D
    sub    c               ; 1D04 91
    and    (hl)            ; 1D05 A6
    xor    e               ; 1D06 AB
    push   de              ; 1D07 D5
    sub    c               ; 1D08 91
    and    (hl)            ; 1D09 A6
    dec    sp              ; 1D0A 3B
    ld     l,l             ; 1D0B 6D
    pop    bc              ; 1D0C C1
    sub    $83             ; 1D0D D6 83
    or     l               ; 1D0F B5
    pop    bc              ; 1D10 C1
    sub    $ff             ; 1D11 D6 FF
    ld     h,e             ; 1D13 63
    adc    a,l             ; 1D14 8D
    ld     sp,$3346        ; 1D15 31 46 33
    ld     l,l             ; 1D18 6D
    ld     h,c             ; 1D19 61
    halt                   ; 1D1A 76
    add    a,e             ; 1D1B 83
    cp     l               ; 1D1C BD
    ld     h,c             ; 1D1D 61
    halt                   ; 1D1E 76
    inc    sp              ; 1D1F 33
    ld     l,l             ; 1D20 6D
    ld     a,c             ; 1D21 79
    adc    a,(hl)          ; 1D22 8E
    add    a,e             ; 1D23 83
    cp     l               ; 1D24 BD
    ld     a,c             ; 1D25 79
    adc    a,(hl)          ; 1D26 8E
    ld     h,e             ; 1D27 63
    adc    a,l             ; 1D28 8D
    xor    c               ; 1D29 A9
    cp     (hl)            ; 1D2A BE
    rst    38h             ; 1D2B FF
    inc    hl              ; 1D2C 23
    ld     d,l             ; 1D2D 55
    ld     sp,$9b46        ; 1D2E 31 46 9B
    call   $4631           ; 1D31 CD 31 46
    inc    hl              ; 1D34 23
    ld     d,l             ; 1D35 55
    xor    c               ; 1D36 A9
    cp     (hl)            ; 1D37 BE
    sbc    a,e             ; 1D38 9B
    call   $bea9           ; 1D39 CD A9 BE
    rst    38h             ; 1D3C FF
    dec    sp              ; 1D3D 3B
    ld     l,l             ; 1D3E 6D
    ld     sp,$8346        ; 1D3F 31 46 83
    or     l               ; 1D42 B5
    ld     sp,$6346        ; 1D43 31 46 63
    adc    a,l             ; 1D46 8D
    sub    c               ; 1D47 91
    and    (hl)            ; 1D48 A6
    dec    de              ; 1D49 1B
    ld     b,l             ; 1D4A 45
    xor    c               ; 1D4B A9
    cp     (hl)            ; 1D4C BE
    xor    e               ; 1D4D AB
    push   de              ; 1D4E D5
    xor    c               ; 1D4F A9
    cp     (hl)            ; 1D50 BE
    rst    38h             ; 1D51 FF
    dec    sp              ; 1D52 3B
    or     l               ; 1D53 B5
    ld     sp,$3346        ; 1D54 31 46 33
    ld     b,l             ; 1D57 45
    ld     sp,$ab9e        ; 1D58 31 9E AB
    cp     l               ; 1D5B BD
    ld     sp,$639e        ; 1D5C 31 9E 63
    adc    a,l             ; 1D5F 8D
    sub    c               ; 1D60 91
    and    (hl)            ; 1D61 A6
    rst    38h             ; 1D62 FF
    ld     l,e             ; 1D63 6B
    add    a,l             ; 1D64 85
    add    hl,de           ; 1D65 19
    ld     l,$63           ; 1D66 2E 63
    ld     (hl),l          ; 1D68 75
    add    hl,de           ; 1D69 19
    ld     b,(hl)          ; 1D6A 46
    ld     h,e             ; 1D6B 63
    adc    a,l             ; 1D6C 8D
    ld     sp,$4b46        ; 1D6D 31 46 4B
    adc    a,l             ; 1D70 8D
    ld     c,c             ; 1D71 49
    ld     e,(hl)          ; 1D72 5E
    ld     h,e             ; 1D73 63
    ld     (hl),l          ; 1D74 75
    ld     c,c             ; 1D75 49
    halt                   ; 1D76 76
    ld     h,e             ; 1D77 63
    adc    a,l             ; 1D78 8D
    ld     h,c             ; 1D79 61
    halt                   ; 1D7A 76
    ld     l,e             ; 1D7B 6B
    cp     l               ; 1D7C BD
    ld     a,c             ; 1D7D 79
    adc    a,(hl)          ; 1D7E 8E
    ld     h,e             ; 1D7F 63
    ld     (hl),l          ; 1D80 75
    ld     a,c             ; 1D81 79
    and    (hl)            ; 1D82 A6
    ld     h,e             ; 1D83 63
    adc    a,l             ; 1D84 8D
    sub    c               ; 1D85 91
    and    (hl)            ; 1D86 A6
    dec    de              ; 1D87 1B
    adc    a,l             ; 1D88 8D
    xor    c               ; 1D89 A9
    cp     (hl)            ; 1D8A BE
    ld     h,e             ; 1D8B 63
    ld     (hl),l          ; 1D8C 75
    or     c               ; 1D8D B1
    sub    $63             ; 1D8E D6 63
    adc    a,l             ; 1D90 8D
    pop    bc              ; 1D91 C1
    sub    $ff             ; 1D92 D6 FF
    dec    bc              ; 1D94 0B
    dec    a               ; 1D95 3D
    ld     c,c             ; 1D96 49
    ld     e,(hl)          ; 1D97 5E
    or     e               ; 1D98 B3
    push   hl              ; 1D99 E5
    ld     c,c             ; 1D9A 49
    ld     e,(hl)          ; 1D9B 5E
    inc    hl              ; 1D9C 23
    ld     d,l             ; 1D9D 55
    ld     a,c             ; 1D9E 79
    adc    a,(hl)          ; 1D9F 8E
    sbc    a,e             ; 1DA0 9B
    call   $8e79           ; 1DA1 CD 79 8E
    dec    sp              ; 1DA4 3B
    ld     l,l             ; 1DA5 6D
    xor    c               ; 1DA6 A9
    cp     (hl)            ; 1DA7 BE
    add    a,e             ; 1DA8 83
    or     l               ; 1DA9 B5
    xor    c               ; 1DAA A9
    cp     (hl)            ; 1DAB BE
    rst    38h             ; 1DAC FF
    dec    sp              ; 1DAD 3B
    add    a,l             ; 1DAE 85
    ld     sp,$9346        ; 1DAF 31 46 93
    or     l               ; 1DB2 B5
    ld     sp,$3346        ; 1DB3 31 46 33
    ld     b,l             ; 1DB6 45
    ld     sp,$abbe        ; 1DB7 31 BE AB
    cp     l               ; 1DBA BD
    ld     sp,$33be        ; 1DBB 31 BE 33
    ld     e,l             ; 1DBE 5D
    xor    c               ; 1DBF A9
    cp     (hl)            ; 1DC0 BE
    ld     l,e             ; 1DC1 6B
    cp     l               ; 1DC2 BD
    xor    c               ; 1DC3 A9
    cp     (hl)            ; 1DC4 BE
    rst    38h             ; 1DC5 FF
    inc    sp              ; 1DC6 33
    add    a,l             ; 1DC7 85
    add    hl,de           ; 1DC8 19
    ld     l,$7b           ; 1DC9 2E 7B
    adc    a,l             ; 1DCB 8D
    add    hl,de           ; 1DCC 19
    ld     e,(hl)          ; 1DCD 5E
    inc    hl              ; 1DCE 23
    ld     e,l             ; 1DCF 5D
    ld     h,c             ; 1DD0 61
    halt                   ; 1DD1 76
    dec    de              ; 1DD2 1B
    dec    l               ; 1DD3 2D
    ld     h,c             ; 1DD4 61
    cp     (hl)            ; 1DD5 BE
    jp     $31d5           ; 1DD6 C3 D5 31
    adc    a,(hl)          ; 1DD9 8E
    sub    e               ; 1DDA 93
    push   de              ; 1DDB D5
    ld     a,c             ; 1DDC 79
    adc    a,(hl)          ; 1DDD 8E
    ld     h,e             ; 1DDE 63
    ld     (hl),l          ; 1DDF 75
    sub    c               ; 1DE0 91
    sub    $63             ; 1DE1 D6 63
    cp     l               ; 1DE3 BD
    pop    bc              ; 1DE4 C1
    sub    $ff             ; 1DE5 D6 FF
    or     e               ; 1DE7 B3
    push   hl              ; 1DE8 E5
    ld     sp,$0b46        ; 1DE9 31 46 0B
    dec    a               ; 1DEC 3D
    ld     c,c             ; 1DED 49
    ld     e,(hl)          ; 1DEE 5E
    or     e               ; 1DEF B3
    push   hl              ; 1DF0 E5
    ld     h,c             ; 1DF1 61
    halt                   ; 1DF2 76
    dec    bc              ; 1DF3 0B
    dec    a               ; 1DF4 3D
    ld     a,c             ; 1DF5 79
    adc    a,(hl)          ; 1DF6 8E
    or     e               ; 1DF7 B3
    push   hl              ; 1DF8 E5
    sub    c               ; 1DF9 91
    and    (hl)            ; 1DFA A6
    dec    bc              ; 1DFB 0B
    dec    a               ; 1DFC 3D
    xor    c               ; 1DFD A9
    cp     (hl)            ; 1DFE BE
    rst    38h             ; 1DFF FF
    dec    sp              ; 1E00 3B
    ld     l,l             ; 1E01 6D
    add    hl,de           ; 1E02 19
    ld     l,$83           ; 1E03 2E 83
    or     l               ; 1E05 B5
    add    hl,de           ; 1E06 19
    ld     l,$1b           ; 1E07 2E 1B
    dec    l               ; 1E09 2D
    add    hl,sp           ; 1E0A 39
    ld     l,(hl)          ; 1E0B 6E
    jp     $39d5           ; 1E0C C3 D5 39
    ld     l,(hl)          ; 1E0F 6E
    dec    de              ; 1E10 1B
    dec    l               ; 1E11 2D
    add    a,c             ; 1E12 81
    or     (hl)            ; 1E13 B6
    jp     $81d5           ; 1E14 C3 D5 81
    or     (hl)            ; 1E17 B6
    ld     h,e             ; 1E18 63
    adc    a,l             ; 1E19 8D
    ld     c,c             ; 1E1A 49
    ld     e,(hl)          ; 1E1B 5E
    ld     c,e             ; 1E1C 4B
    ld     e,l             ; 1E1D 5D
    ld     h,c             ; 1E1E 61
    adc    a,(hl)          ; 1E1F 8E
    sub    e               ; 1E20 93
    and    l               ; 1E21 A5
    ld     h,c             ; 1E22 61
    adc    a,(hl)          ; 1E23 8E
    ld     h,e             ; 1E24 63
    adc    a,l             ; 1E25 8D
    sub    c               ; 1E26 91
    and    (hl)            ; 1E27 A6
    dec    sp              ; 1E28 3B
    ld     l,l             ; 1E29 6D
    pop    bc              ; 1E2A C1
    sub    $83             ; 1E2B D6 83
    or     l               ; 1E2D B5
    pop    bc              ; 1E2E C1
    sub    $ff             ; 1E2F D6 FF
    inc    b               ; 1E31 04
    db     $fd             ; 1E32 FD
    inc    b               ; 1E33 04
    ld     d,$04           ; 1E34 16 04
    dec    d               ; 1E36 15
    inc    b               ; 1E37 04
    cp     $db             ; 1E38 FE DB
    db     $fd             ; 1E3A FD
    inc    b               ; 1E3B 04
    cp     $04             ; 1E3C FE 04
    db     $fd             ; 1E3E FD
    exx                    ; 1E3F D9
    cp     $ff             ; 1E40 FE FF
    rst    38h             ; 1E42 FF
    jr     $1e45           ; 1E43 18 00
    nop                    ; 1E45 00
    nop                    ; 1E46 00
    nop                    ; 1E47 00
    nop                    ; 1E48 00
    nop                    ; 1E49 00
    nop                    ; 1E4A 00
    nop                    ; 1E4B 00
    nop                    ; 1E4C 00
    add    hl,de           ; 1E4D 19
    nop                    ; 1E4E 00
    rst    38h             ; 1E4F FF
    rst    38h             ; 1E50 FF
    ld     bc,$0000        ; 1E51 01 00 00
    nop                    ; 1E54 00
    rst    38h             ; 1E55 FF
    rst    38h             ; 1E56 FF
    ld     a,(de)          ; 1E57 1A
    nop                    ; 1E58 00
    rst    38h             ; 1E59 FF
    rst    38h             ; 1E5A FF
    ld     bc,$0000        ; 1E5B 01 00 00
    nop                    ; 1E5E 00
    rst    38h             ; 1E5F FF
    rst    38h             ; 1E60 FF
    rst    38h             ; 1E61 FF
    rst    38h             ; 1E62 FF
    ld     h,a             ; 1E63 67
    ld     e,$6a           ; 1E64 1E 6A
    ld     e,$18           ; 1E66 1E 18
    nop                    ; 1E68 00
    rst    38h             ; 1E69 FF
    ld     a,(de)          ; 1E6A 1A
    nop                    ; 1E6B 00
    jr     nz,$1e72        ; 1E6C 20 04
    rst    38h             ; 1E6E FF
    rst    38h             ; 1E6F FF
    nop                    ; 1E70 00
    add    hl,de           ; 1E71 19
    nop                    ; 1E72 00
    jr     z,$1e79         ; 1E73 28 04
    rst    38h             ; 1E75 FF
    rst    38h             ; 1E76 FF
    nop                    ; 1E77 00
    jr     $1e7a           ; 1E78 18 00
    inc    b               ; 1E7A 04
    inc    b               ; 1E7B 04
    rst    38h             ; 1E7C FF
    rst    38h             ; 1E7D FF
    nop                    ; 1E7E 00
    jr     $1e81           ; 1E7F 18 00
    rst    38h             ; 1E81 FF
    rst    38h             ; 1E82 FF
    rst    38h             ; 1E83 FF
    rst    38h             ; 1E84 FF
    rst    38h             ; 1E85 FF
    djnz   $1e88           ; 1E86 10 00
    ex     af,af'          ; 1E88 08
    nop                    ; 1E89 00
    ret    p               ; 1E8A F0
    rst    38h             ; 1E8B FF
    ret    m               ; 1E8C F8
    rst    38h             ; 1E8D FF
    add    a,b             ; 1E8E 80
    ld     bc,$ffff        ; 1E8F 01 FF FF
    ex     af,af'          ; 1E92 08
    nop                    ; 1E93 00
    djnz   $1e96           ; 1E94 10 00
    jr     nz,$1e98        ; 1E96 20 00
    jr     nc,$1e9a        ; 1E98 30 00
    ld     bc,$ff00        ; 1E9A 01 00 FF
    rst    38h             ; 1E9D FF
    ld     (bc),a          ; 1E9E 02
    nop                    ; 1E9F 00
    cp     $ff             ; 1EA0 FE FF
    add    a,b             ; 1EA2 80
    ld     bc,$0000        ; 1EA3 01 00 00
    add    a,b             ; 1EA6 80
    cp     $00             ; 1EA7 FE 00
    nop                    ; 1EA9 00
    exx                    ; 1EAA D9
    nop                    ; 1EAB 00
    ret    c               ; 1EAC D8
    nop                    ; 1EAD 00
    rst    10h             ; 1EAE D7
    nop                    ; 1EAF 00
    sub    $00             ; 1EB0 D6 00
    inc    sp              ; 1EB2 33
    nop                    ; 1EB3 00
    inc    (hl)            ; 1EB4 34
    nop                    ; 1EB5 00
    dec    (hl)            ; 1EB6 35
    nop                    ; 1EB7 00
    push   de              ; 1EB8 D5
    rst    38h             ; 1EB9 FF
    ret    nz              ; 1EBA C0
    rst    38h             ; 1EBB FF
    nop                    ; 1EBC 00
    rst    38h             ; 1EBD FF
    sub    b               ; 1EBE 90

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 1EBF
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 1ECF
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 1EDF
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 1EEF
    hex    00                                              ; 1EFF

    call   $029f           ; 1F00 CD 9F 02
    ld     a,($b003)       ; 1F03 3A 03 B0
    jp     $01cb           ; 1F06 C3 CB 01
    nop                    ; 1F09 00
    nop                    ; 1F0A 00
    nop                    ; 1F0B 00
    nop                    ; 1F0C 00
    nop                    ; 1F0D 00
    nop                    ; 1F0E 00
    nop                    ; 1F0F 00
    ld     a,($b003)       ; 1F10 3A 03 B0
    ld     a,($b000)       ; 1F13 3A 00 B0
    jp     $0427           ; 1F16 C3 27 04
    nop                    ; 1F19 00
    nop                    ; 1F1A 00
    nop                    ; 1F1B 00
    nop                    ; 1F1C 00
    nop                    ; 1F1D 00
    nop                    ; 1F1E 00
    nop                    ; 1F1F 00
    ld     a,($b003)       ; 1F20 3A 03 B0
    ld     a,($b000)       ; 1F23 3A 00 B0
    jp     $045c           ; 1F26 C3 5C 04
    nop                    ; 1F29 00
    nop                    ; 1F2A 00
    nop                    ; 1F2B 00
    nop                    ; 1F2C 00
    nop                    ; 1F2D 00
    nop                    ; 1F2E 00
    nop                    ; 1F2F 00
    ld     a,($b003)       ; 1F30 3A 03 B0
    ld     a,($b000)       ; 1F33 3A 00 B0
    jp     $04a4           ; 1F36 C3 A4 04
    nop                    ; 1F39 00
    nop                    ; 1F3A 00
    nop                    ; 1F3B 00
    nop                    ; 1F3C 00
    nop                    ; 1F3D 00
    nop                    ; 1F3E 00
    nop                    ; 1F3F 00
    ld     a,($b003)       ; 1F40 3A 03 B0
    ld     a,($b000)       ; 1F43 3A 00 B0
    jp     $051f           ; 1F46 C3 1F 05
    nop                    ; 1F49 00
    nop                    ; 1F4A 00
    nop                    ; 1F4B 00
    nop                    ; 1F4C 00
    nop                    ; 1F4D 00
    nop                    ; 1F4E 00
    nop                    ; 1F4F 00
    ld     a,($b003)       ; 1F50 3A 03 B0
    ld     a,($b000)       ; 1F53 3A 00 B0
    jp     $054c           ; 1F56 C3 4C 05
    nop                    ; 1F59 00
    nop                    ; 1F5A 00
    nop                    ; 1F5B 00
    nop                    ; 1F5C 00
    nop                    ; 1F5D 00
    nop                    ; 1F5E 00
    nop                    ; 1F5F 00
    ld     a,($b003)       ; 1F60 3A 03 B0
    ld     a,($b000)       ; 1F63 3A 00 B0
    jp     $0560           ; 1F66 C3 60 05
    nop                    ; 1F69 00
    nop                    ; 1F6A 00
    nop                    ; 1F6B 00
    nop                    ; 1F6C 00
    nop                    ; 1F6D 00
    nop                    ; 1F6E 00
    nop                    ; 1F6F 00
    nop                    ; 1F70 00
    nop                    ; 1F71 00
    nop                    ; 1F72 00
    nop                    ; 1F73 00
    nop                    ; 1F74 00
    nop                    ; 1F75 00
    nop                    ; 1F76 00
    nop                    ; 1F77 00
    nop                    ; 1F78 00
    nop                    ; 1F79 00
    nop                    ; 1F7A 00
    nop                    ; 1F7B 00
    nop                    ; 1F7C 00
    nop                    ; 1F7D 00
    nop                    ; 1F7E 00
    nop                    ; 1F7F 00
    ld     ($b003),a       ; 1F80 32 03 B0
    ld     (ix+$00),$30    ; 1F83 DD 36 00 30
    jp     $05f8           ; 1F87 C3 F8 05
    nop                    ; 1F8A 00
    nop                    ; 1F8B 00
    nop                    ; 1F8C 00
    nop                    ; 1F8D 00
    nop                    ; 1F8E 00
    nop                    ; 1F8F 00
    ld     a,($b003)       ; 1F90 3A 03 B0
    ld     a,($b000)       ; 1F93 3A 00 B0
    jp     $0772           ; 1F96 C3 72 07
    nop                    ; 1F99 00
    nop                    ; 1F9A 00
    nop                    ; 1F9B 00
    nop                    ; 1F9C 00
    nop                    ; 1F9D 00
    nop                    ; 1F9E 00
    nop                    ; 1F9F 00
    ld     a,($b003)       ; 1FA0 3A 03 B0
    ld     a,($b000)       ; 1FA3 3A 00 B0
    jp     $078b           ; 1FA6 C3 8B 07
    nop                    ; 1FA9 00
    nop                    ; 1FAA 00
    nop                    ; 1FAB 00
    nop                    ; 1FAC 00
    nop                    ; 1FAD 00
    nop                    ; 1FAE 00
    nop                    ; 1FAF 00
    ld     a,($b003)       ; 1FB0 3A 03 B0
    ld     a,($b000)       ; 1FB3 3A 00 B0
    jp     $0798           ; 1FB6 C3 98 07
    nop                    ; 1FB9 00
    nop                    ; 1FBA 00
    nop                    ; 1FBB 00
    nop                    ; 1FBC 00
    nop                    ; 1FBD 00
    nop                    ; 1FBE 00
    nop                    ; 1FBF 00
    ld     a,($b003)       ; 1FC0 3A 03 B0
    ld     de,$0008        ; 1FC3 11 08 00
    jp     $04fc           ; 1FC6 C3 FC 04
    nop                    ; 1FC9 00
    nop                    ; 1FCA 00
    nop                    ; 1FCB 00
    nop                    ; 1FCC 00
    nop                    ; 1FCD 00
    nop                    ; 1FCE 00
    nop                    ; 1FCF 00
    nop                    ; 1FD0 00
    nop                    ; 1FD1 00
    nop                    ; 1FD2 00
    nop                    ; 1FD3 00
    nop                    ; 1FD4 00
    nop                    ; 1FD5 00
    nop                    ; 1FD6 00
    nop                    ; 1FD7 00
    nop                    ; 1FD8 00
    nop                    ; 1FD9 00
    nop                    ; 1FDA 00
    nop                    ; 1FDB 00
    nop                    ; 1FDC 00
    nop                    ; 1FDD 00
    nop                    ; 1FDE 00
    nop                    ; 1FDF 00
    call   $00d2           ; 1FE0 CD D2 00
    ld     a,($b003)       ; 1FE3 3A 03 B0
    jp     $04f9           ; 1FE6 C3 F9 04
    nop                    ; 1FE9 00
    nop                    ; 1FEA 00
    nop                    ; 1FEB 00
    nop                    ; 1FEC 00
    nop                    ; 1FED 00
    nop                    ; 1FEE 00
    nop                    ; 1FEF 00
    jp     $18c7           ; 1FF0 C3 C7 18
    jp     $18d9           ; 1FF3 C3 D9 18
    nop                    ; 1FF6 00
    ld     a,($b003)       ; 1FF7 3A 03 B0
    call   $0602           ; 1FFA CD 02 06
    jp     $05ab           ; 1FFD C3 AB 05
    call   $2205           ; 2000 CD 05 22
    ld     a,($808d)       ; 2003 3A 8D 80
    cp     $00             ; 2006 FE 00
    jr     nz,$2014        ; 2008 20 0A
    ld     a,($8028)       ; 200A 3A 28 80
    bit    2,a             ; 200D CB 57
    jr     nz,$2014        ; 200F 20 03
    call   $20c0           ; 2011 CD C0 20
    ld     a,($8453)       ; 2014 3A 53 84
    and    $03             ; 2017 E6 03
    cp     $03             ; 2019 FE 03
    jr     z,$2034         ; 201B 28 17
    ld     a,($81a7)       ; 201D 3A A7 81
    or     a               ; 2020 B7
    cp     $14             ; 2021 FE 14
    jr     c,$203a         ; 2023 38 15
    ld     a,$00           ; 2025 3E 00
    ld     ($81a7),a       ; 2027 32 A7 81
    ld     a,$03           ; 202A 3E 03
    ld     ($8453),a       ; 202C 32 53 84
    ld     a,$1b           ; 202F 3E 1B
    call   $00fc           ; 2031 CD FC 00
    call   $234b           ; 2034 CD 4B 23
    call   $00c9           ; 2037 CD C9 00
    ld     a,($8900)       ; 203A 3A 00 89
    bit    0,a             ; 203D CB 47
    jr     z,$2047         ; 203F 28 06
    call   $2533           ; 2041 CD 33 25
    call   $00c9           ; 2044 CD C9 00
    ld     a,($8460)       ; 2047 3A 60 84
    cp     $00             ; 204A FE 00
    jr     z,$2065         ; 204C 28 17
    dec    a               ; 204E 3D
    ld     ($8460),a       ; 204F 32 60 84
    cp     $00             ; 2052 FE 00
    jr     nz,$2065        ; 2054 20 0F
    ld     hl,$0000        ; 2056 21 00 00
    ld     ($8508),hl      ; 2059 22 08 85
    ld     ($850a),hl      ; 205C 22 0A 85
    ld     ($850c),hl      ; 205F 22 0C 85
    ld     ($850e),hl      ; 2062 22 0E 85
    ld     a,($8902)       ; 2065 3A 02 89
    cp     $ff             ; 2068 FE FF
    jr     z,$2082         ; 206A 28 16
    ld     a,($8913)       ; 206C 3A 13 89
    cp     $00             ; 206F FE 00
    jr     z,$20af         ; 2071 28 3C
    dec    a               ; 2073 3D
    ld     ($8913),a       ; 2074 32 13 89
    cp     $00             ; 2077 FE 00
    jr     nz,$20af        ; 2079 20 34
    ld     a,($890c)       ; 207B 3A 0C 89
    cp     $02             ; 207E FE 02
    jr     nz,$208c        ; 2080 20 0A
    call   $6036           ; 2082 CD 36 60
    ld     a,($8902)       ; 2085 3A 02 89
    cp     $ff             ; 2088 FE FF
    jr     z,$20af         ; 208A 28 23
    ld     hl,$0000        ; 208C 21 00 00
    ld     ($8510),hl      ; 208F 22 10 85
    ld     ($8512),hl      ; 2092 22 12 85
    ld     ($8514),hl      ; 2095 22 14 85
    ld     ($8516),hl      ; 2098 22 16 85
    ld     a,($890c)       ; 209B 3A 0C 89
    cp     $03             ; 209E FE 03
    jr     nz,$20aa        ; 20A0 20 08
    ld     a,$01           ; 20A2 3E 01
    ld     ($8080),a       ; 20A4 32 80 80
    ld     ($8077),a       ; 20A7 32 77 80
    ld     a,$00           ; 20AA 3E 00
    ld     ($890c),a       ; 20AC 32 0C 89
    ld     a,($808d)       ; 20AF 3A 8D 80
    cp     $00             ; 20B2 FE 00
    jp     z,$00c3         ; 20B4 CA C3 00
    ld     a,($8028)       ; 20B7 3A 28 80
    bit    2,a             ; 20BA CB 57
    jp     nz,$00c3        ; 20BC C2 C3 00
    ret                    ; 20BF C9
    ld     a,($8028)       ; 20C0 3A 28 80
    bit    2,a             ; 20C3 CB 57
    jp     nz,$2175        ; 20C5 C2 75 21
    ld     ix,$81a2        ; 20C8 DD 21 A2 81
    ld     iy,$8480        ; 20CC FD 21 80 84
    ld     a,($81a2)       ; 20D0 3A A2 81
    cp     $00             ; 20D3 FE 00
    jp     z,$215d         ; 20D5 CA 5D 21
    bit    6,(iy+$00)      ; 20D8 FD CB 00 76
    jp     nz,$214b        ; 20DC C2 4B 21
    ld     a,($8502)       ; 20DF 3A 02 85
    ld     d,$00           ; 20E2 16 00
    ld     e,a             ; 20E4 5F
    ld     hl,$0008        ; 20E5 21 08 00
    add    hl,de           ; 20E8 19
    ld     d,h             ; 20E9 54
    ld     e,l             ; 20EA 5D
    ld     h,$00           ; 20EB 26 00
    ld     l,(iy+$01)      ; 20ED FD 6E 01
    ld     bc,$0008        ; 20F0 01 08 00
    add    hl,bc           ; 20F3 09
    or     a               ; 20F4 B7
    sbc    hl,de           ; 20F5 ED 52
    call   $1ff0           ; 20F7 CD F0 1F
    ld     de,$000a        ; 20FA 11 0A 00
    or     a               ; 20FD B7
    sbc    hl,de           ; 20FE ED 52
    jp     nc,$214b        ; 2100 D2 4B 21
    ld     a,($8503)       ; 2103 3A 03 85
    ld     d,$00           ; 2106 16 00
    ld     e,a             ; 2108 5F
    ld     hl,$0008        ; 2109 21 08 00
    add    hl,de           ; 210C 19
    ld     d,h             ; 210D 54
    ld     e,l             ; 210E 5D
    ld     h,$00           ; 210F 26 00
    ld     l,(iy+$02)      ; 2111 FD 6E 02
    ld     bc,$0008        ; 2114 01 08 00
    add    hl,bc           ; 2117 09
    or     a               ; 2118 B7
    sbc    hl,de           ; 2119 ED 52
    call   $1ff0           ; 211B CD F0 1F
    ld     de,$0008        ; 211E 11 08 00
    or     a               ; 2121 B7
    sbc    hl,de           ; 2122 ED 52
    jp     nc,$214b        ; 2124 D2 4B 21
    call   $2176           ; 2127 CD 76 21
    ld     a,($8453)       ; 212A 3A 53 84
    and    $03             ; 212D E6 03
    cp     $03             ; 212F FE 03
    jr     z,$213d         ; 2131 28 0A
    ld     a,($8028)       ; 2133 3A 28 80
    bit    7,a             ; 2136 CB 7F
    jr     nz,$213d        ; 2138 20 03
    call   $2906           ; 213A CD 06 29
    call   $22cf           ; 213D CD CF 22
    ld     a,($81a2)       ; 2140 3A A2 81
    cp     $00             ; 2143 FE 00
    jp     z,$215d         ; 2145 CA 5D 21
    jp     $2175           ; 2148 C3 75 21
    ld     a,(iy+$00)      ; 214B FD 7E 00
    and    $3f             ; 214E E6 3F
    cp     $17             ; 2150 FE 17
    jp     z,$2175         ; 2152 CA 75 21
    ld     de,$0005        ; 2155 11 05 00
    add    iy,de           ; 2158 FD 19
    jp     $20d8           ; 215A C3 D8 20
    ld     a,($8070)       ; 215D 3A 70 80
    cp     $00             ; 2160 FE 00
    jr     z,$216e         ; 2162 28 0A
    ld     a,$01           ; 2164 3E 01
    ld     ($8074),a       ; 2166 32 74 80
    ld     ($8077),a       ; 2169 32 77 80
    jr     $2175           ; 216C 18 07
    ld     a,$01           ; 216E 3E 01
    ld     ($8078),a       ; 2170 32 78 80
    jr     $2175           ; 2173 18 00
    ret                    ; 2175 C9
    push   bc              ; 2176 C5
    push   ix              ; 2177 DD E5
    push   iy              ; 2179 FD E5
    ld     (iy+$04),$00    ; 217B FD 36 04 00
    ld     bc,$0100        ; 217F 01 00 01
    ld     a,$19           ; 2182 3E 19
    bit    7,(iy+$00)      ; 2184 FD CB 00 7E
    jr     z,$219a         ; 2188 28 10
    ld     bc,$0200        ; 218A 01 00 02
    ld     a,($81ab)       ; 218D 3A AB 81
    inc    a               ; 2190 3C
    ld     ($81ab),a       ; 2191 32 AB 81
    ld     (iy+$04),$01    ; 2194 FD 36 04 01
    ld     a,$18           ; 2198 3E 18
    call   $00fc           ; 219A CD FC 00
    call   $00e4           ; 219D CD E4 00
    ld     b,(iy+$01)      ; 21A0 FD 46 01
    ld     c,(iy+$02)      ; 21A3 FD 4E 02
    call   $00de           ; 21A6 CD DE 00
    push   hl              ; 21A9 E5
    pop    ix              ; 21AA DD E1
    ld     (ix+$00),$86    ; 21AC DD 36 00 86
    ld     (ix+$01),$87    ; 21B0 DD 36 01 87
    ld     de,$ffe0        ; 21B4 11 E0 FF
    add    ix,de           ; 21B7 DD 19
    ld     (ix+$00),$84    ; 21B9 DD 36 00 84
    ld     (ix+$01),$85    ; 21BD DD 36 01 85
    set    6,(iy+$00)      ; 21C1 FD CB 00 F6
    ld     (iy+$03),$01    ; 21C5 FD 36 03 01
    ld     a,($81a2)       ; 21C9 3A A2 81
    cp     $00             ; 21CC FE 00
    jr     z,$21d1         ; 21CE 28 01
    dec    a               ; 21D0 3D
    ld     ($81a2),a       ; 21D1 32 A2 81
    ld     a,($81a7)       ; 21D4 3A A7 81
    inc    a               ; 21D7 3C
    bit    7,(iy+$00)      ; 21D8 FD CB 00 7E
    jr     z,$21ea         ; 21DC 28 0C
    inc    a               ; 21DE 3C
    ld     c,a             ; 21DF 4F
    res    7,(iy+$00)      ; 21E0 FD CB 00 BE
    ld     a,$00           ; 21E4 3E 00
    ld     ($81a6),a       ; 21E6 32 A6 81
    ld     a,c             ; 21E9 79
    ld     c,a             ; 21EA 4F
    ld     a,($8453)       ; 21EB 3A 53 84
    and    $03             ; 21EE E6 03
    cp     $03             ; 21F0 FE 03
    jr     z,$21ff         ; 21F2 28 0B
    ld     a,($8028)       ; 21F4 3A 28 80
    bit    7,a             ; 21F7 CB 7F
    jr     nz,$21ff        ; 21F9 20 04
    ld     a,c             ; 21FB 79
    ld     ($81a7),a       ; 21FC 32 A7 81
    pop    iy              ; 21FF FD E1
    pop    ix              ; 2201 DD E1
    pop    bc              ; 2203 C1
    ret                    ; 2204 C9
    push   bc              ; 2205 C5
    push   de              ; 2206 D5
    push   hl              ; 2207 E5
    push   ix              ; 2208 DD E5
    push   iy              ; 220A FD E5
    ld     ix,$8480        ; 220C DD 21 80 84
    ld     a,(ix+$00)      ; 2210 DD 7E 00
    cp     $ff             ; 2213 FE FF
    jp     z,$22c7         ; 2215 CA C7 22
    bit    6,(ix+$00)      ; 2218 DD CB 00 76
    jp     z,$22bc         ; 221C CA BC 22
    ld     a,(ix+$03)      ; 221F DD 7E 03
    cp     $00             ; 2222 FE 00
    jp     z,$22bc         ; 2224 CA BC 22
    ld     a,(ix+$03)      ; 2227 DD 7E 03
    inc    a               ; 222A 3C
    ld     (ix+$03),a      ; 222B DD 77 03
    cp     $05             ; 222E FE 05
    jp     nz,$2257        ; 2230 C2 57 22
    ld     b,(ix+$01)      ; 2233 DD 46 01
    ld     c,(ix+$02)      ; 2236 DD 4E 02
    call   $00de           ; 2239 CD DE 00
    push   hl              ; 223C E5
    pop    iy              ; 223D FD E1
    ld     (iy+$00),$8a    ; 223F FD 36 00 8A
    ld     (iy+$01),$8b    ; 2243 FD 36 01 8B
    ld     de,$ffe0        ; 2247 11 E0 FF
    add    iy,de           ; 224A FD 19
    ld     (iy+$00),$88    ; 224C FD 36 00 88
    ld     (iy+$01),$89    ; 2250 FD 36 01 89
    jp     $22bc           ; 2254 C3 BC 22
    cp     $09             ; 2257 FE 09
    jp     nz,$2280        ; 2259 C2 80 22
    ld     b,(ix+$01)      ; 225C DD 46 01
    ld     c,(ix+$02)      ; 225F DD 4E 02
    call   $00de           ; 2262 CD DE 00
    push   hl              ; 2265 E5
    pop    iy              ; 2266 FD E1
    ld     (iy+$00),$8e    ; 2268 FD 36 00 8E
    ld     (iy+$01),$8f    ; 226C FD 36 01 8F
    ld     de,$ffe0        ; 2270 11 E0 FF
    add    iy,de           ; 2273 FD 19
    ld     (iy+$00),$8c    ; 2275 FD 36 00 8C
    ld     (iy+$01),$8d    ; 2279 FD 36 01 8D
    jp     $22bc           ; 227D C3 BC 22
    cp     $0d             ; 2280 FE 0D
    jp     c,$22bc         ; 2282 DA BC 22
    ld     b,(ix+$01)      ; 2285 DD 46 01
    ld     c,(ix+$02)      ; 2288 DD 4E 02
    call   $00de           ; 228B CD DE 00
    push   hl              ; 228E E5
    pop    iy              ; 228F FD E1
    ld     (iy+$00),$00    ; 2291 FD 36 00 00
    ld     (iy+$01),$00    ; 2295 FD 36 01 00
    ld     de,$ffe0        ; 2299 11 E0 FF
    add    iy,de           ; 229C FD 19
    ld     (iy+$00),$00    ; 229E FD 36 00 00
    ld     (iy+$01),$00    ; 22A2 FD 36 01 00
    ld     de,$0400        ; 22A6 11 00 04
    add    hl,de           ; 22A9 19
    ld     (hl),$0a        ; 22AA 36 0A
    inc    hl              ; 22AC 23
    ld     (hl),$0a        ; 22AD 36 0A
    ld     de,$ffe0        ; 22AF 11 E0 FF
    add    hl,de           ; 22B2 19
    ld     (hl),$0a        ; 22B3 36 0A
    dec    hl              ; 22B5 2B
    ld     (hl),$0a        ; 22B6 36 0A
    ld     (ix+$03),$00    ; 22B8 DD 36 03 00
    call   $602d           ; 22BC CD 2D 60
    ld     de,$0005        ; 22BF 11 05 00
    add    ix,de           ; 22C2 DD 19
    jp     $2210           ; 22C4 C3 10 22
    pop    iy              ; 22C7 FD E1
    pop    ix              ; 22C9 DD E1
    pop    hl              ; 22CB E1
    pop    de              ; 22CC D1
    pop    bc              ; 22CD C1
    ret                    ; 22CE C9
    push   bc              ; 22CF C5
    push   de              ; 22D0 D5
    push   hl              ; 22D1 E5
    push   ix              ; 22D2 DD E5
    push   iy              ; 22D4 FD E5
    ld     a,($81a2)       ; 22D6 3A A2 81
    cp     $00             ; 22D9 FE 00
    jp     z,$2343         ; 22DB CA 43 23
    ld     a,($81a6)       ; 22DE 3A A6 81
    cp     $01             ; 22E1 FE 01
    jp     z,$2343         ; 22E3 CA 43 23
    ld     a,$01           ; 22E6 3E 01
    ld     ($81a6),a       ; 22E8 32 A6 81
    ld     a,(iy+$00)      ; 22EB FD 7E 00
    cp     $ff             ; 22EE FE FF
    jr     nz,$22f6        ; 22F0 20 04
    ld     iy,$8480        ; 22F2 FD 21 80 84
    bit    6,(iy+$00)      ; 22F6 FD CB 00 76
    jr     z,$2303         ; 22FA 28 07
    ld     de,$0005        ; 22FC 11 05 00
    add    iy,de           ; 22FF FD 19
    jr     $22eb           ; 2301 18 E8
    set    7,(iy+$00)      ; 2303 FD CB 00 FE
    ld     b,(iy+$01)      ; 2307 FD 46 01
    ld     c,(iy+$02)      ; 230A FD 4E 02
    call   $00de           ; 230D CD DE 00
    push   hl              ; 2310 E5
    pop    ix              ; 2311 DD E1
    ld     de,$0400        ; 2313 11 00 04
    add    hl,de           ; 2316 19
    ld     a,($85a4)       ; 2317 3A A4 85
    ld     b,a             ; 231A 47
    ld     (hl),b          ; 231B 70
    ld     a,($85a2)       ; 231C 3A A2 85
    ld     (ix+$00),a      ; 231F DD 77 00
    inc    hl              ; 2322 23
    inc    ix              ; 2323 DD 23
    ld     (hl),b          ; 2325 70
    ld     a,($85a3)       ; 2326 3A A3 85
    ld     (ix+$00),a      ; 2329 DD 77 00
    ld     de,$ffdf        ; 232C 11 DF FF
    add    hl,de           ; 232F 19
    add    ix,de           ; 2330 DD 19
    ld     (hl),b          ; 2332 70
    ld     a,($85a0)       ; 2333 3A A0 85
    ld     (ix+$00),a      ; 2336 DD 77 00
    inc    hl              ; 2339 23
    inc    ix              ; 233A DD 23
    ld     (hl),b          ; 233C 70
    ld     a,($85a1)       ; 233D 3A A1 85
    ld     (ix+$00),a      ; 2340 DD 77 00
    pop    iy              ; 2343 FD E1
    pop    ix              ; 2345 DD E1
    pop    hl              ; 2347 E1
    pop    de              ; 2348 D1
    pop    bc              ; 2349 C1
    ret                    ; 234A C9
    ld     a,($8028)       ; 234B 3A 28 80
    bit    2,a             ; 234E CB 57
    ret    nz              ; 2350 C0
    ld     a,($808e)       ; 2351 3A 8E 80
    cp     $00             ; 2354 FE 00
    ret    nz              ; 2356 C0
    ld     ix,$8453        ; 2357 DD 21 53 84
    ld     a,($8453)       ; 235B 3A 53 84
    and    $03             ; 235E E6 03
    cp     $03             ; 2360 FE 03
    jp     nz,$2446        ; 2362 C2 46 24
    bit    2,(ix+$00)      ; 2365 DD CB 00 56
    jr     nz,$2398        ; 2369 20 2D
    ld     hl,$0000        ; 236B 21 00 00
    ld     ($850c),hl      ; 236E 22 0C 85
    ld     ($850e),hl      ; 2371 22 0E 85
    set    2,(ix+$00)      ; 2374 DD CB 00 D6
    ld     (ix+$02),$3d    ; 2378 DD 36 02 3D
    ld     (ix+$03),$0e    ; 237C DD 36 03 0E
    ld     (ix+$04),$78    ; 2380 DD 36 04 78
    ld     (ix+$05),$78    ; 2384 DD 36 05 78
    ld     (ix+$0c),$01    ; 2388 DD 36 0C 01
    ld     (ix+$0d),$00    ; 238C DD 36 0D 00
    ld     a,$00           ; 2390 3E 00
    ld     ($8858),a       ; 2392 32 58 88
    call   $24a9           ; 2395 CD A9 24
    call   $2447           ; 2398 CD 47 24
    cp     $00             ; 239B FE 00
    jp     nz,$2401        ; 239D C2 01 24
    ld     b,(ix+$04)      ; 23A0 DD 46 04
    ld     c,(ix+$05)      ; 23A3 DD 4E 05
    ld     (ix+$01),$00    ; 23A6 DD 36 01 00
    push   iy              ; 23AA FD E5
    ld     iy,$8508        ; 23AC FD 21 08 85
    call   $5ff2           ; 23B0 CD F2 5F
    pop    iy              ; 23B3 FD E1
    cp     $00             ; 23B5 FE 00
    jp     z,$23db         ; 23B7 CA DB 23
    ld     c,a             ; 23BA 4F
    and    $0c             ; 23BB E6 0C
    jr     z,$23c8         ; 23BD 28 09
    ld     hl,($845d)      ; 23BF 2A 5D 84
    call   $1ff3           ; 23C2 CD F3 1F
    ld     ($845d),hl      ; 23C5 22 5D 84
    ld     a,$1c           ; 23C8 3E 1C
    call   $00fc           ; 23CA CD FC 00
    ld     a,c             ; 23CD 79
    and    $03             ; 23CE E6 03
    jr     z,$23db         ; 23D0 28 09
    ld     hl,($845b)      ; 23D2 2A 5B 84
    call   $1ff3           ; 23D5 CD F3 1F
    ld     ($845b),hl      ; 23D8 22 5B 84
    ld     a,($8453)       ; 23DB 3A 53 84
    and    $c0             ; 23DE E6 C0
    cp     $c0             ; 23E0 FE C0
    jr     nz,$23fe        ; 23E2 20 1A
    res    7,(ix+$00)      ; 23E4 DD CB 00 BE
    res    6,(ix+$00)      ; 23E8 DD CB 00 B6
    ld     a,($845f)       ; 23EC 3A 5F 84
    inc    a               ; 23EF 3C
    cp     $07             ; 23F0 FE 07
    jr     z,$23f8         ; 23F2 28 04
    jr     c,$23f8         ; 23F4 38 02
    ld     a,$01           ; 23F6 3E 01
    ld     ($845f),a       ; 23F8 32 5F 84
    call   $24a9           ; 23FB CD A9 24
    jp     $2438           ; 23FE C3 38 24
    ld     a,$1a           ; 2401 3E 1A
    call   $00fc           ; 2403 CD FC 00
    ld     a,($8028)       ; 2406 3A 28 80
    set    7,a             ; 2409 CB FF
    ld     ($8028),a       ; 240B 32 28 80
    ld     hl,$012c        ; 240E 21 2C 01
    ld     ($8036),hl      ; 2411 22 36 80
    ld     hl,$0000        ; 2414 21 00 00
    ld     ($8453),hl      ; 2417 22 53 84
    ld     ($8455),hl      ; 241A 22 55 84
    call   $2510           ; 241D CD 10 25
    ld     h,(ix+$0c)      ; 2420 DD 66 0C
    ld     (ix+$0c),$00    ; 2423 DD 36 0C 00
    ld     a,h             ; 2427 7C
    ld     hl,$0000        ; 2428 21 00 00
    ld     iy,$8508        ; 242B FD 21 08 85
    call   $00f3           ; 242F CD F3 00
    ld     (ix+$0d),$1e    ; 2432 DD 36 0D 1E
    jr     $243b           ; 2436 18 03
    call   $2cf3           ; 2438 CD F3 2C
    ld     ix,$8454        ; 243B DD 21 54 84
    ld     iy,$8508        ; 243F FD 21 08 85
    call   $2d24           ; 2443 CD 24 2D
    ret                    ; 2446 C9
    push   bc              ; 2447 C5
    push   de              ; 2448 D5
    push   hl              ; 2449 E5
    push   ix              ; 244A DD E5
    push   iy              ; 244C FD E5
    ld     a,($808d)       ; 244E 3A 8D 80
    cp     $00             ; 2451 FE 00
    jr     nz,$249f        ; 2453 20 4A
    ld     a,($8028)       ; 2455 3A 28 80
    bit    2,a             ; 2458 CB 57
    jr     nz,$249f        ; 245A 20 43
    ld     de,$0000        ; 245C 11 00 00
    ld     a,($8502)       ; 245F 3A 02 85
    ld     e,a             ; 2462 5F
    ld     hl,$0008        ; 2463 21 08 00
    add    hl,de           ; 2466 19
    ex     de,hl           ; 2467 EB
    ld     bc,$0000        ; 2468 01 00 00
    ld     c,(ix+$04)      ; 246B DD 4E 04
    ld     hl,$0008        ; 246E 21 08 00
    add    hl,bc           ; 2471 09
    sbc    hl,de           ; 2472 ED 52
    call   $1ff0           ; 2474 CD F0 1F
    ld     a,l             ; 2477 7D
    cp     $0a             ; 2478 FE 0A
    jp     nc,$249f        ; 247A D2 9F 24
    ld     de,$0000        ; 247D 11 00 00
    ld     a,($8503)       ; 2480 3A 03 85
    ld     e,a             ; 2483 5F
    ld     hl,$000a        ; 2484 21 0A 00
    add    hl,de           ; 2487 19
    ex     de,hl           ; 2488 EB
    ld     bc,$0000        ; 2489 01 00 00
    ld     c,(ix+$05)      ; 248C DD 4E 05
    ld     hl,$0008        ; 248F 21 08 00
    add    hl,bc           ; 2492 09
    sbc    hl,de           ; 2493 ED 52
    call   $1ff0           ; 2495 CD F0 1F
    ld     a,l             ; 2498 7D
    cp     $0a             ; 2499 FE 0A
    jr     z,$24a1         ; 249B 28 04
    jr     c,$24a1         ; 249D 38 02
    ld     a,$00           ; 249F 3E 00
    pop    iy              ; 24A1 FD E1
    pop    ix              ; 24A3 DD E1
    pop    hl              ; 24A5 E1
    pop    de              ; 24A6 D1
    pop    bc              ; 24A7 C1
    ret                    ; 24A8 C9
    push   bc              ; 24A9 C5
    push   de              ; 24AA D5
    push   hl              ; 24AB E5
    push   ix              ; 24AC DD E5
    push   iy              ; 24AE FD E5
    ld     iy,$8800        ; 24B0 FD 21 00 88
    ld     de,$000a        ; 24B4 11 0A 00
    ld     a,(iy+$00)      ; 24B7 FD 7E 00
    and    $7f             ; 24BA E6 7F
    cp     $0e             ; 24BC FE 0E
    jr     z,$24c4         ; 24BE 28 04
    add    iy,de           ; 24C0 FD 19
    jr     $24b7           ; 24C2 18 F3
    ld     hl,$0d48        ; 24C4 21 48 0D
    ld     a,($845f)       ; 24C7 3A 5F 84
    cp     $00             ; 24CA FE 00
    jr     z,$24dd         ; 24CC 28 0F
    dec    a               ; 24CE 3D
    ld     hl,$0d9e        ; 24CF 21 9E 0D
    ld     de,$0012        ; 24D2 11 12 00
    cp     $00             ; 24D5 FE 00
    jr     z,$24dd         ; 24D7 28 04
    add    hl,de           ; 24D9 19
    dec    a               ; 24DA 3D
    jr     $24d5           ; 24DB 18 F8
    ld     (iy+$01),l      ; 24DD FD 75 01
    ld     (iy+$02),h      ; 24E0 FD 74 02
    push   hl              ; 24E3 E5
    pop    ix              ; 24E4 DD E1
    ld     b,$07           ; 24E6 06 07
    ld     iy,$8ce2        ; 24E8 FD 21 E2 8C
    ld     a,b             ; 24EC 78
    cp     $00             ; 24ED FE 00
    jr     z,$2508         ; 24EF 28 17
    ld     a,(ix+$00)      ; 24F1 DD 7E 00
    ld     (iy+$00),a      ; 24F4 FD 77 00
    ld     a,(ix+$01)      ; 24F7 DD 7E 01
    ld     (iy+$01),a      ; 24FA FD 77 01
    inc    ix              ; 24FD DD 23
    inc    ix              ; 24FF DD 23
    inc    iy              ; 2501 FD 23
    inc    iy              ; 2503 FD 23
    dec    b               ; 2505 05
    jr     $24ec           ; 2506 18 E4
    pop    iy              ; 2508 FD E1
    pop    ix              ; 250A DD E1
    pop    hl              ; 250C E1
    pop    de              ; 250D D1
    pop    bc              ; 250E C1
    ret                    ; 250F C9
    push   bc              ; 2510 C5
    push   iy              ; 2511 FD E5
    ld     a,($845f)       ; 2513 3A 5F 84
    dec    a               ; 2516 3D
    ld     iy,$2f7e        ; 2517 FD 21 7E 2F
    cp     $00             ; 251B FE 00
    jr     z,$2526         ; 251D 28 07
    inc    iy              ; 251F FD 23
    inc    iy              ; 2521 FD 23
    dec    a               ; 2523 3D
    jr     $251b           ; 2524 18 F5
    ld     b,(iy+$01)      ; 2526 FD 46 01
    ld     c,(iy+$00)      ; 2529 FD 4E 00
    call   $00e4           ; 252C CD E4 00
    pop    iy              ; 252F FD E1
    pop    bc              ; 2531 C1
    ret                    ; 2532 C9
    push   bc              ; 2533 C5
    push   de              ; 2534 D5
    push   hl              ; 2535 E5
    push   ix              ; 2536 DD E5
    push   iy              ; 2538 FD E5
    ld     ix,$8900        ; 253A DD 21 00 89
    bit    1,(ix+$00)      ; 253E DD CB 00 4E
    jr     nz,$254a        ; 2542 20 06
    call   $2727           ; 2544 CD 27 27
    call   $2800           ; 2547 CD 00 28
    ld     a,(ix+$11)      ; 254A DD 7E 11
    cp     $00             ; 254D FE 00
    jp     nz,$2695        ; 254F C2 95 26
    bit    7,(ix+$00)      ; 2552 DD CB 00 7E
    jp     nz,$26e6        ; 2556 C2 E6 26
    bit    2,(ix+$00)      ; 2559 DD CB 00 56
    jp     nz,$259e        ; 255D C2 9E 25
    call   $27c4           ; 2560 CD C4 27
    cp     $00             ; 2563 FE 00
    jp     nz,$2633        ; 2565 C2 33 26
    ld     (ix+$03),$0c    ; 2568 DD 36 03 0C
    ld     (ix+$0d),$ff    ; 256C DD 36 0D FF
    ld     (ix+$0e),$03    ; 2570 DD 36 0E 03
    ld     (ix+$0f),$00    ; 2574 DD 36 0F 00
    set    2,(ix+$00)      ; 2578 DD CB 00 D6
    ld     (ix+$07),$00    ; 257C DD 36 07 00
    ld     (ix+$0a),$00    ; 2580 DD 36 0A 00
    ld     (ix+$0b),$00    ; 2584 DD 36 0B 00
    ld     hl,$00aa        ; 2588 21 AA 00
    ld     ($8908),hl      ; 258B 22 08 89
    ld     a,($802b)       ; 258E 3A 2B 80
    bit    0,a             ; 2591 CB 47
    jr     z,$259b         ; 2593 28 06
    ld     hl,$ff56        ; 2595 21 56 FF
    ld     ($8908),hl      ; 2598 22 08 89
    jp     $271f           ; 259B C3 1F 27
    ld     b,(ix+$04)      ; 259E DD 46 04
    ld     c,(ix+$05)      ; 25A1 DD 4E 05
    ld     (ix+$01),$01    ; 25A4 DD 36 01 01
    push   iy              ; 25A8 FD E5
    ld     iy,$8510        ; 25AA FD 21 10 85
    call   $5ff2           ; 25AE CD F2 5F
    pop    iy              ; 25B1 FD E1
    ld     b,a             ; 25B3 47
    ld     c,(ix+$10)      ; 25B4 DD 4E 10
    ld     (ix+$10),$00    ; 25B7 DD 36 10 00
    and    $03             ; 25BB E6 03
    cp     $00             ; 25BD FE 00
    jr     z,$25c2         ; 25BF 28 01
    inc    c               ; 25C1 0C
    ld     a,b             ; 25C2 78
    ld     hl,($890a)      ; 25C3 2A 0A 89
    ld     de,$000a        ; 25C6 11 0A 00
    add    hl,de           ; 25C9 19
    ld     ($890a),hl      ; 25CA 22 0A 89
    bit    2,a             ; 25CD CB 57
    jp     z,$262b         ; 25CF CA 2B 26
    ld     de,($890a)      ; 25D2 ED 5B 0A 89
    ld     hl,$0000        ; 25D6 21 00 00
    ld     (ix+$10),c      ; 25D9 DD 71 10
    ld     ($890a),hl      ; 25DC 22 0A 89
    ld     a,e             ; 25DF 7B
    cp     $46             ; 25E0 FE 46
    jr     c,$25f5         ; 25E2 38 11
    ld     a,$1d           ; 25E4 3E 1D
    call   $00fc           ; 25E6 CD FC 00
    ld     (ix+$0d),$10    ; 25E9 DD 36 0D 10
    ld     (ix+$0e),$01    ; 25ED DD 36 0E 01
    ld     (ix+$0f),$00    ; 25F1 DD 36 0F 00
    ld     a,($890d)       ; 25F5 3A 0D 89
    cp     $ff             ; 25F8 FE FF
    jr     z,$2608         ; 25FA 28 0C
    cp     $00             ; 25FC FE 00
    jr     nz,$2608        ; 25FE 20 08
    ld     (ix+$0d),$ff    ; 2600 DD 36 0D FF
    ld     (ix+$0e),$03    ; 2604 DD 36 0E 03
    ld     a,b             ; 2608 78
    and    $03             ; 2609 E6 03
    cp     $00             ; 260B FE 00
    jr     z,$2633         ; 260D 28 24
    ld     a,$1d           ; 260F 3E 1D
    call   $00fc           ; 2611 CD FC 00
    ld     hl,($8908)      ; 2614 2A 08 89
    call   $1ff3           ; 2617 CD F3 1F
    ld     ($8908),hl      ; 261A 22 08 89
    ld     (ix+$0d),$10    ; 261D DD 36 0D 10
    ld     (ix+$0e),$01    ; 2621 DD 36 0E 01
    ld     (ix+$0f),$00    ; 2625 DD 36 0F 00
    jr     $2633           ; 2629 18 08
    ld     (ix+$0d),$ff    ; 262B DD 36 0D FF
    ld     (ix+$0e),$03    ; 262F DD 36 0E 03
    ld     a,($8910)       ; 2633 3A 10 89
    cp     $03             ; 2636 FE 03
    jp     nc,$26c9        ; 2638 D2 C9 26
    ld     b,(ix+$04)      ; 263B DD 46 04
    ld     c,(ix+$05)      ; 263E DD 4E 05
    call   $2447           ; 2641 CD 47 24
    cp     $00             ; 2644 FE 00
    jp     z,$2719         ; 2646 CA 19 27
    ld     a,($890c)       ; 2649 3A 0C 89
    cp     $02             ; 264C FE 02
    jr     nz,$2655        ; 264E 20 05
    ld     a,$01           ; 2650 3E 01
    ld     ($8088),a       ; 2652 32 88 80
    ld     a,($890c)       ; 2655 3A 0C 89
    cp     $03             ; 2658 FE 03
    ld     a,$1e           ; 265A 3E 1E
    jr     nz,$266a        ; 265C 20 0C
    ld     a,$01           ; 265E 3E 01
    call   $00fc           ; 2660 CD FC 00
    ld     a,$01           ; 2663 3E 01
    ld     ($808e),a       ; 2665 32 8E 80
    ld     a,$28           ; 2668 3E 28
    call   $00fc           ; 266A CD FC 00
    ld     (ix+$11),$10    ; 266D DD 36 11 10
    ld     (ix+$02),$ad    ; 2671 DD 36 02 AD
    ld     (ix+$03),$2b    ; 2675 DD 36 03 2B
    ld     a,($8904)       ; 2679 3A 04 89
    sub    $08             ; 267C D6 08
    ld     ($8904),a       ; 267E 32 04 89
    ld     a,($8905)       ; 2681 3A 05 89
    sub    $08             ; 2684 D6 08
    ld     ($8905),a       ; 2686 32 05 89
    ld     hl,$0000        ; 2689 21 00 00
    ld     ($8908),hl      ; 268C 22 08 89
    ld     ($890a),hl      ; 268F 22 0A 89
    jp     $271c           ; 2692 C3 1C 27
    dec    (ix+$11)        ; 2695 DD 35 11
    ld     a,(ix+$11)      ; 2698 DD 7E 11
    cp     $00             ; 269B FE 00
    jr     nz,$271c        ; 269D 20 7D
    ld     a,($8904)       ; 269F 3A 04 89
    add    a,$08           ; 26A2 C6 08
    ld     ($8904),a       ; 26A4 32 04 89
    ld     a,($8905)       ; 26A7 3A 05 89
    add    a,$08           ; 26AA C6 08
    ld     ($8905),a       ; 26AC 32 05 89
    ld     (ix+$13),$1e    ; 26AF DD 36 13 1E
    ld     a,($890c)       ; 26B3 3A 0C 89
    dec    a               ; 26B6 3D
    ld     iy,$8510        ; 26B7 FD 21 10 85
    ld     hl,$0001        ; 26BB 21 01 00
    call   $00f3           ; 26BE CD F3 00
    call   $2889           ; 26C1 CD 89 28
    call   $28fc           ; 26C4 CD FC 28
    jr     $271c           ; 26C7 18 53
    set    7,(ix+$00)      ; 26C9 DD CB 00 FE
    ld     (ix+$12),$05    ; 26CD DD 36 12 05
    ld     (ix+$0d),$05    ; 26D1 DD 36 0D 05
    ld     (ix+$0e),$05    ; 26D5 DD 36 0E 05
    ld     (ix+$0f),$00    ; 26D9 DD 36 0F 00
    ld     hl,$0000        ; 26DD 21 00 00
    ld     ($8908),hl      ; 26E0 22 08 89
    ld     ($890a),hl      ; 26E3 22 0A 89
    ld     b,(ix+$04)      ; 26E6 DD 46 04
    ld     c,(ix+$05)      ; 26E9 DD 4E 05
    call   $2447           ; 26EC CD 47 24
    cp     $00             ; 26EF FE 00
    jp     nz,$2649        ; 26F1 C2 49 26
    call   $27c4           ; 26F4 CD C4 27
    cp     $00             ; 26F7 FE 00
    jr     nz,$271c        ; 26F9 20 21
    ld     a,($8912)       ; 26FB 3A 12 89
    cp     $10             ; 26FE FE 10
    jr     nc,$2714        ; 2700 30 12
    inc    (ix+$12)        ; 2702 DD 34 12
    ld     a,($8912)       ; 2705 3A 12 89
    ld     ($890d),a       ; 2708 32 0D 89
    ld     ($890e),a       ; 270B 32 0E 89
    ld     (ix+$0f),$00    ; 270E DD 36 0F 00
    jr     $271c           ; 2712 18 08
    call   $28a7           ; 2714 CD A7 28
    jr     $271c           ; 2717 18 03
    call   $27c4           ; 2719 CD C4 27
    call   $283c           ; 271C CD 3C 28
    pop    iy              ; 271F FD E1
    pop    ix              ; 2721 DD E1
    pop    hl              ; 2723 E1
    pop    de              ; 2724 D1
    pop    bc              ; 2725 C1
    ret                    ; 2726 C9
    push   hl              ; 2727 E5
    ld     hl,$0000        ; 2728 21 00 00
    ld     ($8514),hl      ; 272B 22 14 85
    ld     ($8516),hl      ; 272E 22 16 85
    set    0,(ix+$00)      ; 2731 DD CB 00 C6
    set    1,(ix+$00)      ; 2735 DD CB 00 CE
    ld     (ix+$03),$1c    ; 2739 DD 36 03 1C
    ld     hl,$0000        ; 273D 21 00 00
    ld     ($8906),hl      ; 2740 22 06 89
    ld     ($890a),hl      ; 2743 22 0A 89
    ld     hl,$ff80        ; 2746 21 80 FF
    ld     ($890a),hl      ; 2749 22 0A 89
    ld     (ix+$0d),$20    ; 274C DD 36 0D 20
    ld     (ix+$0e),$01    ; 2750 DD 36 0E 01
    ld     (ix+$0f),$00    ; 2754 DD 36 0F 00
    ld     (ix+$10),$00    ; 2758 DD 36 10 00
    ld     (ix+$11),$00    ; 275C DD 36 11 00
    ld     (ix+$12),$00    ; 2760 DD 36 12 00
    ld     (ix+$13),$00    ; 2764 DD 36 13 00
    call   $00c9           ; 2768 CD C9 00
    ld     a,($8025)       ; 276B 3A 25 80
    cp     $aa             ; 276E FE AA
    jr     nz,$2791        ; 2770 20 1F
    ld     a,($8070)       ; 2772 3A 70 80
    cp     $00             ; 2775 FE 00
    jr     z,$2791         ; 2777 28 18
    ld     a,($8014)       ; 2779 3A 14 80
    bit    7,a             ; 277C CB 7F
    jr     z,$2787         ; 277E 28 07
    ld     a,($8024)       ; 2780 3A 24 80
    bit    4,a             ; 2783 CB 67
    jr     z,$2791         ; 2785 28 0A
    ld     (ix+$02),$14    ; 2787 DD 36 02 14
    ld     (ix+$0c),$03    ; 278B DD 36 0C 03
    jr     $27bf           ; 278F 18 2E
    ld     a,($8014)       ; 2791 3A 14 80
    bit    7,a             ; 2794 CB 7F
    jr     z,$27a1         ; 2796 28 09
    ld     a,($81b3)       ; 2798 3A B3 81
    cp     $3c             ; 279B FE 3C
    jr     c,$27b7         ; 279D 38 18
    jr     $27a8           ; 279F 18 07
    ld     a,($81b3)       ; 27A1 3A B3 81
    cp     $28             ; 27A4 FE 28
    jr     c,$27b7         ; 27A6 38 0F
    ld     a,$00           ; 27A8 3E 00
    ld     ($81b3),a       ; 27AA 32 B3 81
    ld     (ix+$02),$10    ; 27AD DD 36 02 10
    ld     (ix+$0c),$02    ; 27B1 DD 36 0C 02
    jr     $27bf           ; 27B5 18 08
    ld     (ix+$02),$0c    ; 27B7 DD 36 02 0C
    ld     (ix+$0c),$01    ; 27BB DD 36 0C 01
    call   $3ff0           ; 27BF CD F0 3F
    pop    hl              ; 27C2 E1
    ret                    ; 27C3 C9
    push   hl              ; 27C4 E5
    ld     a,($890d)       ; 27C5 3A 0D 89
    cp     $ff             ; 27C8 FE FF
    jr     z,$27d4         ; 27CA 28 08
    cp     $00             ; 27CC FE 00
    jp     z,$27fe         ; 27CE CA FE 27
    dec    (ix+$0d)        ; 27D1 DD 35 0D
    inc    (ix+$0f)        ; 27D4 DD 34 0F
    ld     a,($890e)       ; 27D7 3A 0E 89
    cp     (ix+$0f)        ; 27DA DD BE 0F
    jr     z,$27e2         ; 27DD 28 03
    jp     nc,$27fc        ; 27DF D2 FC 27
    ld     a,($8902)       ; 27E2 3A 02 89
    and    $03             ; 27E5 E6 03
    cp     $03             ; 27E7 FE 03
    jr     nz,$27f5        ; 27E9 20 0A
    res    0,(ix+$02)      ; 27EB DD CB 02 86
    res    1,(ix+$02)      ; 27EF DD CB 02 8E
    jr     $27f8           ; 27F3 18 03
    inc    (ix+$02)        ; 27F5 DD 34 02
    ld     (ix+$0f),$00    ; 27F8 DD 36 0F 00
    ld     a,$01           ; 27FC 3E 01
    pop    hl              ; 27FE E1
    ret                    ; 27FF C9
    push   de              ; 2800 D5
    push   hl              ; 2801 E5
    push   iy              ; 2802 FD E5
    ld     iy,$8800        ; 2804 FD 21 00 88
    ld     de,$000a        ; 2808 11 0A 00
    ld     a,(iy+$00)      ; 280B FD 7E 00
    and    $7f             ; 280E E6 7F
    cp     $0c             ; 2810 FE 0C
    jr     z,$2818         ; 2812 28 04
    add    iy,de           ; 2814 FD 19
    jr     $280b           ; 2816 18 F3
    ld     hl,$0ec4        ; 2818 21 C4 0E
    ld     a,($890c)       ; 281B 3A 0C 89
    cp     $00             ; 281E FE 00
    jr     z,$2831         ; 2820 28 0F
    dec    a               ; 2822 3D
    ld     hl,$0ed4        ; 2823 21 D4 0E
    ld     de,$0012        ; 2826 11 12 00
    cp     $00             ; 2829 FE 00
    jr     z,$2831         ; 282B 28 04
    add    hl,de           ; 282D 19
    dec    a               ; 282E 3D
    jr     $2829           ; 282F 18 F8
    ld     (iy+$01),l      ; 2831 FD 75 01
    ld     (iy+$02),h      ; 2834 FD 74 02
    pop    iy              ; 2837 FD E1
    pop    hl              ; 2839 E1
    pop    de              ; 283A D1
    ret                    ; 283B C9
    push   de              ; 283C D5
    push   hl              ; 283D E5
    ld     a,($808d)       ; 283E 3A 8D 80
    cp     $00             ; 2841 FE 00
    jr     nz,$286e        ; 2843 20 29
    ld     hl,($890a)      ; 2845 2A 0A 89
    ld     a,h             ; 2848 7C
    or     l               ; 2849 B5
    jr     nz,$285d        ; 284A 20 11
    ld     h,(ix+$04)      ; 284C DD 66 04
    ld     l,(ix+$06)      ; 284F DD 6E 06
    ld     de,($8908)      ; 2852 ED 5B 08 89
    add    hl,de           ; 2856 19
    ld     (ix+$04),h      ; 2857 DD 74 04
    ld     (ix+$06),l      ; 285A DD 75 06
    ld     h,(ix+$05)      ; 285D DD 66 05
    ld     l,(ix+$07)      ; 2860 DD 6E 07
    ld     de,($890a)      ; 2863 ED 5B 0A 89
    add    hl,de           ; 2867 19
    ld     (ix+$05),h      ; 2868 DD 74 05
    ld     (ix+$07),l      ; 286B DD 75 07
    ld     a,($8902)       ; 286E 3A 02 89
    ld     ($8510),a       ; 2871 32 10 85
    ld     a,($8903)       ; 2874 3A 03 89
    ld     ($8511),a       ; 2877 32 11 85
    ld     a,($8904)       ; 287A 3A 04 89
    ld     ($8512),a       ; 287D 32 12 85
    ld     a,($8905)       ; 2880 3A 05 89
    ld     ($8513),a       ; 2883 32 13 85
    pop    hl              ; 2886 E1
    pop    de              ; 2887 D1
    ret                    ; 2888 C9
    call   $28c1           ; 2889 CD C1 28
    ld     a,($890c)       ; 288C 3A 0C 89
    cp     $01             ; 288F FE 01
    jr     nz,$289e        ; 2891 20 0B
    ld     a,($81ac)       ; 2893 3A AC 81
    cp     $04             ; 2896 FE 04
    jr     nc,$289e        ; 2898 30 04
    inc    a               ; 289A 3C
    ld     ($81ac),a       ; 289B 32 AC 81
    ld     a,$00           ; 289E 3E 00
    ld     ($8900),a       ; 28A0 32 00 89
    ld     ($8910),a       ; 28A3 32 10 89
    ret                    ; 28A6 C9
    push   hl              ; 28A7 E5
    ld     a,$00           ; 28A8 3E 00
    ld     ($8900),a       ; 28AA 32 00 89
    ld     ($8910),a       ; 28AD 32 10 89
    ld     hl,$0000        ; 28B0 21 00 00
    ld     ($8902),hl      ; 28B3 22 02 89
    ld     ($8906),hl      ; 28B6 22 06 89
    ld     ($8908),hl      ; 28B9 22 08 89
    ld     ($890a),hl      ; 28BC 22 0A 89
    pop    hl              ; 28BF E1
    ret                    ; 28C0 C9
    push   bc              ; 28C1 C5
    push   iy              ; 28C2 FD E5
    ld     a,($890c)       ; 28C4 3A 0C 89
    dec    a               ; 28C7 3D
    ld     iy,$2f8c        ; 28C8 FD 21 8C 2F
    cp     $00             ; 28CC FE 00
    jr     z,$28d7         ; 28CE 28 07
    inc    iy              ; 28D0 FD 23
    inc    iy              ; 28D2 FD 23
    dec    a               ; 28D4 3D
    jr     $28cc           ; 28D5 18 F5
    ld     b,(iy+$01)      ; 28D7 FD 46 01
    ld     c,(iy+$00)      ; 28DA FD 4E 00
    call   $00e4           ; 28DD CD E4 00
    pop    iy              ; 28E0 FD E1
    pop    bc              ; 28E2 C1
    ret                    ; 28E3 C9
    push   bc              ; 28E4 C5
    push   de              ; 28E5 D5
    push   hl              ; 28E6 E5
    ld     hl,$2dea        ; 28E7 21 EA 2D
    ld     de,$8914        ; 28EA 11 14 89
    ld     bc,$0068        ; 28ED 01 68 00
    ldir                   ; 28F0 ED B0
    call   $00cf           ; 28F2 CD CF 00
    jp     z,$102d         ; 28F5 CA 2D 10
    pop    hl              ; 28F8 E1
    pop    de              ; 28F9 D1
    pop    bc              ; 28FA C1
    ret                    ; 28FB C9
    call   $2910           ; 28FC CD 10 29
    call   $00cf           ; 28FF CD CF 00
    adc    a,$2d           ; 2902 CE 2D
    ld     (bc),a          ; 2904 02
    ret                    ; 2905 C9
    call   $2950           ; 2906 CD 50 29
    call   $00cf           ; 2909 CD CF 00
    jp     nc,$0c2d        ; 290C D2 2D 0C
    ret                    ; 290F C9
    push   de              ; 2910 D5
    push   hl              ; 2911 E5
    push   iy              ; 2912 FD E5
    ld     iy,$2e52        ; 2914 FD 21 52 2E
    ld     a,($81ac)       ; 2918 3A AC 81
    ld     de,$0008        ; 291B 11 08 00
    cp     $00             ; 291E FE 00
    jr     z,$2927         ; 2920 28 05
    add    iy,de           ; 2922 FD 19
    dec    a               ; 2924 3D
    jr     $291e           ; 2925 18 F7
    ld     h,(iy+$01)      ; 2927 FD 66 01
    ld     l,(iy+$00)      ; 292A FD 6E 00
    ld     ($8926),hl      ; 292D 22 26 89
    ld     h,(iy+$03)      ; 2930 FD 66 03
    ld     l,(iy+$02)      ; 2933 FD 6E 02
    ld     ($8928),hl      ; 2936 22 28 89
    ld     h,(iy+$05)      ; 2939 FD 66 05
    ld     l,(iy+$04)      ; 293C FD 6E 04
    ld     ($892e),hl      ; 293F 22 2E 89
    ld     h,(iy+$07)      ; 2942 FD 66 07
    ld     l,(iy+$06)      ; 2945 FD 6E 06
    ld     ($8930),hl      ; 2948 22 30 89
    pop    iy              ; 294B FD E1
    pop    hl              ; 294D E1
    pop    de              ; 294E D1
    ret                    ; 294F C9
    push   de              ; 2950 D5
    push   hl              ; 2951 E5
    push   iy              ; 2952 FD E5
    ld     hl,$2424        ; 2954 21 24 24
    ld     ($8936),hl      ; 2957 22 36 89
    ld     ($893c),hl      ; 295A 22 3C 89
    ld     ($8942),hl      ; 295D 22 42 89
    ld     ($8948),hl      ; 2960 22 48 89
    ld     ($894e),hl      ; 2963 22 4E 89
    ld     ($8954),hl      ; 2966 22 54 89
    ld     ($895a),hl      ; 2969 22 5A 89
    ld     ($8960),hl      ; 296C 22 60 89
    ld     ($8966),hl      ; 296F 22 66 89
    ld     ($896c),hl      ; 2972 22 6C 89
    ld     ($8972),hl      ; 2975 22 72 89
    ld     ($8978),hl      ; 2978 22 78 89
    ld     iy,$2e7a        ; 297B FD 21 7A 2E
    ld     a,($81a7)       ; 297F 3A A7 81
    srl    a               ; 2982 CB 3F
    ld     de,$0004        ; 2984 11 04 00
    cp     $00             ; 2987 FE 00
    jr     z,$2995         ; 2989 28 0A
    add    iy,de           ; 298B FD 19
    dec    a               ; 298D 3D
    cp     $00             ; 298E FE 00
    jr     z,$2995         ; 2990 28 03
    add    iy,de           ; 2992 FD 19
    dec    a               ; 2994 3D
    ld     h,(iy+$01)      ; 2995 FD 66 01
    ld     l,(iy+$00)      ; 2998 FD 6E 00
    ld     ($8936),hl      ; 299B 22 36 89
    ld     ($893c),hl      ; 299E 22 3C 89
    ld     h,(iy+$03)      ; 29A1 FD 66 03
    ld     l,(iy+$02)      ; 29A4 FD 6E 02
    ld     ($8942),hl      ; 29A7 22 42 89
    ld     ($8948),hl      ; 29AA 22 48 89
    cp     $00             ; 29AD FE 00
    jp     z,$2a18         ; 29AF CA 18 2A
    ld     iy,$2e86        ; 29B2 FD 21 86 2E
    dec    a               ; 29B6 3D
    cp     $00             ; 29B7 FE 00
    jr     z,$29cc         ; 29B9 28 11
    add    iy,de           ; 29BB FD 19
    dec    a               ; 29BD 3D
    cp     $00             ; 29BE FE 00
    jr     z,$29cc         ; 29C0 28 0A
    add    iy,de           ; 29C2 FD 19
    dec    a               ; 29C4 3D
    cp     $00             ; 29C5 FE 00
    jr     z,$29cc         ; 29C7 28 03
    add    iy,de           ; 29C9 FD 19
    dec    a               ; 29CB 3D
    ld     h,(iy+$01)      ; 29CC FD 66 01
    ld     l,(iy+$00)      ; 29CF FD 6E 00
    ld     ($894e),hl      ; 29D2 22 4E 89
    ld     ($8954),hl      ; 29D5 22 54 89
    ld     h,(iy+$03)      ; 29D8 FD 66 03
    ld     l,(iy+$02)      ; 29DB FD 6E 02
    ld     ($895a),hl      ; 29DE 22 5A 89
    ld     ($8960),hl      ; 29E1 22 60 89
    cp     $00             ; 29E4 FE 00
    jp     z,$2a18         ; 29E6 CA 18 2A
    ld     iy,$2e96        ; 29E9 FD 21 96 2E
    dec    a               ; 29ED 3D
    jr     z,$2a00         ; 29EE 28 10
    add    iy,de           ; 29F0 FD 19
    dec    a               ; 29F2 3D
    cp     $00             ; 29F3 FE 00
    jr     z,$2a00         ; 29F5 28 09
    add    iy,de           ; 29F7 FD 19
    dec    a               ; 29F9 3D
    cp     $00             ; 29FA FE 00
    jr     z,$2a00         ; 29FC 28 02
    add    iy,de           ; 29FE FD 19
    ld     h,(iy+$01)      ; 2A00 FD 66 01
    ld     l,(iy+$00)      ; 2A03 FD 6E 00
    ld     ($8966),hl      ; 2A06 22 66 89
    ld     ($896c),hl      ; 2A09 22 6C 89
    ld     h,(iy+$03)      ; 2A0C FD 66 03
    ld     l,(iy+$02)      ; 2A0F FD 6E 02
    ld     ($8972),hl      ; 2A12 22 72 89
    ld     ($8978),hl      ; 2A15 22 78 89
    pop    iy              ; 2A18 FD E1
    pop    hl              ; 2A1A E1
    pop    de              ; 2A1B D1
    ret                    ; 2A1C C9
    ld     a,(ix+$10)      ; 2A1D DD 7E 10
    cp     $00             ; 2A20 FE 00
    jr     z,$2a2a         ; 2A22 28 06
    dec    (ix+$10)        ; 2A24 DD 35 10
    jp     $2aa9           ; 2A27 C3 A9 2A
    ld     hl,($825c)      ; 2A2A 2A 5C 82
    or     a               ; 2A2D B7
    sra    h               ; 2A2E CB 2C
    rr     l               ; 2A30 CB 1D
    bit    7,(ix+$09)      ; 2A32 DD CB 09 7E
    jr     z,$2a3b         ; 2A36 28 03
    call   $1ff3           ; 2A38 CD F3 1F
    ld     (ix+$09),h      ; 2A3B DD 74 09
    ld     (ix+$08),l      ; 2A3E DD 75 08
    ld     hl,($825c)      ; 2A41 2A 5C 82
    bit    7,(ix+$0b)      ; 2A44 DD CB 0B 7E
    jr     z,$2a4d         ; 2A48 28 03
    call   $1ff3           ; 2A4A CD F3 1F
    ld     (ix+$0b),h      ; 2A4D DD 74 0B
    ld     (ix+$0a),l      ; 2A50 DD 75 0A
    ld     b,(ix+$04)      ; 2A53 DD 46 04
    ld     c,(ix+$05)      ; 2A56 DD 4E 05
    call   $5ff2           ; 2A59 CD F2 5F
    cp     $00             ; 2A5C FE 00
    jp     z,$2a94         ; 2A5E CA 94 2A
    ld     (ix+$10),$01    ; 2A61 DD 36 10 01
    ld     c,a             ; 2A65 4F
    and    $0c             ; 2A66 E6 0C
    cp     $00             ; 2A68 FE 00
    jr     z,$2a7b         ; 2A6A 28 0F
    ld     h,(ix+$0b)      ; 2A6C DD 66 0B
    ld     l,(ix+$0a)      ; 2A6F DD 6E 0A
    call   $1ff3           ; 2A72 CD F3 1F
    ld     (ix+$0b),h      ; 2A75 DD 74 0B
    ld     (ix+$0a),l      ; 2A78 DD 75 0A
    ld     a,c             ; 2A7B 79
    and    $03             ; 2A7C E6 03
    cp     $00             ; 2A7E FE 00
    jr     z,$2a91         ; 2A80 28 0F
    ld     h,(ix+$09)      ; 2A82 DD 66 09
    ld     l,(ix+$08)      ; 2A85 DD 6E 08
    call   $1ff3           ; 2A88 CD F3 1F
    ld     (ix+$09),h      ; 2A8B DD 74 09
    ld     (ix+$08),l      ; 2A8E DD 75 08
    jp     $2aa9           ; 2A91 C3 A9 2A
    ld     hl,$2f6e        ; 2A94 21 6E 2F
    call   $2b10           ; 2A97 CD 10 2B
    ld     (iy+$00),h      ; 2A9A FD 74 00
    ld     (iy+$01),l      ; 2A9D FD 75 01
    call   $2aaa           ; 2AA0 CD AA 2A
    call   $2cf3           ; 2AA3 CD F3 2C
    call   $2b89           ; 2AA6 CD 89 2B
    ret                    ; 2AA9 C9
    push   bc              ; 2AAA C5
    push   de              ; 2AAB D5
    push   hl              ; 2AAC E5
    ld     hl,$2ea6        ; 2AAD 21 A6 2E
    ld     a,(ix+$01)      ; 2AB0 DD 7E 01
    cp     $02             ; 2AB3 FE 02
    jr     z,$2acf         ; 2AB5 28 18
    ld     hl,$2ece        ; 2AB7 21 CE 2E
    cp     $03             ; 2ABA FE 03
    jr     z,$2acf         ; 2ABC 28 11
    ld     hl,$2ef6        ; 2ABE 21 F6 2E
    cp     $04             ; 2AC1 FE 04
    jr     z,$2acf         ; 2AC3 28 0A
    ld     hl,$2f1e        ; 2AC5 21 1E 2F
    cp     $05             ; 2AC8 FE 05
    jr     z,$2acf         ; 2ACA 28 03
    ld     hl,$2f46        ; 2ACC 21 46 2F
    bit    7,(ix+$09)      ; 2ACF DD CB 09 7E
    jr     nz,$2ad9        ; 2AD3 20 04
    ld     de,$0014        ; 2AD5 11 14 00
    add    hl,de           ; 2AD8 19
    inc    (ix+$11)        ; 2AD9 DD 34 11
    ld     c,(ix+$11)      ; 2ADC DD 4E 11
    ld     a,(hl)          ; 2ADF 7E
    cp     c               ; 2AE0 B9
    jr     z,$2ae5         ; 2AE1 28 02
    jr     nc,$2b0c        ; 2AE3 30 27
    inc    hl              ; 2AE5 23
    ld     d,h             ; 2AE6 54
    ld     e,l             ; 2AE7 5D
    inc    (ix+$12)        ; 2AE8 DD 34 12
    inc    (ix+$12)        ; 2AEB DD 34 12
    ld     b,$00           ; 2AEE 06 00
    ld     c,(ix+$12)      ; 2AF0 DD 4E 12
    add    hl,bc           ; 2AF3 09
    ld     a,(hl)          ; 2AF4 7E
    cp     $ff             ; 2AF5 FE FF
    jr     nz,$2b00        ; 2AF7 20 07
    ld     (ix+$12),$00    ; 2AF9 DD 36 12 00
    ld     h,d             ; 2AFD 62
    ld     l,e             ; 2AFE 6B
    ld     a,(hl)          ; 2AFF 7E
    ld     (ix+$02),a      ; 2B00 DD 77 02
    inc    hl              ; 2B03 23
    ld     a,(hl)          ; 2B04 7E
    ld     (ix+$03),a      ; 2B05 DD 77 03
    ld     (ix+$11),$00    ; 2B08 DD 36 11 00
    pop    hl              ; 2B0C E1
    pop    de              ; 2B0D D1
    pop    bc              ; 2B0E C1
    ret                    ; 2B0F C9
    push   de              ; 2B10 D5
    ld     de,$0000        ; 2B11 11 00 00
    ld     a,(ix+$08)      ; 2B14 DD 7E 08
    or     (ix+$09)        ; 2B17 DD B6 09
    jr     nz,$2b23        ; 2B1A 20 07
    bit    7,(ix+$0b)      ; 2B1C DD CB 0B 7E
    jp     nz,$2b83        ; 2B20 C2 83 2B
    inc    hl              ; 2B23 23
    inc    hl              ; 2B24 23
    bit    7,(ix+$09)      ; 2B25 DD CB 09 7E
    jr     nz,$2b32        ; 2B29 20 07
    bit    7,(ix+$0b)      ; 2B2B DD CB 0B 7E
    jp     nz,$2b83        ; 2B2F C2 83 2B
    inc    hl              ; 2B32 23
    inc    hl              ; 2B33 23
    ld     a,(ix+$0a)      ; 2B34 DD 7E 0A
    or     (ix+$0b)        ; 2B37 DD B6 0B
    jr     nz,$2b43        ; 2B3A 20 07
    bit    7,(ix+$09)      ; 2B3C DD CB 09 7E
    jp     z,$2b83         ; 2B40 CA 83 2B
    inc    hl              ; 2B43 23
    inc    hl              ; 2B44 23
    bit    7,(ix+$09)      ; 2B45 DD CB 09 7E
    jr     nz,$2b52        ; 2B49 20 07
    bit    7,(ix+$0b)      ; 2B4B DD CB 0B 7E
    jp     z,$2b83         ; 2B4F CA 83 2B
    inc    hl              ; 2B52 23
    inc    hl              ; 2B53 23
    ld     a,(ix+$08)      ; 2B54 DD 7E 08
    or     (ix+$09)        ; 2B57 DD B6 09
    jr     nz,$2b63        ; 2B5A 20 07
    bit    7,(ix+$0b)      ; 2B5C DD CB 0B 7E
    jp     z,$2b83         ; 2B60 CA 83 2B
    inc    hl              ; 2B63 23
    inc    hl              ; 2B64 23
    bit    7,(ix+$09)      ; 2B65 DD CB 09 7E
    jr     z,$2b71         ; 2B69 28 06
    bit    7,(ix+$0b)      ; 2B6B DD CB 0B 7E
    jr     nz,$2b83        ; 2B6F 20 12
    inc    hl              ; 2B71 23
    inc    hl              ; 2B72 23
    ld     a,(ix+$0a)      ; 2B73 DD 7E 0A
    or     (ix+$0b)        ; 2B76 DD B6 0B
    jr     nz,$2b81        ; 2B79 20 06
    bit    7,(ix+$09)      ; 2B7B DD CB 09 7E
    jr     nz,$2b83        ; 2B7F 20 02
    inc    hl              ; 2B81 23
    inc    hl              ; 2B82 23
    ld     d,(hl)          ; 2B83 56
    inc    hl              ; 2B84 23
    ld     e,(hl)          ; 2B85 5E
    ex     de,hl           ; 2B86 EB
    pop    de              ; 2B87 D1
    ret                    ; 2B88 C9
    push   bc              ; 2B89 C5
    ld     a,(ix+$04)      ; 2B8A DD 7E 04
    ld     (iy+$02),a      ; 2B8D FD 77 02
    ld     a,(ix+$01)      ; 2B90 DD 7E 01
    ld     c,$04           ; 2B93 0E 04
    bit    1,a             ; 2B95 CB 4F
    jr     z,$2b9b         ; 2B97 28 02
    ld     c,$fc           ; 2B99 0E FC
    ld     a,(ix+$05)      ; 2B9B DD 7E 05
    add    a,c             ; 2B9E 81
    ld     (iy+$03),a      ; 2B9F FD 77 03
    pop    bc              ; 2BA2 C1
    ret                    ; 2BA3 C9
    ld     hl,($8260)      ; 2BA4 2A 60 82
    ld     (ix+$0c),l      ; 2BA7 DD 75 0C
    ld     (ix+$0d),h      ; 2BAA DD 74 0D
    ld     (ix+$0e),l      ; 2BAD DD 75 0E
    ld     (ix+$0f),h      ; 2BB0 DD 74 0F
    ld     a,(ix+$01)      ; 2BB3 DD 7E 01
    cp     $04             ; 2BB6 FE 04
    jr     nz,$2bd6        ; 2BB8 20 1C
    ld     (ix+$0c),$00    ; 2BBA DD 36 0C 00
    ld     (ix+$0d),$00    ; 2BBE DD 36 0D 00
    ld     hl,($825c)      ; 2BC2 2A 5C 82
    bit    7,(ix+$09)      ; 2BC5 DD CB 09 7E
    jr     z,$2bce         ; 2BC9 28 03
    call   $1ff3           ; 2BCB CD F3 1F
    ld     (ix+$09),h      ; 2BCE DD 74 09
    ld     (ix+$08),l      ; 2BD1 DD 75 08
    jr     $2bef           ; 2BD4 18 19
    ld     a,($8502)       ; 2BD6 3A 02 85
    ld     b,a             ; 2BD9 47
    ld     a,(ix+$04)      ; 2BDA DD 7E 04
    sub    b               ; 2BDD 90
    jr     c,$2bef         ; 2BDE 38 0F
    ld     h,(ix+$0d)      ; 2BE0 DD 66 0D
    ld     l,(ix+$0c)      ; 2BE3 DD 6E 0C
    call   $1ff3           ; 2BE6 CD F3 1F
    ld     (ix+$0d),h      ; 2BE9 DD 74 0D
    ld     (ix+$0c),l      ; 2BEC DD 75 0C
    ld     a,(ix+$01)      ; 2BEF DD 7E 01
    cp     $03             ; 2BF2 FE 03
    jr     nz,$2c12        ; 2BF4 20 1C
    ld     (ix+$0e),$00    ; 2BF6 DD 36 0E 00
    ld     (ix+$0f),$00    ; 2BFA DD 36 0F 00
    ld     hl,($825c)      ; 2BFE 2A 5C 82
    bit    7,(ix+$0b)      ; 2C01 DD CB 0B 7E
    jr     z,$2c0a         ; 2C05 28 03
    call   $1ff3           ; 2C07 CD F3 1F
    ld     (ix+$0b),h      ; 2C0A DD 74 0B
    ld     (ix+$0a),l      ; 2C0D DD 75 0A
    jr     $2c2b           ; 2C10 18 19
    ld     a,($8503)       ; 2C12 3A 03 85
    ld     b,a             ; 2C15 47
    ld     a,(ix+$05)      ; 2C16 DD 7E 05
    sub    b               ; 2C19 90
    jr     c,$2c2b         ; 2C1A 38 0F
    ld     h,(ix+$0f)      ; 2C1C DD 66 0F
    ld     l,(ix+$0e)      ; 2C1F DD 6E 0E
    call   $1ff3           ; 2C22 CD F3 1F
    ld     (ix+$0f),h      ; 2C25 DD 74 0F
    ld     (ix+$0e),l      ; 2C28 DD 75 0E
    ld     b,(ix+$04)      ; 2C2B DD 46 04
    ld     c,(ix+$05)      ; 2C2E DD 4E 05
    call   $5ff2           ; 2C31 CD F2 5F
    cp     $00             ; 2C34 FE 00
    jp     z,$2c79         ; 2C36 CA 79 2C
    ld     (ix+$10),$01    ; 2C39 DD 36 10 01
    ld     c,a             ; 2C3D 4F
    and    $0c             ; 2C3E E6 0C
    cp     $00             ; 2C40 FE 00
    jr     z,$2c5b         ; 2C42 28 17
    ld     h,(ix+$0b)      ; 2C44 DD 66 0B
    ld     l,(ix+$0a)      ; 2C47 DD 6E 0A
    call   $1ff3           ; 2C4A CD F3 1F
    ld     (ix+$0b),h      ; 2C4D DD 74 0B
    ld     (ix+$0a),l      ; 2C50 DD 75 0A
    ld     (ix+$0e),$00    ; 2C53 DD 36 0E 00
    ld     (ix+$0f),$00    ; 2C57 DD 36 0F 00
    ld     a,c             ; 2C5B 79
    and    $03             ; 2C5C E6 03
    cp     $00             ; 2C5E FE 00
    jr     z,$2c79         ; 2C60 28 17
    ld     h,(ix+$09)      ; 2C62 DD 66 09
    ld     l,(ix+$08)      ; 2C65 DD 6E 08
    call   $1ff3           ; 2C68 CD F3 1F
    ld     (ix+$09),h      ; 2C6B DD 74 09
    ld     (ix+$08),l      ; 2C6E DD 75 08
    ld     (ix+$0c),$00    ; 2C71 DD 36 0C 00
    ld     (ix+$0d),$00    ; 2C75 DD 36 0D 00
    call   $2c92           ; 2C79 CD 92 2C
    call   $2cf3           ; 2C7C CD F3 2C
    ld     hl,$2f6e        ; 2C7F 21 6E 2F
    call   $2b10           ; 2C82 CD 10 2B
    ld     (iy+$00),h      ; 2C85 FD 74 00
    ld     (iy+$01),l      ; 2C88 FD 75 01
    call   $2aaa           ; 2C8B CD AA 2A
    call   $2b89           ; 2C8E CD 89 2B
    ret                    ; 2C91 C9
    push   bc              ; 2C92 C5
    push   de              ; 2C93 D5
    push   hl              ; 2C94 E5
    ld     a,(ix+$01)      ; 2C95 DD 7E 01
    cp     $04             ; 2C98 FE 04
    jr     z,$2cc2         ; 2C9A 28 26
    ld     h,(ix+$09)      ; 2C9C DD 66 09
    ld     l,(ix+$08)      ; 2C9F DD 6E 08
    ld     d,(ix+$0d)      ; 2CA2 DD 56 0D
    ld     e,(ix+$0c)      ; 2CA5 DD 5E 0C
    add    hl,de           ; 2CA8 19
    ld     b,h             ; 2CA9 44
    ld     c,l             ; 2CAA 4D
    call   $1ff0           ; 2CAB CD F0 1F
    ld     de,($825c)      ; 2CAE ED 5B 5C 82
    sbc    hl,de           ; 2CB2 ED 52
    ld     a,h             ; 2CB4 7C
    or     l               ; 2CB5 B5
    jr     z,$2cbc         ; 2CB6 28 04
    bit    7,h             ; 2CB8 CB 7C
    jr     z,$2cc2         ; 2CBA 28 06
    ld     (ix+$09),b      ; 2CBC DD 70 09
    ld     (ix+$08),c      ; 2CBF DD 71 08
    ld     a,(ix+$01)      ; 2CC2 DD 7E 01
    cp     $03             ; 2CC5 FE 03
    jr     z,$2cef         ; 2CC7 28 26
    ld     h,(ix+$0b)      ; 2CC9 DD 66 0B
    ld     l,(ix+$0a)      ; 2CCC DD 6E 0A
    ld     d,(ix+$0f)      ; 2CCF DD 56 0F
    ld     e,(ix+$0e)      ; 2CD2 DD 5E 0E
    add    hl,de           ; 2CD5 19
    ld     b,h             ; 2CD6 44
    ld     c,l             ; 2CD7 4D
    call   $1ff0           ; 2CD8 CD F0 1F
    ld     de,($825c)      ; 2CDB ED 5B 5C 82
    sbc    hl,de           ; 2CDF ED 52
    ld     a,h             ; 2CE1 7C
    or     l               ; 2CE2 B5
    jr     z,$2ce9         ; 2CE3 28 04
    bit    7,h             ; 2CE5 CB 7C
    jr     z,$2cef         ; 2CE7 28 06
    ld     (ix+$0b),b      ; 2CE9 DD 70 0B
    ld     (ix+$0a),c      ; 2CEC DD 71 0A
    pop    de              ; 2CEF D1
    pop    hl              ; 2CF0 E1
    pop    bc              ; 2CF1 C1
    ret                    ; 2CF2 C9
    ld     a,($808d)       ; 2CF3 3A 8D 80
    cp     $00             ; 2CF6 FE 00
    ret    nz              ; 2CF8 C0
    push   hl              ; 2CF9 E5
    push   de              ; 2CFA D5
    ld     h,(ix+$04)      ; 2CFB DD 66 04
    ld     l,(ix+$06)      ; 2CFE DD 6E 06
    ld     d,(ix+$09)      ; 2D01 DD 56 09
    ld     e,(ix+$08)      ; 2D04 DD 5E 08
    add    hl,de           ; 2D07 19
    ld     (ix+$04),h      ; 2D08 DD 74 04
    ld     (ix+$06),l      ; 2D0B DD 75 06
    ld     h,(ix+$05)      ; 2D0E DD 66 05
    ld     l,(ix+$07)      ; 2D11 DD 6E 07
    ld     d,(ix+$0b)      ; 2D14 DD 56 0B
    ld     e,(ix+$0a)      ; 2D17 DD 5E 0A
    add    hl,de           ; 2D1A 19
    ld     (ix+$05),h      ; 2D1B DD 74 05
    ld     (ix+$07),l      ; 2D1E DD 75 07
    pop    de              ; 2D21 D1
    pop    hl              ; 2D22 E1
    ret                    ; 2D23 C9
    ld     a,(ix+$01)      ; 2D24 DD 7E 01
    ld     (iy+$00),a      ; 2D27 FD 77 00
    ld     a,(ix+$02)      ; 2D2A DD 7E 02
    ld     (iy+$01),a      ; 2D2D FD 77 01
    ld     a,(ix+$03)      ; 2D30 DD 7E 03
    ld     (iy+$02),a      ; 2D33 FD 77 02
    ld     a,(ix+$04)      ; 2D36 DD 7E 04
    ld     (iy+$03),a      ; 2D39 FD 77 03
    ret                    ; 2D3C C9
    ld     b,(ix+$04)      ; 2D3D DD 46 04
    ld     c,(ix+$05)      ; 2D40 DD 4E 05
    call   $5ff2           ; 2D43 CD F2 5F
    cp     $00             ; 2D46 FE 00
    jp     z,$2db4         ; 2D48 CA B4 2D
    ld     c,a             ; 2D4B 4F
    ld     d,$00           ; 2D4C 16 00
    ld     e,(ix+$04)      ; 2D4E DD 5E 04
    ld     a,($8502)       ; 2D51 3A 02 85
    ld     h,$00           ; 2D54 26 00
    ld     l,a             ; 2D56 6F
    or     a               ; 2D57 B7
    sbc    hl,de           ; 2D58 ED 52
    ld     a,($8264)       ; 2D5A 3A 64 82
    ld     d,h             ; 2D5D 54
    ld     e,l             ; 2D5E 5D
    cp     $00             ; 2D5F FE 00
    jr     z,$2d67         ; 2D61 28 04
    add    hl,de           ; 2D63 19
    dec    a               ; 2D64 3D
    jr     $2d5f           ; 2D65 18 F8
    bit    7,h             ; 2D67 CB 7C
    jr     z,$2d6f         ; 2D69 28 04
    bit    1,c             ; 2D6B CB 49
    jr     nz,$2d77        ; 2D6D 20 08
    bit    7,h             ; 2D6F CB 7C
    jr     nz,$2d7a        ; 2D71 20 07
    bit    0,c             ; 2D73 CB 41
    jr     z,$2d7a         ; 2D75 28 03
    call   $1ff3           ; 2D77 CD F3 1F
    ld     (ix+$08),l      ; 2D7A DD 75 08
    ld     (ix+$09),h      ; 2D7D DD 74 09
    ld     d,$00           ; 2D80 16 00
    ld     e,(ix+$05)      ; 2D82 DD 5E 05
    ld     a,($8503)       ; 2D85 3A 03 85
    ld     h,$00           ; 2D88 26 00
    ld     l,a             ; 2D8A 6F
    or     a               ; 2D8B B7
    sbc    hl,de           ; 2D8C ED 52
    ld     a,($8264)       ; 2D8E 3A 64 82
    ld     d,h             ; 2D91 54
    ld     e,l             ; 2D92 5D
    cp     $00             ; 2D93 FE 00
    jr     z,$2d9b         ; 2D95 28 04
    add    hl,de           ; 2D97 19
    dec    a               ; 2D98 3D
    jr     $2d93           ; 2D99 18 F8
    bit    7,h             ; 2D9B CB 7C
    jr     z,$2da3         ; 2D9D 28 04
    bit    3,c             ; 2D9F CB 59
    jr     nz,$2dab        ; 2DA1 20 08
    bit    7,h             ; 2DA3 CB 7C
    jr     nz,$2dae        ; 2DA5 20 07
    bit    2,c             ; 2DA7 CB 51
    jr     z,$2dae         ; 2DA9 28 03
    call   $1ff3           ; 2DAB CD F3 1F
    ld     (ix+$0a),l      ; 2DAE DD 75 0A
    ld     (ix+$0b),h      ; 2DB1 DD 74 0B
    call   $2cf3           ; 2DB4 CD F3 2C
    ld     hl,$2f6e        ; 2DB7 21 6E 2F
    call   $2b10           ; 2DBA CD 10 2B
    ld     (iy+$00),h      ; 2DBD FD 74 00
    ld     (iy+$01),l      ; 2DC0 FD 75 01
    call   $2aaa           ; 2DC3 CD AA 2A
    call   $2b89           ; 2DC6 CD 89 2B
    ret                    ; 2DC9 C9
    inc    d               ; 2DCA 14
    adc    a,c             ; 2DCB 89
    inc    e               ; 2DCC 1C
    adc    a,c             ; 2DCD 89
    inc    h               ; 2DCE 24
    adc    a,c             ; 2DCF 89
    inc    l               ; 2DD0 2C
    adc    a,c             ; 2DD1 89
    inc    (hl)            ; 2DD2 34
    adc    a,c             ; 2DD3 89
    ld     a,($4089)       ; 2DD4 3A 89 40
    adc    a,c             ; 2DD7 89
    ld     b,(hl)          ; 2DD8 46
    adc    a,c             ; 2DD9 89
    ld     c,h             ; 2DDA 4C
    adc    a,c             ; 2DDB 89
    ld     d,d             ; 2DDC 52
    adc    a,c             ; 2DDD 89
    ld     e,b             ; 2DDE 58
    adc    a,c             ; 2DDF 89
    ld     e,(hl)          ; 2DE0 5E
    adc    a,c             ; 2DE1 89
    ld     h,h             ; 2DE2 64
    adc    a,c             ; 2DE3 89
    ld     l,d             ; 2DE4 6A
    adc    a,c             ; 2DE5 89
    ld     (hl),b          ; 2DE6 70
    adc    a,c             ; 2DE7 89
    halt                   ; 2DE8 76
    adc    a,c             ; 2DE9 89

; Looks suspiciously like a string of attributes and graphics codes

    nop                    ; 2DEA 00
    ld     c,$c2           ; 2DEB 0E C2
    ld     c,$c0           ; 2DED 0E C0
    ld     c,$ff           ; 2DEF 0E FF
    rst    38h             ; 2DF1 FF
    ld     bc,$c30e        ; 2DF2 01 0E C3
    ld     c,$c1           ; 2DF5 0E C1
    ld     c,$ff           ; 2DF7 0E FF
    rst    38h             ; 2DF9 FF
    nop                    ; 2DFA 00
    djnz   $2dc3           ; 2DFB 10 C6
    ld     c,$c4           ; 2DFD 0E C4
    ld     c,$ff           ; 2DFF 0E FF
    rst    38h             ; 2E01 FF
    ld     bc,$c710        ; 2E02 01 10 C7
    ld     c,$c5           ; 2E05 0E C5
    ld     c,$ff           ; 2E07 0E FF
    rst    38h             ; 2E09 FF
    nop                    ; 2E0A 00
    dec    c               ; 2E0B 0D
    call   po,$ff0e        ; 2E0C E4 0E FF
    rst    38h             ; 2E0F FF
    ld     bc,$e40d        ; 2E10 01 0D E4
    ld     c,$ff           ; 2E13 0E FF
    rst    38h             ; 2E15 FF
    nop                    ; 2E16 00
    ld     (de),a          ; 2E17 12
    ret    c               ; 2E18 D8
    ld     c,$ff           ; 2E19 0E FF
    rst    38h             ; 2E1B FF
    ld     bc,$d812        ; 2E1C 01 12 D8
    ld     c,$ff           ; 2E1F 0E FF
    rst    38h             ; 2E21 FF
    nop                    ; 2E22 00
    inc    c               ; 2E23 0C
    nop                    ; 2E24 00
    ld     c,$ff           ; 2E25 0E FF
    rst    38h             ; 2E27 FF
    ld     bc,$000c        ; 2E28 01 0C 00
    ld     c,$ff           ; 2E2B 0E FF
    rst    38h             ; 2E2D FF
    nop                    ; 2E2E 00
    inc    de              ; 2E2F 13
    nop                    ; 2E30 00
    ld     c,$ff           ; 2E31 0E FF
    rst    38h             ; 2E33 FF
    ld     bc,$0013        ; 2E34 01 13 00
    ld     c,$ff           ; 2E37 0E FF
    rst    38h             ; 2E39 FF
    nop                    ; 2E3A 00
    dec    bc              ; 2E3B 0B
    nop                    ; 2E3C 00
    ld     c,$ff           ; 2E3D 0E FF
    rst    38h             ; 2E3F FF
    ld     bc,$000b        ; 2E40 01 0B 00
    ld     c,$ff           ; 2E43 0E FF
    rst    38h             ; 2E45 FF
    nop                    ; 2E46 00
    inc    d               ; 2E47 14
    nop                    ; 2E48 00
    ld     c,$ff           ; 2E49 0E FF
    rst    38h             ; 2E4B FF
    ld     bc,$0014        ; 2E4C 01 14 00
    ld     c,$ff           ; 2E4F 0E FF
    rst    38h             ; 2E51 FF
    add    a,$0e           ; 2E52 C6 0E
    call   nz,$c70e        ; 2E54 C4 0E C7
    ld     c,$c5           ; 2E57 0E C5
    ld     c,$ca           ; 2E59 0E CA
    ld     c,$c8           ; 2E5B 0E C8
    ld     c,$cb           ; 2E5D 0E CB
    ld     c,$c9           ; 2E5F 0E C9
    ld     c,$ce           ; 2E61 0E CE
    ld     c,$cc           ; 2E63 0E CC
    ld     c,$cf           ; 2E65 0E CF
    ld     c,$cd           ; 2E67 0E CD
    ld     c,$d2           ; 2E69 0E D2
    ld     c,$d0           ; 2E6B 0E D0
    ld     c,$d3           ; 2E6D 0E D3
    ld     c,$d1           ; 2E6F 0E D1
    ld     c,$d6           ; 2E71 0E D6
    ld     c,$d4           ; 2E73 0E D4
    ld     c,$d7           ; 2E75 0E D7
    ld     c,$d5           ; 2E77 0E D5
    ld     c,$e4           ; 2E79 0E E4
    ld     c,$d8           ; 2E7B 0E D8
    ld     c,$e5           ; 2E7D 0E E5
    ld     c,$d9           ; 2E7F 0E D9
    ld     c,$e6           ; 2E81 0E E6
    ld     c,$da           ; 2E83 0E DA
    ld     c,$e8           ; 2E85 0E E8
    ld     c,$dc           ; 2E87 0E DC
    ld     c,$e9           ; 2E89 0E E9
    ld     c,$dd           ; 2E8B 0E DD
    ld     c,$ea           ; 2E8D 0E EA
    ld     c,$de           ; 2E8F 0E DE
    ld     c,$eb           ; 2E91 0E EB
    ld     c,$df           ; 2E93 0E DF
    ld     c,$ec           ; 2E95 0E EC
    ld     c,$e0           ; 2E97 0E E0
    ld     c,$ed           ; 2E99 0E ED
    ld     c,$e1           ; 2E9B 0E E1
    ld     c,$ee           ; 2E9D 0E EE
    ld     c,$e2           ; 2E9F 0E E2
    ld     c,$ef           ; 2EA1 0E EF
    ld     c,$e3           ; 2EA3 0E E3
    ld     c,$07           ; 2EA5 0E 07
    ld     d,b             ; 2EA7 50
    inc    b               ; 2EA8 04
    ld     d,c             ; 2EA9 51
    inc    b               ; 2EAA 04
    ld     d,d             ; 2EAB 52
    inc    b               ; 2EAC 04
    ld     d,e             ; 2EAD 53
    inc    b               ; 2EAE 04
    rst    38h             ; 2EAF FF
    rst    38h             ; 2EB0 FF
    rst    38h             ; 2EB1 FF
    rst    38h             ; 2EB2 FF
    rst    38h             ; 2EB3 FF
    rst    38h             ; 2EB4 FF
    rst    38h             ; 2EB5 FF
    rst    38h             ; 2EB6 FF
    rst    38h             ; 2EB7 FF
    rst    38h             ; 2EB8 FF
    rst    38h             ; 2EB9 FF
    rlca                   ; 2EBA 07
    ld     d,b             ; 2EBB 50
    add    a,h             ; 2EBC 84
    ld     d,c             ; 2EBD 51
    add    a,h             ; 2EBE 84
    ld     d,d             ; 2EBF 52
    add    a,h             ; 2EC0 84
    ld     d,e             ; 2EC1 53
    add    a,h             ; 2EC2 84
    rst    38h             ; 2EC3 FF
    rst    38h             ; 2EC4 FF
    rst    38h             ; 2EC5 FF
    rst    38h             ; 2EC6 FF
    rst    38h             ; 2EC7 FF
    rst    38h             ; 2EC8 FF
    rst    38h             ; 2EC9 FF
    rst    38h             ; 2ECA FF
    rst    38h             ; 2ECB FF
    rst    38h             ; 2ECC FF
    rst    38h             ; 2ECD FF
    ld     (bc),a          ; 2ECE 02
    ld     h,a             ; 2ECF 67
    inc    b               ; 2ED0 04
    ld     l,b             ; 2ED1 68
    inc    b               ; 2ED2 04
    ld     l,c             ; 2ED3 69
    inc    b               ; 2ED4 04
    ld     l,d             ; 2ED5 6A
    inc    b               ; 2ED6 04
    ld     l,e             ; 2ED7 6B
    inc    b               ; 2ED8 04
    rst    38h             ; 2ED9 FF
    rst    38h             ; 2EDA FF
    rst    38h             ; 2EDB FF
    rst    38h             ; 2EDC FF
    rst    38h             ; 2EDD FF
    rst    38h             ; 2EDE FF
    rst    38h             ; 2EDF FF
    rst    38h             ; 2EE0 FF
    rst    38h             ; 2EE1 FF
    ld     (bc),a          ; 2EE2 02
    ld     h,a             ; 2EE3 67
    inc    b               ; 2EE4 04
    ld     l,b             ; 2EE5 68
    inc    b               ; 2EE6 04
    ld     l,c             ; 2EE7 69
    inc    b               ; 2EE8 04
    ld     l,d             ; 2EE9 6A
    inc    b               ; 2EEA 04
    ld     l,e             ; 2EEB 6B
    inc    b               ; 2EEC 04
    rst    38h             ; 2EED FF
    rst    38h             ; 2EEE FF
    rst    38h             ; 2EEF FF
    rst    38h             ; 2EF0 FF
    rst    38h             ; 2EF1 FF
    rst    38h             ; 2EF2 FF
    rst    38h             ; 2EF3 FF
    rst    38h             ; 2EF4 FF
    rst    38h             ; 2EF5 FF
    inc    bc              ; 2EF6 03
    ld     d,h             ; 2EF7 54
    inc    b               ; 2EF8 04
    ld     d,l             ; 2EF9 55
    inc    b               ; 2EFA 04
    ld     d,(hl)          ; 2EFB 56
    inc    b               ; 2EFC 04
    ld     d,a             ; 2EFD 57
    inc    b               ; 2EFE 04
    ld     e,b             ; 2EFF 58
    inc    b               ; 2F00 04
    ld     e,c             ; 2F01 59
    inc    b               ; 2F02 04
    ld     e,d             ; 2F03 5A
    inc    b               ; 2F04 04
    rst    38h             ; 2F05 FF
    rst    38h             ; 2F06 FF
    rst    38h             ; 2F07 FF
    rst    38h             ; 2F08 FF
    rst    38h             ; 2F09 FF
    inc    bc              ; 2F0A 03
    ld     e,d             ; 2F0B 5A
    inc    b               ; 2F0C 04
    ld     e,c             ; 2F0D 59
    inc    b               ; 2F0E 04
    ld     e,b             ; 2F0F 58
    inc    b               ; 2F10 04
    ld     d,a             ; 2F11 57
    inc    b               ; 2F12 04
    ld     d,(hl)          ; 2F13 56
    inc    b               ; 2F14 04
    ld     d,l             ; 2F15 55
    inc    b               ; 2F16 04
    ld     d,h             ; 2F17 54
    inc    b               ; 2F18 04
    rst    38h             ; 2F19 FF
    rst    38h             ; 2F1A FF
    rst    38h             ; 2F1B FF
    rst    38h             ; 2F1C FF
    rst    38h             ; 2F1D FF
    ld     b,$38           ; 2F1E 06 38
    inc    b               ; 2F20 04
    add    hl,sp           ; 2F21 39
    inc    b               ; 2F22 04
    ld     a,($3b04)       ; 2F23 3A 04 3B
    inc    b               ; 2F26 04
    rst    38h             ; 2F27 FF
    rst    38h             ; 2F28 FF
    rst    38h             ; 2F29 FF
    rst    38h             ; 2F2A FF
    rst    38h             ; 2F2B FF
    rst    38h             ; 2F2C FF
    rst    38h             ; 2F2D FF
    rst    38h             ; 2F2E FF
    rst    38h             ; 2F2F FF
    rst    38h             ; 2F30 FF
    rst    38h             ; 2F31 FF
    ld     b,$38           ; 2F32 06 38
    add    a,h             ; 2F34 84
    add    hl,sp           ; 2F35 39
    add    a,h             ; 2F36 84
    ld     a,($3b84)       ; 2F37 3A 84 3B
    add    a,h             ; 2F3A 84
    rst    38h             ; 2F3B FF
    rst    38h             ; 2F3C FF
    add    hl,sp           ; 2F3D 39
    inc    b               ; 2F3E 04
    rst    38h             ; 2F3F FF
    rst    38h             ; 2F40 FF
    rst    38h             ; 2F41 FF
    rst    38h             ; 2F42 FF
    rst    38h             ; 2F43 FF
    rst    38h             ; 2F44 FF
    rst    38h             ; 2F45 FF
    ld     b,$60           ; 2F46 06 60
    inc    b               ; 2F48 04
    ld     h,c             ; 2F49 61
    inc    b               ; 2F4A 04
    ld     h,d             ; 2F4B 62
    inc    b               ; 2F4C 04
    ld     h,e             ; 2F4D 63
    inc    b               ; 2F4E 04
    ld     h,h             ; 2F4F 64
    inc    b               ; 2F50 04
    ld     h,l             ; 2F51 65
    inc    b               ; 2F52 04
    rst    38h             ; 2F53 FF
    rst    38h             ; 2F54 FF
    rst    38h             ; 2F55 FF
    rst    38h             ; 2F56 FF
    rst    38h             ; 2F57 FF
    rst    38h             ; 2F58 FF
    rst    38h             ; 2F59 FF
    ld     b,$65           ; 2F5A 06 65
    inc    b               ; 2F5C 04
    ld     h,h             ; 2F5D 64
    inc    b               ; 2F5E 04
    ld     h,e             ; 2F5F 63
    inc    b               ; 2F60 04
    ld     h,d             ; 2F61 62
    inc    b               ; 2F62 04
    ld     h,c             ; 2F63 61
    inc    b               ; 2F64 04
    ld     h,b             ; 2F65 60
    inc    b               ; 2F66 04
    rst    38h             ; 2F67 FF
    rst    38h             ; 2F68 FF
    rst    38h             ; 2F69 FF
    rst    38h             ; 2F6A FF
    rst    38h             ; 2F6B FF
    rst    38h             ; 2F6C FF
    rst    38h             ; 2F6D FF
    nop                    ; 2F6E 00
    nop                    ; 2F6F 00
    nop                    ; 2F70 00
    nop                    ; 2F71 00
    nop                    ; 2F72 00
    nop                    ; 2F73 00
    nop                    ; 2F74 00
    nop                    ; 2F75 00
    nop                    ; 2F76 00
    nop                    ; 2F77 00
    nop                    ; 2F78 00
    nop                    ; 2F79 00
    nop                    ; 2F7A 00
    nop                    ; 2F7B 00
    nop                    ; 2F7C 00
    nop                    ; 2F7D 00
    nop                    ; 2F7E 00
    ld     bc,$0200        ; 2F7F 01 00 02
    nop                    ; 2F82 00
    inc    bc              ; 2F83 03
    nop                    ; 2F84 00
    dec    b               ; 2F85 05
    nop                    ; 2F86 00
    ex     af,af'          ; 2F87 08
    nop                    ; 2F88 00
    ld     (de),a          ; 2F89 12
    nop                    ; 2F8A 00
    jr     nz,$2f8d        ; 2F8B 20 00
    djnz   $2f8f           ; 2F8D 10 00
    jr     nc,$2f91        ; 2F8F 30 00
    ld     d,b             ; 2F91 50

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 2F92
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 2FA2
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 2FB2
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 2FC2
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00       ; 2FD2

    jp     $28e4           ; 2FE0 C3 E4 28
    jp     $28fc           ; 2FE3 C3 FC 28
    jp     $2906           ; 2FE6 C3 06 29
    jp     $2205           ; 2FE9 C3 05 22
    jp     $ffff           ; 2FEC C3 FF FF
    nop                    ; 2FEF 00
    jp     $2a1d           ; 2FF0 C3 1D 2A
    jp     $2ba4           ; 2FF3 C3 A4 2B
    jp     $2d3d           ; 2FF6 C3 3D 2D
    jp     $24a9           ; 2FF9 C3 A9 24
    nop                    ; 2FFC 00
    nop                    ; 2FFD 00
    nop                    ; 2FFE 00
    nop                    ; 2FFF 00
    ld     a,($8088)       ; 3000 3A 88 80
    cp     $03             ; 3003 FE 03
    jp     z,$3083         ; 3005 CA 83 30
    ld     a,($808e)       ; 3008 3A 8E 80
    or     a               ; 300B B7
    jp     nz,$3083        ; 300C C2 83 30
    call   $3c8f           ; 300F CD 8F 3C
    ld     a,($8028)       ; 3012 3A 28 80
    bit    7,a             ; 3015 CB 7F
    jp     nz,$303a        ; 3017 C2 3A 30
    ld     a,($808d)       ; 301A 3A 8D 80
    or     a               ; 301D B7
    jp     nz,$303a        ; 301E C2 3A 30
    call   $3183           ; 3021 CD 83 31
    call   $321a           ; 3024 CD 1A 32
    call   $335a           ; 3027 CD 5A 33
    ld     a,$01           ; 302A 3E 01
    call   $3d81           ; 302C CD 81 3D
    call   nz,$3d44        ; 302F C4 44 3D
    ld     a,$20           ; 3032 3E 20
    call   $3d81           ; 3034 CD 81 3D
    call   nz,$3cc8        ; 3037 C4 C8 3C
    ld     ix,$8278        ; 303A DD 21 78 82
    ld     iy,$8518        ; 303E FD 21 18 85
    ld     de,$0019        ; 3042 11 19 00
    ld     b,$09           ; 3045 06 09
    ld     c,$00           ; 3047 0E 00
    push   bc              ; 3049 C5
    push   de              ; 304A D5
    ld     a,(ix+$00)      ; 304B DD 7E 00
    or     a               ; 304E B7
    jp     z,$305e         ; 304F CA 5E 30
    rra                    ; 3052 1F
    inc    c               ; 3053 0C
    jp     nc,$3052        ; 3054 D2 52 30
    ld     hl,$3086        ; 3057 21 86 30
    ld     a,c             ; 305A 79
    call   $00ea           ; 305B CD EA 00
    pop    de              ; 305E D1
    pop    bc              ; 305F C1
    add    ix,de           ; 3060 DD 19
    push   de              ; 3062 D5
    ld     de,$0008        ; 3063 11 08 00
    add    iy,de           ; 3066 FD 19
    pop    de              ; 3068 D1
    djnz   $3049           ; 3069 10 DE
    call   $6000           ; 306B CD 00 60
    ld     a,($808d)       ; 306E 3A 8D 80
    or     a               ; 3071 B7
    jp     z,$3083         ; 3072 CA 83 30
    ld     a,($8028)       ; 3075 3A 28 80
    bit    2,a             ; 3078 CB 57
    jp     nz,$3083        ; 307A C2 83 30
    ld     a,$00           ; 307D 3E 00
    ld     ($824b),a       ; 307F 32 4B 82
    ret                    ; 3082 C9
    jp     $00c3           ; 3083 C3 C3 00
    add    a,c             ; 3086 81
    ld     sp,$3098        ; 3087 31 98 30
    add    a,c             ; 308A 81
    ld     sp,$30ef        ; 308B 31 EF 30
    add    a,c             ; 308E 81
    ld     sp,$3181        ; 308F 31 81 31
    dec    (hl)            ; 3092 35
    ld     sp,$3181        ; 3093 31 81 31
    add    a,c             ; 3096 81
    ld     sp,$7edd        ; 3097 31 DD 7E
    ld     bc,$cf21        ; 309A 01 21 CF
    jr     nc,$306c        ; 309D 30 CD
    jp     pe,$dd00        ; 309F EA 00 DD
    ld     a,(hl)          ; 30A2 7E
    ld     bc,$0cfe        ; 30A3 01 FE 0C
    jp     nc,$30ce        ; 30A6 D2 CE 30
    ld     a,(ix+$01)      ; 30A9 DD 7E 01
    cp     $01             ; 30AC FE 01
    jp     z,$30ce         ; 30AE CA CE 30
    ld     a,(ix+$01)      ; 30B1 DD 7E 01
    cp     $08             ; 30B4 FE 08
    jp     z,$30ce         ; 30B6 CA CE 30
    ld     a,(ix+$01)      ; 30B9 DD 7E 01
    cp     $07             ; 30BC FE 07
    jp     z,$30ce         ; 30BE CA CE 30
    ld     a,($824a)       ; 30C1 3A 4A 82
    or     a               ; 30C4 B7
    jp     nz,$30ce        ; 30C5 C2 CE 30
    ld     hl,$3dbb        ; 30C8 21 BB 3D
    call   $3607           ; 30CB CD 07 36
    ret                    ; 30CE C9
    add    a,c             ; 30CF 81
    ld     sp,$321c        ; 30D0 31 1C 32
    ld     (hl),e          ; 30D3 73
    ld     ($3181),a       ; 30D4 32 81 31
    djnz   $310c           ; 30D7 10 33
    add    a,c             ; 30D9 81
    ld     sp,$3181        ; 30DA 31 81 31
    dec    e               ; 30DD 1D
    inc    sp              ; 30DE 33
    ld     h,(hl)          ; 30DF 66
    ld     ($3181),a       ; 30E0 32 81 31
    add    a,c             ; 30E3 81
    ld     sp,$3181        ; 30E4 31 81 31
    adc    a,$39           ; 30E7 CE 39
    rra                    ; 30E9 1F
    ld     a,($3ac0)       ; 30EA 3A C0 3A
    add    a,c             ; 30ED 81
    ld     sp,$dd00        ; 30EE 31 00 DD
    ld     a,(hl)          ; 30F1 7E
    ld     bc,$1521        ; 30F2 01 21 15
    ld     sp,$eacd        ; 30F5 31 CD EA
    nop                    ; 30F8 00
    ld     a,(ix+$01)      ; 30F9 DD 7E 01
    cp     $0c             ; 30FC FE 0C
    jp     nc,$3114        ; 30FE D2 14 31
    call   $34cb           ; 3101 CD CB 34
    call   $3b32           ; 3104 CD 32 3B
    ld     a,($824a)       ; 3107 3A 4A 82
    or     a               ; 310A B7
    jp     nz,$3114        ; 310B C2 14 31
    ld     hl,$3dc3        ; 310E 21 C3 3D
    call   $3607           ; 3111 CD 07 36
    ret                    ; 3114 C9
    add    a,c             ; 3115 81
    ld     sp,$3181        ; 3116 31 81 31
    jp     po,$6333        ; 3119 E2 33 63
    inc    (hl)            ; 311C 34
    add    a,c             ; 311D 81
    ld     sp,$3181        ; 311E 31 81 31
    add    a,c             ; 3121 81
    ld     sp,$3181        ; 3122 31 81 31
    add    a,c             ; 3125 81
    ld     sp,$3181        ; 3126 31 81 31
    add    a,c             ; 3129 81
    ld     sp,$3181        ; 312A 31 81 31
    adc    a,$39           ; 312D CE 39
    rra                    ; 312F 1F
    ld     a,($3ac0)       ; 3130 3A C0 3A
    add    a,c             ; 3133 81
    ld     sp,$dd00        ; 3134 31 00 DD
    ld     a,(hl)          ; 3137 7E
    ld     bc,$6121        ; 3138 01 21 61
    ld     sp,$eacd        ; 313B 31 CD EA
    nop                    ; 313E 00
    ld     a,(ix+$01)      ; 313F DD 7E 01
    cp     $0c             ; 3142 FE 0C
    jr     nc,$3160        ; 3144 30 1A
    ld     a,($824a)       ; 3146 3A 4A 82
    or     a               ; 3149 B7
    jp     nz,$3160        ; 314A C2 60 31
    ld     a,(ix+$13)      ; 314D DD 7E 13
    or     a               ; 3150 B7
    jp     z,$315a         ; 3151 CA 5A 31
    dec    (ix+$13)        ; 3154 DD 35 13
    jp     $3160           ; 3157 C3 60 31
    ld     hl,$3dc7        ; 315A 21 C7 3D
    call   $3607           ; 315D CD 07 36
    ret                    ; 3160 C9
    add    a,c             ; 3161 81
    ld     sp,$356b        ; 3162 31 6B 35
    jp     p,$f935         ; 3165 F2 35 F9
    dec    (hl)            ; 3168 35
    ld     sp,hl           ; 3169 F9
    dec    (hl)            ; 316A 35
    ld     sp,hl           ; 316B F9
    dec    (hl)            ; 316C 35
    nop                    ; 316D 00
    ld     (hl),$81        ; 316E 36 81
    ld     sp,$3181        ; 3170 31 81 31
    add    a,c             ; 3173 81
    ld     sp,$3181        ; 3174 31 81 31
    add    a,c             ; 3177 81
    ld     sp,$39ce        ; 3178 31 CE 39
    rra                    ; 317B 1F
    ld     a,($3ac0)       ; 317C 3A C0 3A
    add    a,c             ; 317F 81
    ld     sp,$c900        ; 3180 31 00 C9
    ld     hl,$80d2        ; 3183 21 D2 80
    ld     a,(hl)          ; 3186 7E
    inc    hl              ; 3187 23
    cp     (hl)            ; 3188 BE
    jp     z,$3219         ; 3189 CA 19 32
    ld     hl,($80da)      ; 318C 2A DA 80
    dec    hl              ; 318F 2B
    ld     ($80da),hl      ; 3190 22 DA 80
    ld     a,l             ; 3193 7D
    or     h               ; 3194 B4
    jp     nz,$3219        ; 3195 C2 19 32
    ld     a,($80d3)       ; 3198 3A D3 80
    inc    a               ; 319B 3C
    ld     ($80d3),a       ; 319C 32 D3 80
    ld     hl,($80d4)      ; 319F 2A D4 80
    ld     ($80da),hl      ; 31A2 22 DA 80
    ld     ix,$82aa        ; 31A5 DD 21 AA 82
    ld     b,$07           ; 31A9 06 07
    call   $38b7           ; 31AB CD B7 38
    or     a               ; 31AE B7
    jp     nz,$3219        ; 31AF C2 19 32
    ld     hl,$3dcf        ; 31B2 21 CF 3D
    call   $3867           ; 31B5 CD 67 38
    ld     a,($802b)       ; 31B8 3A 2B 80
    sub    $78             ; 31BB D6 78
    jr     c,$31c8         ; 31BD 38 09
    ld     a,(ix+$04)      ; 31BF DD 7E 04
    cp     $78             ; 31C2 FE 78
    jr     nc,$31b2        ; 31C4 30 EC
    jr     $31cf           ; 31C6 18 07
    ld     a,(ix+$04)      ; 31C8 DD 7E 04
    cp     $78             ; 31CB FE 78
    jr     c,$31b2         ; 31CD 38 E3
    ld     a,(ix+$04)      ; 31CF DD 7E 04
    add    a,$f8           ; 31D2 C6 F8
    ld     (ix+$04),a      ; 31D4 DD 77 04
    ld     a,(ix+$05)      ; 31D7 DD 7E 05
    add    a,$f8           ; 31DA C6 F8
    ld     (ix+$05),a      ; 31DC DD 77 05
    ld     (ix+$00),$01    ; 31DF DD 36 00 01
    ld     (ix+$01),$01    ; 31E3 DD 36 01 01
    ld     (ix+$03),$0f    ; 31E7 DD 36 03 0F
    set    5,(ix+$03)      ; 31EB DD CB 03 EE
    ld     hl,$0080        ; 31EF 21 80 00
    ld     (ix+$08),l      ; 31F2 DD 75 08
    ld     (ix+$09),h      ; 31F5 DD 74 09
    ld     (ix+$0e),$10    ; 31F8 DD 36 0E 10
    ld     (ix+$0f),$00    ; 31FC DD 36 0F 00
    ld     (ix+$14),$01    ; 3200 DD 36 14 01
    ld     hl,($80d6)      ; 3204 2A D6 80
    ld     (ix+$16),l      ; 3207 DD 75 16
    ld     (ix+$17),h      ; 320A DD 74 17
    ld     a,($808d)       ; 320D 3A 8D 80
    or     a               ; 3210 B7
    jp     nz,$3219        ; 3211 C2 19 32
    ld     a,$15           ; 3214 3E 15
    call   $00fc           ; 3216 CD FC 00
    ret                    ; 3219 C9
    nop                    ; 321A 00
    ret                    ; 321B C9
    nop                    ; 321C 00
    dec    (ix+$14)        ; 321D DD 35 14
    jp     nz,$3265        ; 3220 C2 65 32
    ld     (ix+$14),$06    ; 3223 DD 36 14 06
    ld     hl,$3e73        ; 3227 21 73 3E
    ld     bc,$0001        ; 322A 01 01 00
    ld     a,(ix+$10)      ; 322D DD 7E 10
    call   $00f6           ; 3230 CD F6 00
    ld     a,(hl)          ; 3233 7E
    cp     $ff             ; 3234 FE FF
    jp     z,$3241         ; 3236 CA 41 32
    ld     (ix+$02),a      ; 3239 DD 77 02
    inc    (ix+$10)        ; 323C DD 34 10
    jr     $3265           ; 323F 18 24
    ld     (ix+$02),$04    ; 3241 DD 36 02 04
    ld     (ix+$03),$04    ; 3245 DD 36 03 04
    ld     (ix+$10),$00    ; 3249 DD 36 10 00
    ld     (ix+$14),$1e    ; 324D DD 36 14 1E
    ld     (ix+$01),$08    ; 3251 DD 36 01 08
    ld     a,(ix+$04)      ; 3255 DD 7E 04
    add    a,$08           ; 3258 C6 08
    ld     (ix+$04),a      ; 325A DD 77 04
    ld     a,(ix+$05)      ; 325D DD 7E 05
    add    a,$08           ; 3260 C6 08
    ld     (ix+$05),a      ; 3262 DD 77 05
    ret                    ; 3265 C9
    dec    (ix+$14)        ; 3266 DD 35 14
    jp     nz,$326f        ; 3269 C2 6F 32
    call   $3baf           ; 326C CD AF 3B
    call   $32d6           ; 326F CD D6 32
    ret                    ; 3272 C9
    ld     l,(ix+$16)      ; 3273 DD 6E 16
    ld     h,(ix+$17)      ; 3276 DD 66 17
    ld     a,h             ; 3279 7C
    or     l               ; 327A B5
    jp     z,$3285         ; 327B CA 85 32
    dec    hl              ; 327E 2B
    ld     (ix+$16),l      ; 327F DD 75 16
    ld     (ix+$17),h      ; 3282 DD 74 17
    call   $3633           ; 3285 CD 33 36
    ld     a,(ix+$01)      ; 3288 DD 7E 01
    cp     $02             ; 328B FE 02
    jp     nz,$3293        ; 328D C2 93 32
    call   $6009           ; 3290 CD 09 60
    call   $32d6           ; 3293 CD D6 32
    call   $3728           ; 3296 CD 28 37
    ld     a,(ix+$05)      ; 3299 DD 7E 05
    cp     $d0             ; 329C FE D0
    jp     c,$32d5         ; 329E DA D5 32
    ld     (ix+$01),$07    ; 32A1 DD 36 01 07
    ld     (ix+$02),$bc    ; 32A5 DD 36 02 BC
    ld     (ix+$03),$0f    ; 32A9 DD 36 03 0F
    ld     a,(ix+$04)      ; 32AD DD 7E 04
    add    a,$f8           ; 32B0 C6 F8
    ld     (ix+$04),a      ; 32B2 DD 77 04
    ld     a,(ix+$05)      ; 32B5 DD 7E 05
    add    a,$f8           ; 32B8 C6 F8
    ld     (ix+$05),a      ; 32BA DD 77 05
    set    5,(ix+$03)      ; 32BD DD CB 03 EE
    ld     (ix+$10),$00    ; 32C1 DD 36 10 00
    ld     (ix+$14),$01    ; 32C5 DD 36 14 01
    ld     a,($808d)       ; 32C9 3A 8D 80
    or     a               ; 32CC B7
    jp     nz,$32d5        ; 32CD C2 D5 32
    ld     a,$2d           ; 32D0 3E 2D
    call   $00fc           ; 32D2 CD FC 00
    ret                    ; 32D5 C9
    ld     a,($824b)       ; 32D6 3A 4B 82
    or     a               ; 32D9 B7
    jp     nz,$330f        ; 32DA C2 0F 33
    ld     a,($8028)       ; 32DD 3A 28 80
    bit    7,a             ; 32E0 CB 7F
    jp     z,$330f         ; 32E2 CA 0F 33
    push   ix              ; 32E5 DD E5
    push   ix              ; 32E7 DD E5
    pop    hl              ; 32E9 E1
    ld     de,$00e1        ; 32EA 11 E1 00
    add    ix,de           ; 32ED DD 19
    push   ix              ; 32EF DD E5
    pop    de              ; 32F1 D1
    ld     bc,$0019        ; 32F2 01 19 00
    ldir                   ; 32F5 ED B0
    pop    ix              ; 32F7 DD E1
    ld     (ix+$01),$0e    ; 32F9 DD 36 01 0E
    ld     a,$22           ; 32FD 3E 22
    ld     (ix+$03),$0b    ; 32FF DD 36 03 0B
    ld     (ix+$02),$3f    ; 3303 DD 36 02 3F
    ld     (iy+$04),$00    ; 3307 FD 36 04 00
    ld     (iy+$05),$00    ; 330B FD 36 05 00
    ret                    ; 330F C9
    call   $6003           ; 3310 CD 03 60
    call   $36a9           ; 3313 CD A9 36
    call   $32d6           ; 3316 CD D6 32
    call   $377a           ; 3319 CD 7A 37
    ret                    ; 331C C9
    dec    (ix+$14)        ; 331D DD 35 14
    jp     nz,$3359        ; 3320 C2 59 33
    ld     (ix+$14),$06    ; 3323 DD 36 14 06
    ld     hl,$3e73        ; 3327 21 73 3E
    ld     bc,$0001        ; 332A 01 01 00
    ld     a,(ix+$10)      ; 332D DD 7E 10
    call   $00f6           ; 3330 CD F6 00
    ld     a,(hl)          ; 3333 7E
    cp     $ff             ; 3334 FE FF
    jp     z,$3341         ; 3336 CA 41 33
    ld     (ix+$02),a      ; 3339 DD 77 02
    inc    (ix+$10)        ; 333C DD 34 10
    jr     $3359           ; 333F 18 18
    ld     (ix+$00),$20    ; 3341 DD 36 00 20
    ld     (ix+$01),$01    ; 3345 DD 36 01 01
    ld     a,(ix+$04)      ; 3349 DD 7E 04
    add    a,$08           ; 334C C6 08
    ld     (ix+$04),a      ; 334E DD 77 04
    ld     a,(ix+$05)      ; 3351 DD 7E 05
    add    a,$08           ; 3354 C6 08
    ld     (ix+$05),a      ; 3356 DD 77 05
    ret                    ; 3359 C9
    ld     hl,$80c0        ; 335A 21 C0 80
    ld     a,(hl)          ; 335D 7E
    inc    hl              ; 335E 23
    cp     (hl)            ; 335F BE
    jp     z,$33e1         ; 3360 CA E1 33
    ld     b,a             ; 3363 47
    push   bc              ; 3364 C5
    ld     ix,$8278        ; 3365 DD 21 78 82
    ld     b,$02           ; 3369 06 02
    call   $38b7           ; 336B CD B7 38
    or     a               ; 336E B7
    jp     nz,$33de        ; 336F C2 DE 33
    ld     (ix+$00),$04    ; 3372 DD 36 00 04
    ld     (ix+$01),$02    ; 3376 DD 36 01 02
    ld     (ix+$02),$29    ; 337A DD 36 02 29
    ld     (ix+$03),$84    ; 337E DD 36 03 84
    ld     (ix+$0d),$01    ; 3382 DD 36 0D 01
    ld     (ix+$0e),$00    ; 3386 DD 36 0E 00
    ld     (ix+$12),$01    ; 338A DD 36 12 01
    ld     hl,$3e4f        ; 338E 21 4F 3E
    call   $3870           ; 3391 CD 70 38
    ld     a,($802b)       ; 3394 3A 2B 80
    sub    $78             ; 3397 D6 78
    jr     c,$33a4         ; 3399 38 09
    ld     a,(ix+$04)      ; 339B DD 7E 04
    cp     $78             ; 339E FE 78
    jr     nc,$338e        ; 33A0 30 EC
    jr     $33ab           ; 33A2 18 07
    ld     a,(ix+$04)      ; 33A4 DD 7E 04
    cp     $78             ; 33A7 FE 78
    jr     c,$338e         ; 33A9 38 E3
    pop    bc              ; 33AB C1
    push   bc              ; 33AC C5
    dec    b               ; 33AD 05
    jp     nz,$33c2        ; 33AE C2 C2 33
    ld     hl,($824c)      ; 33B1 2A 4C 82
    ld     e,(ix+$04)      ; 33B4 DD 5E 04
    ld     d,(ix+$05)      ; 33B7 DD 56 05
    or     a               ; 33BA B7
    sbc    hl,de           ; 33BB ED 52
    jp     z,$338e         ; 33BD CA 8E 33
    jr     $33cb           ; 33C0 18 09
    ld     l,(ix+$04)      ; 33C2 DD 6E 04
    ld     h,(ix+$05)      ; 33C5 DD 66 05
    ld     ($824c),hl      ; 33C8 22 4C 82
    ld     hl,$80c4        ; 33CB 21 C4 80
    call   $3887           ; 33CE CD 87 38
    ld     a,($80c1)       ; 33D1 3A C1 80
    inc    a               ; 33D4 3C
    ld     ($80c1),a       ; 33D5 32 C1 80
    ld     a,($80c8)       ; 33D8 3A C8 80
    ld     (ix+$16),a      ; 33DB DD 77 16
    pop    bc              ; 33DE C1
    djnz   $3364           ; 33DF 10 83
    ret                    ; 33E1 C9
    ld     a,(ix+$18)      ; 33E2 DD 7E 18
    or     a               ; 33E5 B7
    jp     nz,$3459        ; 33E6 C2 59 34
    ld     (ix+$18),$01    ; 33E9 DD 36 18 01
    call   $3c1d           ; 33ED CD 1D 3C
    or     a               ; 33F0 B7
    jp     nz,$3459        ; 33F1 C2 59 34
    dec    (ix+$12)        ; 33F4 DD 35 12
    jp     nz,$3459        ; 33F7 C2 59 34
    ld     (ix+$12),$03    ; 33FA DD 36 12 03
    ld     hl,($80c4)      ; 33FE 2A C4 80
    ld     (ix+$08),l      ; 3401 DD 75 08
    ld     (ix+$09),h      ; 3404 DD 74 09
    ld     (ix+$0a),l      ; 3407 DD 75 0A
    ld     (ix+$0b),h      ; 340A DD 74 0B
    ld     a,(ix+$11)      ; 340D DD 7E 11
    ld     (ix+$10),a      ; 3410 DD 77 10
    ld     a,($80c2)       ; 3413 3A C2 80
    ld     ($846c),a       ; 3416 32 6C 84
    ld     ($846d),a       ; 3419 32 6D 84
    ld     ($846e),a       ; 341C 32 6E 84
    ld     ($846f),a       ; 341F 32 6F 84
    ld     hl,$846c        ; 3422 21 6C 84
    call   $389e           ; 3425 CD 9E 38
    call   $6006           ; 3428 CD 06 60
    or     a               ; 342B B7
    jp     $3434           ; 342C C3 34 34
    call   $38dc           ; 342F CD DC 38
    jr     $3442           ; 3432 18 0E
    call   $391c           ; 3434 CD 1C 39
    ld     (ix+$11),b      ; 3437 DD 70 11
    ld     a,d             ; 343A 7A
    cp     e               ; 343B BB
    jp     nc,$3442        ; 343C D2 42 34
    ld     (ix+$11),c      ; 343F DD 71 11
    call   $3bfc           ; 3442 CD FC 3B
    call   $3984           ; 3445 CD 84 39
    or     a               ; 3448 B7
    jp     z,$344f         ; 3449 CA 4F 34
    call   $370d           ; 344C CD 0D 37
    call   $3b06           ; 344F CD 06 3B
    ld     a,(ix+$0f)      ; 3452 DD 7E 0F
    or     a               ; 3455 B7
    jp     nz,$345f        ; 3456 C2 5F 34
    call   $6009           ; 3459 CD 09 60
    call   $3b7a           ; 345C CD 7A 3B
    call   $32d6           ; 345F CD D6 32
    ret                    ; 3462 C9
    dec    (ix+$12)        ; 3463 DD 35 12
    jp     nz,$34c7        ; 3466 C2 C7 34
    ld     a,(ix+$13)      ; 3469 DD 7E 13
    ld     (ix+$12),a      ; 346C DD 77 12
    ld     l,(ix+$14)      ; 346F DD 6E 14
    ld     h,(ix+$15)      ; 3472 DD 66 15
    ld     a,(hl)          ; 3475 7E
    cp     $ff             ; 3476 FE FF
    jp     z,$3487         ; 3478 CA 87 34
    ld     (ix+$02),a      ; 347B DD 77 02
    inc    hl              ; 347E 23
    ld     (ix+$14),l      ; 347F DD 75 14
    ld     (ix+$15),h      ; 3482 DD 74 15
    jr     $34c7           ; 3485 18 40
    inc    hl              ; 3487 23
    ld     a,$04           ; 3488 3E 04
    or     (hl)            ; 348A B6
    ld     (ix+$03),a      ; 348B DD 77 03
    inc    hl              ; 348E 23
    ld     a,(hl)          ; 348F 7E
    ld     (ix+$02),a      ; 3490 DD 77 02
    ld     (ix+$0d),$06    ; 3493 DD 36 0D 06
    ld     (ix+$0e),$00    ; 3497 DD 36 0E 00
    ld     (ix+$12),$03    ; 349B DD 36 12 03
    ld     (ix+$13),$00    ; 349F DD 36 13 00
    ld     (ix+$14),$00    ; 34A3 DD 36 14 00
    ld     (ix+$15),$00    ; 34A7 DD 36 15 00
    ld     (ix+$01),$02    ; 34AB DD 36 01 02
    ld     a,(ix+$0f)      ; 34AF DD 7E 0F
    or     a               ; 34B2 B7
    jp     z,$34c7         ; 34B3 CA C7 34
    ld     a,(ix+$17)      ; 34B6 DD 7E 17
    ld     (ix+$11),a      ; 34B9 DD 77 11
    ld     (ix+$17),$00    ; 34BC DD 36 17 00
    call   $34cb           ; 34C0 CD CB 34
    ld     (ix+$0f),$00    ; 34C3 DD 36 0F 00
    call   $32d6           ; 34C7 CD D6 32
    ret                    ; 34CA C9
    ld     a,(ix+$10)      ; 34CB DD 7E 10
    cp     (ix+$11)        ; 34CE DD BE 11
    jp     z,$34f0         ; 34D1 CA F0 34
    ld     hl,$34f1        ; 34D4 21 F1 34
    ld     bc,$0008        ; 34D7 01 08 00
    ld     a,(ix+$10)      ; 34DA DD 7E 10
    call   $00f6           ; 34DD CD F6 00
    ld     a,(ix+$11)      ; 34E0 DD 7E 11
    call   $00ea           ; 34E3 CD EA 00
    ld     a,(ix+$11)      ; 34E6 DD 7E 11
    ld     (ix+$10),a      ; 34E9 DD 77 10
    ld     (ix+$01),$03    ; 34EC DD 36 01 03
    ret                    ; 34F0 C9
    add    a,c             ; 34F1 81
    ld     sp,$3511        ; 34F2 31 11 35
    ld     d,$35           ; 34F5 16 35
    dec    de              ; 34F7 1B
    dec    (hl)            ; 34F8 35
    jr     nz,$3530        ; 34F9 20 35
    add    a,c             ; 34FB 81
    ld     sp,$3525        ; 34FC 31 25 35
    ld     hl,($2f35)      ; 34FF 2A 35 2F
    dec    (hl)            ; 3502 35
    inc    (hl)            ; 3503 34
    dec    (hl)            ; 3504 35
    add    a,c             ; 3505 81
    ld     sp,$3539        ; 3506 31 39 35
    ld     a,$35           ; 3509 3E 35
    ld     b,e             ; 350B 43
    dec    (hl)            ; 350C 35
    ld     c,b             ; 350D 48
    dec    (hl)            ; 350E 35
    add    a,c             ; 350F 81
    ld     sp,$7821        ; 3510 31 21 78
    ld     a,$18           ; 3513 3E 18
    scf                    ; 3515 37
    ld     hl,$3e7f        ; 3516 21 7F 3E
    jr     $354d           ; 3519 18 32
    ld     hl,$3e86        ; 351B 21 86 3E
    jr     $354d           ; 351E 18 2D
    ld     hl,$3e8d        ; 3520 21 8D 3E
    jr     $354d           ; 3523 18 28
    ld     hl,$3e94        ; 3525 21 94 3E
    jr     $354d           ; 3528 18 23
    ld     hl,$3e9b        ; 352A 21 9B 3E
    jr     $354d           ; 352D 18 1E
    ld     hl,$3ea2        ; 352F 21 A2 3E
    jr     $354d           ; 3532 18 19
    ld     hl,$3ea9        ; 3534 21 A9 3E
    jr     $354d           ; 3537 18 14
    ld     hl,$3eb0        ; 3539 21 B0 3E
    jr     $354d           ; 353C 18 0F
    ld     hl,$3eb7        ; 353E 21 B7 3E
    jr     $354d           ; 3541 18 0A
    ld     hl,$3ebe        ; 3543 21 BE 3E
    jr     $354d           ; 3546 18 05
    ld     hl,$3ec5        ; 3548 21 C5 3E
    jr     $354d           ; 354B 18 00
    ld     a,(ix+$03)      ; 354D DD 7E 03
    and    $0f             ; 3550 E6 0F
    or     (hl)            ; 3552 B6
    ld     (ix+$03),a      ; 3553 DD 77 03
    inc    hl              ; 3556 23
    ld     a,(hl)          ; 3557 7E
    ld     (ix+$02),a      ; 3558 DD 77 02
    inc    hl              ; 355B 23
    ld     (ix+$14),l      ; 355C DD 75 14
    ld     (ix+$15),h      ; 355F DD 74 15
    ld     (ix+$12),$06    ; 3562 DD 36 12 06
    ld     (ix+$13),$06    ; 3566 DD 36 13 06
    ret                    ; 356A C9

; robot transformation (spawn new enemy) ?

    ld     a,($81b0)       ; 356B 3A B0 81	; hl = 0x8268 + (0x81b0) * 1
    ld     hl,$8268        ; 356E 21 68 82
    ld     bc,$0001        ; 3571 01 01 00
    call   $00f6           ; 3574 CD F6 00
    ld     a,(hl)          ; 3577 7E 		; a = *hl & 0x0f
    and    $0f             ; 3578 E6 0F
    push   af              ; 357A F5 		; ix[1] = a + 2
    add    a,$02           ; 357B C6 02
    ld     (ix+$01),a      ; 357D DD 77 01
    pop    af              ; 3580 F1
    ld     hl,$3ed2        ; 3581 21 D2 3E	; hl = 0x3ed2 + a * 2
    ld     bc,$0002        ; 3584 01 02 00
    call   $00f6           ; 3587 CD F6 00
    ld     a,(hl)          ; 358A 7E 		; ix[3] = *hl
    ld     (ix+$03),a      ; 358B DD 77 03
    inc    hl              ; 358E 23 		; ix[2] = hl[1]
    ld     a,(hl)          ; 358F 7E
    ld     (ix+$02),a      ; 3590 DD 77 02
    push   ix              ; 3593 DD E5		; memset (ix + 6, 0, 19);
    ld     b,$13           ; 3595 06 13
    ld     (ix+$06),$00    ; 3597 DD 36 06 00
    inc    ix              ; 359B DD 23
    djnz   $3597           ; 359D 10 F8
    pop    ix              ; 359F DD E1
    ld     (ix+$13),$1e    ; 35A1 DD 36 13 1E	; ix[19] = 30
    ld     a,$01           ; 35A5 3E 01		; (0x8277) = 1
    ld     ($8277),a       ; 35A7 32 77 82
    ld     a,($81b0)       ; 35AA 3A B0 81	; (0x81b0)++
    inc    a               ; 35AD 3C
    ld     ($81b0),a       ; 35AE 32 B0 81
    ld     a,($81b0)       ; 35B1 3A B0 81	; if ((0x81b0) >= 8) {
    cp     $08             ; 35B4 FE 08
    jp     c,$35be         ; 35B6 DA BE 35
    ld     a,$00           ; 35B9 3E 00		;   (0x81b0) = 0;
    ld     ($81b0),a       ; 35BB 32 B0 81	; }
    ld     (ix+$0a),$0a    ; 35BE DD 36 0A 0A	; ix[10] = 10
    ld     a,(ix+$01)      ; 35C2 DD 7E 01	; if (ix[1] == 4) {
    cp     $04             ; 35C5 FE 04
    jp     nz,$35d3        ; 35C7 C2 D3 35
    ld     hl,($825c)      ; 35CA 2A 5C 82	;   ix[10] = (0x825c)
    ld     (ix+$0a),l      ; 35CD DD 75 0A	;   ix[11] = (0x825d)
    ld     (ix+$0b),h      ; 35D0 DD 74 0B	; }
    ld     a,(ix+$01)      ; 35D3 DD 7E 01	; if (ix[1] == 2) {
    cp     $02             ; 35D6 FE 02
    jp     nz,$35f1        ; 35D8 C2 F1 35
    ld     (iy+$00),$69    ; 35DB FD 36 00 69	;   iy[0] = 0x69
    ld     (iy+$01),$45    ; 35DF FD 36 01 45	;   iy[1] = 0x45
    ld     a,(ix+$04)      ; 35E3 DD 7E 04	;   iy[2] = ix[4]
    ld     (iy+$02),a      ; 35E6 FD 77 02
    ld     a,(ix+$05)      ; 35E9 DD 7E 05	;   iy[3] = ix[5] + 0xfc
    add    a,$fc           ; 35EC C6 FC
    ld     (iy+$03),a      ; 35EE FD 77 03	}
    ret                    ; 35F1 C9

    call   $2ff0           ; 35F2 CD F0 2F
    call   $32d6           ; 35F5 CD D6 32
    ret                    ; 35F8 C9
    call   $2ff3           ; 35F9 CD F3 2F
    call   $32d6           ; 35FC CD D6 32
    ret                    ; 35FF C9
    call   $2ff6           ; 3600 CD F6 2F
    call   $32d6           ; 3603 CD D6 32
    ret                    ; 3606 C9
    nop                    ; 3607 00
    nop                    ; 3608 00
    nop                    ; 3609 00
    nop                    ; 360A 00
    nop                    ; 360B 00
    nop                    ; 360C 00
    nop                    ; 360D 00
    nop                    ; 360E 00
    ld     a,($808d)       ; 360F 3A 8D 80
    or     a               ; 3612 B7
    jp     nz,$3632        ; 3613 C2 32 36
    ld     a,($8028)       ; 3616 3A 28 80
    bit    7,a             ; 3619 CB 7F
    jp     nz,$3632        ; 361B C2 32 36
    call   $389e           ; 361E CD 9E 38
    call   $6006           ; 3621 CD 06 60
    or     a               ; 3624 B7
    jp     z,$3632         ; 3625 CA 32 36
    ld     hl,$8028        ; 3628 21 28 80
    set    2,(hl)          ; 362B CB D6
    ld     a,$01           ; 362D 3E 01
    ld     ($824b),a       ; 362F 32 4B 82
    ret                    ; 3632 C9
    ld     b,(ix+$04)      ; 3633 DD 46 04
    ld     c,(ix+$05)      ; 3636 DD 4E 05
    ld     a,b             ; 3639 78
    and    $07             ; 363A E6 07
    jp     nz,$36a8        ; 363C C2 A8 36
    call   $3800           ; 363F CD 00 38
    or     a               ; 3642 B7
    jp     z,$36a8         ; 3643 CA A8 36
    ld     a,(ix+$11)      ; 3646 DD 7E 11
    inc    a               ; 3649 3C
    and    $01             ; 364A E6 01
    ld     (ix+$11),a      ; 364C DD 77 11
    xor    a               ; 364F AF
    or     c               ; 3650 B1
    jp     nz,$36a8        ; 3651 C2 A8 36
    ld     a,(ix+$16)      ; 3654 DD 7E 16
    or     (ix+$17)        ; 3657 DD B6 17
    jp     nz,$36a8        ; 365A C2 A8 36
    ld     hl,($80d6)      ; 365D 2A D6 80
    ld     (ix+$16),l      ; 3660 DD 75 16
    ld     (ix+$17),h      ; 3663 DD 74 17
    ld     (ix+$01),$04    ; 3666 DD 36 01 04
    ld     (ix+$08),$00    ; 366A DD 36 08 00
    ld     (ix+$09),$00    ; 366E DD 36 09 00
    ld     hl,$0080        ; 3672 21 80 00
    ld     (ix+$0a),l      ; 3675 DD 75 0A
    ld     (ix+$0b),h      ; 3678 DD 74 0B
    ld     (ix+$10),$00    ; 367B DD 36 10 00
    ld     a,(ix+$11)      ; 367F DD 7E 11
    or     a               ; 3682 B7
    jp     nz,$3690        ; 3683 C2 90 36
    ld     a,(ix+$04)      ; 3686 DD 7E 04
    sub    $08             ; 3689 D6 08
    ld     (ix+$04),a      ; 368B DD 77 04
    jr     $3698           ; 368E 18 08
    ld     a,(ix+$04)      ; 3690 DD 7E 04
    add    a,$08           ; 3693 C6 08
    ld     (ix+$04),a      ; 3695 DD 77 04
    ld     (ix+$14),$01    ; 3698 DD 36 14 01
    ld     a,($808d)       ; 369C 3A 8D 80
    or     a               ; 369F B7
    jp     nz,$36a8        ; 36A0 C2 A8 36
    ld     a,$16           ; 36A3 3E 16
    call   $00fc           ; 36A5 CD FC 00
    ret                    ; 36A8 C9
    ld     a,(ix+$04)      ; 36A9 DD 7E 04
    add    a,$08           ; 36AC C6 08
    ld     b,a             ; 36AE 47
    ld     a,(ix+$05)      ; 36AF DD 7E 05
    add    a,$14           ; 36B2 C6 14
    ld     c,a             ; 36B4 4F
    call   $00de           ; 36B5 CD DE 00
    ld     a,(hl)          ; 36B8 7E
    call   $5fd6           ; 36B9 CD D6 5F
    or     a               ; 36BC B7
    jp     z,$3704         ; 36BD CA 04 37
    ld     (ix+$0a),$00    ; 36C0 DD 36 0A 00
    ld     (ix+$0b),$00    ; 36C4 DD 36 0B 00
    ld     hl,$0080        ; 36C8 21 80 00
    ld     (ix+$08),l      ; 36CB DD 75 08
    ld     (ix+$09),h      ; 36CE DD 74 09
    ld     (ix+$0e),$10    ; 36D1 DD 36 0E 10
    ld     (ix+$0f),$00    ; 36D5 DD 36 0F 00
    ld     (ix+$10),$00    ; 36D9 DD 36 10 00
    ld     (ix+$14),$06    ; 36DD DD 36 14 06
    ld     (ix+$15),$00    ; 36E1 DD 36 15 00
    ld     a,(ix+$05)      ; 36E5 DD 7E 05
    and    $f8             ; 36E8 E6 F8
    add    a,$08           ; 36EA C6 08
    ld     (ix+$05),a      ; 36EC DD 77 05
    ld     (ix+$01),$02    ; 36EF DD 36 01 02
    ld     a,($808d)       ; 36F3 3A 8D 80
    or     a               ; 36F6 B7
    jp     nz,$3704        ; 36F7 C2 04 37
    ld     a,$17           ; 36FA 3E 17
    call   $00fc           ; 36FC CD FC 00
    ld     a,$96           ; 36FF 3E 96
    call   $00fc           ; 3701 CD FC 00
    ret                    ; 3704 C9
    ld     de,$3ef5        ; 3705 11 F5 3E
    call   $396b           ; 3708 CD 6B 39
    or     a               ; 370B B7
    ret                    ; 370C C9
    ld     hl,$3e57        ; 370D 21 57 3E
    ld     a,(ix+$10)      ; 3710 DD 7E 10
    ld     bc,$0004        ; 3713 01 04 00
    call   $00f6           ; 3716 CD F6 00
    ld     a,(hl)          ; 3719 7E
    ld     (ix+$11),a      ; 371A DD 77 11
    push   hl              ; 371D E5
    call   $3984           ; 371E CD 84 39
    pop    hl              ; 3721 E1
    inc    hl              ; 3722 23
    or     a               ; 3723 B7
    jp     nz,$3719        ; 3724 C2 19 37
    ret                    ; 3727 C9
    ld     a,(ix+$12)      ; 3728 DD 7E 12
    cp     (ix+$11)        ; 372B DD BE 11
    jp     nz,$374f        ; 372E C2 4F 37
    dec    (ix+$14)        ; 3731 DD 35 14
    jp     nz,$3761        ; 3734 C2 61 37
    ld     (ix+$14),$06    ; 3737 DD 36 14 06
    call   $3762           ; 373B CD 62 37
    inc    (ix+$10)        ; 373E DD 34 10
    ld     a,(ix+$10)      ; 3741 DD 7E 10
    cp     $04             ; 3744 FE 04
    jp     c,$3761         ; 3746 DA 61 37
    ld     (ix+$10),$00    ; 3749 DD 36 10 00
    jr     $3761           ; 374D 18 12
    ld     a,(ix+$11)      ; 374F DD 7E 11
    ld     (ix+$12),a      ; 3752 DD 77 12
    ld     (ix+$10),$00    ; 3755 DD 36 10 00
    ld     (ix+$14),$06    ; 3759 DD 36 14 06
    ld     (ix+$02),$04    ; 375D DD 36 02 04
    ret                    ; 3761 C9
    ld     a,(ix+$11)      ; 3762 DD 7E 11
    or     a               ; 3765 B7
    ld     hl,$3e6f        ; 3766 21 6F 3E
    jp     nz,$376f        ; 3769 C2 6F 37
    ld     hl,$3e6b        ; 376C 21 6B 3E
    ld     e,(ix+$10)      ; 376F DD 5E 10
    ld     d,$00           ; 3772 16 00
    add    hl,de           ; 3774 19
    ld     a,(hl)          ; 3775 7E
    ld     (ix+$02),a      ; 3776 DD 77 02
    ret                    ; 3779 C9
    dec    (ix+$14)        ; 377A DD 35 14
    jr     nz,$3799        ; 377D 20 1A
    ld     (ix+$14),$04    ; 377F DD 36 14 04
    ld     hl,$3e67        ; 3783 21 67 3E
    ld     c,(ix+$10)      ; 3786 DD 4E 10
    ld     b,$00           ; 3789 06 00
    add    hl,bc           ; 378B 09
    ld     a,(hl)          ; 378C 7E
    ld     (ix+$02),a      ; 378D DD 77 02
    ld     a,(ix+$10)      ; 3790 DD 7E 10
    inc    a               ; 3793 3C
    and    $03             ; 3794 E6 03
    ld     (ix+$10),a      ; 3796 DD 77 10
    ret                    ; 3799 C9

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 379A
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 37AA
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 37BA
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 37CA
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 37DA
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 37EA
    hex    00 00 00 00 00 00                               ; 37FA

    ld     hl,$3d9f        ; 3800 21 9F 3D
    call   $3830           ; 3803 CD 30 38
    call   $00de           ; 3806 CD DE 00
    ld     a,(hl)          ; 3809 7E
    call   $5fd6           ; 380A CD D6 5F
    or     a               ; 380D B7
    jp     nz,$3826        ; 380E C2 26 38
    ld     hl,$3d97        ; 3811 21 97 3D
    call   $3830           ; 3814 CD 30 38
    call   $00de           ; 3817 CD DE 00
    ld     a,(hl)          ; 381A 7E
    call   $5fd6           ; 381B CD D6 5F
    or     a               ; 381E B7
    jp     z,$382b         ; 381F CA 2B 38
    xor    a               ; 3822 AF
    jp     $382f           ; 3823 C3 2F 38
    ld     c,$01           ; 3826 0E 01
    jp     $382d           ; 3828 C3 2D 38
    ld     c,$00           ; 382B 0E 00
    ld     a,$01           ; 382D 3E 01
    ret                    ; 382F C9
    ld     a,(ix+$11)      ; 3830 DD 7E 11
    push   hl              ; 3833 E5
    ld     hl,$383a        ; 3834 21 3A 38
    jp     $00ea           ; 3837 C3 EA 00
    ld     b,d             ; 383A 42
    jr     c,$3884         ; 383B 38 47
    jr     c,$388e         ; 383D 38 4F
    jr     c,$3898         ; 383F 38 57
    jr     c,$3824         ; 3841 38 E1
    call   $385b           ; 3843 CD 5B 38
    ret                    ; 3846 C9
    pop    hl              ; 3847 E1
    call   $389b           ; 3848 CD 9B 38
    call   $385b           ; 384B CD 5B 38
    ret                    ; 384E C9
    pop    hl              ; 384F E1
    call   $3899           ; 3850 CD 99 38
    call   $385b           ; 3853 CD 5B 38
    ret                    ; 3856 C9
    pop    hl              ; 3857 E1
    call   $3897           ; 3858 CD 97 38
    ld     a,(ix+$04)      ; 385B DD 7E 04
    add    a,(hl)          ; 385E 86
    ld     b,a             ; 385F 47
    inc    hl              ; 3860 23
    ld     a,(ix+$05)      ; 3861 DD 7E 05
    add    a,(hl)          ; 3864 86
    ld     c,a             ; 3865 4F
    ret                    ; 3866 C9
    ld     bc,$0008        ; 3867 01 08 00
    ld     a,($81a1)       ; 386A 3A A1 81
    call   $00f6           ; 386D CD F6 00
    call   $00c9           ; 3870 CD C9 00
    ld     a,($8025)       ; 3873 3A 25 80
    and    $03             ; 3876 E6 03
    add    a,a             ; 3878 87
    ld     d,$00           ; 3879 16 00
    ld     e,a             ; 387B 5F
    add    hl,de           ; 387C 19
    ld     a,(hl)          ; 387D 7E
    ld     (ix+$05),a      ; 387E DD 77 05
    inc    hl              ; 3881 23
    ld     a,(hl)          ; 3882 7E
    ld     (ix+$04),a      ; 3883 DD 77 04
    ret                    ; 3886 C9
    ld     e,(hl)          ; 3887 5E
    inc    hl              ; 3888 23
    ld     d,(hl)          ; 3889 56
    ld     (ix+$08),e      ; 388A DD 73 08
    ld     (ix+$09),d      ; 388D DD 72 09
    ld     (ix+$0a),e      ; 3890 DD 73 0A
    ld     (ix+$0b),d      ; 3893 DD 72 0B
    ret                    ; 3896 C9
    inc    hl              ; 3897 23
    inc    hl              ; 3898 23
    inc    hl              ; 3899 23
    inc    hl              ; 389A 23
    inc    hl              ; 389B 23
    inc    hl              ; 389C 23
    ret                    ; 389D C9
    ld     a,($802b)       ; 389E 3A 2B 80
    add    a,$08           ; 38A1 C6 08
    ld     b,a             ; 38A3 47
    ld     a,($802c)       ; 38A4 3A 2C 80
    add    a,$08           ; 38A7 C6 08
    ld     c,a             ; 38A9 4F
    ld     a,(ix+$04)      ; 38AA DD 7E 04
    add    a,$08           ; 38AD C6 08
    ld     d,a             ; 38AF 57
    ld     a,(ix+$05)      ; 38B0 DD 7E 05
    add    a,$08           ; 38B3 C6 08
    ld     e,a             ; 38B5 5F
    ret                    ; 38B6 C9
    ld     de,$0019        ; 38B7 11 19 00
    xor    a               ; 38BA AF
    or     (ix+$00)        ; 38BB DD B6 00
    jp     z,$38ca         ; 38BE CA CA 38
    add    ix,de           ; 38C1 DD 19
    djnz   $38ba           ; 38C3 10 F5
    ld     a,$01           ; 38C5 3E 01
    jp     $38cc           ; 38C7 C3 CC 38
    ld     a,$00           ; 38CA 3E 00
    ret                    ; 38CC C9
    push   ix              ; 38CD DD E5
    ld     b,$19           ; 38CF 06 19
    ld     (ix+$00),$00    ; 38D1 DD 36 00 00
    inc    ix              ; 38D5 DD 23
    djnz   $38d1           ; 38D7 10 F8
    pop    ix              ; 38D9 DD E1
    ret                    ; 38DB C9
    call   $00c9           ; 38DC CD C9 00
    ld     a,($8025)       ; 38DF 3A 25 80
    and    $03             ; 38E2 E6 03
    ld     hl,$38ea        ; 38E4 21 EA 38
    jp     $00ea           ; 38E7 C3 EA 00
    dec    de              ; 38EA 1B
    add    hl,sp           ; 38EB 39
    jp     p,$1b38         ; 38EC F2 38 1B
    add    hl,sp           ; 38EF 39
    ex     af,af'          ; 38F0 08
    add    hl,sp           ; 38F1 39
    ld     a,(ix+$11)      ; 38F2 DD 7E 11
    cp     $02             ; 38F5 FE 02
    jp     nc,$3901        ; 38F7 D2 01 39
    ld     (ix+$11),$02    ; 38FA DD 36 11 02
    jp     $391b           ; 38FE C3 1B 39
    ld     (ix+$11),$00    ; 3901 DD 36 11 00
    jp     $391b           ; 3905 C3 1B 39
    ld     a,(ix+$11)      ; 3908 DD 7E 11
    cp     $02             ; 390B FE 02
    jp     nc,$3917        ; 390D D2 17 39
    ld     (ix+$11),$03    ; 3910 DD 36 11 03
    jp     $391b           ; 3914 C3 1B 39
    ld     (ix+$11),$01    ; 3917 DD 36 11 01
    ret                    ; 391B C9
    ld     a,($802b)       ; 391C 3A 2B 80
    add    a,$08           ; 391F C6 08
    ld     l,a             ; 3921 6F
    ld     h,$00           ; 3922 26 00
    ld     a,(ix+$04)      ; 3924 DD 7E 04
    add    a,$08           ; 3927 C6 08
    ld     e,a             ; 3929 5F
    ld     d,$00           ; 392A 16 00
    or     a               ; 392C B7
    sbc    hl,de           ; 392D ED 52
    ld     ($8250),hl      ; 392F 22 50 82
    ld     a,($802c)       ; 3932 3A 2C 80
    add    a,$08           ; 3935 C6 08
    ld     l,a             ; 3937 6F
    ld     h,$00           ; 3938 26 00
    ld     a,(ix+$05)      ; 393A DD 7E 05
    add    a,$08           ; 393D C6 08
    ld     e,a             ; 393F 5F
    ld     d,$00           ; 3940 16 00
    or     a               ; 3942 B7
    sbc    hl,de           ; 3943 ED 52
    ld     ($8252),hl      ; 3945 22 52 82
    ld     hl,($8250)      ; 3948 2A 50 82
    ld     a,l             ; 394B 7D
    ld     d,a             ; 394C 57
    ld     b,$00           ; 394D 06 00
    bit    7,h             ; 394F CB 7C
    jp     p,$3959         ; 3951 F2 59 39
    neg                    ; 3954 ED 44
    ld     d,a             ; 3956 57
    ld     b,$01           ; 3957 06 01
    ld     hl,($8252)      ; 3959 2A 52 82
    ld     a,l             ; 395C 7D
    ld     e,a             ; 395D 5F
    ld     c,$03           ; 395E 0E 03
    bit    7,h             ; 3960 CB 7C
    jp     p,$396a         ; 3962 F2 6A 39
    neg                    ; 3965 ED 44
    ld     e,a             ; 3967 5F
    ld     c,$02           ; 3968 0E 02
    ret                    ; 396A C9
    push   bc              ; 396B C5
    push   hl              ; 396C E5
    push   de              ; 396D D5
    pop    hl              ; 396E E1
    ld     b,a             ; 396F 47
    ld     a,(hl)          ; 3970 7E
    inc    hl              ; 3971 23
    cp     $ff             ; 3972 FE FF
    jp     z,$397f         ; 3974 CA 7F 39
    cp     b               ; 3977 B8
    jp     nz,$3970        ; 3978 C2 70 39
    xor    a               ; 397B AF
    jp     $3981           ; 397C C3 81 39
    ld     a,$01           ; 397F 3E 01
    pop    hl              ; 3981 E1
    pop    bc              ; 3982 C1
    ret                    ; 3983 C9
    ld     hl,$3daf        ; 3984 21 AF 3D
    call   $3830           ; 3987 CD 30 38
    call   $00de           ; 398A CD DE 00
    ld     b,$04           ; 398D 06 04
    call   $39a8           ; 398F CD A8 39
    ld     a,(hl)          ; 3992 7E
    push   bc              ; 3993 C5
    push   hl              ; 3994 E5
    call   $5fd6           ; 3995 CD D6 5F
    pop    hl              ; 3998 E1
    pop    bc              ; 3999 C1
    or     a               ; 399A B7
    jp     nz,$39a5        ; 399B C2 A5 39
    djnz   $398f           ; 399E 10 EF
    ld     a,$00           ; 39A0 3E 00
    jp     $39a7           ; 39A2 C3 A7 39
    ld     a,$01           ; 39A5 3E 01
    ret                    ; 39A7 C9
    push   hl              ; 39A8 E5
    ld     hl,$39b0        ; 39A9 21 B0 39
    ld     a,b             ; 39AC 78
    jp     $00ea           ; 39AD C3 EA 00
    call   z,$ba39         ; 39B0 CC 39 BA
    add    hl,sp           ; 39B3 39
    cp     a               ; 39B4 BF
    add    hl,sp           ; 39B5 39
    rst    00h             ; 39B6 C7
    add    hl,sp           ; 39B7 39
    call   z,$e139         ; 39B8 CC 39 E1
    inc    hl              ; 39BB 23
    jp     $39cd           ; 39BC C3 CD 39
    pop    hl              ; 39BF E1
    ld     de,$001f        ; 39C0 11 1F 00
    add    hl,de           ; 39C3 19
    jp     $39cd           ; 39C4 C3 CD 39
    pop    hl              ; 39C7 E1
    inc    hl              ; 39C8 23
    jp     $39cd           ; 39C9 C3 CD 39
    pop    hl              ; 39CC E1
    ret                    ; 39CD C9
    ld     a,($8249)       ; 39CE 3A 49 82
    cp     $02             ; 39D1 FE 02
    jp     nc,$39dc        ; 39D3 D2 DC 39
    dec    (ix+$14)        ; 39D6 DD 35 14
    jp     nz,$3a1e        ; 39D9 C2 1E 3A
    ld     (iy+$04),$00    ; 39DC FD 36 04 00
    ld     (iy+$05),$00    ; 39E0 FD 36 05 00
    ld     (iy+$06),$00    ; 39E4 FD 36 06 00
    ld     (iy+$07),$00    ; 39E8 FD 36 07 00
    ld     a,(ix+$00)      ; 39EC DD 7E 00
    cp     $01             ; 39EF FE 01
    jp     nz,$39fe        ; 39F1 C2 FE 39
    ld     a,($80d3)       ; 39F4 3A D3 80
    dec    a               ; 39F7 3D
    ld     ($80d3),a       ; 39F8 32 D3 80
    jp     $3a0d           ; 39FB C3 0D 3A
    ld     a,(ix+$00)      ; 39FE DD 7E 00
    cp     $20             ; 3A01 FE 20
    jp     z,$39f4         ; 3A03 CA F4 39
    ld     a,($80c1)       ; 3A06 3A C1 80
    dec    a               ; 3A09 3D
    ld     ($80c1),a       ; 3A0A 32 C1 80
    call   $38cd           ; 3A0D CD CD 38
    ld     a,($8249)       ; 3A10 3A 49 82
    or     a               ; 3A13 B7
    jp     z,$3a1e         ; 3A14 CA 1E 3A
    ld     a,($8249)       ; 3A17 3A 49 82
    dec    a               ; 3A1A 3D
    ld     ($8249),a       ; 3A1B 32 49 82
    ret                    ; 3A1E C9
    dec    (ix+$14)        ; 3A1F DD 35 14
    jp     nz,$3a81        ; 3A22 C2 81 3A
    ld     a,(ix+$15)      ; 3A25 DD 7E 15
    ld     (ix+$14),a      ; 3A28 DD 77 14
    ld     hl,$3edc        ; 3A2B 21 DC 3E
    ld     d,$00           ; 3A2E 16 00
    ld     e,(ix+$10)      ; 3A30 DD 5E 10
    add    hl,de           ; 3A33 19
    ld     a,(hl)          ; 3A34 7E
    cp     $ff             ; 3A35 FE FF
    jp     nz,$3a7b        ; 3A37 C2 7B 3A
    res    5,(ix+$03)      ; 3A3A DD CB 03 AE
    ld     a,(ix+$04)      ; 3A3E DD 7E 04
    add    a,$08           ; 3A41 C6 08
    ld     (ix+$04),a      ; 3A43 DD 77 04
    ld     a,(ix+$05)      ; 3A46 DD 7E 05
    add    a,$08           ; 3A49 C6 08
    ld     (ix+$05),a      ; 3A4B DD 77 05
    ld     a,($8247)       ; 3A4E 3A 47 82
    ld     hl,$0003        ; 3A51 21 03 00
    call   $00f3           ; 3A54 CD F3 00
    call   $3af1           ; 3A57 CD F1 3A
    ld     a,($8247)       ; 3A5A 3A 47 82
    cp     $06             ; 3A5D FE 06
    jp     nc,$3a69        ; 3A5F D2 69 3A
    ld     a,($8247)       ; 3A62 3A 47 82
    inc    a               ; 3A65 3C
    ld     ($8247),a       ; 3A66 32 47 82
    ld     (ix+$14),$1e    ; 3A69 DD 36 14 1E
    ld     (ix+$01),$0c    ; 3A6D DD 36 01 0C
    ld     a,($8249)       ; 3A71 3A 49 82
    inc    a               ; 3A74 3C
    ld     ($8249),a       ; 3A75 32 49 82
    jp     $3a81           ; 3A78 C3 81 3A
    ld     (ix+$02),a      ; 3A7B DD 77 02
    inc    (ix+$10)        ; 3A7E DD 34 10
    ret                    ; 3A81 C9
    ld     hl,$3dcb        ; 3A82 21 CB 3D
    call   $389e           ; 3A85 CD 9E 38
    call   $6006           ; 3A88 CD 06 60
    or     a               ; 3A8B B7
    jp     z,$3abf         ; 3A8C CA BF 3A
    ld     a,$2c           ; 3A8F 3E 2C
    call   $00fc           ; 3A91 CD FC 00
    ld     a,($81b3)       ; 3A94 3A B3 81
    inc    a               ; 3A97 3C
    ld     ($81b3),a       ; 3A98 32 B3 81
    ld     (ix+$10),$00    ; 3A9B DD 36 10 00
    ld     (ix+$14),$01    ; 3A9F DD 36 14 01
    ld     (ix+$15),$04    ; 3AA3 DD 36 15 04
    ld     (ix+$03),$2d    ; 3AA7 DD 36 03 2D
    ld     (ix+$01),$0d    ; 3AAB DD 36 01 0D
    ld     a,(ix+$04)      ; 3AAF DD 7E 04
    add    a,$f8           ; 3AB2 C6 F8
    ld     (ix+$04),a      ; 3AB4 DD 77 04
    ld     a,(ix+$05)      ; 3AB7 DD 7E 05
    add    a,$f8           ; 3ABA C6 F8
    ld     (ix+$05),a      ; 3ABC DD 77 05
    ret                    ; 3ABF C9
    ld     a,($8028)       ; 3AC0 3A 28 80
    bit    6,a             ; 3AC3 CB 77
    jp     z,$3acf         ; 3AC5 CA CF 3A
    ld     (ix+$02),$00    ; 3AC8 DD 36 02 00
    jp     $3ad3           ; 3ACC C3 D3 3A
    ld     (ix+$02),$3f    ; 3ACF DD 36 02 3F
    ld     a,($8028)       ; 3AD3 3A 28 80
    bit    7,a             ; 3AD6 CB 7F
    jp     z,$3ae1         ; 3AD8 CA E1 3A
    call   $3a82           ; 3ADB CD 82 3A
    jp     $3af0           ; 3ADE C3 F0 3A
    push   ix              ; 3AE1 DD E5
    push   ix              ; 3AE3 DD E5
    pop    de              ; 3AE5 D1
    pop    hl              ; 3AE6 E1
    ld     bc,$00e1        ; 3AE7 01 E1 00
    add    hl,bc           ; 3AEA 09
    ld     bc,$0019        ; 3AEB 01 19 00
    ldir                   ; 3AEE ED B0
    ret                    ; 3AF0 C9
    ld     a,($8247)       ; 3AF1 3A 47 82
    ld     bc,$0002        ; 3AF4 01 02 00
    ld     hl,$3f01        ; 3AF7 21 01 3F
    call   $00f6           ; 3AFA CD F6 00
    ld     a,(hl)          ; 3AFD 7E
    ld     c,a             ; 3AFE 4F
    inc    hl              ; 3AFF 23
    ld     a,(hl)          ; 3B00 7E
    ld     b,a             ; 3B01 47
    call   $00e4           ; 3B02 CD E4 00
    ret                    ; 3B05 C9
    dec    (ix+$16)        ; 3B06 DD 35 16
    jp     nz,$3b31        ; 3B09 C2 31 3B
    ld     a,($80c8)       ; 3B0C 3A C8 80
    ld     (ix+$16),a      ; 3B0F DD 77 16
    ld     a,(ix+$11)      ; 3B12 DD 7E 11
    ld     (ix+$17),a      ; 3B15 DD 77 17
    call   $00c9           ; 3B18 CD C9 00
    ld     a,($8025)       ; 3B1B 3A 25 80
    and    $03             ; 3B1E E6 03
    cp     (ix+$10)        ; 3B20 DD BE 10
    jr     z,$3b18         ; 3B23 28 F3
    cp     (ix+$11)        ; 3B25 DD BE 11
    jr     z,$3b18         ; 3B28 28 EE
    ld     (ix+$11),a      ; 3B2A DD 77 11
    ld     (ix+$0f),$01    ; 3B2D DD 36 0F 01
    ret                    ; 3B31 C9
    ld     a,($80cc)       ; 3B32 3A CC 80
    dec    a               ; 3B35 3D
    ld     ($80cc),a       ; 3B36 32 CC 80
    jp     nz,$3b79        ; 3B39 C2 79 3B
    ld     a,($80c6)       ; 3B3C 3A C6 80
    ld     ($80cc),a       ; 3B3F 32 CC 80
    ld     hl,($80c4)      ; 3B42 2A C4 80
    ld     de,$0008        ; 3B45 11 08 00
    add    hl,de           ; 3B48 19
    push   hl              ; 3B49 E5
    ld     de,$0200        ; 3B4A 11 00 02
    or     a               ; 3B4D B7
    sbc    hl,de           ; 3B4E ED 52
    pop    hl              ; 3B50 E1
    jp     c,$3b55         ; 3B51 DA 55 3B
    ex     de,hl           ; 3B54 EB
    ld     ($80c4),hl      ; 3B55 22 C4 80
    ld     a,($824f)       ; 3B58 3A 4F 82
    dec    a               ; 3B5B 3D
    ld     ($824f),a       ; 3B5C 32 4F 82
    ld     a,($80c8)       ; 3B5F 3A C8 80
    ld     h,a             ; 3B62 67
    ld     a,($80c9)       ; 3B63 3A C9 80
    ld     l,a             ; 3B66 6F
    ld     a,h             ; 3B67 7C
    cp     $18             ; 3B68 FE 18
    jp     nc,$3b79        ; 3B6A D2 79 3B
    ld     de,$0020        ; 3B6D 11 20 00
    add    hl,de           ; 3B70 19
    ld     a,h             ; 3B71 7C
    ld     ($80c8),a       ; 3B72 32 C8 80
    ld     a,l             ; 3B75 7D
    ld     ($80c9),a       ; 3B76 32 C9 80
    ret                    ; 3B79 C9
    dec    (ix+$0d)        ; 3B7A DD 35 0D
    jp     nz,$3bae        ; 3B7D C2 AE 3B
    ld     (ix+$0d),$06    ; 3B80 DD 36 0D 06
    ld     hl,$3ecc        ; 3B84 21 CC 3E
    ld     a,(ix+$11)      ; 3B87 DD 7E 11
    cp     $02             ; 3B8A FE 02
    jp     c,$3b92         ; 3B8C DA 92 3B
    ld     hl,$3ecf        ; 3B8F 21 CF 3E
    ld     a,(ix+$0e)      ; 3B92 DD 7E 0E
    ld     bc,$0001        ; 3B95 01 01 00
    call   $00f6           ; 3B98 CD F6 00
    ld     a,(hl)          ; 3B9B 7E
    ld     (ix+$02),a      ; 3B9C DD 77 02
    inc    (ix+$0e)        ; 3B9F DD 34 0E
    ld     a,(ix+$0e)      ; 3BA2 DD 7E 0E
    cp     $03             ; 3BA5 FE 03
    jp     c,$3bae         ; 3BA7 DA AE 3B
    ld     (ix+$0e),$00    ; 3BAA DD 36 0E 00
    ret                    ; 3BAE C9
    ld     a,(ix+$04)      ; 3BAF DD 7E 04
    add    a,$08           ; 3BB2 C6 08
    ld     b,a             ; 3BB4 47
    ld     a,(ix+$05)      ; 3BB5 DD 7E 05
    add    a,$14           ; 3BB8 C6 14
    ld     c,a             ; 3BBA 4F
    call   $00de           ; 3BBB CD DE 00
    ld     a,(hl)          ; 3BBE 7E
    call   $5fd6           ; 3BBF CD D6 5F
    or     a               ; 3BC2 B7
    jp     nz,$3bf3        ; 3BC3 C2 F3 3B
    ld     hl,($80d6)      ; 3BC6 2A D6 80
    ld     (ix+$16),l      ; 3BC9 DD 75 16
    ld     (ix+$17),h      ; 3BCC DD 74 17
    ld     (ix+$08),$00    ; 3BCF DD 36 08 00
    ld     (ix+$09),$00    ; 3BD3 DD 36 09 00
    ld     hl,$0080        ; 3BD7 21 80 00
    ld     (ix+$0a),l      ; 3BDA DD 75 0A
    ld     (ix+$0b),h      ; 3BDD DD 74 0B
    ld     (ix+$10),$00    ; 3BE0 DD 36 10 00
    ld     (ix+$14),$01    ; 3BE4 DD 36 14 01
    ld     (ix+$01),$04    ; 3BE8 DD 36 01 04
    ld     a,$16           ; 3BEC 3E 16
    call   $00fc           ; 3BEE CD FC 00
    jr     $3bfb           ; 3BF1 18 08
    ld     (ix+$14),$06    ; 3BF3 DD 36 14 06
    ld     (ix+$01),$02    ; 3BF7 DD 36 01 02
    ret                    ; 3BFB C9
    ld     e,(ix+$10)      ; 3BFC DD 5E 10
    ld     d,$00           ; 3BFF 16 00
    ld     hl,$3f11        ; 3C01 21 11 3F
    add    hl,de           ; 3C04 19
    ld     a,(hl)          ; 3C05 7E
    cp     (ix+$11)        ; 3C06 DD BE 11
    jp     nz,$3c1c        ; 3C09 C2 1C 3C
    ld     a,(ix+$11)      ; 3C0C DD 7E 11
    cp     $02             ; 3C0F FE 02
    jp     nc,$3c19        ; 3C11 D2 19 3C
    ld     (ix+$11),c      ; 3C14 DD 71 11
    jr     $3c1c           ; 3C17 18 03
    ld     (ix+$11),b      ; 3C19 DD 70 11
    ret                    ; 3C1C C9
    ld     a,(ix+$04)      ; 3C1D DD 7E 04
    add    a,$08           ; 3C20 C6 08
    and    $0f             ; 3C22 E6 0F
    ld     de,$3c88        ; 3C24 11 88 3C
    call   $396b           ; 3C27 CD 6B 39
    or     a               ; 3C2A B7
    jr     nz,$3c81        ; 3C2B 20 54
    ld     a,(ix+$05)      ; 3C2D DD 7E 05
    add    a,$08           ; 3C30 C6 08
    and    $0f             ; 3C32 E6 0F
    ld     de,$3c88        ; 3C34 11 88 3C
    call   $396b           ; 3C37 CD 6B 39
    or     a               ; 3C3A B7
    jr     nz,$3c81        ; 3C3B 20 44
    ld     a,(ix+$0c)      ; 3C3D DD 7E 0C
    or     a               ; 3C40 B7
    jr     nz,$3c85        ; 3C41 20 42
    ld     (ix+$0c),$01    ; 3C43 DD 36 0C 01
    ld     a,(ix+$04)      ; 3C47 DD 7E 04
    inc    a               ; 3C4A 3C
    and    $0f             ; 3C4B E6 0F
    cp     $08             ; 3C4D FE 08
    jr     nc,$3c5e        ; 3C4F 30 0D
    ld     a,$f0           ; 3C51 3E F0
    inc    (ix+$04)        ; 3C53 DD 34 04
    and    (ix+$04)        ; 3C56 DD A6 04
    ld     (ix+$04),a      ; 3C59 DD 77 04
    jr     $3c62           ; 3C5C 18 04
    ld     a,$f8           ; 3C5E 3E F8
    jr     $3c53           ; 3C60 18 F1
    ld     a,(ix+$05)      ; 3C62 DD 7E 05
    inc    a               ; 3C65 3C
    and    $0f             ; 3C66 E6 0F
    cp     $08             ; 3C68 FE 08
    jr     nc,$3c79        ; 3C6A 30 0D
    ld     a,$f0           ; 3C6C 3E F0
    inc    (ix+$05)        ; 3C6E DD 34 05
    and    (ix+$05)        ; 3C71 DD A6 05
    ld     (ix+$05),a      ; 3C74 DD 77 05
    jr     $3c7d           ; 3C77 18 04
    ld     a,$f8           ; 3C79 3E F8
    jr     $3c6e           ; 3C7B 18 F1
    ld     a,$00           ; 3C7D 3E 00
    jr     $3c87           ; 3C7F 18 06
    ld     (ix+$0c),$00    ; 3C81 DD 36 0C 00
    ld     a,$01           ; 3C85 3E 01
    ret                    ; 3C87 C9
    rrca                   ; 3C88 0F
    nop                    ; 3C89 00
    ld     bc,$0807        ; 3C8A 01 07 08
    add    hl,bc           ; 3C8D 09
    rst    38h             ; 3C8E FF
    ld     a,($8028)       ; 3C8F 3A 28 80
    bit    7,a             ; 3C92 CB 7F
    jp     nz,$3cc2        ; 3C94 C2 C2 3C
    ld     ix,$8278        ; 3C97 DD 21 78 82
    ld     de,$0019        ; 3C9B 11 19 00
    ld     b,$09           ; 3C9E 06 09
    ld     a,(ix+$01)      ; 3CA0 DD 7E 01
    cp     $0c             ; 3CA3 FE 0C
    jp     nc,$3cc2        ; 3CA5 D2 C2 3C
    add    ix,de           ; 3CA8 DD 19
    djnz   $3ca0           ; 3CAA 10 F4
    ld     a,$00           ; 3CAC 3E 00
    ld     ($8247),a       ; 3CAE 32 47 82
    ld     a,($824a)       ; 3CB1 3A 4A 82
    or     a               ; 3CB4 B7
    jp     z,$3cc7         ; 3CB5 CA C7 3C
    ld     a,($824a)       ; 3CB8 3A 4A 82
    dec    a               ; 3CBB 3D
    ld     ($824a),a       ; 3CBC 32 4A 82
    jp     $3cc7           ; 3CBF C3 C7 3C
    ld     a,$1e           ; 3CC2 3E 1E
    ld     ($824a),a       ; 3CC4 32 4A 82
    ret                    ; 3CC7 C9
    ld     hl,($8270)      ; 3CC8 2A 70 82
    dec    hl              ; 3CCB 2B
    ld     ($8270),hl      ; 3CCC 22 70 82
    ld     a,h             ; 3CCF 7C
    or     l               ; 3CD0 B5
    jp     nz,$3cf3        ; 3CD1 C2 F3 3C
    ld     hl,($825e)      ; 3CD4 2A 5E 82
    ld     ($8270),hl      ; 3CD7 22 70 82
    ld     hl,($825c)      ; 3CDA 2A 5C 82
    ld     de,$0004        ; 3CDD 11 04 00
    add    hl,de           ; 3CE0 19
    ld     ($825c),hl      ; 3CE1 22 5C 82
    ld     bc,$0440        ; 3CE4 01 40 04
    or     a               ; 3CE7 B7
    sbc    hl,bc           ; 3CE8 ED 42
    jp     c,$3cf3         ; 3CEA DA F3 3C
    ld     hl,$0440        ; 3CED 21 40 04
    ld     ($825c),hl      ; 3CF0 22 5C 82
    ld     hl,($8272)      ; 3CF3 2A 72 82
    dec    hl              ; 3CF6 2B
    ld     ($8272),hl      ; 3CF7 22 72 82
    ld     a,h             ; 3CFA 7C
    or     l               ; 3CFB B5
    jp     nz,$3d1c        ; 3CFC C2 1C 3D
    ld     hl,($8262)      ; 3CFF 2A 62 82
    ld     ($8272),hl      ; 3D02 22 72 82
    ld     hl,($8260)      ; 3D05 2A 60 82
    ld     de,$0002        ; 3D08 11 02 00
    add    hl,de           ; 3D0B 19
    ld     ($8260),hl      ; 3D0C 22 60 82
    ld     bc,$0080        ; 3D0F 01 80 00
    or     a               ; 3D12 B7
    sbc    hl,bc           ; 3D13 ED 42
    jp     c,$3d1c         ; 3D15 DA 1C 3D
    ld     ($8274),bc      ; 3D18 ED 43 74 82
    ld     hl,($8274)      ; 3D1C 2A 74 82
    dec    hl              ; 3D1F 2B
    ld     ($8274),hl      ; 3D20 22 74 82
    ld     a,h             ; 3D23 7C
    or     l               ; 3D24 B5
    jp     nz,$3d43        ; 3D25 C2 43 3D
    ld     hl,($8266)      ; 3D28 2A 66 82
    ld     ($8274),hl      ; 3D2B 22 74 82
    ld     a,($8264)       ; 3D2E 3A 64 82
    cp     $0a             ; 3D31 FE 0A
    jp     nc,$3d3e        ; 3D33 D2 3E 3D
    add    a,$01           ; 3D36 C6 01
    ld     ($8264),a       ; 3D38 32 64 82
    jp     $3d43           ; 3D3B C3 43 3D
    ld     a,$0a           ; 3D3E 3E 0A
    ld     ($8264),a       ; 3D40 32 64 82
    ret                    ; 3D43 C9
    ld     hl,($80dc)      ; 3D44 2A DC 80
    dec    hl              ; 3D47 2B
    ld     ($80dc),hl      ; 3D48 22 DC 80
    ld     a,h             ; 3D4B 7C
    or     l               ; 3D4C B5
    jp     nz,$3d80        ; 3D4D C2 80 3D
    ld     hl,($80d8)      ; 3D50 2A D8 80
    ld     ($80dc),hl      ; 3D53 22 DC 80
    ld     de,$0010        ; 3D56 11 10 00
    ld     hl,($80d4)      ; 3D59 2A D4 80
    or     a               ; 3D5C B7
    sbc    hl,de           ; 3D5D ED 52
    jp     z,$3d65         ; 3D5F CA 65 3D
    jp     p,$3d68         ; 3D62 F2 68 3D
    ld     hl,$0018        ; 3D65 21 18 00
    ld     ($80d4),hl      ; 3D68 22 D4 80
    ld     hl,($80d6)      ; 3D6B 2A D6 80
    ld     de,$0010        ; 3D6E 11 10 00
    or     a               ; 3D71 B7
    sbc    hl,de           ; 3D72 ED 52
    jp     z,$3d7a         ; 3D74 CA 7A 3D
    jp     p,$3d7d         ; 3D77 F2 7D 3D
    ld     hl,$0001        ; 3D7A 21 01 00
    ld     ($80d6),hl      ; 3D7D 22 D6 80
    ret                    ; 3D80 C9
    ld     hl,$8278        ; 3D81 21 78 82
    ld     de,$0019        ; 3D84 11 19 00
    ld     b,$09           ; 3D87 06 09
    cp     (hl)            ; 3D89 BE
    jp     z,$3d93         ; 3D8A CA 93 3D
    add    hl,de           ; 3D8D 19
    djnz   $3d89           ; 3D8E 10 F9
    xor    a               ; 3D90 AF
    jr     $3d95           ; 3D91 18 02
    ld     a,$01           ; 3D93 3E 01
    or     a               ; 3D95 B7
    ret                    ; 3D96 C9

; 3D97h - data

    inc    c               ; 3D97 0C
    inc    d               ; 3D98 14
    inc    b               ; 3D99 04
    inc    d               ; 3D9A 14
    nop                    ; 3D9B 00
    nop                    ; 3D9C 00
    nop                    ; 3D9D 00
    nop                    ; 3D9E 00
    inc    d               ; 3D9F 14
    inc    c               ; 3DA0 0C
    call   m,$000c         ; 3DA1 FC 0C 00
    nop                    ; 3DA4 00
    nop                    ; 3DA5 00
    nop                    ; 3DA6 00
    djnz   $3dbd           ; 3DA7 10 14
    nop                    ; 3DA9 00
    inc    d               ; 3DAA 14
    djnz   $3da9           ; 3DAB 10 FC
    nop                    ; 3DAD 00
    call   m,$041c         ; 3DAE FC 1C 04
    call   m,$0c04         ; 3DB1 FC 04 0C
    call   p,$140c         ; 3DB4 F4 0C 14
    ex     af,af'          ; 3DB7 08
    ex     af,af'          ; 3DB8 08
    ld     b,b             ; 3DB9 40
    nop                    ; 3DBA 00

; 3DBBh - data

    ex     af,af'          ; 3DBB 08
    ex     af,af'          ; 3DBC 08
    ex     af,af'          ; 3DBD 08
    ex     af,af'          ; 3DBE 08
    ex     af,af'          ; 3DBF 08
    ex     af,af'          ; 3DC0 08
    ex     af,af'          ; 3DC1 08
    ex     af,af'          ; 3DC2 08
    ex     af,af'          ; 3DC3 08
    ex     af,af'          ; 3DC4 08
    ex     af,af'          ; 3DC5 08
    ex     af,af'          ; 3DC6 08
    ex     af,af'          ; 3DC7 08
    ex     af,af'          ; 3DC8 08
    ex     af,af'          ; 3DC9 08
    ex     af,af'          ; 3DCA 08

; 3DCBh - data

    inc    c               ; 3DCB 0C
    inc    c               ; 3DCC 0C
    inc    c               ; 3DCD 0C
    inc    c               ; 3DCE 0C
    ld     c,b             ; 3DCF 48
    ld     c,b             ; 3DD0 48
    jr     nc,$3d7b        ; 3DD1 30 A8
    ld     c,b             ; 3DD3 48
    ld     c,b             ; 3DD4 48
    jr     nc,$3d7f        ; 3DD5 30 A8
    jr     nc,$3e39        ; 3DD7 30 60
    jr     nc,$3d7b        ; 3DD9 30 A0
    jr     nc,$3e3d        ; 3DDB 30 60
    jr     nc,$3d7f        ; 3DDD 30 A0
    ld     c,b             ; 3DDF 48
    jr     nc,$3e42        ; 3DE0 30 60
    xor    b               ; 3DE2 A8
    ld     c,b             ; 3DE3 48
    jr     nc,$3e46        ; 3DE4 30 60
    xor    b               ; 3DE6 A8
    ld     c,b             ; 3DE7 48
    ld     c,b             ; 3DE8 48
    ld     c,b             ; 3DE9 48
    ret    nz              ; 3DEA C0
    ld     c,b             ; 3DEB 48
    ld     c,b             ; 3DEC 48
    ld     c,b             ; 3DED 48
    ret    nz              ; 3DEE C0
    ld     c,b             ; 3DEF 48
    ld     c,b             ; 3DF0 48
    ld     c,b             ; 3DF1 48
    xor    b               ; 3DF2 A8
    ld     c,b             ; 3DF3 48
    ld     c,b             ; 3DF4 48
    ld     c,b             ; 3DF5 48
    xor    b               ; 3DF6 A8
    jr     nc,$3e29        ; 3DF7 30 30
    jr     nc,$3dbb        ; 3DF9 30 C0
    jr     nc,$3e2d        ; 3DFB 30 30
    jr     nc,$3dbf        ; 3DFD 30 C0
    ld     h,b             ; 3DFF 60
    ld     c,b             ; 3E00 48
    ld     h,b             ; 3E01 60
    xor    b               ; 3E02 A8
    ld     h,b             ; 3E03 60
    ld     c,b             ; 3E04 48
    ld     h,b             ; 3E05 60
    xor    b               ; 3E06 A8
    jr     nc,$3e39        ; 3E07 30 30
    jr     nc,$3dcb        ; 3E09 30 C0
    jr     nc,$3e3d        ; 3E0B 30 30
    jr     nc,$3dcf        ; 3E0D 30 C0
    jr     nc,$3e59        ; 3E0F 30 48
    jr     nc,$3da3        ; 3E11 30 90
    jr     nc,$3e5d        ; 3E13 30 48
    jr     nc,$3da7        ; 3E15 30 90
    jr     nc,$3e61        ; 3E17 30 48
    jr     nc,$3dc3        ; 3E19 30 A8
    jr     nc,$3e65        ; 3E1B 30 48
    jr     nc,$3dc7        ; 3E1D 30 A8
    ld     c,b             ; 3E1F 48
    ld     h,b             ; 3E20 60
    ld     c,b             ; 3E21 48
    xor    b               ; 3E22 A8
    ld     c,b             ; 3E23 48
    ld     h,b             ; 3E24 60
    ld     c,b             ; 3E25 48
    xor    b               ; 3E26 A8
    ld     c,b             ; 3E27 48
    jr     nc,$3e72        ; 3E28 30 48
    ret    nz              ; 3E2A C0
    ld     c,b             ; 3E2B 48
    jr     nc,$3e76        ; 3E2C 30 48
    ret    nz              ; 3E2E C0
    jr     nc,$3e91        ; 3E2F 30 60
    jr     nc,$3ddb        ; 3E31 30 A8
    jr     nc,$3e95        ; 3E33 30 60
    jr     nc,$3ddf        ; 3E35 30 A8
    jr     $3e99           ; 3E37 18 60
    ld     a,b             ; 3E39 78
    xor    b               ; 3E3A A8
    jr     $3e9d           ; 3E3B 18 60
    ld     a,b             ; 3E3D 78
    xor    b               ; 3E3E A8
    ld     c,b             ; 3E3F 48
    jr     nc,$3e72        ; 3E40 30 30
    ret    nz              ; 3E42 C0
    ld     c,b             ; 3E43 48
    jr     nc,$3e76        ; 3E44 30 30
    ret    nz              ; 3E46 C0
    jr     $3ea9           ; 3E47 18 60
    jr     $3df3           ; 3E49 18 A8
    jr     $3ead           ; 3E4B 18 60
    jr     $3df7           ; 3E4D 18 A8
    jr     $3e29           ; 3E4F 18 D8
    jr     $3e6b           ; 3E51 18 18
    ret    c               ; 3E53 D8
    jr     $3e2e           ; 3E54 18 D8
    ret    c               ; 3E56 D8
    nop                    ; 3E57 00
    ld     (bc),a          ; 3E58 02
    inc    bc              ; 3E59 03
    ld     bc,$0301        ; 3E5A 01 01 03
    ld     (bc),a          ; 3E5D 02
    nop                    ; 3E5E 00
    ld     (bc),a          ; 3E5F 02
    ld     bc,$0300        ; 3E60 01 00 03
    inc    bc              ; 3E63 03
    nop                    ; 3E64 00
    ld     bc,$0402        ; 3E65 01 02 04
    dec    b               ; 3E68 05
    inc    b               ; 3E69 04
    dec    b               ; 3E6A 05
    ld     b,$07           ; 3E6B 06 07
    ex     af,af'          ; 3E6D 08
    rlca                   ; 3E6E 07
    add    hl,bc           ; 3E6F 09
    ld     a,(bc)          ; 3E70 0A
    dec    bc              ; 3E71 0B
    ld     a,(bc)          ; 3E72 0A
    cp     h               ; 3E73 BC
    cp     l               ; 3E74 BD
    cp     (hl)            ; 3E75 BE
    cp     a               ; 3E76 BF
    rst    38h             ; 3E77 FF
    nop                    ; 3E78 00
    jr     nc,$3ea8        ; 3E79 30 2D
    cpl                    ; 3E7B 2F
    rst    38h             ; 3E7C FF
    nop                    ; 3E7D 00
    add    hl,hl           ; 3E7E 29
    nop                    ; 3E7F 00
    jr     nc,$3eaf        ; 3E80 30 2D
    ld     l,$ff           ; 3E82 2E FF
    nop                    ; 3E84 00
    inc    l               ; 3E85 2C
    nop                    ; 3E86 00
    jr     nc,$3eb6        ; 3E87 30 2D
    ld     l,$ff           ; 3E89 2E FF
    nop                    ; 3E8B 00
    inc    l               ; 3E8C 2C
    add    a,b             ; 3E8D 80
    jr     nc,$3ebd        ; 3E8E 30 2D
    cpl                    ; 3E90 2F
    rst    38h             ; 3E91 FF
    add    a,b             ; 3E92 80
    add    hl,hl           ; 3E93 29
    add    a,b             ; 3E94 80
    jr     nc,$3ec4        ; 3E95 30 2D
    ld     l,$ff           ; 3E97 2E FF
    nop                    ; 3E99 00
    inc    l               ; 3E9A 2C
    add    a,b             ; 3E9B 80
    jr     nc,$3ecb        ; 3E9C 30 2D
    ld     l,$ff           ; 3E9E 2E FF
    nop                    ; 3EA0 00
    inc    l               ; 3EA1 2C
    add    a,b             ; 3EA2 80
    cpl                    ; 3EA3 2F
    add    hl,hl           ; 3EA4 29
    ld     hl,($80ff)      ; 3EA5 2A FF 80
    dec    hl              ; 3EA8 2B
    nop                    ; 3EA9 00
    cpl                    ; 3EAA 2F
    add    hl,hl           ; 3EAB 29
    ld     hl,($00ff)      ; 3EAC 2A FF 00
    dec    hl              ; 3EAF 2B
    nop                    ; 3EB0 00
    inc    l               ; 3EB1 2C
    dec    l               ; 3EB2 2D
    ld     l,$ff           ; 3EB3 2E FF
    nop                    ; 3EB5 00
    dec    l               ; 3EB6 2D
    add    a,b             ; 3EB7 80
    cpl                    ; 3EB8 2F
    add    hl,hl           ; 3EB9 29
    ld     hl,($80ff)      ; 3EBA 2A FF 80
    dec    hl              ; 3EBD 2B
    nop                    ; 3EBE 00
    cpl                    ; 3EBF 2F
    add    hl,hl           ; 3EC0 29
    ld     hl,($00ff)      ; 3EC1 2A FF 00
    dec    hl              ; 3EC4 2B
    nop                    ; 3EC5 00
    inc    l               ; 3EC6 2C
    dec    l               ; 3EC7 2D
    ld     l,$ff           ; 3EC8 2E FF
    nop                    ; 3ECA 00
    dec    l               ; 3ECB 2D
    add    hl,hl           ; 3ECC 29
    ld     hl,($2c2b)      ; 3ECD 2A 2B 2C
    dec    l               ; 3ED0 2D
    ld     l,$04           ; 3ED1 2E 04
    ld     d,b             ; 3ED3 50
    inc    b               ; 3ED4 04
    inc    (hl)            ; 3ED5 34
    inc    b               ; 3ED6 04
    ld     d,h             ; 3ED7 54
    inc    b               ; 3ED8 04
    jr     c,$3edf         ; 3ED9 38 04
    ld     h,b             ; 3EDB 60
    and    b               ; 3EDC A0
    and    c               ; 3EDD A1
    and    d               ; 3EDE A2
    and    e               ; 3EDF A3
    rst    38h             ; 3EE0 FF
    nop                    ; 3EE1 00
    ld     bc,$0302        ; 3EE2 01 02 03
    inc    b               ; 3EE5 04
    dec    b               ; 3EE6 05
    add    a,b             ; 3EE7 80
    add    a,c             ; 3EE8 81
    add    a,d             ; 3EE9 82
    add    a,e             ; 3EEA 83
    sub    b               ; 3EEB 90
    sub    c               ; 3EEC 91
    sub    d               ; 3EED 92
    sub    e               ; 3EEE 93
    rst    38h             ; 3EEF FF
    add    a,b             ; 3EF0 80
    add    a,c             ; 3EF1 81
    add    a,d             ; 3EF2 82
    add    a,e             ; 3EF3 83
    rst    38h             ; 3EF4 FF
    ld     h,b             ; 3EF5 60
    ld     h,c             ; 3EF6 61
    ld     h,d             ; 3EF7 62
    ld     h,e             ; 3EF8 63
    ld     h,(hl)          ; 3EF9 66
    ld     h,a             ; 3EFA 67
    ld     l,b             ; 3EFB 68
    ld     l,c             ; 3EFC 69
    ld     l,d             ; 3EFD 6A
    and    b               ; 3EFE A0
    and    d               ; 3EFF A2
    rst    38h             ; 3F00 FF
    nop                    ; 3F01 00
    ld     bc,$0200        ; 3F02 01 00 02
    nop                    ; 3F05 00
    inc    bc              ; 3F06 03
    nop                    ; 3F07 00
    dec    b               ; 3F08 05
    nop                    ; 3F09 00
    ex     af,af'          ; 3F0A 08
    nop                    ; 3F0B 00
    ld     (de),a          ; 3F0C 12
    nop                    ; 3F0D 00
    jr     nz,$3f10        ; 3F0E 20 00
    nop                    ; 3F10 00
    ld     bc,$0300        ; 3F11 01 00 03
    ld     (bc),a          ; 3F14 02
    nop                    ; 3F15 00
    nop                    ; 3F16 00
    nop                    ; 3F17 00
    nop                    ; 3F18 00
    nop                    ; 3F19 00
    nop                    ; 3F1A 00
    nop                    ; 3F1B 00
    nop                    ; 3F1C 00
    nop                    ; 3F1D 00
    nop                    ; 3F1E 00
    nop                    ; 3F1F 00
    ld     a,($b003)       ; 3F20 3A 03 B0
    ld     a,(iy+$03)      ; 3F23 FD 7E 03
    jp     $0498           ; 3F26 C3 98 04
    nop                    ; 3F29 00
    nop                    ; 3F2A 00
    nop                    ; 3F2B 00
    nop                    ; 3F2C 00
    nop                    ; 3F2D 00
    nop                    ; 3F2E 00
    nop                    ; 3F2F 00
    ld     a,($b003)       ; 3F30 3A 03 B0
    ld     a,(iy+$03)      ; 3F33 FD 7E 03
    jp     $04d8           ; 3F36 C3 D8 04
    nop                    ; 3F39 00
    nop                    ; 3F3A 00
    nop                    ; 3F3B 00
    nop                    ; 3F3C 00
    nop                    ; 3F3D 00
    nop                    ; 3F3E 00
    nop                    ; 3F3F 00
    ld     a,($b003)       ; 3F40 3A 03 B0
    ld     a,($b000)       ; 3F43 3A 00 B0
    and    $10             ; 3F46 E6 10
    jp     nz,$0539        ; 3F48 C2 39 05
    dec    hl              ; 3F4B 2B
    ld     a,h             ; 3F4C 7C
    or     l               ; 3F4D B5
    jp     $052a           ; 3F4E C3 2A 05

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3F51
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3F61
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3F71
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3F81
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3F91
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3FA1
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3FB1
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3FC1
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 3FD1
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ; 3FE1

    ld     hl,$3dcf        ; 3FF0 21 CF 3D
    jp     $3867           ; 3FF3 C3 67 38
    jp     $3181           ; 3FF6 C3 81 31
    jp     $3181           ; 3FF9 C3 81 31
    jp     $3181           ; 3FFC C3 81 31
    nop                    ; 3FFF 00
    call   $42e4           ; 4000 CD E4 42
    ld     b,$00           ; 4003 06 00
    ld     a,($8077)       ; 4005 3A 77 80
    or     a               ; 4008 B7
    jp     z,$4028         ; 4009 CA 28 40
    inc    b               ; 400C 04
    ld     a,($8074)       ; 400D 3A 74 80
    or     a               ; 4010 B7
    jp     nz,$4028        ; 4011 C2 28 40
    inc    b               ; 4014 04
    ld     a,($8080)       ; 4015 3A 80 80
    or     a               ; 4018 B7
    jp     nz,$4028        ; 4019 C2 28 40
    inc    b               ; 401C 04
    ld     a,($819b)       ; 401D 3A 9B 81
    dec    a               ; 4020 3D
    ld     ($819b),a       ; 4021 32 9B 81
    jp     p,$4028         ; 4024 F2 28 40
    inc    b               ; 4027 04
    ld     a,b             ; 4028 78
    ld     hl,$402f        ; 4029 21 2F 40
    jp     $00ea           ; 402C C3 EA 00
    ld     e,h             ; 402F 5C
    ld     b,b             ; 4030 40
    xor    l               ; 4031 AD
    ld     b,b             ; 4032 40
    add    a,a             ; 4033 87
    ld     b,c             ; 4034 41
    cp     d               ; 4035 BA
    ld     b,c             ; 4036 41
    ld     (hl),l          ; 4037 75
    ld     b,d             ; 4038 42
    and    $42             ; 4039 E6 42
    and    $42             ; 403B E6 42
    call   $4455           ; 403D CD 55 44
    ld     a,$00           ; 4040 3E 00
    ld     ($824e),a       ; 4042 32 4E 82
    ld     a,$00           ; 4045 3E 00
    ld     ($824f),a       ; 4047 32 4F 82
    ld     a,$00           ; 404A 3E 00
    ld     ($8249),a       ; 404C 32 49 82
    ld     a,$00           ; 404F 3E 00
    ld     ($8277),a       ; 4051 32 77 82
    ld     a,$01           ; 4054 3E 01
    ld     ($8003),a       ; 4056 32 03 80
    jp     $00c0           ; 4059 C3 C0 00
    call   $42e8           ; 405C CD E8 42
    call   $433d           ; 405F CD 3D 43
    ld     a,$00           ; 4062 3E 00
    ld     ($824b),a       ; 4064 32 4B 82
    ld     a,($8017)       ; 4067 3A 17 80
    or     a               ; 406A B7
    jp     z,$408c         ; 406B CA 8C 40
    ld     a,$a0           ; 406E 3E A0
    call   $00fc           ; 4070 CD FC 00
    xor    a               ; 4073 AF
    call   $4bd9           ; 4074 CD D9 4B
    call   $4b67           ; 4077 CD 67 4B
    ld     a,$01           ; 407A 3E 01
    ld     ($8070),a       ; 407C 32 70 80
    call   $43fd           ; 407F CD FD 43
    ld     a,$00           ; 4082 3E 00
    ld     ($8070),a       ; 4084 32 70 80
    call   $4369           ; 4087 CD 69 43
    jr     $408f           ; 408A 18 03
    call   $45cf           ; 408C CD CF 45
    call   $4495           ; 408F CD 95 44
    call   $4455           ; 4092 CD 55 44
    ld     a,$00           ; 4095 3E 00
    ld     ($8081),a       ; 4097 32 81 80
    ld     a,$01           ; 409A 3E 01
    ld     ($8070),a       ; 409C 32 70 80
    ld     bc,$001e        ; 409F 01 1E 00
    call   $00e1           ; 40A2 CD E1 00
    ld     a,$01           ; 40A5 3E 01
    ld     ($8003),a       ; 40A7 32 03 80
    jp     $00c0           ; 40AA C3 C0 00
    ld     a,$00           ; 40AD 3E 00
    ld     ($8028),a       ; 40AF 32 28 80
    ld     ($8077),a       ; 40B2 32 77 80
    ld     ($8074),a       ; 40B5 32 74 80
    ld     ($8088),a       ; 40B8 32 88 80
    nop                    ; 40BB 00
    call   $5ff8           ; 40BC CD F8 5F
    call   $6030           ; 40BF CD 30 60
    ld     a,$01           ; 40C2 3E 01
    call   $00fc           ; 40C4 CD FC 00
    ld     a,$25           ; 40C7 3E 25
    call   $00fc           ; 40C9 CD FC 00
    ld     a,$01           ; 40CC 3E 01
    ld     ($808d),a       ; 40CE 32 8D 80
    ld     bc,$00f0        ; 40D1 01 F0 00
    ld     a,$01           ; 40D4 3E 01
    ld     ($806e),a       ; 40D6 32 6E 80
    ld     a,($806e)       ; 40D9 3A 6E 80
    or     a               ; 40DC B7
    jp     nz,$40d9        ; 40DD C2 D9 40
    push   bc              ; 40E0 C5
    call   $6033           ; 40E1 CD 33 60
    call   $2000           ; 40E4 CD 00 20
    call   $3010           ; 40E7 CD 10 30
    pop    bc              ; 40EA C1
    dec    bc              ; 40EB 0B
    ld     a,b             ; 40EC 78
    or     c               ; 40ED B1
    jr     nz,$40d4        ; 40EE 20 E4
    ld     hl,$0d48        ; 40F0 21 48 0D
    ld     ($8851),hl      ; 40F3 22 51 88
    ld     a,$00           ; 40F6 3E 00
    ld     ($808d),a       ; 40F8 32 8D 80
    call   $4e20           ; 40FB CD 20 4E
    ld     a,$00           ; 40FE 3E 00
    ld     ($81ac),a       ; 4100 32 AC 81
    call   $4b67           ; 4103 CD 67 4B
    xor    a               ; 4106 AF
    call   $4bd9           ; 4107 CD D9 4B
    call   $5ff5           ; 410A CD F5 5F
    call   $2fe0           ; 410D CD E0 2F
    call   $2fe6           ; 4110 CD E6 2F
    call   $4e20           ; 4113 CD 20 4E
    ld     hl,$81a2        ; 4116 21 A2 81
    ld     bc,$0005        ; 4119 01 05 00
    call   $00cc           ; 411C CD CC 00
    ld     a,$00           ; 411F 3E 00		; (0x81ab) = 0;
    ld     ($81ab),a       ; 4121 32 AB 81
    ld     ($81ac),a       ; 4124 32 AC 81	; (0x81ac) = 0;
    ld     ($81b0),a       ; 4127 32 B0 81	; (0x81b0) = 0;
    ld     a,($81a0)       ; 412A 3A A0 81	; (0x81a0)++ /* next level */
    inc    a               ; 412D 3C
    ld     ($81a0),a       ; 412E 32 A0 81
    ld     a,($81a0)       ; 4131 3A A0 81	; if ((0x81a0) == 65) {
    cp     $41             ; 4134 FE 41
    jp     c,$413e         ; 4136 DA 3E 41
    ld     a,$20           ; 4139 3E 20		;   (0x81a0) = 32
    ld     ($81a0),a       ; 413B 32 A0 81	; }
    ld     a,($81af)       ; 413E 3A AF 81	; if ((0x81af) == 99)
    cp     $99             ; 4141 FE 99
    jr     z,$414a         ; 4143 28 05
    add    a,$01           ; 4145 C6 01
    daa                    ; 4147 27
    jr     $414c           ; 4148 18 02
    ld     a,$00           ; 414A 3E 00		; (0x81af) = 0
    ld     ($81af),a       ; 414C 32 AF 81
    ld     a,($8027)       ; 414F 3A 27 80
    call   $4440           ; 4152 CD 40 44
    ld     a,$01           ; 4155 3E 01
    ld     ($8248),a       ; 4157 32 48 82
    call   $4ece           ; 415A CD CE 4E
    call   $4bf4           ; 415D CD F4 4B
    ld     a,$18           ; 4160 3E 18
    ld     ($81a2),a       ; 4162 32 A2 81
    call   $600c           ; 4165 CD 0C 60
    call   $5fe9           ; 4168 CD E9 5F
    call   $42e8           ; 416B CD E8 42
    call   $433d           ; 416E CD 3D 43
    call   $441f           ; 4171 CD 1F 44
    jp     $4fa0           ; 4174 C3 A0 4F
    call   $5fe0           ; 4177 CD E0 5F
    ld     a,$00           ; 417A 3E 00
    ld     ($8453),a       ; 417C 32 53 84
    ld     a,$01           ; 417F 3E 01
    ld     ($8454),a       ; 4181 32 54 84
    jp     $403d           ; 4184 C3 3D 40
    ld     a,$00           ; 4187 3E 00
    ld     ($8077),a       ; 4189 32 77 80
    ld     a,$00           ; 418C 3E 00
    ld     ($8080),a       ; 418E 32 80 80
    ld     a,$00           ; 4191 3E 00
    ld     ($8088),a       ; 4193 32 88 80
    ld     a,$00           ; 4196 3E 00
    ld     ($808e),a       ; 4198 32 8E 80
    call   $4e20           ; 419B CD 20 4E
    xor    a               ; 419E AF
    call   $4bd9           ; 419F CD D9 4B
    call   $00f0           ; 41A2 CD F0 00
    ld     ix,$d24a        ; 41A5 DD 21 4A D2
    call   $00d2           ; 41A9 CD D2 00
    ld     a,$0d           ; 41AC 3E 0D
    call   $6018           ; 41AE CD 18 60
    call   $5ffb           ; 41B1 CD FB 5F
    call   $5ff8           ; 41B4 CD F8 5F
    jp     $40f0           ; 41B7 C3 F0 40
    ld     a,$00           ; 41BA 3E 00
    ld     ($8077),a       ; 41BC 32 77 80
    ld     a,$00           ; 41BF 3E 00
    ld     ($8088),a       ; 41C1 32 88 80
    ld     a,$00           ; 41C4 3E 00
    ld     ($8095),a       ; 41C6 32 95 80
    ld     a,($8028)       ; 41C9 3A 28 80
    res    2,a             ; 41CC CB 97
    ld     ($8028),a       ; 41CE 32 28 80
    ld     a,$01           ; 41D1 3E 01
    ld     ($808d),a       ; 41D3 32 8D 80
    ld     b,$7e           ; 41D6 06 7E
    ld     a,$01           ; 41D8 3E 01
    ld     ($806e),a       ; 41DA 32 6E 80
    ld     a,($806e)       ; 41DD 3A 6E 80
    or     a               ; 41E0 B7
    jp     nz,$41dd        ; 41E1 C2 DD 41
    push   bc              ; 41E4 C5
    call   $2000           ; 41E5 CD 00 20
    call   $3010           ; 41E8 CD 10 30
    pop    bc              ; 41EB C1
    ld     a,b             ; 41EC 78
    cp     $78             ; 41ED FE 78
    jp     nz,$41f7        ; 41EF C2 F7 41
    ld     a,$01           ; 41F2 3E 01
    call   $00fc           ; 41F4 CD FC 00
    djnz   $41d8           ; 41F7 10 DF
    ld     hl,$0d48        ; 41F9 21 48 0D
    ld     ($8851),hl      ; 41FC 22 51 88
    ld     a,$00           ; 41FF 3E 00
    ld     ($808d),a       ; 4201 32 8D 80
    ld     a,$00           ; 4204 3E 00
    call   $4bd9           ; 4206 CD D9 4B
    call   $4b67           ; 4209 CD 67 4B
    ld     a,$00           ; 420C 3E 00		; (0x81b0) = 0
    ld     ($81b0),a       ; 420E 32 B0 81
    call   $4e20           ; 4211 CD 20 4E
    call   $5fef           ; 4214 CD EF 5F
    ld     a,($8027)       ; 4217 3A 27 80
    call   $4440           ; 421A CD 40 44
    call   $4587           ; 421D CD 87 45
    ld     a,($8027)       ; 4220 3A 27 80
    call   $442b           ; 4223 CD 2B 44
    ld     a,($81ac)       ; 4226 3A AC 81
    cp     $00             ; 4229 FE 00
    jr     z,$4232         ; 422B 28 05
    call   $2fe3           ; 422D CD E3 2F
    jr     $4235           ; 4230 18 03
    call   $2fe0           ; 4232 CD E0 2F
    call   $2fe6           ; 4235 CD E6 2F
    call   $600c           ; 4238 CD 0C 60
    call   $5fec           ; 423B CD EC 5F
    ld     a,$00           ; 423E 3E 00
    ld     ($8248),a       ; 4240 32 48 82
    call   $4ece           ; 4243 CD CE 4E
    call   $4e20           ; 4246 CD 20 4E
    call   $4bf4           ; 4249 CD F4 4B
    call   $2fe6           ; 424C CD E6 2F
    call   $5fe9           ; 424F CD E9 5F
    call   $42e8           ; 4252 CD E8 42
    call   $441f           ; 4255 CD 1F 44
    call   $47c9           ; 4258 CD C9 47
    ld     a,($819b)       ; 425B 3A 9B 81
    inc    a               ; 425E 3C
    ld     ($819b),a       ; 425F 32 9B 81
    call   $433d           ; 4262 CD 3D 43
    call   $5fe0           ; 4265 CD E0 5F
    ld     a,($819b)       ; 4268 3A 9B 81
    dec    a               ; 426B 3D
    ld     ($819b),a       ; 426C 32 9B 81
    call   $433d           ; 426F CD 3D 43
    jp     $403d           ; 4272 C3 3D 40
    ld     a,$00           ; 4275 3E 00
    ld     ($8077),a       ; 4277 32 77 80
    ld     a,$00           ; 427A 3E 00
    ld     ($8088),a       ; 427C 32 88 80
    ld     a,$00           ; 427F 3E 00
    ld     ($808d),a       ; 4281 32 8D 80
    ld     a,$00           ; 4284 3E 00
    ld     ($824b),a       ; 4286 32 4B 82
    ld     a,$01           ; 4289 3E 01
    call   $00fc           ; 428B CD FC 00
    ld     a,$26           ; 428E 3E 26
    call   $00fc           ; 4290 CD FC 00
    ld     a,$01           ; 4293 3E 01
    call   $4bd9           ; 4295 CD D9 4B
    ld     a,($8027)       ; 4298 3A 27 80
    call   $4440           ; 429B CD 40 44
    call   $5fe3           ; 429E CD E3 5F
    call   $4e20           ; 42A1 CD 20 4E
    call   $4b67           ; 42A4 CD 67 4B
    call   $4800           ; 42A7 CD 00 48
    ld     a,$01           ; 42AA 3E 01
    call   $00fc           ; 42AC CD FC 00
    call   $4e20           ; 42AF CD 20 4E
    ld     a,($808b)       ; 42B2 3A 8B 80
    or     a               ; 42B5 B7
    jp     nz,$42d3        ; 42B6 C2 D3 42
    ld     a,($8026)       ; 42B9 3A 26 80
    or     a               ; 42BC B7
    jp     z,$42d3         ; 42BD CA D3 42
    ld     a,($8027)       ; 42C0 3A 27 80
    or     a               ; 42C3 B7
    jp     nz,$42cc        ; 42C4 C2 CC 42
    ld     a,($81cd)       ; 42C7 3A CD 81
    jr     $42cf           ; 42CA 18 03
    ld     a,($81b4)       ; 42CC 3A B4 81
    or     a               ; 42CF B7
    jp     p,$4211         ; 42D0 F2 11 42
    call   $4b39           ; 42D3 CD 39 4B	; reset variables
    ld     a,$00           ; 42D6 3E 00  	; (char *) 0x8070 = 0
    ld     ($8070),a       ; 42D8 32 70 80
    ld     a,$01           ; 42DB 3E 01		; (char *) 0x8003 = 1
    ld     ($8003),a       ; 42DD 32 03 80
    jp     $00c6           ; 42E0 C3 C6 00
    ret                    ; 42E3 C9 		; useless

    nop                    ; 42E4 00
    ret                    ; 42E5 C9
    nop                    ; 42E6 00
    ret                    ; 42E7 C9
    ld     a,($8070)       ; 42E8 3A 70 80
    or     a               ; 42EB B7
    jp     z,$4314         ; 42EC CA 14 43
    ld     a,($8027)       ; 42EF 3A 27 80
    or     a               ; 42F2 B7
    jp     z,$42fe         ; 42F3 CA FE 42
    call   $00cf           ; 42F6 CD CF 00
    sub    c               ; 42F9 91
    jp     z,$1803         ; 42FA CA 03 18
    ld     hl,($263a)      ; 42FD 2A 3A 26
    add    a,b             ; 4300 80
    or     a               ; 4301 B7
    jr     nz,$430c        ; 4302 20 08
    call   $00cf           ; 4304 CD CF 00
    adc    a,c             ; 4307 89
    jp     z,$1804         ; 4308 CA 04 18
    inc    e               ; 430B 1C
    call   $00cf           ; 430C CD CF 00
    add    a,e             ; 430F 83
    jp     z,$1803         ; 4310 CA 03 18
    inc    d               ; 4313 14
    ld     a,($8026)       ; 4314 3A 26 80
    or     a               ; 4317 B7
    jr     nz,$4322        ; 4318 20 08
    call   $00cf           ; 431A CD CF 00
    ld     l,l             ; 431D 6D
    jp     z,$1804         ; 431E CA 04 18
    ld     b,$cd           ; 4321 06 CD
    rst    08h             ; 4323 CF
    nop                    ; 4324 00
    ld     h,a             ; 4325 67
    jp     z,$3a03         ; 4326 CA 03 3A
    ld     h,$80           ; 4329 26 80
    or     a               ; 432B B7
    jr     nz,$4336        ; 432C 20 08
    call   $00d8           ; 432E CD D8 00
    ld     a,e             ; 4331 7B
    jp     z,$1802         ; 4332 CA 02 18
    ld     b,$cd           ; 4335 06 CD
    ret    c               ; 4337 D8
    nop                    ; 4338 00
    ld     (hl),l          ; 4339 75
    jp     z,$c903         ; 433A CA 03 C9
    call   $44ac           ; 433D CD AC 44
    ld     ix,$caa7        ; 4340 DD 21 A7 CA
    call   $00d2           ; 4344 CD D2 00
    ld     ix,$cac1        ; 4347 DD 21 C1 CA
    call   $00d5           ; 434B CD D5 00
    ld     ix,$cac7        ; 434E DD 21 C7 CA
    ld     a,($81af)       ; 4352 3A AF 81
    cp     $10             ; 4355 FE 10
    jp     c,$435e         ; 4357 DA 5E 43
    ld     ix,$cacf        ; 435A DD 21 CF CA
    call   $00d2           ; 435E CD D2 00
    ld     ix,$cad5        ; 4361 DD 21 D5 CA
    call   $00d2           ; 4365 CD D2 00
    ret                    ; 4368 C9
    call   $42e8           ; 4369 CD E8 42
    call   $433d           ; 436C CD 3D 43
    call   $2fe0           ; 436F CD E0 2F
    call   $4e20           ; 4372 CD 20 4E
    ld     a,($8017)       ; 4375 3A 17 80
    or     a               ; 4378 B7
    jp     z,$43c8         ; 4379 CA C8 43
    ld     a,($808b)       ; 437C 3A 8B 80
    or     a               ; 437F B7
    jp     nz,$438b        ; 4380 C2 8B 43
    call   $4cdf           ; 4383 CD DF 4C
    ld     a,$01           ; 4386 3E 01
    ld     ($8081),a       ; 4388 32 81 80
    call   $450b           ; 438B CD 0B 45
    nop                    ; 438E 00
    nop                    ; 438F 00
    nop                    ; 4390 00
    nop                    ; 4391 00
    nop                    ; 4392 00
    nop                    ; 4393 00
    nop                    ; 4394 00
    nop                    ; 4395 00
    nop                    ; 4396 00
    nop                    ; 4397 00
    nop                    ; 4398 00
    nop                    ; 4399 00
    nop                    ; 439A 00
    ld     a,$01           ; 439B 3E 01
    ld     ($8070),a       ; 439D 32 70 80
    call   $4e20           ; 43A0 CD 20 4E
    call   $602a           ; 43A3 CD 2A 60
    ld     hl,$81b4        ; 43A6 21 B4 81
    ld     bc,$0032        ; 43A9 01 32 00
    call   $00cc           ; 43AC CD CC 00
    call   $43cc           ; 43AF CD CC 43
    xor    a               ; 43B2 AF
    call   $4440           ; 43B3 CD 40 44
    ld     a,$01           ; 43B6 3E 01
    call   $4440           ; 43B8 CD 40 44
    ld     a,$00           ; 43BB 3E 00
    ld     ($81e5),a       ; 43BD 32 E5 81
    ld     a,($81cd)       ; 43C0 3A CD 81
    ld     ($8075),a       ; 43C3 32 75 80
    jr     $43cb           ; 43C6 18 03
    call   $43cc           ; 43C8 CD CC 43
    ret                    ; 43CB C9
    call   $4e20           ; 43CC CD 20 4E
    ld     a,$00           ; 43CF 3E 00
    ld     ($8248),a       ; 43D1 32 48 82
    call   $4ece           ; 43D4 CD CE 4E
    call   $4bf4           ; 43D7 CD F4 4B
    call   $600c           ; 43DA CD 0C 60
    call   $5fe9           ; 43DD CD E9 5F
    call   $47c9           ; 43E0 CD C9 47
    ld     a,($8017)       ; 43E3 3A 17 80
    or     a               ; 43E6 B7
    jr     z,$43e9         ; 43E7 28 00
    call   $42e8           ; 43E9 CD E8 42
    ld     a,($819b)       ; 43EC 3A 9B 81
    dec    a               ; 43EF 3D
    ld     ($819b),a       ; 43F0 32 9B 81
    call   $441f           ; 43F3 CD 1F 44
    call   $5fe0           ; 43F6 CD E0 5F
    call   $433d           ; 43F9 CD 3D 43
    ret                    ; 43FC C9

; 43FDh - start new game ?
;
; Also called before demo 1.

    ld     hl,$819b        ; 43FD 21 9B 81	; memset (0x819b, 0, 25)
    ld     bc,$0019        ; 4400 01 19 00
    call   $00cc           ; 4403 CD CC 00
    ld     a,($8071)       ; 4406 3A 71 80	; (0x819b) = (0x8071)
    ld     ($819b),a       ; 4409 32 9B 81
    ld     a,$01           ; 440C 3E 01		; (0x81a0) = 1
    ld     ($81a0),a       ; 440E 32 A0 81
    ld     a,$18           ; 4411 3E 18		; (0x81a2) = 24
    ld     ($81a2),a       ; 4413 32 A2 81
    ld     a,$01           ; 4416 3E 01		; (0x81af) = 1
    ld     ($81af),a       ; 4418 32 AF 81
    call   $6015           ; 441B CD 15 60
    ret                    ; 441E C9

    call   $00e7           ; 441F CD E7 00
    call   $4e27           ; 4422 CD 27 4E
    ld     a,$01           ; 4425 3E 01
    ld     ($808c),a       ; 4427 32 8C 80
    ret                    ; 442A C9
    or     a               ; 442B B7
    jp     nz,$4434        ; 442C C2 34 44
    ld     hl,$81b4        ; 442F 21 B4 81
    jr     $4437           ; 4432 18 03
    ld     hl,$81cd        ; 4434 21 CD 81
    ld     de,$819b        ; 4437 11 9B 81
    ld     bc,$0019        ; 443A 01 19 00
    ldir                   ; 443D ED B0
    ret                    ; 443F C9
    or     a               ; 4440 B7
    jp     nz,$4449        ; 4441 C2 49 44
    ld     de,$81b4        ; 4444 11 B4 81
    jr     $444c           ; 4447 18 03
    ld     de,$81cd        ; 4449 11 CD 81
    ld     hl,$819b        ; 444C 21 9B 81
    ld     bc,$0019        ; 444F 01 19 00
    ldir                   ; 4452 ED B0
    ret                    ; 4454 C9
    ld     ix,$8028        ; 4455 DD 21 28 80
    ld     (ix+$00),$00    ; 4459 DD 36 00 00
    ld     (ix+$01),$18    ; 445D DD 36 01 18
    ld     (ix+$02),$00    ; 4461 DD 36 02 00
    ld     (ix+$03),$78    ; 4465 DD 36 03 78
    ld     (ix+$04),$78    ; 4469 DD 36 04 78
    ld     (ix+$05),$00    ; 446D DD 36 05 00
    ld     (ix+$06),$00    ; 4471 DD 36 06 00
    ld     (ix+$09),$80    ; 4475 DD 36 09 80
    ld     (ix+$0b),$ff    ; 4479 DD 36 0B FF
    ld     (ix+$0e),$00    ; 447D DD 36 0E 00
    ld     (ix+$0e),$00    ; 4481 DD 36 0E 00
    ld     (ix+$12),$ff    ; 4485 DD 36 12 FF
    ld     hl,$8029        ; 4489 21 29 80
    ld     bc,$0004        ; 448C 01 04 00
    ld     de,$8500        ; 448F 11 00 85
    ldir                   ; 4492 ED B0
    ret                    ; 4494 C9
    ld     a,$00           ; 4495 3E 00
    ld     ($8453),a       ; 4497 32 53 84
    ld     a,$01           ; 449A 3E 01
    ld     ($8454),a       ; 449C 32 54 84
    ld     hl,$00e0        ; 449F 21 E0 00
    ld     ($845b),hl      ; 44A2 22 5B 84
    ld     hl,$fee0        ; 44A5 21 E0 FE
    ld     ($845d),hl      ; 44A8 22 5D 84
    ret                    ; 44AB C9
    ld     a,($819b)       ; 44AC 3A 9B 81
    or     a               ; 44AF B7
    jp     z,$44d3         ; 44B0 CA D3 44
    cp     $01             ; 44B3 FE 01
    jp     z,$44da         ; 44B5 CA DA 44
    cp     $02             ; 44B8 FE 02
    jp     z,$44e1         ; 44BA CA E1 44
    cp     $03             ; 44BD FE 03
    jp     z,$44e8         ; 44BF CA E8 44
    cp     $04             ; 44C2 FE 04
    jp     z,$44ef         ; 44C4 CA EF 44
    cp     $05             ; 44C7 FE 05
    jp     z,$44f6         ; 44C9 CA F6 44
    cp     $06             ; 44CC FE 06
    jp     z,$44fd         ; 44CE CA FD 44
    jr     $4504           ; 44D1 18 31
    call   $00cf           ; 44D3 CD CF 00
    in     a,($ca)         ; 44D6 DB CA
    ld     (bc),a          ; 44D8 02
    ret                    ; 44D9 C9
    call   $00cf           ; 44DA CD CF 00
    rst    18h             ; 44DD DF
    jp     z,$c902         ; 44DE CA 02 C9
    call   $00cf           ; 44E1 CD CF 00
    ex     (sp),hl         ; 44E4 E3
    jp     z,$c902         ; 44E5 CA 02 C9
    call   $00cf           ; 44E8 CD CF 00
    rst    20h             ; 44EB E7
    jp     z,$c902         ; 44EC CA 02 C9
    call   $00cf           ; 44EF CD CF 00
    ex     de,hl           ; 44F2 EB
    jp     z,$c902         ; 44F3 CA 02 C9
    call   $00cf           ; 44F6 CD CF 00
    rst    28h             ; 44F9 EF
    jp     z,$c902         ; 44FA CA 02 C9
    call   $00cf           ; 44FD CD CF 00
    di                     ; 4500 F3
    jp     z,$c902         ; 4501 CA 02 C9
    call   $00cf           ; 4504 CD CF 00
    rst    30h             ; 4507 F7
    jp     z,$c902         ; 4508 CA 02 C9
    ld     a,$02           ; 450B 3E 02
    ld     ($808c),a       ; 450D 32 8C 80
    ld     a,($808b)       ; 4510 3A 8B 80
    push   af              ; 4513 F5
    ld     a,$00           ; 4514 3E 00
    ld     ($808b),a       ; 4516 32 8B 80
    pop    af              ; 4519 F1
    cp     $02             ; 451A FE 02
    jp     z,$453e         ; 451C CA 3E 45
    cp     $01             ; 451F FE 01
    jp     z,$4568         ; 4521 CA 68 45
    ld     a,($b002)       ; 4524 3A 02 B0
    and    $08             ; 4527 E6 08
    jp     nz,$4536        ; 4529 C2 36 45
    ld     a,($b002)       ; 452C 3A 02 B0
    and    $04             ; 452F E6 04
    jp     nz,$4568        ; 4531 C2 68 45
    jr     $450b           ; 4534 18 D5
    ld     a,($8017)       ; 4536 3A 17 80
    cp     $02             ; 4539 FE 02
    jp     c,$450b         ; 453B DA 0B 45
    ld     a,$01           ; 453E 3E 01
    ld     ($8026),a       ; 4540 32 26 80
    ld     a,($8087)       ; 4543 3A 87 80
    or     a               ; 4546 B7
    jp     z,$455d         ; 4547 CA 5D 45
    ld     a,($8087)       ; 454A 3A 87 80
    dec    a               ; 454D 3D
    ld     ($8087),a       ; 454E 32 87 80
    jp     z,$457d         ; 4551 CA 7D 45
    ld     a,($8087)       ; 4554 3A 87 80
    dec    a               ; 4557 3D
    ld     ($8087),a       ; 4558 32 87 80
    jr     $4586           ; 455B 18 29
    ld     a,($8017)       ; 455D 3A 17 80
    add    a,$98           ; 4560 C6 98
    daa                    ; 4562 27
    ld     ($8017),a       ; 4563 32 17 80
    jr     $4586           ; 4566 18 1E
    ld     a,$00           ; 4568 3E 00
    ld     ($8026),a       ; 456A 32 26 80
    ld     a,($8087)       ; 456D 3A 87 80
    or     a               ; 4570 B7
    jp     z,$457d         ; 4571 CA 7D 45
    ld     a,($8087)       ; 4574 3A 87 80
    dec    a               ; 4577 3D
    ld     ($8087),a       ; 4578 32 87 80
    jr     $4586           ; 457B 18 09
    ld     a,($8017)       ; 457D 3A 17 80
    add    a,$99           ; 4580 C6 99
    daa                    ; 4582 27
    ld     ($8017),a       ; 4583 32 17 80
    ret                    ; 4586 C9
    ld     a,($8026)       ; 4587 3A 26 80
    or     a               ; 458A B7
    jp     z,$45ce         ; 458B CA CE 45
    ld     a,($8075)       ; 458E 3A 75 80
    or     a               ; 4591 B7
    jp     m,$45ce         ; 4592 FA CE 45
    ld     a,($8027)       ; 4595 3A 27 80
    inc    a               ; 4598 3C
    and    $01             ; 4599 E6 01
    ld     ($8027),a       ; 459B 32 27 80
    ld     a,($8013)       ; 459E 3A 13 80
    and    $40             ; 45A1 E6 40
    jp     nz,$45b9        ; 45A3 C2 B9 45
    ld     a,($8027)       ; 45A6 3A 27 80
    or     a               ; 45A9 B7
    jp     z,$45b4         ; 45AA CA B4 45
    ld     a,$01           ; 45AD 3E 01
    ld     ($b004),a       ; 45AF 32 04 B0
    jr     $45b9           ; 45B2 18 05
    ld     a,$00           ; 45B4 3E 00
    ld     ($b004),a       ; 45B6 32 04 B0
    ld     a,($8027)       ; 45B9 3A 27 80
    or     a               ; 45BC B7
    jp     nz,$45c8        ; 45BD C2 C8 45
    ld     a,($81cd)       ; 45C0 3A CD 81
    ld     ($8075),a       ; 45C3 32 75 80
    jr     $45ce           ; 45C6 18 06
    ld     a,($81b4)       ; 45C8 3A B4 81
    ld     ($8075),a       ; 45CB 32 75 80
    ret                    ; 45CE C9
    ld     a,$00           ; 45CF 3E 00
    ld     ($8088),a       ; 45D1 32 88 80
    ld     a,$00           ; 45D4 3E 00
    ld     ($808d),a       ; 45D6 32 8D 80
    ld     a,$01           ; 45D9 3E 01
    call   $00fc           ; 45DB CD FC 00
    call   $4e20           ; 45DE CD 20 4E
    xor    a               ; 45E1 AF
    call   $4bd9           ; 45E2 CD D9 4B
    call   $42e4           ; 45E5 CD E4 42
    call   $4b67           ; 45E8 CD 67 4B
    ld     a,($81ab)       ; 45EB 3A AB 81
    push   af              ; 45EE F5
    call   $43fd           ; 45EF CD FD 43
    pop    af              ; 45F2 F1
    ld     ($81ab),a       ; 45F3 32 AB 81
    call   $433d           ; 45F6 CD 3D 43
    ld     a,($8079)       ; 45F9 3A 79 80
    ld     hl,$4622        ; 45FC 21 22 46
    call   $00ea           ; 45FF CD EA 00
    ld     a,($8079)       ; 4602 3A 79 80
    inc    a               ; 4605 3C
    ld     ($8079),a       ; 4606 32 79 80
    ld     a,($8079)       ; 4609 3A 79 80
    cp     $04             ; 460C FE 04
    jp     c,$4616         ; 460E DA 16 46
    ld     a,$00           ; 4611 3E 00
    ld     ($8079),a       ; 4613 32 79 80
    ld     a,$00           ; 4616 3E 00
    ld     ($8078),a       ; 4618 32 78 80
    ld     a,$00           ; 461B 3E 00
    ld     ($824b),a       ; 461D 32 4B 82
    jr     $45cf           ; 4620 18 AD
    ld     ($5a46),a       ; 4622 32 46 5A
    ld     b,(hl)          ; 4625 46
    ld     d,b             ; 4626 50
    ld     b,a             ; 4627 47
    push   bc              ; 4628 C5
    ld     b,(hl)          ; 4629 46
    and    $42             ; 462A E6 42
    and    $42             ; 462C E6 42
    and    $42             ; 462E E6 42
    and    $42             ; 4630 E6 42
    call   $00cf           ; 4632 CD CF 00
    nop                    ; 4635 00
    ret    nz              ; 4636 C0
    inc    c               ; 4637 0C
    nop                    ; 4638 00
    call   $00d8           ; 4639 CD D8 00
    sub    (hl)            ; 463C 96
    pop    bc              ; 463D C1
    ld     a,(bc)          ; 463E 0A
    call   $00cf           ; 463F CD CF 00
    and    $c1             ; 4642 E6 C1
    ld     a,(bc)          ; 4644 0A
    call   $00d8           ; 4645 CD D8 00
    jp     m,$0ac1         ; 4648 FA C1 0A
    call   $479b           ; 464B CD 9B 47
    ld     a,$0d           ; 464E 3E 0D
    call   $6018           ; 4650 CD 18 60
    ld     bc,$012c        ; 4653 01 2C 01
    call   $00e1           ; 4656 CD E1 00
    ret                    ; 4659 C9
    call   $43fd           ; 465A CD FD 43
    call   $4369           ; 465D CD 69 43
    call   $4495           ; 4660 CD 95 44
    call   $4455           ; 4663 CD 55 44
    ld     a,$00           ; 4666 3E 00
    ld     ($807a),a       ; 4668 32 7A 80
    ld     a,$00           ; 466B 3E 00
    ld     ($807b),a       ; 466D 32 7B 80
    ld     a,($80e8)       ; 4670 3A E8 80
    cp     $00             ; 4673 FE 00
    jr     nz,$467c        ; 4675 20 05
    ld     hl,($cdf8)      ; 4677 2A F8 CD
    jr     $467f           ; 467A 18 03
    ld     hl,($cdfa)      ; 467C 2A FA CD
    ld     ($807c),hl      ; 467F 22 7C 80
    ld     ($807e),hl      ; 4682 22 7E 80
    ld     bc,$001e        ; 4685 01 1E 00
    call   $00e1           ; 4688 CD E1 00
    jr     $468d           ; 468B 18 00
    ld     hl,$0001        ; 468D 21 01 00
    ld     ($806e),hl      ; 4690 22 6E 80
    call   $4759           ; 4693 CD 59 47
    call   $1000           ; 4696 CD 00 10
    ld     a,($806e)       ; 4699 3A 6E 80
    or     a               ; 469C B7
    jr     z,$468d         ; 469D 28 EE
    call   $2000           ; 469F CD 00 20
    ld     a,($806e)       ; 46A2 3A 6E 80
    or     a               ; 46A5 B7
    jr     z,$468d         ; 46A6 28 E5
    call   $3000           ; 46A8 CD 00 30
    ld     a,($806e)       ; 46AB 3A 6E 80
    or     a               ; 46AE B7
    jr     z,$468d         ; 46AF 28 DC
    call   $5000           ; 46B1 CD 00 50
    ld     a,($8078)       ; 46B4 3A 78 80
    or     a               ; 46B7 B7
    jp     nz,$46c4        ; 46B8 C2 C4 46
    ld     a,($806e)       ; 46BB 3A 6E 80
    or     a               ; 46BE B7
    jp     z,$468d         ; 46BF CA 8D 46
    jr     $46bb           ; 46C2 18 F7
    ret                    ; 46C4 C9
    ld     a,($81ab)       ; 46C5 3A AB 81
    cp     $17             ; 46C8 FE 17
    jr     nz,$46dd        ; 46CA 20 11
    ld     a,$3c           ; 46CC 3E 3C
    ld     ($81b3),a       ; 46CE 32 B3 81
    ld     hl,$00e0        ; 46D1 21 E0 00
    ld     ($845b),hl      ; 46D4 22 5B 84
    ld     hl,$fee0        ; 46D7 21 E0 FE
    ld     ($845d),hl      ; 46DA 22 5D 84

; Set things up for demo 2

    ld     a,$00           ; 46DD 3E 00		; (0x81ab) = 0
    ld     ($81ab),a       ; 46DF 32 AB 81
    ld     a,$02           ; 46E2 3E 02		; (0x81a0) = 2
    ld     ($81a0),a       ; 46E4 32 A0 81
    ld     a,$18           ; 46E7 3E 18		; (0x81a2) = 24
    ld     ($81a2),a       ; 46E9 32 A2 81
    ld     a,$02           ; 46EC 3E 02		; (0x81af) = 2
    ld     ($81af),a       ; 46EE 32 AF 81
    ld     a,($8071)       ; 46F1 3A 71 80	; (0x819b) = (0x8071)
    ld     ($819b),a       ; 46F4 32 9B 81
    ld     a,($80e8)       ; 46F7 3A E8 80	; ?
    cp     $00             ; 46FA FE 00
    jr     nz,$4708        ; 46FC 20 0A
    ld     hl,($cdfc)      ; 46FE 2A FC CD
    ld     a,$01           ; 4701 3E 01
    ld     ($80e8),a       ; 4703 32 E8 80
    jr     $4710           ; 4706 18 08
    ld     hl,($cdfe)      ; 4708 2A FE CD
    ld     a,$00           ; 470B 3E 00
    ld     ($80e8),a       ; 470D 32 E8 80
    ld     ($807c),hl      ; 4710 22 7C 80
    ld     ($807e),hl      ; 4713 22 7E 80
    ld     a,$01           ; 4716 3E 01
    ld     ($8248),a       ; 4718 32 48 82
    ld     a,$00           ; 471B 3E 00
    ld     ($81a7),a       ; 471D 32 A7 81
    jr     $4722           ; 4720 18 00
    call   $2fe0           ; 4722 CD E0 2F
    call   $4ece           ; 4725 CD CE 4E
    call   $4bf4           ; 4728 CD F4 4B
    call   $600c           ; 472B CD 0C 60
    call   $5fe9           ; 472E CD E9 5F
    ld     a,$00           ; 4731 3E 00
    ld     ($807a),a       ; 4733 32 7A 80
    ld     a,$00           ; 4736 3E 00
    ld     ($807b),a       ; 4738 32 7B 80
    call   $433d           ; 473B CD 3D 43
    call   $42e8           ; 473E CD E8 42
    call   $441f           ; 4741 CD 1F 44
    call   $47c9           ; 4744 CD C9 47
    call   $5fe0           ; 4747 CD E0 5F
    call   $4455           ; 474A CD 55 44
    jp     $468d           ; 474D C3 8D 46
    ld     a,$0d           ; 4750 3E 0D
    call   $6018           ; 4752 CD 18 60
    call   $6021           ; 4755 CD 21 60
    ret                    ; 4758 C9
    ld     ix,$807a        ; 4759 DD 21 7A 80
    ld     a,(ix+$01)      ; 475D DD 7E 01
    cp     $00             ; 4760 FE 00
    jp     nz,$4797        ; 4762 C2 97 47
    ld     l,(ix+$02)      ; 4765 DD 6E 02
    ld     h,(ix+$03)      ; 4768 DD 66 03
    ld     a,(hl)          ; 476B 7E
    ld     (ix+$01),a      ; 476C DD 77 01
    cp     $ff             ; 476F FE FF
    jp     nz,$478a        ; 4771 C2 8A 47
    ld     (ix+$00),$00    ; 4774 DD 36 00 00
    ld     (ix+$01),$01    ; 4778 DD 36 01 01
    ld     a,(ix+$04)      ; 477C DD 7E 04
    ld     (ix+$02),a      ; 477F DD 77 02
    ld     a,(ix+$05)      ; 4782 DD 7E 05
    ld     (ix+$03),a      ; 4785 DD 77 03
    jr     $4796           ; 4788 18 0C
    inc    hl              ; 478A 23
    ld     a,(hl)          ; 478B 7E
    ld     (ix+$00),a      ; 478C DD 77 00
    inc    hl              ; 478F 23
    ld     (ix+$02),l      ; 4790 DD 75 02
    ld     (ix+$03),h      ; 4793 DD 74 03
    ret                    ; 4796 C9
    dec    (ix+$01)        ; 4797 DD 35 01
    ret                    ; 479A C9
    ld     hl,$8128        ; 479B 21 28 81
    ld     ix,$9487        ; 479E DD 21 87 94
    ld     b,$0a           ; 47A2 06 0A
    ld     c,(hl)          ; 47A4 4E
    ld     ($80e6),ix      ; 47A5 DD 22 E6 80
    ld     a,(hl)          ; 47A9 7E
    cp     c               ; 47AA B9
    jr     c,$47b4         ; 47AB 38 07
    jr     z,$47b4         ; 47AD 28 05
    ld     ($80e6),ix      ; 47AF DD 22 E6 80
    ld     c,a             ; 47B3 4F
    inc    hl              ; 47B4 23
    inc    ix              ; 47B5 DD 23
    inc    ix              ; 47B7 DD 23
    djnz   $47a9           ; 47B9 10 EE
    ld     hl,($80e6)      ; 47BB 2A E6 80
    ld     (hl),$0f        ; 47BE 36 0F
    or     a               ; 47C0 B7
    ld     de,$0020        ; 47C1 11 20 00
    sbc    hl,de           ; 47C4 ED 52
    ld     (hl),$0f        ; 47C6 36 0F
    ret                    ; 47C8 C9
    ld     a,$21           ; 47C9 3E 21
    call   $00fc           ; 47CB CD FC 00
    ld     hl,$de3d        ; 47CE 21 3D DE
    ld     bc,$0006        ; 47D1 01 06 00
    ld     a,($81a0)       ; 47D4 3A A0 81
    call   $00f6           ; 47D7 CD F6 00
    ld     a,(hl)          ; 47DA 7E
    add    a,$22           ; 47DB C6 22
    call   $00fc           ; 47DD CD FC 00
    ret                    ; 47E0 C9

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 47E1
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00    ; 47F1

    ld     ix,$8100        ; 4800 DD 21 00 81
    ld     b,$0a           ; 4804 06 0A
    ld     c,$00           ; 4806 0E 00
    ld     iy,$819c        ; 4808 FD 21 9C 81
    ld     a,(iy+$03)      ; 480C FD 7E 03
    sub    (ix+$03)        ; 480F DD 96 03
    jr     c,$4836         ; 4812 38 22
    jr     nz,$4843        ; 4814 20 2D
    ld     a,(iy+$02)      ; 4816 FD 7E 02
    sub    (ix+$02)        ; 4819 DD 96 02
    jr     c,$4836         ; 481C 38 18
    jr     nz,$4843        ; 481E 20 23
    ld     a,(iy+$01)      ; 4820 FD 7E 01
    sub    (ix+$01)        ; 4823 DD 96 01
    jr     c,$4836         ; 4826 38 0E
    jr     nz,$4843        ; 4828 20 19
    ld     a,(iy+$00)      ; 482A FD 7E 00
    sub    (ix+$00)        ; 482D DD 96 00
    jr     c,$4836         ; 4830 38 04
    jr     z,$4836         ; 4832 28 02
    jr     $4843           ; 4834 18 0D
    inc    ix              ; 4836 DD 23
    inc    ix              ; 4838 DD 23
    inc    ix              ; 483A DD 23
    inc    ix              ; 483C DD 23
    inc    c               ; 483E 0C
    djnz   $480c           ; 483F 10 CB
    jr     $484a           ; 4841 18 07
    ld     a,c             ; 4843 79
    ld     ($8196),a       ; 4844 32 96 81
    call   $484b           ; 4847 CD 4B 48
    ret                    ; 484A C9
    call   $4e20           ; 484B CD 20 4E
    ld     hl,$8500        ; 484E 21 00 85
    ld     bc,$0060        ; 4851 01 60 00
    call   $00cc           ; 4854 CD CC 00
    call   $48fc           ; 4857 CD FC 48
    call   $497c           ; 485A CD 7C 49
    call   $4b20           ; 485D CD 20 4B
    ld     a,$a6           ; 4860 3E A6
    call   $00fc           ; 4862 CD FC 00
    ld     a,($8196)       ; 4865 3A 96 81
    or     a               ; 4868 B7
    jp     nz,$4879        ; 4869 C2 79 48
    ld     a,$29           ; 486C 3E 29
    call   $00fc           ; 486E CD FC 00
    ld     a,$0d           ; 4871 3E 0D
    call   $6018           ; 4873 CD 18 60
    jp     $4883           ; 4876 C3 83 48
    ld     a,$2a           ; 4879 3E 2A
    call   $00fc           ; 487B CD FC 00
    ld     a,$0d           ; 487E 3E 0D
    call   $6018           ; 4880 CD 18 60
    call   $479b           ; 4883 CD 9B 47
    ld     hl,($80e6)      ; 4886 2A E6 80
    push   hl              ; 4889 E5
    pop    ix              ; 488A DD E1
    ld     (ix+$00),$02    ; 488C DD 36 00 02
    ld     (ix-$20),$02    ; 4890 DD 36 E0 02
    ld     ix,$9687        ; 4894 DD 21 87 96
    ld     (ix-$60),$02    ; 4898 DD 36 A0 02
    ld     (ix-$40),$02    ; 489C DD 36 C0 02
    ld     (ix-$20),$02    ; 48A0 DD 36 E0 02
    ld     (ix+$00),$02    ; 48A4 DD 36 00 02
    ld     (ix+$20),$02    ; 48A8 DD 36 20 02
    ld     (ix+$40),$02    ; 48AC DD 36 40 02
    ld     (ix+$60),$02    ; 48B0 DD 36 60 02
    ld     (ix-$80),$02    ; 48B4 DD 36 80 02
    ld     ix,$961c        ; 48B8 DD 21 1C 96
    ld     (ix-$80),$01    ; 48BC DD 36 80 01
    ld     (ix-$60),$01    ; 48C0 DD 36 A0 01
    ld     (ix-$40),$01    ; 48C4 DD 36 C0 01
    ld     (ix-$20),$01    ; 48C8 DD 36 E0 01
    ld     (ix+$00),$01    ; 48CC DD 36 00 01
    ld     (ix+$20),$01    ; 48D0 DD 36 20 01
    ld     a,($8196)       ; 48D4 3A 96 81
    ld     ix,$9407        ; 48D7 DD 21 07 94
    cp     $00             ; 48DB FE 00
    jr     z,$48e6         ; 48DD 28 07
    inc    ix              ; 48DF DD 23
    inc    ix              ; 48E1 DD 23
    dec    a               ; 48E3 3D
    jr     $48db           ; 48E4 18 F5
    ld     a,$20           ; 48E6 3E 20
    ld     de,$0020        ; 48E8 11 20 00
    cp     $00             ; 48EB FE 00
    jr     z,$48f8         ; 48ED 28 09
    ld     (ix+$00),$0f    ; 48EF DD 36 00 0F
    add    ix,de           ; 48F3 DD 19
    dec    a               ; 48F5 3D
    jr     $48eb           ; 48F6 18 F3
    call   $4993           ; 48F8 CD 93 49
    ret                    ; 48FB C9
    ld     a,($8196)       ; 48FC 3A 96 81
    ld     hl,$8131        ; 48FF 21 31 81
    ld     de,$8130        ; 4902 11 30 81
    push   af              ; 4905 F5
    ld     b,a             ; 4906 47
    ld     a,$09           ; 4907 3E 09
    sub    b               ; 4909 90
    ld     b,a             ; 490A 47
    or     a               ; 490B B7
    jr     z,$4914         ; 490C 28 06
    ld     a,(de)          ; 490E 1A
    ld     (hl),a          ; 490F 77
    dec    hl              ; 4910 2B
    dec    de              ; 4911 1B
    djnz   $490e           ; 4912 10 FA
    ld     a,($81af)       ; 4914 3A AF 81
    ld     (hl),a          ; 4917 77
    pop    af              ; 4918 F1
    push   af              ; 4919 F5
    ld     b,a             ; 491A 47
    ld     a,$09           ; 491B 3E 09
    sub    b               ; 491D 90
    ld     b,a             ; 491E 47
    or     a               ; 491F B7
    jr     z,$4963         ; 4920 28 41
    ld     hl,$8120        ; 4922 21 20 81
    ld     de,$8124        ; 4925 11 24 81
    ld     c,$04           ; 4928 0E 04
    ld     a,(hl)          ; 492A 7E
    ld     (de),a          ; 492B 12
    inc    hl              ; 492C 23
    inc    de              ; 492D 13
    dec    c               ; 492E 0D
    jr     nz,$492a        ; 492F 20 F9
    push   bc              ; 4931 C5
    ld     bc,$fff8        ; 4932 01 F8 FF
    add    hl,bc           ; 4935 09
    push   hl              ; 4936 E5
    ex     de,hl           ; 4937 EB
    add    hl,bc           ; 4938 09
    ex     de,hl           ; 4939 EB
    pop    hl              ; 493A E1
    pop    bc              ; 493B C1
    djnz   $4928           ; 493C 10 EA
    pop    af              ; 493E F1
    push   af              ; 493F F5
    ld     b,a             ; 4940 47
    ld     a,$09           ; 4941 3E 09
    sub    b               ; 4943 90
    ld     b,a             ; 4944 47
    ld     hl,$8184        ; 4945 21 84 81
    ld     de,$818e        ; 4948 11 8E 81
    ld     c,$03           ; 494B 0E 03
    ld     a,(hl)          ; 494D 7E
    ld     (de),a          ; 494E 12
    inc    hl              ; 494F 23
    inc    hl              ; 4950 23
    inc    de              ; 4951 13
    inc    de              ; 4952 13
    dec    c               ; 4953 0D
    jr     nz,$494d        ; 4954 20 F7
    push   bc              ; 4956 C5
    ld     bc,$fff0        ; 4957 01 F0 FF
    add    hl,bc           ; 495A 09
    push   hl              ; 495B E5
    ex     de,hl           ; 495C EB
    add    hl,bc           ; 495D 09
    ex     de,hl           ; 495E EB
    pop    hl              ; 495F E1
    pop    bc              ; 4960 C1
    djnz   $494b           ; 4961 10 E8
    pop    af              ; 4963 F1
    ld     b,a             ; 4964 47
    nop                    ; 4965 00
    ld     hl,$80fc        ; 4966 21 FC 80
    ld     de,$0004        ; 4969 11 04 00
    inc    b               ; 496C 04
    add    hl,de           ; 496D 19
    djnz   $496d           ; 496E 10 FD
    ld     de,$819c        ; 4970 11 9C 81
    ld     b,$04           ; 4973 06 04
    ld     a,(de)          ; 4975 1A
    ld     (hl),a          ; 4976 77
    inc    de              ; 4977 13
    inc    hl              ; 4978 23
    djnz   $4975           ; 4979 10 FA
    ret                    ; 497B C9
    ld     a,($8196)       ; 497C 3A 96 81
    inc    a               ; 497F 3C
    ld     hl,$812a        ; 4980 21 2A 81
    ld     de,$000a        ; 4983 11 0A 00
    add    hl,de           ; 4986 19
    dec    a               ; 4987 3D
    jr     nz,$4986        ; 4988 20 FC
    ld     b,$03           ; 498A 06 03
    ld     (hl),$00        ; 498C 36 00
    inc    hl              ; 498E 23
    inc    hl              ; 498F 23
    djnz   $498c           ; 4990 10 FA
    ret                    ; 4992 C9
    ld     bc,$0e10        ; 4993 01 10 0E
    ld     ($806e),bc      ; 4996 ED 43 6E 80
    ld     a,$01           ; 499A 3E 01
    ld     ($8197),a       ; 499C 32 97 81
    call   $4a41           ; 499F CD 41 4A
    call   $4acd           ; 49A2 CD CD 4A
    call   $4ab4           ; 49A5 CD B4 4A
    ld     (hl),$41        ; 49A8 36 41
    call   $4acd           ; 49AA CD CD 4A
    ld     a,($801b)       ; 49AD 3A 1B 80
    bit    4,a             ; 49B0 CB 67
    jp     nz,$4a05        ; 49B2 C2 05 4A
    ld     a,($8019)       ; 49B5 3A 19 80
    bit    4,a             ; 49B8 CB 67
    jp     nz,$49de        ; 49BA C2 DE 49
    ld     a,($8020)       ; 49BD 3A 20 80
    cp     $03             ; 49C0 FE 03
    jr     z,$4a2d         ; 49C2 28 69
    call   $4e83           ; 49C4 CD 83 4E
    or     a               ; 49C7 B7
    jp     nz,$49dd        ; 49C8 C2 DD 49
    ld     bc,($806e)      ; 49CB ED 4B 6E 80
    ld     a,c             ; 49CF 79
    or     b               ; 49D0 B0
    jr     nz,$49ad        ; 49D1 20 DA
    xor    a               ; 49D3 AF
    call   $4a41           ; 49D4 CD 41 4A
    ld     bc,$00b4        ; 49D7 01 B4 00
    call   $00e1           ; 49DA CD E1 00
    ret                    ; 49DD C9
    ld     a,$00           ; 49DE 3E 00
    ld     ($8019),a       ; 49E0 32 19 80
    call   $4ab4           ; 49E3 CD B4 4A
    ld     a,(hl)          ; 49E6 7E
    cp     $00             ; 49E7 FE 00
    jr     nz,$49ef        ; 49E9 20 04
    ld     (hl),$41        ; 49EB 36 41
    jr     $4a00           ; 49ED 18 11
    cp     $5a             ; 49EF FE 5A
    jr     nz,$49f7        ; 49F1 20 04
    ld     (hl),$2e        ; 49F3 36 2E
    jr     $4a00           ; 49F5 18 09
    cp     $2e             ; 49F7 FE 2E
    jr     nz,$49ff        ; 49F9 20 04
    ld     (hl),$00        ; 49FB 36 00
    jr     $4a00           ; 49FD 18 01
    inc    (hl)            ; 49FF 34
    call   $4acd           ; 4A00 CD CD 4A
    jr     $49c4           ; 4A03 18 BF
    ld     a,$00           ; 4A05 3E 00
    ld     ($801b),a       ; 4A07 32 1B 80
    call   $4ab4           ; 4A0A CD B4 4A
    ld     a,(hl)          ; 4A0D 7E
    cp     $00             ; 4A0E FE 00
    jr     nz,$4a16        ; 4A10 20 04
    ld     (hl),$2e        ; 4A12 36 2E
    jr     $4a27           ; 4A14 18 11
    cp     $41             ; 4A16 FE 41
    jr     nz,$4a1e        ; 4A18 20 04
    ld     (hl),$00        ; 4A1A 36 00
    jr     $4a27           ; 4A1C 18 09
    cp     $2e             ; 4A1E FE 2E
    jr     nz,$4a26        ; 4A20 20 04
    ld     (hl),$5a        ; 4A22 36 5A
    jr     $4a27           ; 4A24 18 01
    dec    (hl)            ; 4A26 35
    call   $4acd           ; 4A27 CD CD 4A
    jp     $49c4           ; 4A2A C3 C4 49
    inc    a               ; 4A2D 3C
    inc    a               ; 4A2E 3C
    ld     ($8020),a       ; 4A2F 32 20 80
    ld     a,($8197)       ; 4A32 3A 97 81
    inc    a               ; 4A35 3C
    cp     $04             ; 4A36 FE 04
    ld     ($8197),a       ; 4A38 32 97 81
    jp     c,$49a5         ; 4A3B DA A5 49
    jp     nc,$49d3        ; 4A3E D2 D3 49
    push   af              ; 4A41 F5
    ld     de,$0000        ; 4A42 11 00 00
    ld     hl,$4a82        ; 4A45 21 82 4A
    ld     a,($8196)       ; 4A48 3A 96 81
    add    a,a             ; 4A4B 87
    ld     e,a             ; 4A4C 5F
    add    hl,de           ; 4A4D 19
    ld     e,(hl)          ; 4A4E 5E
    inc    hl              ; 4A4F 23
    ld     d,(hl)          ; 4A50 56
    ex     de,hl           ; 4A51 EB
    pop    af              ; 4A52 F1
    ld     de,$4a96        ; 4A53 11 96 4A
    or     a               ; 4A56 B7
    jr     z,$4a5c         ; 4A57 28 03
    ld     de,$4aa5        ; 4A59 11 A5 4A
    ld     b,$05           ; 4A5C 06 05
    ld     c,$03           ; 4A5E 0E 03
    push   de              ; 4A60 D5
    push   hl              ; 4A61 E5
    ld     a,(de)          ; 4A62 1A
    cp     $ff             ; 4A63 FE FF
    jr     z,$4a68         ; 4A65 28 01
    ld     (hl),a          ; 4A67 77
    cp     $00             ; 4A68 FE 00
    jr     z,$4a72         ; 4A6A 28 06
    ld     de,$0400        ; 4A6C 11 00 04
    add    hl,de           ; 4A6F 19
    ld     (hl),$0f        ; 4A70 36 0F
    pop    hl              ; 4A72 E1
    pop    de              ; 4A73 D1
    inc    hl              ; 4A74 23
    inc    de              ; 4A75 13
    dec    c               ; 4A76 0D
    jr     nz,$4a60        ; 4A77 20 E7
    push   de              ; 4A79 D5
    ld     de,$001d        ; 4A7A 11 1D 00
    add    hl,de           ; 4A7D 19
    pop    de              ; 4A7E D1
    djnz   $4a5e           ; 4A7F 10 DD
    ret                    ; 4A81 C9

; 4A82h - data

    ld     h,(hl)          ; 4A82 66
    sub    c               ; 4A83 91
    ld     l,b             ; 4A84 68
    sub    c               ; 4A85 91
    ld     l,d             ; 4A86 6A
    sub    c               ; 4A87 91
    ld     l,h             ; 4A88 6C
    sub    c               ; 4A89 91
    ld     l,(hl)          ; 4A8A 6E
    sub    c               ; 4A8B 91
    ld     (hl),b          ; 4A8C 70
    sub    c               ; 4A8D 91
    ld     (hl),d          ; 4A8E 72
    sub    c               ; 4A8F 91
    ld     (hl),h          ; 4A90 74
    sub    c               ; 4A91 91
    halt                   ; 4A92 76
    sub    c               ; 4A93 91
    ld     a,b             ; 4A94 78
    sub    c               ; 4A95 91
    nop                    ; 4A96 00
    nop                    ; 4A97 00
    nop                    ; 4A98 00
    nop                    ; 4A99 00
    rst    38h             ; 4A9A FF
    nop                    ; 4A9B 00
    nop                    ; 4A9C 00
    rst    38h             ; 4A9D FF
    nop                    ; 4A9E 00
    nop                    ; 4A9F 00
    rst    38h             ; 4AA0 FF
    nop                    ; 4AA1 00
    nop                    ; 4AA2 00
    nop                    ; 4AA3 00
    nop                    ; 4AA4 00
    jp     p,$f7f4         ; 4AA5 F2 F4 F7
    pop    af              ; 4AA8 F1
    nop                    ; 4AA9 00
    or     $f1             ; 4AAA F6 F1
    nop                    ; 4AAC 00
    or     $f1             ; 4AAD F6 F1
    nop                    ; 4AAF 00
    or     $f0             ; 4AB0 F6 F0
    di                     ; 4AB2 F3
    push   af              ; 4AB3 F5
    ld     a,($8196)       ; 4AB4 3A 96 81
    ld     hl,$8132        ; 4AB7 21 32 81
    ld     de,$000a        ; 4ABA 11 0A 00
    or     a               ; 4ABD B7
    jr     z,$4ac4         ; 4ABE 28 04
    add    hl,de           ; 4AC0 19
    dec    a               ; 4AC1 3D
    jr     $4abd           ; 4AC2 18 F9
    ld     a,($8197)       ; 4AC4 3A 97 81
    add    a,a             ; 4AC7 87
    ld     d,$00           ; 4AC8 16 00
    ld     e,a             ; 4ACA 5F
    add    hl,de           ; 4ACB 19
    ret                    ; 4ACC C9
    ld     a,($8196)       ; 4ACD 3A 96 81
    ld     hl,$8132        ; 4AD0 21 32 81
    ld     de,$000a        ; 4AD3 11 0A 00
    or     a               ; 4AD6 B7
    jr     z,$4ade         ; 4AD7 28 05
    add    hl,de           ; 4AD9 19
    dec    a               ; 4ADA 3D
    jp     $4ad6           ; 4ADB C3 D6 4A
    push   hl              ; 4ADE E5
    pop    ix              ; 4ADF DD E1
    ld     a,(ix+$03)      ; 4AE1 DD 7E 03
    push   af              ; 4AE4 F5
    ld     a,(ix+$05)      ; 4AE5 DD 7E 05
    push   af              ; 4AE8 F5
    ld     a,(ix+$07)      ; 4AE9 DD 7E 07
    push   af              ; 4AEC F5
    ld     (ix+$03),$06    ; 4AED DD 36 03 06
    ld     (ix+$05),$06    ; 4AF1 DD 36 05 06
    ld     (ix+$07),$06    ; 4AF5 DD 36 07 06
    ld     a,($8197)       ; 4AF9 3A 97 81
    cp     $03             ; 4AFC FE 03
    jr     z,$4b0c         ; 4AFE 28 0C
    cp     $02             ; 4B00 FE 02
    jr     z,$4b08         ; 4B02 28 04
    ld     (ix+$03),$03    ; 4B04 DD 36 03 03
    ld     (ix+$05),$03    ; 4B08 DD 36 05 03
    ld     (ix+$07),$03    ; 4B0C DD 36 07 03
    call   $00d2           ; 4B10 CD D2 00
    pop    af              ; 4B13 F1
    ld     (ix+$07),a      ; 4B14 DD 77 07
    pop    af              ; 4B17 F1
    ld     (ix+$05),a      ; 4B18 DD 77 05
    pop    af              ; 4B1B F1
    ld     (ix+$03),a      ; 4B1C DD 77 03
    ret                    ; 4B1F C9
    call   $00cf           ; 4B20 CD CF 00
    ld     c,d             ; 4B23 4A
    jp     nz,$cd0d        ; 4B24 C2 0D CD
    ret    c               ; 4B27 D8
    nop                    ; 4B28 00
    sub    (hl)            ; 4B29 96
    pop    bc              ; 4B2A C1
    ld     a,(bc)          ; 4B2B 0A
    call   $00cf           ; 4B2C CD CF 00
    and    $c1             ; 4B2F E6 C1
    ld     a,(bc)          ; 4B31 0A
    call   $00d8           ; 4B32 CD D8 00
    jp     m,$0ac1         ; 4B35 FA C1 0A
    ret                    ; 4B38 C9

; 4B39h - reset variables

    call   $4b67           ; 4B39 CD 67 4B
    ld     hl,$819b        ; 4B3C 21 9B 81	; memset (0x819b, 0, 25)
    ld     bc,$0019        ; 4B3F 01 19 00
    call   $00cc           ; 4B42 CD CC 00
    ld     hl,$81b9        ; 4B45 21 B9 81	; memset (0x81b9, 0, 20)
    ld     bc,$0014        ; 4B48 01 14 00
    call   $00cc           ; 4B4B CD CC 00
    ld     hl,$81d2        ; 4B4E 21 D2 81	; memset (0x81d2, 0, 20)
    ld     bc,$0014        ; 4B51 01 14 00
    call   $00cc           ; 4B54 CD CC 00
    ld     a,$00           ; 4B57 3E 00		; (0x8079) = 0
    ld     ($8079),a       ; 4B59 32 79 80
    ld     a,$00           ; 4B5C 3E 00		; (0xb004) = 0
    ld     ($b004),a       ; 4B5E 32 04 B0
    ld     a,$00           ; 4B61 3E 00		; (0x8027) = 0
    ld     ($8027),a       ; 4B63 32 27 80
    ret                    ; 4B66 C9

; 4B67h - reset variables

    ld     hl,($845b)      ; 4B67 2A 5B 84	; push (short *) 0x845b
    push   hl              ; 4B6A E5
    ld     hl,($845d)      ; 4B6B 2A 5D 84	; push (short *) 0x845d
    push   hl              ; 4B6E E5
    ld     hl,$8028        ; 4B6F 21 28 80	; memset (0x8028, 0, 70)
    ld     bc,$0046        ; 4B72 01 46 00
    call   $00cc           ; 4B75 CD CC 00
    ld     hl,$8278        ; 4B78 21 78 82	; memset (0x8278, 0, 0x1f4)
    ld     bc,$01f4        ; 4B7B 01 F4 01
    call   $00cc           ; 4B7E CD CC 00
    ld     hl,$80d2        ; 4B81 21 D2 80	; memset (0x80d2, 0, 16)
    ld     bc,$0010        ; 4B84 01 10 00
    call   $00cc           ; 4B87 CD CC 00
    ld     hl,$80c0        ; 4B8A 21 C0 80	; memset (0x80c0, 0, 18)
    ld     bc,$0012        ; 4B8D 01 12 00
    call   $00cc           ; 4B90 CD CC 00
    ld     hl,$8900        ; 4B93 21 00 89	; memset (0x8900, 0, 20)
    ld     bc,$0014        ; 4B96 01 14 00
    call   $00cc           ; 4B99 CD CC 00
    pop    hl              ; 4B9C E1 		; pop (short *) 0x845d
    ld     ($845d),hl      ; 4B9D 22 5D 84
    pop    hl              ; 4BA0 E1 		; pop (short *) 0x845b
    ld     ($845b),hl      ; 4BA1 22 5B 84
    ret                    ; 4BA4 C9

    ld     hl,$9000        ; 4BA5 21 00 90
    ld     bc,$0400        ; 4BA8 01 00 04
    ld     (hl),$00        ; 4BAB 36 00
    inc    hl              ; 4BAD 23
    dec    bc              ; 4BAE 0B
    ld     a,b             ; 4BAF 78
    or     c               ; 4BB0 B1
    jp     nz,$4bab        ; 4BB1 C2 AB 4B
    ld     hl,$9800        ; 4BB4 21 00 98
    ld     bc,$0400        ; 4BB7 01 00 04
    ld     (hl),$00        ; 4BBA 36 00
    inc    hl              ; 4BBC 23
    dec    bc              ; 4BBD 0B
    ld     a,b             ; 4BBE 78
    or     c               ; 4BBF B1
    jp     nz,$4bba        ; 4BC0 C2 BA 4B
    ret                    ; 4BC3 C9
    push   af              ; 4BC4 F5
    push   bc              ; 4BC5 C5
    push   hl              ; 4BC6 E5
    ld     hl,$8028        ; 4BC7 21 28 80
    ld     bc,$0020        ; 4BCA 01 20 00
    ld     (hl),$00        ; 4BCD 36 00
    inc    hl              ; 4BCF 23
    dec    bc              ; 4BD0 0B
    ld     a,b             ; 4BD1 78
    or     c               ; 4BD2 B1
    jr     nz,$4bcd        ; 4BD3 20 F8
    pop    hl              ; 4BD5 E1
    pop    bc              ; 4BD6 C1
    pop    af              ; 4BD7 F1
    ret                    ; 4BD8 C9
    push   bc              ; 4BD9 C5
    push   hl              ; 4BDA E5
    or     a               ; 4BDB B7
    jp     z,$4be7         ; 4BDC CA E7 4B
    ld     hl,$8508        ; 4BDF 21 08 85
    ld     b,$58           ; 4BE2 06 58
    jp     $4bec           ; 4BE4 C3 EC 4B
    ld     hl,$8500        ; 4BE7 21 00 85
    ld     b,$60           ; 4BEA 06 60
    ld     (hl),$00        ; 4BEC 36 00
    inc    hl              ; 4BEE 23
    djnz   $4bec           ; 4BEF 10 FB
    pop    hl              ; 4BF1 E1
    pop    bc              ; 4BF2 C1
    ret                    ; 4BF3 C9
    ld     a,$01           ; 4BF4 3E 01
    ld     ($808f),a       ; 4BF6 32 8F 80
    ld     a,($81a0)       ; 4BF9 3A A0 81
    dec    a               ; 4BFC 3D
    ld     bc,$0006        ; 4BFD 01 06 00
    ld     hl,$de38        ; 4C00 21 38 DE
    call   $00f6           ; 4C03 CD F6 00
    ld     a,(hl)          ; 4C06 7E
    ld     ($81a1),a       ; 4C07 32 A1 81
    ld     hl,$7c74        ; 4C0A 21 74 7C
    call   $00ea           ; 4C0D CD EA 00
    ld     a,$00           ; 4C10 3E 00
    ld     ($808f),a       ; 4C12 32 8F 80
    ld     hl,$de3a        ; 4C15 21 3A DE
    ld     bc,$0006        ; 4C18 01 06 00
    ld     a,($81a0)       ; 4C1B 3A A0 81
    dec    a               ; 4C1E 3D
    call   $00f6           ; 4C1F CD F6 00
    ld     a,(hl)          ; 4C22 7E
    and    $07             ; 4C23 E6 07
    call   $6018           ; 4C25 CD 18 60
    ret                    ; 4C28 C9

; 4C29h - probably draw level border
;
; Vector: 4FF0

    push   bc              ; 4C29 C5
    push   de              ; 4C2A D5
    push   hl              ; 4C2B E5
    ld     d,a             ; 4C2C 57
    ld     a,$01           ; 4C2D 3E 01
    ld     ($808f),a       ; 4C2F 32 8F 80
    ld     e,$66           ; 4C32 1E 66
    call   $5fd3           ; 4C34 CD D3 5F
    ld     b,$0d           ; 4C37 06 0D
    ld     hl,$9043        ; 4C39 21 43 90
    ld     (hl),e          ; 4C3C 73
    inc    hl              ; 4C3D 23
    ld     (hl),e          ; 4C3E 73
    inc    (hl)            ; 4C3F 34
    inc    hl              ; 4C40 23
    djnz   $4c3c           ; 4C41 10 F9
    ld     hl,$93a3        ; 4C43 21 A3 93
    ld     b,$0d           ; 4C46 06 0D
    ld     (hl),e          ; 4C48 73
    inc    hl              ; 4C49 23
    ld     (hl),e          ; 4C4A 73
    inc    (hl)            ; 4C4B 34
    inc    hl              ; 4C4C 23
    djnz   $4c48           ; 4C4D 10 F9
    ld     a,d             ; 4C4F 7A
    ld     hl,$9443        ; 4C50 21 43 94
    ld     b,$1a           ; 4C53 06 1A
    ld     c,a             ; 4C55 4F
    ld     (hl),a          ; 4C56 77
    inc    hl              ; 4C57 23
    djnz   $4c56           ; 4C58 10 FC
    ld     hl,$97a3        ; 4C5A 21 A3 97
    ld     b,$1a           ; 4C5D 06 1A
    ld     a,c             ; 4C5F 79
    ld     (hl),a          ; 4C60 77
    inc    hl              ; 4C61 23
    djnz   $4c60           ; 4C62 10 FC
    ld     de,$8780        ; 4C64 11 80 87
    ld     hl,$4ca3        ; 4C67 21 A3 4C
    ld     bc,$003c        ; 4C6A 01 3C 00
    ldir                   ; 4C6D ED B0
    ld     c,a             ; 4C6F 4F
    call   $4c98           ; 4C70 CD 98 4C
    ld     ix,$8780        ; 4C73 DD 21 80 87
    call   $00d2           ; 4C77 CD D2 00
    ld     a,$1d           ; 4C7A 3E 1D
    ld     ($8780),a       ; 4C7C 32 80 87
    ld     a,$6a           ; 4C7F 3E 6A		; 6Ah = bottom left corner
    ld     ($8782),a       ; 4C81 32 82 87
    ld     a,$6b           ; 4C84 3E 6B		; 6Bh = bottom right corner
    ld     ($87b8),a       ; 4C86 32 B8 87
    ld     a,c             ; 4C89 79
    call   $4c98           ; 4C8A CD 98 4C
    ld     ix,$8780        ; 4C8D DD 21 80 87
    call   $00d2           ; 4C91 CD D2 00
    pop    hl              ; 4C94 E1
    pop    de              ; 4C95 D1
    pop    bc              ; 4C96 C1
    ret                    ; 4C97 C9

; 4C98h - subroutine of 4C29h
;
; memset (0x8783, A, 28), skipping every other byte.

    ld     b,$1c           ; 4C98 06 1C		; 28 columns
    ld     hl,$8783        ; 4C9A 21 83 87
    ld     (hl),a          ; 4C9D 77
    inc    hl              ; 4C9E 23
    inc    hl              ; 4C9F 23
    djnz   $4c9d           ; 4CA0 10 FB
    ret                    ; 4CA2 C9

; 4CA3h - level border data
;
; Used by 4C29h.

    hex    02 02 68 01 62 01 63 01 62 01 63 01 62 01 63 01 ; 4CA3  ; 68=TLC
    hex    62 01 63 01 62 01 63 01 62 01 63 01 62 01 63 01 ; 4CB3
    hex    62 01 63 01 62 01 63 01 62 01 63 01 62 01 63 01 ; 4CC3
    hex    62 01 63 01 62 01 63 01 69 01 FF FF             ; 4CD3  ; 69=TRC

; 4CDFh - ?

    ld     a,$0d           ; 4CDF 3E 0D
    call   $6018           ; 4CE1 CD 18 60
    call   $6027           ; 4CE4 CD 27 60
    call   $6024           ; 4CE7 CD 24 60
    call   $00cf           ; 4CEA CD CF 00
    nop                    ; 4CED 00
    ret    nc              ; 4CEE D0
    inc    b               ; 4CEF 04
    ld     ix,$d014        ; 4CF0 DD 21 14 D0
    ld     a,($8017)       ; 4CF4 3A 17 80
    cp     $01             ; 4CF7 FE 01
    jp     z,$4d00         ; 4CF9 CA 00 4D
    ld     ix,$d040        ; 4CFC DD 21 40 D0
    call   $00d2           ; 4D00 CD D2 00
    call   $00f0           ; 4D03 CD F0 00
    jp     $4d63           ; 4D06 C3 63 4D
    and    $07             ; 4D09 E6 07
    ld     hl,$4d11        ; 4D0B 21 11 4D
    jp     $00ea           ; 4D0E C3 EA 00
    ld     hl,$244d        ; 4D11 21 4D 24
    ld     c,l             ; 4D14 4D
    dec    l               ; 4D15 2D
    ld     c,l             ; 4D16 4D
    ld     (hl),$4d        ; 4D17 36 4D
    ccf                    ; 4D19 3F
    ld     c,l             ; 4D1A 4D
    ld     c,b             ; 4D1B 48
    ld     c,l             ; 4D1C 4D
    ld     d,c             ; 4D1D 51
    ld     c,l             ; 4D1E 4D
    ld     e,d             ; 4D1F 5A
    ld     c,l             ; 4D20 4D
    jp     $4d68           ; 4D21 C3 68 4D
    call   $00cf           ; 4D24 CD CF 00
    xor    d               ; 4D27 AA
    ret    nc              ; 4D28 D0
    ld     (bc),a          ; 4D29 02
    jp     $4d68           ; 4D2A C3 68 4D
    call   $00cf           ; 4D2D CD CF 00
    xor    (hl)            ; 4D30 AE
    ret    nc              ; 4D31 D0
    ld     (bc),a          ; 4D32 02
    jp     $4d68           ; 4D33 C3 68 4D
    call   $00cf           ; 4D36 CD CF 00
    or     d               ; 4D39 B2
    ret    nc              ; 4D3A D0
    ld     (bc),a          ; 4D3B 02
    jp     $4d68           ; 4D3C C3 68 4D
    call   $00cf           ; 4D3F CD CF 00
    or     (hl)            ; 4D42 B6
    ret    nc              ; 4D43 D0
    ld     (bc),a          ; 4D44 02
    jp     $4d68           ; 4D45 C3 68 4D
    call   $00cf           ; 4D48 CD CF 00
    cp     d               ; 4D4B BA
    ret    nc              ; 4D4C D0
    inc    b               ; 4D4D 04
    jp     $4d68           ; 4D4E C3 68 4D
    call   $00cf           ; 4D51 CD CF 00
    jp     nz,$04d0        ; 4D54 C2 D0 04
    jp     $4d68           ; 4D57 C3 68 4D
    call   $00cf           ; 4D5A CD CF 00
    jp     z,$06d0         ; 4D5D CA D0 06
    jp     $4d68           ; 4D60 C3 68 4D
    ld     a,$0d           ; 4D63 3E 0D
    call   $6018           ; 4D65 CD 18 60
    ret                    ; 4D68 C9
    ld     a,($8070)       ; 4D69 3A 70 80
    cp     $00             ; 4D6C FE 00
    jr     z,$4d78         ; 4D6E 28 08
    ld     a,($8014)       ; 4D70 3A 14 80
    and    $18             ; 4D73 E6 18
    rrca                   ; 4D75 0F
    rrca                   ; 4D76 0F
    rrca                   ; 4D77 0F
    ld     hl,$4d7e        ; 4D78 21 7E 4D
    jp     $00ea           ; 4D7B C3 EA 00
    add    a,(hl)          ; 4D7E 86
    ld     c,l             ; 4D7F 4D
    sub    b               ; 4D80 90
    ld     c,l             ; 4D81 4D
    sbc    a,e             ; 4D82 9B
    ld     c,l             ; 4D83 4D
    and    (hl)            ; 4D84 A6
    ld     c,l             ; 4D85 4D
    ld     a,($81a8)       ; 4D86 3A A8 81
    inc    a               ; 4D89 3C
    ld     ($81a8),a       ; 4D8A 32 A8 81
    jp     $4db1           ; 4D8D C3 B1 4D
    ld     a,($81a8)       ; 4D90 3A A8 81
    add    a,$02           ; 4D93 C6 02
    ld     ($81a8),a       ; 4D95 32 A8 81
    jp     $4db1           ; 4D98 C3 B1 4D
    ld     a,($81a8)       ; 4D9B 3A A8 81
    add    a,$04           ; 4D9E C6 04
    ld     ($81a8),a       ; 4DA0 32 A8 81
    jp     $4db1           ; 4DA3 C3 B1 4D
    ld     a,($81a8)       ; 4DA6 3A A8 81
    add    a,$08           ; 4DA9 C6 08
    ld     ($81a8),a       ; 4DAB 32 A8 81
    jp     $4db1           ; 4DAE C3 B1 4D
    ld     a,($81a8)       ; 4DB1 3A A8 81
    cp     $20             ; 4DB4 FE 20
    jp     c,$4dbe         ; 4DB6 DA BE 4D
    ld     a,$1f           ; 4DB9 3E 1F
    ld     ($81a8),a       ; 4DBB 32 A8 81
    ret                    ; 4DBE C9

; 4DBFh - next level
;
; Why is this called during game over ? Don't need to change level while
; playing a demo, right ?

    ld     a,($8070)       ; 4DBF 3A 70 80	; if (! (0x8070))
    cp     $00             ; 4DC2 FE 00		;   difficulty = 0
    jr     z,$4dce         ; 4DC4 28 08		; else
    ld     a,($8014)       ; 4DC6 3A 14 80	;   difficulty =
    and    $60             ; 4DC9 E6 60		;     (((0x8014) & 0x60) >> 5);
    rlca                   ; 4DCB 07 		; switch (difficulty) {
    rlca                   ; 4DCC 07 		;
    rlca                   ; 4DCD 07 		;
    ld     hl,$4dd4        ; 4DCE 21 D4 4D
    jp     $00ea           ; 4DD1 C3 EA 00

    word   $4DDC           ; 4DD4 DC 4D
    word   $4DE6           ; 4DD6 E6 4D
    word   $4DFC           ; 4DD8 FC 4D
    word   $4E07           ; 4DDA 07 4E

    ld     a,($81a9)       ; 4DDC 3A A9 81	;   case 0:  /* medium */
    inc    a               ; 4DDF 3C		;     (0x81a9)++;
    ld     ($81a9),a       ; 4DE0 32 A9 81
    jp     $4e12           ; 4DE3 C3 12 4E	;     break;

    ld     a,($81aa)       ; 4DE6 3A AA 81	;   case 1:  /* easy */
    inc    a               ; 4DE9 3C 		;     (0x81aa) = ! (0x81aa);
    and    $01             ; 4DEA E6 01
    ld     ($81aa),a       ; 4DEC 32 AA 81
    jp     nz,$4e12        ; 4DEF C2 12 4E	;     if (! (0x81aa)) {
    ld     a,($81a9)       ; 4DF2 3A A9 81	;       0x81a9++;
    inc    a               ; 4DF5 3C
    ld     ($81a9),a       ; 4DF6 32 A9 81	;     }
    jp     $4e12           ; 4DF9 C3 12 4E	;     break;

    ld     a,($81a9)       ; 4DFC 3A A9 81	;   case 2: /* hard */
    add    a,$02           ; 4DFF C6 02		;     (0x81a9) += 2
    ld     ($81a9),a       ; 4E01 32 A9 81
    jp     $4e12           ; 4E04 C3 12 4E	;     break;

    ld     a,($81a9)       ; 4E07 3A A9 81	;   case 3: /* hardest */
    add    a,$04           ; 4E0A C6 04		;     (0x81a9) += 4
    ld     ($81a9),a       ; 4E0C 32 A9 81
    jp     $4e12           ; 4E0F C3 12 4E	;     break;

    ld     a,($81a9)       ; 4E12 3A A9 81	; }
    cp     $20             ; 4E15 FE 20		; if ((0x81a9) >= 32) {
    jp     c,$4e1f         ; 4E17 DA 1F 4E
    ld     a,$13           ; 4E1A 3E 13		;   (0x81a9) = 19;
    ld     ($81a9),a       ; 4E1C 32 A9 81	; }
    ret                    ; 4E1F C9

    ld     bc,$021c        ; 4E20 01 1C 02
    call   $00ed           ; 4E23 CD ED 00
    ret                    ; 4E26 C9

; 4E27h - 

    ld     hl,$de39        ; 4E27 21 39 DE
    ld     bc,$0006        ; 4E2A 01 06 00
    ld     a,($81a0)       ; 4E2D 3A A0 81
    dec    a               ; 4E30 3D
    call   $00f6           ; 4E31 CD F6 00
    inc    hl              ; 4E34 23
    ld     a,(hl)          ; 4E35 7E
    push   af              ; 4E36 F5
    dec    hl              ; 4E37 2B
    ld     a,(hl)          ; 4E38 7E
    ld     hl,$d600        ; 4E39 21 00 D6
    ld     bc,$0010        ; 4E3C 01 10 00
    call   $00f6           ; 4E3F CD F6 00
    ld     de,$8c90        ; 4E42 11 90 8C
    ldir                   ; 4E45 ED B0
    pop    af              ; 4E47 F1
    push   af              ; 4E48 F5
    ld     hl,$d700        ; 4E49 21 00 D7
    ld     bc,$0010        ; 4E4C 01 10 00
    call   $00f6           ; 4E4F CD F6 00
    ld     de,$8ca0        ; 4E52 11 A0 8C
    ldir                   ; 4E55 ED B0
    pop    af              ; 4E57 F1
    ld     hl,$d780        ; 4E58 21 80 D7
    ld     bc,$0010        ; 4E5B 01 10 00
    call   $00f6           ; 4E5E CD F6 00
    ld     de,$8c80        ; 4E61 11 80 8C
    ldir                   ; 4E64 ED B0
    ld     hl,$de3c        ; 4E66 21 3C DE
    ld     bc,$0006        ; 4E69 01 06 00
    ld     a,($81a0)       ; 4E6C 3A A0 81
    dec    a               ; 4E6F 3D
    call   $00f6           ; 4E70 CD F6 00
    ld     a,(hl)          ; 4E73 7E
    ld     hl,$d680        ; 4E74 21 80 D6
    ld     bc,$0010        ; 4E77 01 10 00
    call   $00f6           ; 4E7A CD F6 00
    ld     de,$8c50        ; 4E7D 11 50 8C
    ldir                   ; 4E80 ED B0
    ret                    ; 4E82 C9
    ld     a,($8017)       ; 4E83 3A 17 80
    or     a               ; 4E86 B7
    jp     z,$4eb4         ; 4E87 CA B4 4E
    ld     a,($8026)       ; 4E8A 3A 26 80
    or     a               ; 4E8D B7
    jp     z,$4ea4         ; 4E8E CA A4 4E
    ld     a,($8027)       ; 4E91 3A 27 80
    or     a               ; 4E94 B7
    jp     nz,$4e9d        ; 4E95 C2 9D 4E
    ld     a,($81cd)       ; 4E98 3A CD 81
    jr     $4ea0           ; 4E9B 18 03
    ld     a,($81b4)       ; 4E9D 3A B4 81
    or     a               ; 4EA0 B7
    jp     p,$4eb4         ; 4EA1 F2 B4 4E
    ld     a,($b002)       ; 4EA4 3A 02 B0
    and    $08             ; 4EA7 E6 08
    jp     nz,$4ebe        ; 4EA9 C2 BE 4E
    ld     a,($b002)       ; 4EAC 3A 02 B0
    and    $04             ; 4EAF E6 04
    jp     nz,$4eb7        ; 4EB1 C2 B7 4E
    ld     a,$00           ; 4EB4 3E 00
    ret                    ; 4EB6 C9
    ld     a,$01           ; 4EB7 3E 01
    ld     ($808b),a       ; 4EB9 32 8B 80
    jr     $4ecb           ; 4EBC 18 0D
    ld     a,($8017)       ; 4EBE 3A 17 80
    cp     $02             ; 4EC1 FE 02
    jp     c,$4eb4         ; 4EC3 DA B4 4E
    ld     a,$02           ; 4EC6 3E 02
    ld     ($808b),a       ; 4EC8 32 8B 80
    ld     a,$01           ; 4ECB 3E 01
    ret                    ; 4ECD C9
    nop                    ; 4ECE 00
    ld     hl,$d8d0        ; 4ECF 21 D0 D8
    ld     a,($81a0)       ; 4ED2 3A A0 81
    dec    a               ; 4ED5 3D
    cp     $20             ; 4ED6 FE 20
    jp     c,$4edd         ; 4ED8 DA DD 4E
    ld     a,$1f           ; 4EDB 3E 1F
    ld     bc,$0008        ; 4EDD 01 08 00
    call   $00f6           ; 4EE0 CD F6 00
    ld     de,$80d2        ; 4EE3 11 D2 80
    ldir                   ; 4EE6 ED B0
    ld     de,$0001        ; 4EE8 11 01 00
    ld     ($80da),de      ; 4EEB ED 53 DA 80
    ld     hl,($80d8)      ; 4EEF 2A D8 80
    ld     ($80dc),hl      ; 4EF2 22 DC 80
    ld     a,($8248)       ; 4EF5 3A 48 82
    cp     $01             ; 4EF8 FE 01
    jp     z,$4f02         ; 4EFA CA 02 4F
    ld     a,($81a9)       ; 4EFD 3A A9 81
    jr     $4f05           ; 4F00 18 03
    call   $4dbf           ; 4F02 CD BF 4D	; next level
    ld     hl,$dbb8        ; 4F05 21 B8 DB	; hl = 0xdbb8 + 20 * a
    ld     bc,$0014        ; 4F08 01 14 00
    call   $00f6           ; 4F0B CD F6 00
    ld     de,$825c        ; 4F0E 11 5C 82
    ldir                   ; 4F11 ED B0
    ld     hl,($825e)      ; 4F13 2A 5E 82
    ld     ($8270),hl      ; 4F16 22 70 82
    ld     hl,($8262)      ; 4F19 2A 62 82
    ld     ($8272),hl      ; 4F1C 22 72 82
    ld     hl,($8266)      ; 4F1F 2A 66 82
    ld     ($8274),hl      ; 4F22 22 74 82
    ld     a,($8248)       ; 4F25 3A 48 82
    cp     $01             ; 4F28 FE 01
    jp     z,$4f32         ; 4F2A CA 32 4F
    ld     a,($81a8)       ; 4F2D 3A A8 81
    jr     $4f35           ; 4F30 18 03
    call   $4d69           ; 4F32 CD 69 4D
    ld     hl,$d9d8        ; 4F35 21 D8 D9
    ld     bc,$000c        ; 4F38 01 0C 00
    call   $00f6           ; 4F3B CD F6 00
    ld     de,$80c0        ; 4F3E 11 C0 80
    ldir                   ; 4F41 ED B0
    ld     hl,($80c6)      ; 4F43 2A C6 80
    ld     ($80cc),hl      ; 4F46 22 CC 80
    ld     hl,($80c8)      ; 4F49 2A C8 80
    ld     ($80cd),hl      ; 4F4C 22 CD 80
    ld     hl,($80ca)      ; 4F4F 2A CA 80
    ld     ($80cf),hl      ; 4F52 22 CF 80
    ret                    ; 4F55 C9

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 4F56
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 4F66
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 4F76
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 4F86
    hex    00 00 00 00 00 00 00 00 00 00                   ; 4F96

    call   $47c9           ; 4FA0 CD C9 47
    jp     $4177           ; 4FA3 C3 77 41

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 4FA6
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 4FB6
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 4FC6
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 4FD6
    hex    00 00 00 00 00 00 00 00 00 00                   ; 4FE6

    jp     $4c29           ; 4FF0 C3 29 4C
    jp     $433d           ; 4FF3 C3 3D 43
    jp     $4bd9           ; 4FF6 C3 D9 4B
    jp     $42e6           ; 4FF9 C3 E6 42
    jp     $42e6           ; 4FFC C3 E6 42
    nop                    ; 4FFF 00
    call   $5009           ; 5000 CD 09 50
    nop                    ; 5003 00
    nop                    ; 5004 00
    nop                    ; 5005 00
    jp     $00c3           ; 5006 C3 C3 00
    ld     a,($8070)       ; 5009 3A 70 80
    or     a               ; 500C B7
    jp     z,$5030         ; 500D CA 30 50
    call   $5031           ; 5010 CD 31 50
    or     a               ; 5013 B7
    jp     z,$5030         ; 5014 CA 30 50
    ld     ix,$ca21        ; 5017 DD 21 21 CA
    call   $00d2           ; 501B CD D2 00
    ld     de,$80e2        ; 501E 11 E2 80
    ld     hl,$819c        ; 5021 21 9C 81
    ld     bc,$0004        ; 5024 01 04 00
    ldir                   ; 5027 ED B0
    ld     ix,$ca55        ; 5029 DD 21 55 CA
    call   $00d5           ; 502D CD D5 00
    ret                    ; 5030 C9
    ld     ix,$819f        ; 5031 DD 21 9F 81
    ld     hl,$80e5        ; 5035 21 E5 80
    ld     bc,$0004        ; 5038 01 04 00
    ld     a,(hl)          ; 503B 7E
    cp     (ix+$00)        ; 503C DD BE 00
    jp     c,$504d         ; 503F DA 4D 50
    jp     nz,$5052        ; 5042 C2 52 50
    dec    ix              ; 5045 DD 2B
    dec    hl              ; 5047 2B
    djnz   $503b           ; 5048 10 F1
    jp     $5052           ; 504A C3 52 50
    ld     a,$01           ; 504D 3E 01
    jp     $5053           ; 504F C3 53 50
    xor    a               ; 5052 AF
    ret                    ; 5053 C9
    rrca                   ; 5054 0F
    rrca                   ; 5055 0F
    rrca                   ; 5056 0F
    rrca                   ; 5057 0F
    ret                    ; 5058 C9
    ld     a,($b000)       ; 5059 3A 00 B0
    and    $40             ; 505C E6 40
    jp     z,$506b         ; 505E CA 6B 50
    ld     a,$01           ; 5061 3E 01
    ld     ($8074),a       ; 5063 32 74 80
    ld     a,$01           ; 5066 3E 01
    ld     ($8077),a       ; 5068 32 77 80
    ret                    ; 506B C9

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 506C
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 507C
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 508C
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 509C
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 50AC
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 50BC
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 50CC
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 50DC
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 50EC
    hex    00 00 00 00                                     ; 50FC

    ld     a,$00           ; 5100 3E 00
    ld     ($8095),a       ; 5102 32 95 80
    ld     ($808d),a       ; 5105 32 8D 80
    ld     ($8088),a       ; 5108 32 88 80
    ld     a,($8070)       ; 510B 3A 70 80
    cp     $00             ; 510E FE 00
    jr     nz,$5127        ; 5110 20 15
    ld     ix,$8340        ; 5112 DD 21 40 83
    ld     (ix+$02),$ac    ; 5116 DD 36 02 AC
    ld     (ix+$03),$2f    ; 511A DD 36 03 2F
    ld     (ix+$04),$70    ; 511E DD 36 04 70
    ld     (ix+$05),$73    ; 5122 DD 36 05 73
    ret                    ; 5126 C9
    ld     a,$ff           ; 5127 3E FF
    ld     ($8094),a       ; 5129 32 94 80
    ld     a,$00           ; 512C 3E 00
    ld     ($846c),a       ; 512E 32 6C 84
    ld     a,$01           ; 5131 3E 01
    ld     ($806e),a       ; 5133 32 6E 80
    ld     a,($806e)       ; 5136 3A 6E 80
    cp     $00             ; 5139 FE 00
    jr     z,$5140         ; 513B 28 03
    nop                    ; 513D 00
    jr     $5136           ; 513E 18 F6
    ld     ix,$846c        ; 5140 DD 21 6C 84
    ld     a,(ix+$00)      ; 5144 DD 7E 00
    and    $07             ; 5147 E6 07
    cp     $05             ; 5149 FE 05
    jr     z,$51b0         ; 514B 28 63
    ld     b,$00           ; 514D 06 00
    ld     a,$14           ; 514F 3E 14
    cp     $00             ; 5151 FE 00
    jr     z,$515d         ; 5153 28 08
    ld     (ix+$00),b      ; 5155 DD 70 00
    inc    ix              ; 5158 DD 23
    dec    a               ; 515A 3D
    jr     $5151           ; 515B 18 F4
    ld     ix,$846c        ; 515D DD 21 6C 84
    ld     a,$8b           ; 5161 3E 8B
    ld     ($8814),a       ; 5163 32 14 88
    ld     iy,$8800        ; 5166 FD 21 00 88
    ld     (iy+$00),$0b    ; 516A FD 36 00 0B
    ld     hl,$0d48        ; 516E 21 48 0D
    ld     ($8801),hl      ; 5171 22 01 88
    ld     (iy+$03),$02    ; 5174 FD 36 03 02
    ld     (iy+$04),$01    ; 5178 FD 36 04 01
    ld     (iy+$05),$01    ; 517C FD 36 05 01
    ld     (iy+$06),$01    ; 5180 FD 36 06 01
    ld     (iy+$07),$02    ; 5184 FD 36 07 02
    ld     (iy+$08),$00    ; 5188 FD 36 08 00
    ld     hl,$0000        ; 518C 21 00 00
    ld     ($8cb4),hl      ; 518F 22 B4 8C
    set    2,(ix+$00)      ; 5192 DD CB 00 D6
    res    1,(ix+$00)      ; 5196 DD CB 00 8E
    set    0,(ix+$00)      ; 519A DD CB 00 C6
    ld     (ix+$06),$03    ; 519E DD 36 06 03
    ld     (ix+$08),$ee    ; 51A2 DD 36 08 EE
    ld     (ix+$0a),$03    ; 51A6 DD 36 0A 03
    call   $5243           ; 51AA CD 43 52
    jp     $5131           ; 51AD C3 31 51
    ld     a,(ix+$0b)      ; 51B0 DD 7E 0B
    cp     $84             ; 51B3 FE 84
    jp     c,$51d6         ; 51B5 DA D6 51
    set    1,(ix+$00)      ; 51B8 DD CB 00 CE
    ld     (ix+$01),$00    ; 51BC DD 36 01 00
    ld     iy,$8500        ; 51C0 FD 21 00 85
    ld     a,$40           ; 51C4 3E 40
    cp     $00             ; 51C6 FE 00
    jr     z,$51d3         ; 51C8 28 09
    ld     (iy+$00),$00    ; 51CA FD 36 00 00
    inc    iy              ; 51CE FD 23
    dec    a               ; 51D0 3D
    jr     $51c6           ; 51D1 18 F3
    jp     $521e           ; 51D3 C3 1E 52
    ld     a,(ix+$0b)      ; 51D6 DD 7E 0B
    cp     $48             ; 51D9 FE 48
    jp     c,$51e1         ; 51DB DA E1 51
    jp     $5215           ; 51DE C3 15 52
    ld     a,(ix+$0b)      ; 51E1 DD 7E 0B
    cp     $10             ; 51E4 FE 10
    jp     c,$51f4         ; 51E6 DA F4 51
    ld     hl,($8475)      ; 51E9 2A 75 84
    ld     de,$000c        ; 51EC 11 0C 00
    sbc    hl,de           ; 51EF ED 52
    ld     ($8475),hl      ; 51F1 22 75 84
    ld     hl,($8471)      ; 51F4 2A 71 84
    ld     de,($8475)      ; 51F7 ED 5B 75 84
    add    hl,de           ; 51FB 19
    ld     ($8471),hl      ; 51FC 22 71 84
    ld     hl,($8473)      ; 51FF 2A 73 84
    sbc    hl,de           ; 5202 ED 52
    ld     ($8473),hl      ; 5204 22 73 84
    ld     a,(ix+$0c)      ; 5207 DD 7E 0C
    cp     $50             ; 520A FE 50
    jp     nc,$5215        ; 520C D2 15 52
    add    a,(ix+$0a)      ; 520F DD 86 0A
    ld     (ix+$0c),a      ; 5212 DD 77 0C
    call   $5243           ; 5215 CD 43 52
    inc    (ix+$0b)        ; 5218 DD 34 0B
    jp     $5131           ; 521B C3 31 51
    ld     a,$00           ; 521E 3E 00
    ld     ($8094),a       ; 5220 32 94 80
    ld     a,$89           ; 5223 3E 89
    ld     ($8800),a       ; 5225 32 00 88
    ld     a,$8b           ; 5228 3E 8B
    ld     ($8814),a       ; 522A 32 14 88
    ld     a,($81a0)       ; 522D 3A A0 81
    cp     $06             ; 5230 FE 06
    jr     nc,$5242        ; 5232 30 0E
    dec    a               ; 5234 3D
    add    a,a             ; 5235 87
    ld     b,a             ; 5236 47
    ld     a,$0a           ; 5237 3E 0A
    sub    b               ; 5239 90
    ld     b,a             ; 523A 47
    ld     a,($81b3)       ; 523B 3A B3 81
    add    a,b             ; 523E 80
    ld     ($81b3),a       ; 523F 32 B3 81
    ret                    ; 5242 C9
    push   iy              ; 5243 FD E5
    ld     (ix+$02),$0b    ; 5245 DD 36 02 0B
    ld     a,(ix+$0b)      ; 5249 DD 7E 0B
    cp     $48             ; 524C FE 48
    jr     nz,$5261        ; 524E 20 11
    ld     hl,$0d00        ; 5250 21 00 0D
    ld     ($8801),hl      ; 5253 22 01 88
    ld     a,$02           ; 5256 3E 02
    ld     ($8805),a       ; 5258 32 05 88
    ld     hl,$00c0        ; 525B 21 C0 00
    ld     ($8cb2),hl      ; 525E 22 B2 8C
    ld     a,(ix+$0b)      ; 5261 DD 7E 0B
    cp     $48             ; 5264 FE 48
    jr     c,$5278         ; 5266 38 10
    bit    1,a             ; 5268 CB 4F
    jr     z,$5278         ; 526A 28 0C
    ld     a,($8cb2)       ; 526C 3A B2 8C
    cp     $00             ; 526F FE 00
    jr     z,$5278         ; 5271 28 05
    sub    $10             ; 5273 D6 10
    ld     ($8cb2),a       ; 5275 32 B2 8C
    ld     (ix+$04),$7a    ; 5278 DD 36 04 7A
    ld     a,(ix+$0c)      ; 527C DD 7E 0C
    cp     $10             ; 527F FE 10
    jp     c,$5300         ; 5281 DA 00 53
    cp     $20             ; 5284 FE 20
    jp     c,$52da         ; 5286 DA DA 52
    cp     $30             ; 5289 FE 30
    jp     c,$52b4         ; 528B DA B4 52
    ld     (ix+$01),$40    ; 528E DD 36 01 40
    ld     a,(ix+$06)      ; 5292 DD 7E 06
    sub    $30             ; 5295 D6 30
    ld     (ix+$03),a      ; 5297 DD 77 03
    ld     iy,$8500        ; 529A FD 21 00 85
    call   $5325           ; 529E CD 25 53
    ld     (ix+$01),$47    ; 52A1 DD 36 01 47
    ld     a,(ix+$08)      ; 52A5 DD 7E 08
    add    a,$30           ; 52A8 C6 30
    ld     (ix+$03),a      ; 52AA DD 77 03
    ld     iy,$8538        ; 52AD FD 21 38 85
    call   $5325           ; 52B1 CD 25 53
    ld     (ix+$01),$41    ; 52B4 DD 36 01 41
    ld     a,(ix+$06)      ; 52B8 DD 7E 06
    sub    $20             ; 52BB D6 20
    ld     (ix+$03),a      ; 52BD DD 77 03
    ld     iy,$8508        ; 52C0 FD 21 08 85
    call   $5325           ; 52C4 CD 25 53
    ld     (ix+$01),$46    ; 52C7 DD 36 01 46
    ld     a,(ix+$08)      ; 52CB DD 7E 08
    add    a,$20           ; 52CE C6 20
    ld     (ix+$03),a      ; 52D0 DD 77 03
    ld     iy,$8530        ; 52D3 FD 21 30 85
    call   $5325           ; 52D7 CD 25 53
    ld     (ix+$01),$42    ; 52DA DD 36 01 42
    ld     a,(ix+$06)      ; 52DE DD 7E 06
    sub    $10             ; 52E1 D6 10
    ld     (ix+$03),a      ; 52E3 DD 77 03
    ld     iy,$8510        ; 52E6 FD 21 10 85
    call   $5325           ; 52EA CD 25 53
    ld     (ix+$01),$45    ; 52ED DD 36 01 45
    ld     a,(ix+$08)      ; 52F1 DD 7E 08
    add    a,$10           ; 52F4 C6 10
    ld     (ix+$03),a      ; 52F6 DD 77 03
    ld     iy,$8528        ; 52F9 FD 21 28 85
    call   $5325           ; 52FD CD 25 53
    ld     (ix+$01),$43    ; 5300 DD 36 01 43
    ld     a,(ix+$06)      ; 5304 DD 7E 06
    ld     (ix+$03),a      ; 5307 DD 77 03
    ld     iy,$8518        ; 530A FD 21 18 85
    call   $5325           ; 530E CD 25 53
    ld     (ix+$01),$44    ; 5311 DD 36 01 44
    ld     a,(ix+$08)      ; 5315 DD 7E 08
    ld     (ix+$03),a      ; 5318 DD 77 03
    ld     iy,$8520        ; 531B FD 21 20 85
    call   $5325           ; 531F CD 25 53
    pop    iy              ; 5322 FD E1
    ret                    ; 5324 C9
    ld     a,(ix+$01)      ; 5325 DD 7E 01
    ld     (iy+$00),a      ; 5328 FD 77 00
    ld     a,(ix+$02)      ; 532B DD 7E 02
    ld     (iy+$01),a      ; 532E FD 77 01
    ld     a,(ix+$03)      ; 5331 DD 7E 03
    ld     (iy+$02),a      ; 5334 FD 77 02
    ld     a,(ix+$04)      ; 5337 DD 7E 04
    ld     (iy+$03),a      ; 533A FD 77 03
    ret                    ; 533D C9
    ld     a,$ff           ; 533E 3E FF
    ld     ($8094),a       ; 5340 32 94 80
    ld     a,$01           ; 5343 3E 01
    ld     ($806e),a       ; 5345 32 6E 80
    ld     a,($806e)       ; 5348 3A 6E 80
    cp     $00             ; 534B FE 00
    jr     z,$5352         ; 534D 28 03
    nop                    ; 534F 00
    jr     $5348           ; 5350 18 F6
    ld     ix,$846c        ; 5352 DD 21 6C 84
    ld     a,(ix+$00)      ; 5356 DD 7E 00
    and    $0f             ; 5359 E6 0F
    cp     $05             ; 535B FE 05
    jr     z,$53d6         ; 535D 28 77
    ld     (ix+$00),$05    ; 535F DD 36 00 05
    ld     (ix+$01),$ac    ; 5363 DD 36 01 AC
    ld     (ix+$02),$2b    ; 5367 DD 36 02 2B
    ld     a,($8502)       ; 536B 3A 02 85
    ld     (ix+$03),a      ; 536E DD 77 03
    ld     a,($8503)       ; 5371 3A 03 85
    ld     (ix+$04),a      ; 5374 DD 77 04
    ld     hl,$0000        ; 5377 21 00 00
    ld     ($8471),hl      ; 537A 22 71 84
    ld     ($8477),hl      ; 537D 22 77 84
    ld     hl,$0fff        ; 5380 21 FF 0F
    ld     ($8cbe),hl      ; 5383 22 BE 8C
    ld     a,$8b           ; 5386 3E 8B
    ld     ($8814),a       ; 5388 32 14 88
    ld     a,(ix+$03)      ; 538B DD 7E 03
    ld     hl,$0180        ; 538E 21 80 01
    cp     $71             ; 5391 FE 71
    jr     c,$5398         ; 5393 38 03
    call   $1ff3           ; 5395 CD F3 1F
    ld     ($8473),hl      ; 5398 22 73 84
    ld     a,(ix+$04)      ; 539B DD 7E 04
    ld     hl,$0180        ; 539E 21 80 01
    cp     $73             ; 53A1 FE 73
    jr     c,$53a8         ; 53A3 38 03
    call   $1ff3           ; 53A5 CD F3 1F
    ld     ($8475),hl      ; 53A8 22 75 84
    ld     de,$0071        ; 53AB 11 71 00
    ld     hl,$0000        ; 53AE 21 00 00
    ld     l,(ix+$03)      ; 53B1 DD 6E 03
    sbc    hl,de           ; 53B4 ED 52
    call   $1ff0           ; 53B6 CD F0 1F
    ld     c,l             ; 53B9 4D
    ld     hl,$0000        ; 53BA 21 00 00
    ld     l,(ix+$04)      ; 53BD DD 6E 04
    ld     de,$0073        ; 53C0 11 73 00
    sbc    hl,de           ; 53C3 ED 52
    call   $1ff0           ; 53C5 CD F0 1F
    ld     a,l             ; 53C8 7D
    cp     c               ; 53C9 B9
    jr     c,$53d0         ; 53CA 38 04
    set    6,(ix+$00)      ; 53CC DD CB 00 F6
    call   $54c2           ; 53D0 CD C2 54
    jp     $533e           ; 53D3 C3 3E 53
    ld     a,(ix+$0b)      ; 53D6 DD 7E 0B
    cp     $10             ; 53D9 FE 10
    jr     nc,$53e6        ; 53DB 30 09
    call   $5496           ; 53DD CD 96 54
    inc    (ix+$0b)        ; 53E0 DD 34 0B
    jp     $533e           ; 53E3 C3 3E 53
    ld     a,(ix+$00)      ; 53E6 DD 7E 00
    and    $30             ; 53E9 E6 30
    cp     $30             ; 53EB FE 30
    jr     z,$53fe         ; 53ED 28 0F
    call   $5496           ; 53EF CD 96 54
    inc    (ix+$0b)        ; 53F2 DD 34 0B
    call   $5424           ; 53F5 CD 24 54
    call   $54c2           ; 53F8 CD C2 54
    jp     $533e           ; 53FB C3 3E 53
    ld     a,(ix+$0b)      ; 53FE DD 7E 0B
    cp     $fe             ; 5401 FE FE
    jr     nc,$541b        ; 5403 30 16
    ld     a,$01           ; 5405 3E 01
    ld     ($806e),a       ; 5407 32 6E 80
    ld     a,($806e)       ; 540A 3A 6E 80
    cp     $00             ; 540D FE 00
    jr     z,$5413         ; 540F 28 02
    jr     $540a           ; 5411 18 F7
    call   $5496           ; 5413 CD 96 54
    inc    (ix+$0b)        ; 5416 DD 34 0B
    jr     $53fe           ; 5419 18 E3
    ld     a,$00           ; 541B 3E 00
    ld     ($8094),a       ; 541D 32 94 80
    ld     ($8500),a       ; 5420 32 00 85
    ret                    ; 5423 C9
    bit    6,(ix+$00)      ; 5424 DD CB 00 76
    jp     nz,$5461        ; 5428 C2 61 54
    ld     hl,$0000        ; 542B 21 00 00
    ld     l,(ix+$03)      ; 542E DD 6E 03
    ld     de,$0071        ; 5431 11 71 00
    sbc    hl,de           ; 5434 ED 52
    call   $1ff0           ; 5436 CD F0 1F
    ld     a,l             ; 5439 7D
    cp     $02             ; 543A FE 02
    jr     z,$5440         ; 543C 28 02
    jr     nc,$544e        ; 543E 30 0E
    set    4,(ix+$00)      ; 5440 DD CB 00 E6
    set    6,(ix+$00)      ; 5444 DD CB 00 F6
    ld     (ix+$03),$71    ; 5448 DD 36 03 71
    jr     $5424           ; 544C 18 D6
    ld     h,(ix+$03)      ; 544E DD 66 03
    ld     l,(ix+$05)      ; 5451 DD 6E 05
    ld     de,($8473)      ; 5454 ED 5B 73 84
    add    hl,de           ; 5458 19
    ld     (ix+$03),h      ; 5459 DD 74 03
    ld     (ix+$05),l      ; 545C DD 75 05
    jr     $5495           ; 545F 18 34
    ld     hl,$0000        ; 5461 21 00 00
    ld     l,(ix+$04)      ; 5464 DD 6E 04
    ld     de,$0073        ; 5467 11 73 00
    sbc    hl,de           ; 546A ED 52
    call   $1ff0           ; 546C CD F0 1F
    ld     a,l             ; 546F 7D
    cp     $02             ; 5470 FE 02
    jr     z,$5476         ; 5472 28 02
    jr     nc,$5484        ; 5474 30 0E
    set    5,(ix+$00)      ; 5476 DD CB 00 EE
    res    6,(ix+$00)      ; 547A DD CB 00 B6
    ld     (ix+$04),$73    ; 547E DD 36 04 73
    jr     $5495           ; 5482 18 11
    ld     h,(ix+$04)      ; 5484 DD 66 04
    ld     l,(ix+$06)      ; 5487 DD 6E 06
    ld     de,($8475)      ; 548A ED 5B 75 84
    add    hl,de           ; 548E 19
    ld     (ix+$04),h      ; 548F DD 74 04
    ld     (ix+$06),l      ; 5492 DD 75 06
    ret                    ; 5495 C9
    ld     a,($8477)       ; 5496 3A 77 84
    and    $07             ; 5499 E6 07
    cp     $00             ; 549B FE 00
    jr     nz,$54c1        ; 549D 20 22
    inc    (ix+$0c)        ; 549F DD 34 0C
    ld     a,(ix+$0c)      ; 54A2 DD 7E 0C
    cp     $07             ; 54A5 FE 07
    jr     c,$54ae         ; 54A7 38 05
    ld     a,$00           ; 54A9 3E 00
    ld     ($8478),a       ; 54AB 32 78 84
    add    a,a             ; 54AE 87
    ld     d,$00           ; 54AF 16 00
    ld     e,a             ; 54B1 5F
    ld     iy,$54db        ; 54B2 FD 21 DB 54
    add    iy,de           ; 54B6 FD 19
    ld     h,(iy+$01)      ; 54B8 FD 66 01
    ld     l,(iy+$00)      ; 54BB FD 6E 00
    ld     ($8cbe),hl      ; 54BE 22 BE 8C
    ret                    ; 54C1 C9
    ld     a,(ix+$01)      ; 54C2 DD 7E 01
    ld     ($8500),a       ; 54C5 32 00 85
    ld     a,(ix+$02)      ; 54C8 DD 7E 02
    ld     ($8501),a       ; 54CB 32 01 85
    ld     a,(ix+$03)      ; 54CE DD 7E 03
    ld     ($8502),a       ; 54D1 32 02 85
    ld     a,(ix+$04)      ; 54D4 DD 7E 04
    ld     ($8503),a       ; 54D7 32 03 85
    ret                    ; 54DA C9
    rst    38h             ; 54DB FF
    rrca                   ; 54DC 0F
    xor    a               ; 54DD AF
    ld     a,(bc)          ; 54DE 0A
    ld     e,a             ; 54DF 5F
    dec    b               ; 54E0 05
    rrca                   ; 54E1 0F
    nop                    ; 54E2 00
    rrca                   ; 54E3 0F
    nop                    ; 54E4 00
    ld     e,a             ; 54E5 5F
    dec    b               ; 54E6 05
    xor    a               ; 54E7 AF
    ld     a,(bc)          ; 54E8 0A
    ret                    ; 54E9 C9
    push   hl              ; 54EA E5
    push   ix              ; 54EB DD E5
    push   bc              ; 54ED C5
    push   iy              ; 54EE FD E5
    ld     a,$00           ; 54F0 3E 00
    ld     ($81a6),a       ; 54F2 32 A6 81
    ld     a,($81a2)       ; 54F5 3A A2 81
    cp     $00             ; 54F8 FE 00
    jp     z,$5569         ; 54FA CA 69 55
    ld     a,($81a0)       ; 54FD 3A A0 81
    dec    a               ; 5500 3D
    ld     h,$00           ; 5501 26 00
    ld     l,a             ; 5503 6F
    ld     b,$00           ; 5504 06 00
    ld     c,a             ; 5506 4F
    add    hl,hl           ; 5507 29
    add    hl,hl           ; 5508 29
    add    hl,bc           ; 5509 09
    add    hl,bc           ; 550A 09
    ld     bc,$de38        ; 550B 01 38 DE
    add    hl,bc           ; 550E 09
    inc    hl              ; 550F 23
    inc    hl              ; 5510 23
    inc    hl              ; 5511 23
    inc    hl              ; 5512 23
    ld     a,(hl)          ; 5513 7E
    ld     h,$00           ; 5514 26 00
    ld     l,a             ; 5516 6F
    add    hl,hl           ; 5517 29
    ld     b,h             ; 5518 44
    ld     c,l             ; 5519 4D
    add    hl,hl           ; 551A 29
    add    hl,hl           ; 551B 29
    add    hl,bc           ; 551C 09
    ld     bc,$d880        ; 551D 01 80 D8	; Display a bomb ?
    add    hl,bc           ; 5520 09
    push   hl              ; 5521 E5
    pop    iy              ; 5522 FD E1
    ld     a,(iy+$05)      ; 5524 FD 7E 05
    ld     ($85a0),a       ; 5527 32 A0 85
    ld     a,(iy+$06)      ; 552A FD 7E 06
    ld     ($85a1),a       ; 552D 32 A1 85
    ld     a,(iy+$07)      ; 5530 FD 7E 07
    ld     ($85a2),a       ; 5533 32 A2 85
    ld     a,(iy+$08)      ; 5536 FD 7E 08
    ld     ($85a3),a       ; 5539 32 A3 85
    ld     a,(iy+$09)      ; 553C FD 7E 09
    ld     ($85a4),a       ; 553F 32 A4 85
    ld     ix,$8480        ; 5542 DD 21 80 84
    ld     a,(ix+$00)      ; 5546 DD 7E 00
    cp     $ff             ; 5549 FE FF
    jp     z,$5569         ; 554B CA 69 55
    bit    6,(ix+$00)      ; 554E DD CB 00 76
    jp     nz,$5561        ; 5552 C2 61 55
    ld     b,(ix+$01)      ; 5555 DD 46 01
    ld     c,(ix+$02)      ; 5558 DD 4E 02
    call   $00de           ; 555B CD DE 00
    call   $5570           ; 555E CD 70 55
    ld     bc,$0005        ; 5561 01 05 00
    add    ix,bc           ; 5564 DD 09
    jp     $5546           ; 5566 C3 46 55
    pop    iy              ; 5569 FD E1
    pop    bc              ; 556B C1
    pop    ix              ; 556C DD E1
    pop    hl              ; 556E E1
    ret                    ; 556F C9
    push   hl              ; 5570 E5
    push   de              ; 5571 D5
    push   ix              ; 5572 DD E5
    push   hl              ; 5574 E5
    pop    ix              ; 5575 DD E1
    ld     de,$0400        ; 5577 11 00 04
    add    ix,de           ; 557A DD 19
    ld     b,(iy+$04)      ; 557C FD 46 04
    ld     a,(iy+$02)      ; 557F FD 7E 02
    ld     (hl),a          ; 5582 77
    ld     (ix+$00),b      ; 5583 DD 70 00
    inc    ix              ; 5586 DD 23
    inc    hl              ; 5588 23
    ld     a,(iy+$03)      ; 5589 FD 7E 03
    ld     (hl),a          ; 558C 77
    ld     (ix+$00),b      ; 558D DD 70 00
    ld     de,$ffdf        ; 5590 11 DF FF
    adc    hl,de           ; 5593 ED 5A
    add    ix,de           ; 5595 DD 19
    ld     a,(iy+$00)      ; 5597 FD 7E 00
    ld     (hl),a          ; 559A 77
    ld     (ix+$00),b      ; 559B DD 70 00
    inc    ix              ; 559E DD 23
    inc    hl              ; 55A0 23
    ld     a,(iy+$01)      ; 55A1 FD 7E 01
    ld     (hl),a          ; 55A4 77
    ld     (ix+$00),b      ; 55A5 DD 70 00
    pop    ix              ; 55A8 DD E1
    pop    de              ; 55AA D1
    pop    hl              ; 55AB E1
    ret                    ; 55AC C9
    push   bc              ; 55AD C5
    push   de              ; 55AE D5
    push   ix              ; 55AF DD E5
    push   iy              ; 55B1 FD E5
    ld     ix,$81a3        ; 55B3 DD 21 A3 81
    ld     iy,$8480        ; 55B7 FD 21 80 84
    ld     b,(ix+$00)      ; 55BB DD 46 00
    ld     c,$08           ; 55BE 0E 08
    ld     a,(iy+$00)      ; 55C0 FD 7E 00
    cp     $ff             ; 55C3 FE FF
    jp     z,$55e8         ; 55C5 CA E8 55
    bit    0,b             ; 55C8 CB 40
    jr     z,$55d0         ; 55CA 28 04
    set    6,(iy+$00)      ; 55CC FD CB 00 F6
    ld     de,$0005        ; 55D0 11 05 00
    add    iy,de           ; 55D3 FD 19
    dec    c               ; 55D5 0D
    ld     a,c             ; 55D6 79
    cp     $00             ; 55D7 FE 00
    jr     z,$55e0         ; 55D9 28 05
    srl    b               ; 55DB CB 38
    jp     $55c0           ; 55DD C3 C0 55
    ld     de,$0001        ; 55E0 11 01 00
    add    ix,de           ; 55E3 DD 19
    jp     $55bb           ; 55E5 C3 BB 55
    pop    iy              ; 55E8 FD E1
    pop    ix              ; 55EA DD E1
    pop    de              ; 55EC D1
    pop    bc              ; 55ED C1
    ret                    ; 55EE C9
    push   bc              ; 55EF C5
    push   de              ; 55F0 D5
    push   ix              ; 55F1 DD E5
    push   iy              ; 55F3 FD E5
    ld     ix,$81a3        ; 55F5 DD 21 A3 81
    ld     iy,$8480        ; 55F9 FD 21 80 84
    ld     b,$00           ; 55FD 06 00
    ld     c,$08           ; 55FF 0E 08
    ld     a,(iy+$00)      ; 5601 FD 7E 00
    cp     $ff             ; 5604 FE FF
    jp     z,$562a         ; 5606 CA 2A 56
    srl    b               ; 5609 CB 38
    bit    6,(iy+$00)      ; 560B FD CB 00 76
    jr     z,$5613         ; 560F 28 02
    set    7,b             ; 5611 CB F8
    ld     de,$0005        ; 5613 11 05 00
    add    iy,de           ; 5616 FD 19
    dec    c               ; 5618 0D
    ld     a,c             ; 5619 79
    cp     $00             ; 561A FE 00
    jp     nz,$5601        ; 561C C2 01 56
    ld     (ix+$00),b      ; 561F DD 70 00
    ld     de,$0001        ; 5622 11 01 00
    add    ix,de           ; 5625 DD 19
    jp     $55fd           ; 5627 C3 FD 55
    pop    iy              ; 562A FD E1
    pop    ix              ; 562C DD E1
    pop    de              ; 562E D1
    pop    bc              ; 562F C1
    ret                    ; 5630 C9
    push   bc              ; 5631 C5
    push   de              ; 5632 D5
    push   hl              ; 5633 E5
    push   iy              ; 5634 FD E5
    ld     hl,$7000        ; 5636 21 00 70
    ld     a,(ix+$01)      ; 5639 DD 7E 01
    cp     $02             ; 563C FE 02
    jr     c,$5648         ; 563E 38 08
    inc    iy              ; 5640 FD 23
    inc    iy              ; 5642 FD 23
    inc    iy              ; 5644 FD 23
    inc    iy              ; 5646 FD 23
    add    a,a             ; 5648 87
    ld     d,$00           ; 5649 16 00
    ld     e,a             ; 564B 5F
    add    hl,de           ; 564C 19
    ld     a,(iy+$02)      ; 564D FD 7E 02
    ld     d,(hl)          ; 5650 56
    add    a,d             ; 5651 82
    ld     b,a             ; 5652 47
    ld     a,(iy+$03)      ; 5653 FD 7E 03
    inc    hl              ; 5656 23
    ld     d,(hl)          ; 5657 56
    add    a,d             ; 5658 82
    ld     c,a             ; 5659 4F
    call   $00de           ; 565A CD DE 00
    push   hl              ; 565D E5
    pop    iy              ; 565E FD E1
    ld     de,$0000        ; 5660 11 00 00
    ld     a,(ix+$01)      ; 5663 DD 7E 01
    cp     $01             ; 5666 FE 01
    jr     z,$568e         ; 5668 28 24
    bit    7,(ix+$0b)      ; 566A DD CB 0B 7E
    jr     z,$567c         ; 566E 28 0C
    ld     a,(iy-$01)      ; 5670 FD 7E FF
    call   $56fc           ; 5673 CD FC 56
    cp     $00             ; 5676 FE 00
    jr     z,$567c         ; 5678 28 02
    set    3,e             ; 567A CB DB
    bit    7,(ix+$0b)      ; 567C DD CB 0B 7E
    jr     nz,$568e        ; 5680 20 0C
    ld     a,(iy+$01)      ; 5682 FD 7E 01
    call   $56fc           ; 5685 CD FC 56
    cp     $00             ; 5688 FE 00
    jr     z,$568e         ; 568A 28 02
    set    2,e             ; 568C CB D3
    bit    7,(ix+$09)      ; 568E DD CB 09 7E
    jr     z,$56a0         ; 5692 28 0C
    ld     a,(iy+$20)      ; 5694 FD 7E 20
    call   $56fc           ; 5697 CD FC 56
    cp     $00             ; 569A FE 00
    jr     z,$56a0         ; 569C 28 02
    set    1,e             ; 569E CB CB
    bit    7,(ix+$09)      ; 56A0 DD CB 09 7E
    jr     nz,$56b2        ; 56A4 20 0C
    ld     a,(iy-$20)      ; 56A6 FD 7E E0
    call   $56fc           ; 56A9 CD FC 56
    cp     $00             ; 56AC FE 00
    jr     z,$56b2         ; 56AE 28 02
    set    0,e             ; 56B0 CB C3
    ld     a,(ix+$01)      ; 56B2 DD 7E 01
    cp     $01             ; 56B5 FE 01
    jp     nz,$56f5        ; 56B7 C2 F5 56
    pop    iy              ; 56BA FD E1
    push   iy              ; 56BC FD E5
    ld     a,(iy+$02)      ; 56BE FD 7E 02
    add    a,$01           ; 56C1 C6 01
    ld     b,a             ; 56C3 47
    ld     a,(iy+$03)      ; 56C4 FD 7E 03
    add    a,$08           ; 56C7 C6 08
    ld     c,a             ; 56C9 4F
    call   $00de           ; 56CA CD DE 00
    inc    hl              ; 56CD 23
    ld     a,(hl)          ; 56CE 7E
    call   $56fc           ; 56CF CD FC 56
    cp     $00             ; 56D2 FE 00
    jr     z,$56da         ; 56D4 28 04
    set    2,e             ; 56D6 CB D3
    jr     $56f5           ; 56D8 18 1B
    ld     a,(iy+$02)      ; 56DA FD 7E 02
    add    a,$0e           ; 56DD C6 0E
    ld     b,a             ; 56DF 47
    ld     a,(iy+$03)      ; 56E0 FD 7E 03
    add    a,$08           ; 56E3 C6 08
    ld     c,a             ; 56E5 4F
    call   $00de           ; 56E6 CD DE 00
    inc    hl              ; 56E9 23
    ld     a,(hl)          ; 56EA 7E
    call   $56fc           ; 56EB CD FC 56
    cp     $00             ; 56EE FE 00
    jp     z,$56f5         ; 56F0 CA F5 56
    set    2,e             ; 56F3 CB D3
    ld     a,e             ; 56F5 7B
    pop    iy              ; 56F6 FD E1
    pop    hl              ; 56F8 E1
    pop    de              ; 56F9 D1
    pop    bc              ; 56FA C1
    ret                    ; 56FB C9
    cp     $60             ; 56FC FE 60
    jr     c,$570c         ; 56FE 38 0C
    cp     $80             ; 5700 FE 80
    jr     c,$570e         ; 5702 38 0A
    cp     $a0             ; 5704 FE A0
    jr     c,$570c         ; 5706 38 04
    cp     $c0             ; 5708 FE C0
    jr     c,$570e         ; 570A 38 02
    ld     a,$00           ; 570C 3E 00
    ret                    ; 570E C9
    ld     a,($81ab)       ; 570F 3A AB 81
    cp     $14             ; 5712 FE 14
    jp     c,$5809         ; 5714 DA 09 58
    ld     a,$2b           ; 5717 3E 2B
    call   $00fc           ; 5719 CD FC 00
    ld     a,$00           ; 571C 3E 00
    ld     ($8982),a       ; 571E 32 82 89
    ld     ($8983),a       ; 5721 32 83 89
    ld     ($8984),a       ; 5724 32 84 89
    ld     a,$00           ; 5727 3E 00
    ld     ($81ac),a       ; 5729 32 AC 81
    call   $00cf           ; 572C CD CF 00
    ld     c,$70           ; 572F 0E 70
    rrca                   ; 5731 0F
    ld     a,($81ab)       ; 5732 3A AB 81
    cp     $14             ; 5735 FE 14
    jr     nz,$574d        ; 5737 20 14
    ld     a,$0f           ; 5739 3E 0F
    call   $6018           ; 573B CD 18 60
    call   $00cf           ; 573E CD CF 00
    call   nc,$0271        ; 5741 D4 71 02
    ld     d,$0a           ; 5744 16 0A
    ld     a,$01           ; 5746 3E 01
    ld     ($8984),a       ; 5748 32 84 89
    jr     $5796           ; 574B 18 49
    push   af              ; 574D F5
    ld     a,$0f           ; 574E 3E 0F
    call   $6018           ; 5750 CD 18 60
    pop    af              ; 5753 F1
    cp     $15             ; 5754 FE 15
    jr     nz,$576c        ; 5756 20 14
    ld     a,$0f           ; 5758 3E 0F
    call   $6018           ; 575A CD 18 60
    call   $00cf           ; 575D CD CF 00
    ret    c               ; 5760 D8
    ld     (hl),c          ; 5761 71
    ld     (bc),a          ; 5762 02
    ld     d,$14           ; 5763 16 14
    ld     a,$02           ; 5765 3E 02
    ld     ($8984),a       ; 5767 32 84 89
    jr     $5796           ; 576A 18 2A
    cp     $16             ; 576C FE 16
    jr     nz,$5784        ; 576E 20 14
    ld     a,$0f           ; 5770 3E 0F
    call   $6018           ; 5772 CD 18 60
    call   $00cf           ; 5775 CD CF 00
    call   c,$0271         ; 5778 DC 71 02
    ld     d,$1e           ; 577B 16 1E
    ld     a,$03           ; 577D 3E 03
    ld     ($8984),a       ; 577F 32 84 89
    jr     $5796           ; 5782 18 12
    ld     a,$0e           ; 5784 3E 0E
    call   $6018           ; 5786 CD 18 60
    call   $00cf           ; 5789 CD CF 00
    ret    po              ; 578C E0
    ld     (hl),c          ; 578D 71
    ld     (bc),a          ; 578E 02
    ld     d,$32           ; 578F 16 32
    ld     a,$05           ; 5791 3E 05
    ld     ($8984),a       ; 5793 32 84 89
    ld     b,$00           ; 5796 06 00
    ld     a,$01           ; 5798 3E 01
    ld     ($806e),a       ; 579A 32 6E 80
    ld     a,($806e)       ; 579D 3A 6E 80
    cp     $00             ; 57A0 FE 00
    jr     nz,$579d        ; 57A2 20 F9
    inc    b               ; 57A4 04
    ld     a,b             ; 57A5 78
    cp     $78             ; 57A6 FE 78
    jr     nz,$5798        ; 57A8 20 EE
    ld     e,$00           ; 57AA 1E 00
    ld     a,$01           ; 57AC 3E 01
    ld     ($806e),a       ; 57AE 32 6E 80
    ld     a,($806e)       ; 57B1 3A 6E 80
    cp     $00             ; 57B4 FE 00
    jr     nz,$57b1        ; 57B6 20 F9
    ld     a,d             ; 57B8 7A
    cp     $00             ; 57B9 FE 00
    jr     z,$57e5         ; 57BB 28 28
    inc    e               ; 57BD 1C
    ld     a,e             ; 57BE 7B
    and    $03             ; 57BF E6 03
    jr     nz,$57ac        ; 57C1 20 E9
    ld     bc,$1000        ; 57C3 01 00 10
    call   $00e4           ; 57C6 CD E4 00
    ld     a,($8983)       ; 57C9 3A 83 89
    sub    $0a             ; 57CC D6 0A
    daa                    ; 57CE 27
    ld     ($8983),a       ; 57CF 32 83 89
    ld     a,($8984)       ; 57D2 3A 84 89
    sbc    a,$00           ; 57D5 DE 00
    daa                    ; 57D7 27
    ld     ($8984),a       ; 57D8 32 84 89
    ld     ix,$7244        ; 57DB DD 21 44 72
    call   $00d5           ; 57DF CD D5 00
    dec    d               ; 57E2 15
    jr     $57ac           ; 57E3 18 C7
    ld     a,$00           ; 57E5 3E 00
    ld     ($91f3),a       ; 57E7 32 F3 91
    ld     ($91b3),a       ; 57EA 32 B3 91
    ld     ($9193),a       ; 57ED 32 93 91
    ld     b,$00           ; 57F0 06 00
    ld     a,$01           ; 57F2 3E 01
    ld     ($806e),a       ; 57F4 32 6E 80
    ld     a,($806e)       ; 57F7 3A 6E 80
    cp     $00             ; 57FA FE 00
    jr     nz,$57f7        ; 57FC 20 F9
    inc    b               ; 57FE 04
    ld     a,b             ; 57FF 78
    cp     $3c             ; 5800 FE 3C
    jr     nz,$57f2        ; 5802 20 EE
    ld     a,$ab           ; 5804 3E AB
    call   $00fc           ; 5806 CD FC 00
    ld     a,$00           ; 5809 3E 00
    ld     ($81ab),a       ; 580B 32 AB 81
    ld     ($8900),a       ; 580E 32 00 89
    ld     ($8901),a       ; 5811 32 01 89
    ret                    ; 5814 C9
    ld     ix,$8028        ; 5815 DD 21 28 80
    ld     (ix+$02),$00    ; 5819 DD 36 02 00
    ld     (ix+$03),$00    ; 581D DD 36 03 00
    ld     (ix+$04),$00    ; 5821 DD 36 04 00
    ld     (ix+$05),$00    ; 5825 DD 36 05 00
    ld     (ix+$06),$14    ; 5829 DD 36 06 14
    ld     (ix+$07),$15    ; 582D DD 36 07 15
    ld     iy,$8518        ; 5831 FD 21 18 85
    ld     (iy+$00),$14    ; 5835 FD 36 00 14
    ld     (iy+$01),$0c    ; 5839 FD 36 01 0C
    ld     (iy+$02),$78    ; 583D FD 36 02 78
    ld     (iy+$03),$78    ; 5841 FD 36 03 78
    ld     (iy+$08),$ae    ; 5845 FD 36 08 AE
    ld     (iy+$09),$2b    ; 5849 FD 36 09 2B
    ld     (iy+$0a),$60    ; 584D FD 36 0A 60
    ld     (iy+$0b),$60    ; 5851 FD 36 0B 60
    ld     (iy+$10),$af    ; 5855 FD 36 10 AF
    ld     (iy+$11),$2b    ; 5859 FD 36 11 2B
    ld     (iy+$12),$80    ; 585D FD 36 12 80
    ld     (iy+$13),$60    ; 5861 FD 36 13 60
    ld     (iy+$18),$b0    ; 5865 FD 36 18 B0
    ld     (iy+$19),$2b    ; 5869 FD 36 19 2B
    ld     (iy+$1a),$60    ; 586D FD 36 1A 60
    ld     (iy+$1b),$80    ; 5871 FD 36 1B 80
    ld     (iy+$20),$b1    ; 5875 FD 36 20 B1
    ld     (iy+$21),$2b    ; 5879 FD 36 21 2B
    ld     (iy+$22),$80    ; 587D FD 36 22 80
    ld     (iy+$23),$80    ; 5881 FD 36 23 80
    ld     a,$01           ; 5885 3E 01
    ld     ($806e),a       ; 5887 32 6E 80
    ld     a,($806e)       ; 588A 3A 6E 80
    cp     $00             ; 588D FE 00
    jr     z,$5894         ; 588F 28 03
    nop                    ; 5891 00
    jr     $588a           ; 5892 18 F6
    inc    (ix+$02)        ; 5894 DD 34 02
    ld     a,($802a)       ; 5897 3A 2A 80
    cp     $3c             ; 589A FE 3C
    jr     c,$58a5         ; 589C 38 07
    ld     (ix+$02),$00    ; 589E DD 36 02 00
    inc    (ix+$03)        ; 58A2 DD 34 03
    inc    (ix+$04)        ; 58A5 DD 34 04
    ld     a,($802c)       ; 58A8 3A 2C 80
    cp     $04             ; 58AB FE 04
    jr     c,$58b6         ; 58AD 38 07
    call   $59db           ; 58AF CD DB 59
    ld     (ix+$04),$00    ; 58B2 DD 36 04 00
    inc    (ix+$05)        ; 58B6 DD 34 05
    ld     a,($802d)       ; 58B9 3A 2D 80
    cp     $04             ; 58BC FE 04
    jr     c,$5927         ; 58BE 38 67
    ld     (ix+$05),$00    ; 58C0 DD 36 05 00
    ld     a,($802e)       ; 58C4 3A 2E 80
    cp     $ff             ; 58C7 FE FF
    jr     z,$58d1         ; 58C9 28 06
    call   $5930           ; 58CB CD 30 59
    dec    (ix+$06)        ; 58CE DD 35 06
    ld     a,($802f)       ; 58D1 3A 2F 80
    cp     $ff             ; 58D4 FE FF
    jr     z,$58de         ; 58D6 28 06
    call   $5986           ; 58D8 CD 86 59
    dec    (ix+$07)        ; 58DB DD 35 07
    ld     a,($802b)       ; 58DE 3A 2B 80
    cp     $03             ; 58E1 FE 03
    jr     nz,$5927        ; 58E3 20 42
    ld     a,($802a)       ; 58E5 3A 2A 80
    cp     $10             ; 58E8 FE 10
    jr     nz,$58f8        ; 58EA 20 0C
    ld     a,$00           ; 58EC 3E 00
    ld     ($8540),a       ; 58EE 32 40 85
    ld     a,$0a           ; 58F1 3E 0A
    ld     ($8541),a       ; 58F3 32 41 85
    jr     $5927           ; 58F6 18 2F
    ld     a,($802a)       ; 58F8 3A 2A 80
    cp     $0c             ; 58FB FE 0C
    jr     z,$5901         ; 58FD 28 02
    jr     nc,$5927        ; 58FF 30 26
    cp     $00             ; 5901 FE 00
    jr     nz,$5924        ; 5903 20 1F
    ld     a,($8017)       ; 5905 3A 17 80
    cp     $99             ; 5908 FE 99
    jr     nc,$5914        ; 590A 30 08
    add    a,$01           ; 590C C6 01
    daa                    ; 590E 27
    ld     ($8017),a       ; 590F 32 17 80
    jr     $591d           ; 5912 18 09
    ld     a,($8087)       ; 5914 3A 87 80
    add    a,$01           ; 5917 C6 01
    daa                    ; 5919 27
    ld     ($8087),a       ; 591A 32 87 80
    call   $00f0           ; 591D CD F0 00
    ld     ix,$8028        ; 5920 DD 21 28 80
    call   $59f2           ; 5924 CD F2 59
    ld     a,($802b)       ; 5927 3A 2B 80
    cp     $0b             ; 592A FE 0B
    jp     c,$5885         ; 592C DA 85 58
    ret                    ; 592F C9
    push   ix              ; 5930 DD E5
    ld     ix,$92b6        ; 5932 DD 21 B6 92
    ld     iy,$96b6        ; 5936 FD 21 B6 96
    ld     hl,$d268        ; 593A 21 68 D2
    ld     bc,$0000        ; 593D 01 00 00
    ld     a,c             ; 5940 79
    cp     $14             ; 5941 FE 14
    jr     z,$5948         ; 5943 28 03
    jp     nc,$5983        ; 5945 D2 83 59
    ld     (ix+$00),$00    ; 5948 DD 36 00 00
    ld     (iy+$00),$0a    ; 594C FD 36 00 0A
    ld     a,b             ; 5950 78
    cp     $00             ; 5951 FE 00
    jr     nz,$5970        ; 5953 20 1B
    ld     a,($802e)       ; 5955 3A 2E 80
    cp     c               ; 5958 B9
    jr     z,$595d         ; 5959 28 02
    jr     nc,$5970        ; 595B 30 13
    ld     a,(hl)          ; 595D 7E
    cp     $ff             ; 595E FE FF
    jr     nz,$5966        ; 5960 20 04
    ld     b,$01           ; 5962 06 01
    jr     $5970           ; 5964 18 0A
    ld     a,(hl)          ; 5966 7E
    ld     (ix+$00),a      ; 5967 DD 77 00
    inc    hl              ; 596A 23
    ld     a,(hl)          ; 596B 7E
    ld     (iy+$00),a      ; 596C FD 77 00
    inc    hl              ; 596F 23
    ld     a,c             ; 5970 79
    cp     $0e             ; 5971 FE 0E
    jr     c,$5979         ; 5973 38 04
    dec    ix              ; 5975 DD 2B
    dec    iy              ; 5977 FD 2B
    ld     de,$ffe0        ; 5979 11 E0 FF
    add    ix,de           ; 597C DD 19
    add    iy,de           ; 597E FD 19
    inc    c               ; 5980 0C
    jr     $5940           ; 5981 18 BD
    pop    ix              ; 5983 DD E1
    ret                    ; 5985 C9
    push   ix              ; 5986 DD E5
    ld     ix,$9137        ; 5988 DD 21 37 91
    ld     iy,$9537        ; 598C FD 21 37 95
    ld     hl,$d283        ; 5990 21 83 D2
    ld     bc,$0000        ; 5993 01 00 00
    ld     a,c             ; 5996 79
    cp     $15             ; 5997 FE 15
    jr     z,$599d         ; 5999 28 02
    jr     nc,$59d8        ; 599B 30 3B
    ld     (ix+$00),$00    ; 599D DD 36 00 00
    ld     (iy+$00),$0a    ; 59A1 FD 36 00 0A
    ld     a,b             ; 59A5 78
    cp     $00             ; 59A6 FE 00
    jr     nz,$59c5        ; 59A8 20 1B
    ld     a,($802f)       ; 59AA 3A 2F 80
    cp     c               ; 59AD B9
    jr     z,$59b2         ; 59AE 28 02
    jr     nc,$59c5        ; 59B0 30 13
    ld     a,(hl)          ; 59B2 7E
    cp     $ff             ; 59B3 FE FF
    jr     nz,$59bb        ; 59B5 20 04
    ld     b,$01           ; 59B7 06 01
    jr     $59c5           ; 59B9 18 0A
    ld     a,(hl)          ; 59BB 7E
    ld     (ix+$00),a      ; 59BC DD 77 00
    inc    hl              ; 59BF 23
    ld     a,(hl)          ; 59C0 7E
    ld     (iy+$00),a      ; 59C1 FD 77 00
    inc    hl              ; 59C4 23
    ld     a,c             ; 59C5 79
    cp     $0d             ; 59C6 FE 0D
    jr     c,$59ce         ; 59C8 38 04
    dec    ix              ; 59CA DD 2B
    dec    iy              ; 59CC FD 2B
    ld     de,$0020        ; 59CE 11 20 00
    add    ix,de           ; 59D1 DD 19
    add    iy,de           ; 59D3 FD 19
    inc    c               ; 59D5 0C
    jr     $5996           ; 59D6 18 BE
    pop    ix              ; 59D8 DD E1
    ret                    ; 59DA C9
    push   bc              ; 59DB C5
    ld     a,($8518)       ; 59DC 3A 18 85
    ld     c,a             ; 59DF 4F
    and    $03             ; 59E0 E6 03
    cp     $03             ; 59E2 FE 03
    jr     nz,$59eb        ; 59E4 20 05
    ld     a,c             ; 59E6 79
    and    $fc             ; 59E7 E6 FC
    jr     $59ed           ; 59E9 18 02
    inc    c               ; 59EB 0C
    ld     a,c             ; 59EC 79
    ld     ($8518),a       ; 59ED 32 18 85
    pop    bc              ; 59F0 C1
    ret                    ; 59F1 C9
    ld     a,$ad           ; 59F2 3E AD
    ld     ($8540),a       ; 59F4 32 40 85
    ld     a,$3b           ; 59F7 3E 3B
    ld     ($8541),a       ; 59F9 32 41 85
    ld     a,$8c           ; 59FC 3E 8C
    ld     ($8542),a       ; 59FE 32 42 85
    ld     a,$d4           ; 5A01 3E D4
    ld     ($8543),a       ; 5A03 32 43 85
    ret                    ; 5A06 C9
    ret                    ; 5A07 C9
    push   bc              ; 5A08 C5
    push   de              ; 5A09 D5
    ld     a,($81ae)       ; 5A0A 3A AE 81
    cp     $ff             ; 5A0D FE FF
    jp     z,$5ae3         ; 5A0F CA E3 5A
    ld     a,($8014)       ; 5A12 3A 14 80
    and    $07             ; 5A15 E6 07
    cp     $00             ; 5A17 FE 00
    jr     nz,$5a1e        ; 5A19 20 03
    jp     $5ae3           ; 5A1B C3 E3 5A
    cp     $01             ; 5A1E FE 01
    jr     nz,$5a27        ; 5A20 20 05
    ld     b,$0a           ; 5A22 06 0A
    jp     $5a59           ; 5A24 C3 59 5A
    cp     $02             ; 5A27 FE 02
    jr     nz,$5a30        ; 5A29 20 05
    ld     b,$03           ; 5A2B 06 03
    jp     $5a59           ; 5A2D C3 59 5A
    cp     $03             ; 5A30 FE 03
    jr     nz,$5a39        ; 5A32 20 05
    ld     b,$05           ; 5A34 06 05
    jp     $5a69           ; 5A36 C3 69 5A
    cp     $04             ; 5A39 FE 04
    jr     nz,$5a42        ; 5A3B 20 05
    ld     b,$0a           ; 5A3D 06 0A
    jp     $5a69           ; 5A3F C3 69 5A
    cp     $05             ; 5A42 FE 05
    jr     nz,$5a4d        ; 5A44 20 07
    ld     b,$05           ; 5A46 06 05
    ld     c,$0a           ; 5A48 0E 0A
    jp     $5a7a           ; 5A4A C3 7A 5A
    cp     $06             ; 5A4D FE 06
    jp     nz,$5a9f        ; 5A4F C2 9F 5A
    ld     b,$0a           ; 5A52 06 0A
    ld     c,$1e           ; 5A54 0E 1E
    jp     $5a7a           ; 5A56 C3 7A 5A
    ld     d,$00           ; 5A59 16 00
    ld     a,($81ad)       ; 5A5B 3A AD 81
    cp     b               ; 5A5E B8
    jp     c,$5ad4         ; 5A5F DA D4 5A
    inc    d               ; 5A62 14
    sub    b               ; 5A63 90
    ld     ($81ad),a       ; 5A64 32 AD 81
    jr     $5a5b           ; 5A67 18 F2
    ld     a,($81ad)       ; 5A69 3A AD 81
    cp     b               ; 5A6C B8
    jp     c,$5ae3         ; 5A6D DA E3 5A
    ld     d,$01           ; 5A70 16 01
    ld     a,$ff           ; 5A72 3E FF
    ld     ($81ae),a       ; 5A74 32 AE 81
    jp     $5ad4           ; 5A77 C3 D4 5A
    ld     d,$00           ; 5A7A 16 00
    ld     a,($81ae)       ; 5A7C 3A AE 81
    cp     $01             ; 5A7F FE 01
    jr     z,$5a90         ; 5A81 28 0D
    ld     a,($81ad)       ; 5A83 3A AD 81
    cp     b               ; 5A86 B8
    jp     c,$5ae3         ; 5A87 DA E3 5A
    inc    d               ; 5A8A 14
    ld     a,$01           ; 5A8B 3E 01
    ld     ($81ae),a       ; 5A8D 32 AE 81
    ld     a,($81ad)       ; 5A90 3A AD 81
    cp     c               ; 5A93 B9
    jr     c,$5ad4         ; 5A94 38 3E
    inc    d               ; 5A96 14
    ld     a,$ff           ; 5A97 3E FF
    ld     ($81ae),a       ; 5A99 32 AE 81
    jp     $5ad4           ; 5A9C C3 D4 5A
    ld     d,$00           ; 5A9F 16 00
    ld     a,($81ae)       ; 5AA1 3A AE 81
    cp     $01             ; 5AA4 FE 01
    jr     z,$5aba         ; 5AA6 28 12
    cp     $02             ; 5AA8 FE 02
    jr     z,$5ac7         ; 5AAA 28 1B
    ld     a,($81ad)       ; 5AAC 3A AD 81
    cp     $05             ; 5AAF FE 05
    jp     c,$5ae3         ; 5AB1 DA E3 5A
    inc    d               ; 5AB4 14
    ld     a,$01           ; 5AB5 3E 01
    ld     ($81ae),a       ; 5AB7 32 AE 81
    ld     a,($81ad)       ; 5ABA 3A AD 81
    cp     $0a             ; 5ABD FE 0A
    jr     c,$5ad4         ; 5ABF 38 13
    inc    d               ; 5AC1 14
    ld     a,$02           ; 5AC2 3E 02
    ld     ($81ae),a       ; 5AC4 32 AE 81
    ld     a,($81ad)       ; 5AC7 3A AD 81
    cp     $1e             ; 5ACA FE 1E
    jr     c,$5ad4         ; 5ACC 38 06
    inc    d               ; 5ACE 14
    ld     a,$ff           ; 5ACF 3E FF
    ld     ($81ae),a       ; 5AD1 32 AE 81
    ld     a,d             ; 5AD4 7A
    cp     $00             ; 5AD5 FE 00
    jr     z,$5ae3         ; 5AD7 28 0A
    ld     a,($819b)       ; 5AD9 3A 9B 81
    add    a,d             ; 5ADC 82
    ld     ($819b),a       ; 5ADD 32 9B 81
    call   $4ff3           ; 5AE0 CD F3 4F
    pop    de              ; 5AE3 D1
    pop    bc              ; 5AE4 C1
    pop    af              ; 5AE5 F1
    ret                    ; 5AE6 C9
    ld     a,($b004)       ; 5AE7 3A 04 B0
    and    $03             ; 5AEA E6 03
    cp     $03             ; 5AEC FE 03
    jr     z,$5af3         ; 5AEE 28 03
    inc    a               ; 5AF0 3C
    jr     $5af5           ; 5AF1 18 02
    ld     a,$06           ; 5AF3 3E 06
    ld     ($8016),a       ; 5AF5 32 16 80
    ld     a,($b004)       ; 5AF8 3A 04 B0
    and    $0c             ; 5AFB E6 0C
    srl    a               ; 5AFD CB 3F
    srl    a               ; 5AFF CB 3F
    cp     $01             ; 5B01 FE 01
    jr     nz,$5b13        ; 5B03 20 0E
    ld     a,$ff           ; 5B05 3E FF
    ld     ($8084),a       ; 5B07 32 84 80
    ld     a,$00           ; 5B0A 3E 00
    ld     ($8085),a       ; 5B0C 32 85 80
    ld     a,$01           ; 5B0F 3E 01
    jr     $5b19           ; 5B11 18 06
    cp     $00             ; 5B13 FE 00
    jr     nz,$5b19        ; 5B15 20 02
    ld     a,$01           ; 5B17 3E 01
    ld     ($8083),a       ; 5B19 32 83 80
    ld     a,($b004)       ; 5B1C 3A 04 B0
    and    $30             ; 5B1F E6 30
    srl    a               ; 5B21 CB 3F
    srl    a               ; 5B23 CB 3F
    srl    a               ; 5B25 CB 3F
    srl    a               ; 5B27 CB 3F
    cp     $03             ; 5B29 FE 03
    jr     z,$5b31         ; 5B2B 28 04
    add    a,$03           ; 5B2D C6 03
    jr     $5b33           ; 5B2F 18 02
    ld     a,$02           ; 5B31 3E 02
    ld     ($8071),a       ; 5B33 32 71 80
    ld     a,($b004)       ; 5B36 3A 04 B0
    and    $40             ; 5B39 E6 40
    jr     z,$5b3f         ; 5B3B 28 02
    ld     a,$01           ; 5B3D 3E 01
    ld     ($8072),a       ; 5B3F 32 72 80
    ld     a,($b004)       ; 5B42 3A 04 B0
    and    $80             ; 5B45 E6 80
    jr     z,$5b4b         ; 5B47 28 02
    ld     a,$01           ; 5B49 3E 01
    ld     ($8073),a       ; 5B4B 32 73 80
    ret                    ; 5B4E C9
    push   ix              ; 5B4F DD E5
    ld     ix,$8900        ; 5B51 DD 21 00 89
    bit    0,(ix+$00)      ; 5B55 DD CB 00 46
    jr     z,$5b6a         ; 5B59 28 0F
    call   $2fec           ; 5B5B CD EC 2F
    ld     a,(ix+$02)      ; 5B5E DD 7E 02
    ld     ($8510),a       ; 5B61 32 10 85
    ld     a,(ix+$03)      ; 5B64 DD 7E 03
    ld     ($8511),a       ; 5B67 32 11 85
    pop    ix              ; 5B6A DD E1
    ret                    ; 5B6C C9
    push   af              ; 5B6D F5
    ld     a,($808f)       ; 5B6E 3A 8F 80
    cp     $00             ; 5B71 FE 00
    jr     z,$5bc7         ; 5B73 28 52
    ld     a,e             ; 5B75 7B
    cp     $00             ; 5B76 FE 00
    jr     z,$5bc7         ; 5B78 28 4D
    cp     $ff             ; 5B7A FE FF
    jr     z,$5bc7         ; 5B7C 28 49
    push   bc              ; 5B7E C5
    push   hl              ; 5B7F E5
    push   ix              ; 5B80 DD E5
    ld     a,($81a0)       ; 5B82 3A A0 81
    dec    a               ; 5B85 3D
    ld     h,$00           ; 5B86 26 00
    ld     l,a             ; 5B88 6F
    add    hl,hl           ; 5B89 29
    ld     b,h             ; 5B8A 44
    ld     c,l             ; 5B8B 4D
    add    hl,hl           ; 5B8C 29
    add    hl,bc           ; 5B8D 09
    ld     ix,$de38        ; 5B8E DD 21 38 DE
    ld     b,h             ; 5B92 44
    ld     c,l             ; 5B93 4D
    add    ix,bc           ; 5B94 DD 09
    ld     a,(ix+$02)      ; 5B96 DD 7E 02
    pop    ix              ; 5B99 DD E1
    pop    hl              ; 5B9B E1
    ld     bc,$0000        ; 5B9C 01 00 00
    cp     $04             ; 5B9F FE 04
    jr     c,$5ba9         ; 5BA1 38 06
    ld     b,$10           ; 5BA3 06 10
    dec    a               ; 5BA5 3D
    dec    a               ; 5BA6 3D
    dec    a               ; 5BA7 3D
    dec    a               ; 5BA8 3D
    cp     $00             ; 5BA9 FE 00
    jr     z,$5bbf         ; 5BAB 28 12
    cp     $01             ; 5BAD FE 01
    jr     nz,$5bb5        ; 5BAF 20 04
    ld     c,$10           ; 5BB1 0E 10
    jr     $5bbf           ; 5BB3 18 0A
    cp     $02             ; 5BB5 FE 02
    jr     nz,$5bbd        ; 5BB7 20 04
    ld     c,$40           ; 5BB9 0E 40
    jr     $5bbf           ; 5BBB 18 02
    ld     c,$50           ; 5BBD 0E 50
    ld     a,d             ; 5BBF 7A
    or     a               ; 5BC0 B7
    or     b               ; 5BC1 B0
    ld     d,a             ; 5BC2 57
    ld     a,e             ; 5BC3 7B
    add    a,c             ; 5BC4 81
    ld     e,a             ; 5BC5 5F
    pop    bc              ; 5BC6 C1
    pop    af              ; 5BC7 F1
    ret                    ; 5BC8 C9
    push   af              ; 5BC9 F5
    push   bc              ; 5BCA C5
    push   de              ; 5BCB D5
    push   hl              ; 5BCC E5
    ld     a,($8094)       ; 5BCD 3A 94 80
    cp     $ff             ; 5BD0 FE FF
    jp     z,$5c0b         ; 5BD2 CA 0B 5C
    ld     a,($8091)       ; 5BD5 3A 91 80
    inc    a               ; 5BD8 3C
    ld     ($8091),a       ; 5BD9 32 91 80
    cp     $03             ; 5BDC FE 03
    jp     c,$5c0b         ; 5BDE DA 0B 5C
    ld     a,$00           ; 5BE1 3E 00
    ld     ($8091),a       ; 5BE3 32 91 80
    ld     hl,$0f0a        ; 5BE6 21 0A 0F
    ld     de,$8cb2        ; 5BE9 11 B2 8C
    ld     bc,$000e        ; 5BEC 01 0E 00
    ldir                   ; 5BEF ED B0
    ld     a,($8092)       ; 5BF1 3A 92 80
    inc    a               ; 5BF4 3C
    cp     $07             ; 5BF5 FE 07
    jr     c,$5bfb         ; 5BF7 38 02
    ld     a,$00           ; 5BF9 3E 00
    ld     ($8092),a       ; 5BFB 32 92 80
    add    a,a             ; 5BFE 87
    ld     d,$00           ; 5BFF 16 00
    ld     e,a             ; 5C01 5F
    ld     hl,$8cb2        ; 5C02 21 B2 8C
    add    hl,de           ; 5C05 19
    ld     (hl),$ff        ; 5C06 36 FF
    inc    hl              ; 5C08 23
    ld     (hl),$0f        ; 5C09 36 0F
    pop    hl              ; 5C0B E1
    pop    de              ; 5C0C D1
    pop    bc              ; 5C0D C1
    pop    af              ; 5C0E F1
    ret                    ; 5C0F C9

; 5C10h - zeros

; 5FD0h - jump table

    jp     $5ae7           ; 5FD0 C3 E7 5A
    jp     $5b6d           ; 5FD3 C3 6D 5B
    jp     $56fc           ; 5FD6 C3 FC 56
    jp     $5bc9           ; 5FD9 C3 C9 5B
    nop                    ; 5FDC 00
    jp     $5a07           ; 5FDD C3 07 5A
    jp     $5100           ; 5FE0 C3 00 51
    jp     $533e           ; 5FE3 C3 3E 53
    jp     $54e9           ; 5FE6 C3 E9 54
    jp     $54ea           ; 5FE9 C3 EA 54
    jp     $55ad           ; 5FEC C3 AD 55
    jp     $55ef           ; 5FEF C3 EF 55
    jp     $5631           ; 5FF2 C3 31 56
    jp     $570f           ; 5FF5 C3 0F 57
    jp     $5009           ; 5FF8 C3 09 50
    jp     $5815           ; 5FFB C3 15 58
    nop                    ; 5FFE 00
    nop                    ; 5FFF 00
    jp     $6102           ; 6000 C3 02 61
    jp     $6137           ; 6003 C3 37 61
    jp     $6174           ; 6006 C3 74 61
    jp     $61a6           ; 6009 C3 A6 61
    jp     $6209           ; 600C C3 09 62
    jp     $6281           ; 600F C3 81 62
    jp     $62be           ; 6012 C3 BE 62
    jp     $62e5           ; 6015 C3 E5 62
    jp     $6326           ; 6018 C3 26 63
    jp     $639a           ; 601B C3 9A 63
    jp     $63bf           ; 601E C3 BF 63
    jp     $6479           ; 6021 C3 79 64
    jp     $64d8           ; 6024 C3 D8 64
    jp     $6524           ; 6027 C3 24 65
    jp     $6569           ; 602A C3 69 65
    jp     $6590           ; 602D C3 90 65
    jp     $66fb           ; 6030 C3 FB 66
    jp     $6718           ; 6033 C3 18 67
    jp     $6781           ; 6036 C3 81 67

; 6039h - zeros

; 6101h - ?

    ret                    ; 6101 C9
    ld     hl,$827a        ; 6102 21 7A 82
    ld     de,$8518        ; 6105 11 18 85
    ld     b,$09           ; 6108 06 09
    push   bc              ; 610A C5
    ld     bc,$0004        ; 610B 01 04 00
    dec    hl              ; 610E 2B
    dec    hl              ; 610F 2B
    ld     a,(hl)          ; 6110 7E
    cp     $20             ; 6111 FE 20
    inc    hl              ; 6113 23
    inc    hl              ; 6114 23
    jp     nz,$6129        ; 6115 C2 29 61
    dec    hl              ; 6118 2B
    ld     a,(hl)          ; 6119 7E
    cp     $0c             ; 611A FE 0C
    inc    hl              ; 611C 23
    jp     nc,$6129        ; 611D D2 29 61
    inc    de              ; 6120 13
    inc    de              ; 6121 13
    inc    de              ; 6122 13
    inc    de              ; 6123 13
    ldir                   ; 6124 ED B0
    jp     $612f           ; 6126 C3 2F 61
    ldir                   ; 6129 ED B0
    inc    de              ; 612B 13
    inc    de              ; 612C 13
    inc    de              ; 612D 13
    inc    de              ; 612E 13
    ld     bc,$0015        ; 612F 01 15 00
    add    hl,bc           ; 6132 09
    pop    bc              ; 6133 C1
    djnz   $610a           ; 6134 10 D4
    ret                    ; 6136 C9
    ld     a,($808d)       ; 6137 3A 8D 80
    or     a               ; 613A B7
    jp     nz,$6173        ; 613B C2 73 61
    ld     e,(ix+$0e)      ; 613E DD 5E 0E
    ld     d,(ix+$0f)      ; 6141 DD 56 0F
    push   de              ; 6144 D5
    ld     bc,$0180        ; 6145 01 80 01
    ld     hl,$0010        ; 6148 21 10 00
    ex     de,hl           ; 614B EB
    or     a               ; 614C B7
    sbc    hl,bc           ; 614D ED 42
    ld     a,l             ; 614F 7D
    or     h               ; 6150 B4
    pop    hl              ; 6151 E1
    jp     nz,$6158        ; 6152 C2 58 61
    ld     de,$0008        ; 6155 11 08 00
    add    hl,de           ; 6158 19
    ld     (ix+$0e),l      ; 6159 DD 75 0E
    ld     (ix+$0f),h      ; 615C DD 74 0F
    ld     e,(ix+$0a)      ; 615F DD 5E 0A
    ld     d,(ix+$0b)      ; 6162 DD 56 0B
    add    hl,de           ; 6165 19
    ld     e,(ix+$07)      ; 6166 DD 5E 07
    ld     d,(ix+$05)      ; 6169 DD 56 05
    add    hl,de           ; 616C 19
    ld     (ix+$07),l      ; 616D DD 75 07
    ld     (ix+$05),h      ; 6170 DD 74 05
    ret                    ; 6173 C9
    ld     a,b             ; 6174 78
    sub    d               ; 6175 92
    ld     d,a             ; 6176 57
    ld     a,c             ; 6177 79
    sub    e               ; 6178 93
    ld     e,a             ; 6179 5F
    xor    a               ; 617A AF
    or     d               ; 617B B2
    jp     p,$618c         ; 617C F2 8C 61
    inc    hl              ; 617F 23
    neg                    ; 6180 ED 44
    ld     d,a             ; 6182 57
    ld     a,(hl)          ; 6183 7E
    inc    hl              ; 6184 23
    sub    d               ; 6185 92
    jp     c,$61a4         ; 6186 DA A4 61
    jp     $6191           ; 6189 C3 91 61
    ld     a,(hl)          ; 618C 7E
    inc    hl              ; 618D 23
    jp     $6184           ; 618E C3 84 61
    xor    a               ; 6191 AF
    or     e               ; 6192 B3
    jp     p,$619a         ; 6193 F2 9A 61
    inc    hl              ; 6196 23
    neg                    ; 6197 ED 44
    ld     e,a             ; 6199 5F
    ld     a,(hl)          ; 619A 7E
    sub    e               ; 619B 93
    jp     c,$61a4         ; 619C DA A4 61
    ld     a,$01           ; 619F 3E 01
    jp     $61a5           ; 61A1 C3 A5 61
    xor    a               ; 61A4 AF
    ret                    ; 61A5 C9
    ld     a,($808d)       ; 61A6 3A 8D 80
    or     a               ; 61A9 B7
    jp     nz,$6208        ; 61AA C2 08 62
    ld     a,(ix+$11)      ; 61AD DD 7E 11
    cp     $02             ; 61B0 FE 02
    jp     nc,$61c3        ; 61B2 D2 C3 61
    ld     e,(ix+$08)      ; 61B5 DD 5E 08
    ld     d,(ix+$09)      ; 61B8 DD 56 09
    ld     h,(ix+$04)      ; 61BB DD 66 04
    ld     l,(ix+$06)      ; 61BE DD 6E 06
    jr     $61d2           ; 61C1 18 0F
    ld     e,(ix+$0a)      ; 61C3 DD 5E 0A
    ld     d,(ix+$0b)      ; 61C6 DD 56 0B
    ld     h,(ix+$05)      ; 61C9 DD 66 05
    ld     l,(ix+$07)      ; 61CC DD 6E 07
    dec    a               ; 61CF 3D
    and    $01             ; 61D0 E6 01
    or     a               ; 61D2 B7
    jp     nz,$61d9        ; 61D3 C2 D9 61
    add    hl,de           ; 61D6 19
    jr     $61dc           ; 61D7 18 03
    or     a               ; 61D9 B7
    sbc    hl,de           ; 61DA ED 52
    ld     a,$01           ; 61DC 3E 01
    cp     (ix+$11)        ; 61DE DD BE 11
    jp     c,$61f7         ; 61E1 DA F7 61
    ld     a,h             ; 61E4 7C
    cp     (ix+$04)        ; 61E5 DD BE 04
    jp     z,$61ef         ; 61E8 CA EF 61
    ld     (ix+$18),$00    ; 61EB DD 36 18 00
    ld     (ix+$04),h      ; 61EF DD 74 04
    ld     (ix+$06),l      ; 61F2 DD 75 06
    jr     $6208           ; 61F5 18 11
    ld     a,h             ; 61F7 7C
    cp     (ix+$05)        ; 61F8 DD BE 05
    jp     z,$6202         ; 61FB CA 02 62
    ld     (ix+$18),$00    ; 61FE DD 36 18 00
    ld     (ix+$05),h      ; 6202 DD 74 05
    ld     (ix+$07),l      ; 6205 DD 75 07
    ret                    ; 6208 C9
    push   bc              ; 6209 C5
    push   de              ; 620A D5
    push   hl              ; 620B E5
    push   ix              ; 620C DD E5
    push   iy              ; 620E FD E5
    ld     ix,$c828        ; 6210 DD 21 28 C8	; Draw bombs ??
    ld     a,($81a0)       ; 6214 3A A0 81
    dec    a               ; 6217 3D
    ld     bc,$0006        ; 6218 01 06 00
    ld     hl,$de3b        ; 621B 21 3B DE
    call   $00f6           ; 621E CD F6 00
    ld     a,(hl)          ; 6221 7E
    add    a,a             ; 6222 87
    ld     d,$00           ; 6223 16 00
    ld     e,a             ; 6225 5F
    add    ix,de           ; 6226 DD 19
    ld     h,(ix+$01)      ; 6228 DD 66 01
    ld     l,(ix+$00)      ; 622B DD 6E 00
    push   hl              ; 622E E5
    pop    ix              ; 622F DD E1
    ld     iy,$8480        ; 6231 FD 21 80 84
    ld     c,$00           ; 6235 0E 00
    ld     a,c             ; 6237 79
    cp     $18             ; 6238 FE 18
    jp     z,$6275         ; 623A CA 75 62
    ld     (iy+$00),c      ; 623D FD 71 00
    ld     hl,$c9c8        ; 6240 21 C8 C9
    ld     d,$00           ; 6243 16 00
    ld     a,(ix+$00)      ; 6245 DD 7E 00
    srl    a               ; 6248 CB 3F
    srl    a               ; 624A CB 3F
    srl    a               ; 624C CB 3F
    srl    a               ; 624E CB 3F
    ld     e,a             ; 6250 5F
    add    hl,de           ; 6251 19
    ld     a,(hl)          ; 6252 7E
    ld     (iy+$01),a      ; 6253 FD 77 01
    ld     hl,$c9c8        ; 6256 21 C8 C9
    ld     d,$00           ; 6259 16 00
    ld     a,(ix+$00)      ; 625B DD 7E 00
    and    $0f             ; 625E E6 0F
    ld     e,a             ; 6260 5F
    add    hl,de           ; 6261 19
    ld     a,(hl)          ; 6262 7E
    ld     (iy+$02),a      ; 6263 FD 77 02
    inc    c               ; 6266 0C
    inc    ix              ; 6267 DD 23
    inc    iy              ; 6269 FD 23
    inc    iy              ; 626B FD 23
    inc    iy              ; 626D FD 23
    inc    iy              ; 626F FD 23
    inc    iy              ; 6271 FD 23
    jr     $6237           ; 6273 18 C2
    ld     (iy+$00),$ff    ; 6275 FD 36 00 FF
    pop    iy              ; 6279 FD E1
    pop    ix              ; 627B DD E1
    pop    hl              ; 627D E1
    pop    de              ; 627E D1
    pop    bc              ; 627F C1
    ret                    ; 6280 C9
    push   bc              ; 6281 C5
    push   de              ; 6282 D5
    push   hl              ; 6283 E5
    push   af              ; 6284 F5
    ld     a,($8070)       ; 6285 3A 70 80
    or     a               ; 6288 B7
    jr     nz,$62a1        ; 6289 20 16
    ld     a,($8017)       ; 628B 3A 17 80
    or     a               ; 628E B7
    jp     nz,$62a1        ; 628F C2 A1 62
    ld     a,($8013)       ; 6292 3A 13 80
    bit    7,a             ; 6295 CB 7F
    jr     z,$62b9         ; 6297 28 20
    pop    af              ; 6299 F1
    push   af              ; 629A F5
    and    $7f             ; 629B E6 7F
    cp     $20             ; 629D FE 20
    jr     nz,$62b9        ; 629F 20 18
    ld     hl,$8701        ; 62A1 21 01 87
    ld     bc,$0001        ; 62A4 01 01 00
    ld     a,($8700)       ; 62A7 3A 00 87
    call   $00f6           ; 62AA CD F6 00
    pop    af              ; 62AD F1
    ld     (hl),a          ; 62AE 77
    ld     a,($8700)       ; 62AF 3A 00 87
    inc    a               ; 62B2 3C
    ld     ($8700),a       ; 62B3 32 00 87
    jp     $62ba           ; 62B6 C3 BA 62
    pop    af              ; 62B9 F1
    pop    hl              ; 62BA E1
    pop    de              ; 62BB D1
    pop    bc              ; 62BC C1
    ret                    ; 62BD C9
    ld     a,($8700)       ; 62BE 3A 00 87
    or     a               ; 62C1 B7
    ret    z               ; 62C2 C8
    ld     hl,$8701        ; 62C3 21 01 87
    ld     a,(hl)          ; 62C6 7E
    ld     ($b800),a       ; 62C7 32 00 B8
    ld     a,($8700)       ; 62CA 3A 00 87
    dec    a               ; 62CD 3D
    ld     ($8700),a       ; 62CE 32 00 87
    ret    z               ; 62D1 C8
    ld     b,a             ; 62D2 47
    ld     hl,$8701        ; 62D3 21 01 87
    push   hl              ; 62D6 E5
    pop    ix              ; 62D7 DD E1
    inc    ix              ; 62D9 DD 23
    ld     a,(ix+$00)      ; 62DB DD 7E 00
    ld     (hl),a          ; 62DE 77
    inc    hl              ; 62DF 23
    inc    ix              ; 62E0 DD 23
    djnz   $62db           ; 62E2 10 F7
    ret                    ; 62E4 C9
    ld     a,($8070)       ; 62E5 3A 70 80
    cp     $00             ; 62E8 FE 00
    jr     z,$62f4         ; 62EA 28 08
    ld     a,($8014)       ; 62EC 3A 14 80
    and    $18             ; 62EF E6 18
    rrca                   ; 62F1 0F
    rrca                   ; 62F2 0F
    rrca                   ; 62F3 0F
    ld     hl,$6301        ; 62F4 21 01 63
    ld     d,$00           ; 62F7 16 00
    ld     e,a             ; 62F9 5F
    add    hl,de           ; 62FA 19
    ld     a,(hl)          ; 62FB 7E
    ld     ($81a8),a       ; 62FC 32 A8 81
    jr     $6305           ; 62FF 18 04
    nop                    ; 6301 00
    ld     bc,$0703        ; 6302 01 03 07
    ld     a,($8070)       ; 6305 3A 70 80
    cp     $00             ; 6308 FE 00
    jr     z,$6314         ; 630A 28 08

; reset 81A9h ?

    ld     a,($8014)       ; 630C 3A 14 80	; a = ((0x8014) & 96) * 8 ?
    and    $60             ; 630F E6 60
    rlca                   ; 6311 07
    rlca                   ; 6312 07
    rlca                   ; 6313 07
    ld     hl,$6321        ; 6314 21 21 63	; (0x81a9) = (0x6321 + a)
    ld     d,$00           ; 6317 16 00
    ld     e,a             ; 6319 5F
    add    hl,de           ; 631A 19
    ld     a,(hl)          ; 631B 7E
    ld     ($81a9),a       ; 631C 32 A9 81
    jr     $6325           ; 631F 18 04
    nop                    ; 6321 00
    nop                    ; 6322 00
    ld     bc,$c903        ; 6323 01 03 C9	; FIXME
    push   bc              ; 6326 C5
    push   de              ; 6327 D5
    push   hl              ; 6328 E5
    cp     $11             ; 6329 FE 11
    jp     nc,$6396        ; 632B D2 96 63
    ld     ($8986),a       ; 632E 32 86 89
    cp     $10             ; 6331 FE 10
    jp     z,$6391         ; 6333 CA 91 63
    cp     $08             ; 6336 FE 08
    jp     nc,$6369        ; 6338 D2 69 63
    ld     hl,$de3a        ; 633B 21 3A DE
    ld     bc,$0006        ; 633E 01 06 00
    ld     a,($81a0)       ; 6341 3A A0 81
    dec    a               ; 6344 3D
    call   $00f6           ; 6345 CD F6 00
    ld     a,(hl)          ; 6348 7E
    push   af              ; 6349 F5
    ld     hl,$d700        ; 634A 21 00 D7
    ld     bc,$0010        ; 634D 01 10 00
    call   $00f6           ; 6350 CD F6 00
    ld     de,$8ca0        ; 6353 11 A0 8C
    ldir                   ; 6356 ED B0
    pop    af              ; 6358 F1
    ld     hl,$d780        ; 6359 21 80 D7
    ld     bc,$0010        ; 635C 01 10 00
    call   $00f6           ; 635F CD F6 00
    ld     de,$8c80        ; 6362 11 80 8C
    ldir                   ; 6365 ED B0
    jr     $6391           ; 6367 18 28
    sub    $08             ; 6369 D6 08
    add    a,a             ; 636B 87
    ld     hl,$6381        ; 636C 21 81 63
    ld     d,$00           ; 636F 16 00
    ld     e,a             ; 6371 5F
    add    hl,de           ; 6372 19
    ld     e,(hl)          ; 6373 5E
    inc    hl              ; 6374 23
    ld     d,(hl)          ; 6375 56
    ex     de,hl           ; 6376 EB
    ld     de,$8c70        ; 6377 11 70 8C
    ld     bc,$0010        ; 637A 01 10 00
    ldir                   ; 637D ED B0
    jr     $6391           ; 637F 18 10
    nop                    ; 6381 00
    ret    c               ; 6382 D8
    djnz   $635d           ; 6383 10 D8
    jr     nz,$635f        ; 6385 20 D8
    jr     nc,$6361        ; 6387 30 D8
    ld     b,b             ; 6389 40
    ret    c               ; 638A D8
    ld     d,b             ; 638B 50
    ret    c               ; 638C D8
    ld     h,b             ; 638D 60
    ret    c               ; 638E D8
    ld     (hl),b          ; 638F 70
    ret    c               ; 6390 D8
    ld     a,$01           ; 6391 3E 01
    ld     ($8985),a       ; 6393 32 85 89
    pop    hl              ; 6396 E1
    pop    de              ; 6397 D1
    pop    bc              ; 6398 C1
    ret                    ; 6399 C9
    ld     a,($8985)       ; 639A 3A 85 89
    or     a               ; 639D B7
    ret    z               ; 639E C8
    ld     a,($8986)       ; 639F 3A 86 89
    cp     $10             ; 63A2 FE 10
    jp     z,$63b4         ; 63A4 CA B4 63
    push   af              ; 63A7 F5
    ld     hl,$8c70        ; 63A8 21 70 8C
    ld     de,$9c70        ; 63AB 11 70 9C
    ld     bc,$0040        ; 63AE 01 40 00
    ldir                   ; 63B1 ED B0
    pop    af              ; 63B3 F1
    set    4,a             ; 63B4 CB E7
    ld     ($9e00),a       ; 63B6 32 00 9E
    ld     a,$00           ; 63B9 3E 00
    ld     ($8985),a       ; 63BB 32 85 89
    ret                    ; 63BE C9
    push   af              ; 63BF F5
    push   hl              ; 63C0 E5
    push   ix              ; 63C1 DD E5
    push   de              ; 63C3 D5
    ld     a,($819e)       ; 63C4 3A 9E 81
    and    $0f             ; 63C7 E6 0F
    ld     e,a             ; 63C9 5F
    push   af              ; 63CA F5
    ld     a,($81ac)       ; 63CB 3A AC 81
    ld     d,a             ; 63CE 57
    push   bc              ; 63CF C5
    or     a               ; 63D0 B7
    srl    b               ; 63D1 CB 38
    rr     c               ; 63D3 CB 19
    or     a               ; 63D5 B7
    srl    b               ; 63D6 CB 38
    rr     c               ; 63D8 CB 19
    or     a               ; 63DA B7
    srl    b               ; 63DB CB 38
    rr     c               ; 63DD CB 19
    or     a               ; 63DF B7
    srl    b               ; 63E0 CB 38
    rr     c               ; 63E2 CB 19
    or     a               ; 63E4 B7
    ld     hl,($81b1)      ; 63E5 2A B1 81
    ld     a,l             ; 63E8 7D
    add    a,c             ; 63E9 81
    daa                    ; 63EA 27
    ld     l,a             ; 63EB 6F
    ld     a,h             ; 63EC 7C
    adc    a,b             ; 63ED 88
    daa                    ; 63EE 27
    ld     h,a             ; 63EF 67
    ld     ($81b1),hl      ; 63F0 22 B1 81
    pop    bc              ; 63F3 C1
    ld     hl,$819c        ; 63F4 21 9C 81
    ld     a,(hl)          ; 63F7 7E
    add    a,c             ; 63F8 81
    daa                    ; 63F9 27
    ld     (hl),a          ; 63FA 77
    inc    hl              ; 63FB 23
    ld     a,(hl)          ; 63FC 7E
    adc    a,b             ; 63FD 88
    daa                    ; 63FE 27
    ld     (hl),a          ; 63FF 77
    inc    hl              ; 6400 23
    ld     a,(hl)          ; 6401 7E
    adc    a,$00           ; 6402 CE 00
    daa                    ; 6404 27
    ld     (hl),a          ; 6405 77
    inc    hl              ; 6406 23
    ld     a,(hl)          ; 6407 7E
    adc    a,$00           ; 6408 CE 00
    daa                    ; 640A 27
    ld     (hl),a          ; 640B 77
    ld     a,($81ae)       ; 640C 3A AE 81
    cp     $ff             ; 640F FE FF
    jr     z,$6423         ; 6411 28 10
    ld     a,($819e)       ; 6413 3A 9E 81
    and    $0f             ; 6416 E6 0F
    cp     e               ; 6418 BB
    jr     z,$6423         ; 6419 28 08
    ld     e,a             ; 641B 5F
    ld     a,($81ad)       ; 641C 3A AD 81
    inc    a               ; 641F 3C
    ld     ($81ad),a       ; 6420 32 AD 81
    ld     a,d             ; 6423 7A
    cp     $00             ; 6424 FE 00
    jr     z,$642c         ; 6426 28 04
    dec    d               ; 6428 15
    jp     $63cf           ; 6429 C3 CF 63
    ld     a,($8027)       ; 642C 3A 27 80
    or     a               ; 642F B7
    jp     z,$643a         ; 6430 CA 3A 64
    ld     ix,$ca61        ; 6433 DD 21 61 CA
    jp     $643e           ; 6437 C3 3E 64
    ld     ix,$ca4f        ; 643A DD 21 4F CA
    ld     a,($8070)       ; 643E 3A 70 80
    or     a               ; 6441 B7
    jp     z,$6448         ; 6442 CA 48 64
    call   $00d5           ; 6445 CD D5 00
    pop    af              ; 6448 F1
    ld     a,($81b2)       ; 6449 3A B2 81
    cp     $05             ; 644C FE 05
    jr     c,$6470         ; 644E 38 20
    sub    $05             ; 6450 D6 05
    daa                    ; 6452 27
    cp     $05             ; 6453 FE 05
    jr     nc,$6450        ; 6455 30 F9
    ld     ($81b2),a       ; 6457 32 B2 81
    ld     a,($8088)       ; 645A 3A 88 80
    cp     $00             ; 645D FE 00
    jr     nz,$6470        ; 645F 20 0F
    ld     a,($81ac)       ; 6461 3A AC 81
    cp     $04             ; 6464 FE 04
    jr     nc,$6470        ; 6466 30 08
    ld     a,($8900)       ; 6468 3A 00 89
    set    0,a             ; 646B CB C7
    ld     ($8900),a       ; 646D 32 00 89
    call   $5fdd           ; 6470 CD DD 5F
    pop    de              ; 6473 D1
    pop    ix              ; 6474 DD E1
    pop    hl              ; 6476 E1
    pop    af              ; 6477 F1
    ret                    ; 6478 C9
    call   $6524           ; 6479 CD 24 65
    call   $64d8           ; 647C CD D8 64
    ld     a,($8013)       ; 647F 3A 13 80
    and    $03             ; 6482 E6 03
    cp     $00             ; 6484 FE 00
    jr     nz,$648e        ; 6486 20 06
    ld     ix,$662b        ; 6488 DD 21 2B 66
    jr     $64a6           ; 648C 18 18
    cp     $01             ; 648E FE 01
    jr     nz,$6498        ; 6490 20 06
    ld     ix,$664b        ; 6492 DD 21 4B 66
    jr     $64a6           ; 6496 18 0E
    cp     $02             ; 6498 FE 02
    jr     nz,$64a2        ; 649A 20 06
    ld     ix,$666b        ; 649C DD 21 6B 66
    jr     $64a6           ; 64A0 18 04
    ld     ix,$668b        ; 64A2 DD 21 8B 66
    call   $00d2           ; 64A6 CD D2 00
    ld     ix,$66ab        ; 64A9 DD 21 AB 66
    call   $00d2           ; 64AD CD D2 00
    ld     a,$20           ; 64B0 3E 20
    call   $00fc           ; 64B2 CD FC 00
    ld     bc,$0000        ; 64B5 01 00 00
    ld     a,$01           ; 64B8 3E 01
    ld     ($806e),a       ; 64BA 32 6E 80
    ld     a,$02           ; 64BD 3E 02
    ld     ($808c),a       ; 64BF 32 8C 80
    ld     a,($806e)       ; 64C2 3A 6E 80
    cp     $00             ; 64C5 FE 00
    jr     nz,$64c2        ; 64C7 20 F9
    inc    bc              ; 64C9 03
    ld     a,b             ; 64CA 78
    cp     $02             ; 64CB FE 02
    jr     nz,$64b8        ; 64CD 20 E9
    ld     a,c             ; 64CF 79
    cp     $58             ; 64D0 FE 58
    jr     nz,$64b8        ; 64D2 20 E4
    call   $6569           ; 64D4 CD 69 65
    ret                    ; 64D7 C9
    jp     $6a00           ; 64D8 C3 00 6A
    push   ix              ; 64DB DD E5
    ld     ix,$8500        ; 64DD DD 21 00 85
    ld     b,$a8           ; 64E1 06 A8
    ld     c,$06           ; 64E3 0E 06
    ld     h,$21           ; 64E5 26 21
    ld     a,c             ; 64E7 79
    cp     $00             ; 64E8 FE 00
    jr     z,$651e         ; 64EA 28 32
    cp     $04             ; 64EC FE 04
    jr     nz,$64f2        ; 64EE 20 02
    ld     b,$b4           ; 64F0 06 B4
    ld     (ix+$00),b      ; 64F2 DD 70 00
    ld     (ix+$01),$24    ; 64F5 DD 36 01 24
    ld     (ix+$02),h      ; 64F9 DD 74 02
    ld     (ix+$03),$20    ; 64FC DD 36 03 20
    inc    b               ; 6500 04
    ld     de,$0008        ; 6501 11 08 00
    add    ix,de           ; 6504 DD 19
    ld     (ix+$00),b      ; 6506 DD 70 00
    ld     (ix+$01),$24    ; 6509 DD 36 01 24
    ld     (ix+$02),h      ; 650D DD 74 02
    ld     (ix+$03),$40    ; 6510 DD 36 03 40
    add    ix,de           ; 6514 DD 19
    inc    b               ; 6516 04
    dec    c               ; 6517 0D
    ld     a,$20           ; 6518 3E 20
    add    a,h             ; 651A 84
    ld     h,a             ; 651B 67
    jr     $64e7           ; 651C 18 C9
    pop    ix              ; 651E DD E1
    pop    hl              ; 6520 E1
    pop    de              ; 6521 D1
    pop    bc              ; 6522 C1
    ret                    ; 6523 C9
    push   bc              ; 6524 C5
    push   de              ; 6525 D5
    push   hl              ; 6526 E5
    ld     a,($8864)       ; 6527 3A 64 88
    or     $80             ; 652A F6 80
    ld     ($8864),a       ; 652C 32 64 88
    ld     hl,$0f1a        ; 652F 21 1A 0F
    ld     de,$8800        ; 6532 11 00 88
    ld     bc,$000a        ; 6535 01 0A 00
    ldir                   ; 6538 ED B0
    ld     hl,$0fff        ; 653A 21 FF 0F
    ld     ($8c42),hl      ; 653D 22 42 8C
    ld     hl,$000f        ; 6540 21 0F 00
    ld     ($8c44),hl      ; 6543 22 44 8C
    ld     hl,$00af        ; 6546 21 AF 00
    ld     ($8c46),hl      ; 6549 22 46 8C
    ld     hl,$080f        ; 654C 21 0F 08
    ld     ($8c48),hl      ; 654F 22 48 8C
    ld     hl,$0fad        ; 6552 21 AD 0F
    ld     de,$8c4a        ; 6555 11 4A 8C
    ld     bc,$0006        ; 6558 01 06 00
    ldir                   ; 655B ED B0
    ld     a,($8800)       ; 655D 3A 00 88
    and    $7f             ; 6560 E6 7F
    ld     ($8800),a       ; 6562 32 00 88
    pop    hl              ; 6565 E1
    pop    de              ; 6566 D1
    pop    bc              ; 6567 C1
    ret                    ; 6568 C9
    push   bc              ; 6569 C5
    push   de              ; 656A D5
    push   hl              ; 656B E5
    ld     a,($8800)       ; 656C 3A 00 88
    or     $80             ; 656F F6 80
    ld     ($8800),a       ; 6571 32 00 88
    ld     de,$8c42        ; 6574 11 42 8C
    ld     hl,$d542        ; 6577 21 42 D5
    ld     bc,$000e        ; 657A 01 0E 00
    ldir                   ; 657D ED B0
    ld     a,($8864)       ; 657F 3A 64 88
    and    $7f             ; 6582 E6 7F
    ld     ($8864),a       ; 6584 32 64 88
    ld     a,$00           ; 6587 3E 00
    call   $4ff6           ; 6589 CD F6 4F
    pop    hl              ; 658C E1
    pop    de              ; 658D D1
    pop    bc              ; 658E C1
    ret                    ; 658F C9
    push   af              ; 6590 F5
    push   bc              ; 6591 C5
    push   de              ; 6592 D5
    push   hl              ; 6593 E5
    push   ix              ; 6594 DD E5
    push   iy              ; 6596 FD E5
    bit    6,(ix+$00)      ; 6598 DD CB 00 76
    jp     z,$6622         ; 659C CA 22 66
    ld     a,(ix+$03)      ; 659F DD 7E 03
    cp     $00             ; 65A2 FE 00
    jp     nz,$6622        ; 65A4 C2 22 66
    ld     a,(ix+$04)      ; 65A7 DD 7E 04
    cp     $00             ; 65AA FE 00
    jp     z,$6622         ; 65AC CA 22 66
    cp     $01             ; 65AF FE 01
    jr     nz,$65f8        ; 65B1 20 45
    ld     b,(ix+$01)      ; 65B3 DD 46 01
    ld     c,(ix+$02)      ; 65B6 DD 4E 02
    call   $00de           ; 65B9 CD DE 00
    push   hl              ; 65BC E5
    pop    iy              ; 65BD FD E1
    ld     a,($81ac)       ; 65BF 3A AC 81
    ld     h,$00           ; 65C2 26 00
    ld     l,a             ; 65C4 6F
    add    hl,hl           ; 65C5 29
    add    hl,hl           ; 65C6 29
    add    hl,hl           ; 65C7 29
    ld     de,$66d3        ; 65C8 11 D3 66
    add    hl,de           ; 65CB 19
    ld     a,(hl)          ; 65CC 7E
    ld     (iy-$20),a      ; 65CD FD 77 E0
    inc    hl              ; 65D0 23
    ld     a,(hl)          ; 65D1 7E
    ld     (iy-$1f),a      ; 65D2 FD 77 E1
    inc    hl              ; 65D5 23
    ld     a,(hl)          ; 65D6 7E
    ld     (iy+$00),a      ; 65D7 FD 77 00
    inc    hl              ; 65DA 23
    ld     a,(hl)          ; 65DB 7E
    ld     (iy+$01),a      ; 65DC FD 77 01
    ld     de,$0400        ; 65DF 11 00 04
    add    iy,de           ; 65E2 FD 19
    inc    hl              ; 65E4 23
    ld     a,(hl)          ; 65E5 7E
    ld     (iy-$20),a      ; 65E6 FD 77 E0
    inc    hl              ; 65E9 23
    ld     a,(hl)          ; 65EA 7E
    ld     (iy-$1f),a      ; 65EB FD 77 E1
    inc    hl              ; 65EE 23
    ld     a,(hl)          ; 65EF 7E
    ld     (iy+$00),a      ; 65F0 FD 77 00
    inc    hl              ; 65F3 23
    ld     a,(hl)          ; 65F4 7E
    ld     (iy+$01),a      ; 65F5 FD 77 01
    inc    (ix+$04)        ; 65F8 DD 34 04
    ld     a,(ix+$04)      ; 65FB DD 7E 04
    cp     $1f             ; 65FE FE 1F
    jr     c,$6622         ; 6600 38 20
    ld     (ix+$04),$00    ; 6602 DD 36 04 00
    ld     b,(ix+$01)      ; 6606 DD 46 01
    ld     c,(ix+$02)      ; 6609 DD 4E 02
    call   $00de           ; 660C CD DE 00
    push   hl              ; 660F E5
    pop    iy              ; 6610 FD E1
    ld     (iy+$00),$00    ; 6612 FD 36 00 00
    ld     (iy+$01),$00    ; 6616 FD 36 01 00
    ld     (iy-$1f),$00    ; 661A FD 36 E1 00
    ld     (iy-$20),$00    ; 661E FD 36 E0 00
    pop    iy              ; 6622 FD E1
    pop    ix              ; 6624 DD E1
    pop    hl              ; 6626 E1
    pop    de              ; 6627 D1
    pop    bc              ; 6628 C1
    pop    af              ; 6629 F1
    ret                    ; 662A C9

; 662Bh - "1COIN 1CREDIT"

    hex    13 09 31 05 43 00 4F 00 49 00 4E 00 02 FF 00 00 ; 662B
    hex    31 05 43 00 52 00 45 00 44 00 49 00 54 00 FF FF ; 663B

; 664Bh - "1COIN 2CREDIT"

    hex    13 09 31 05 43 00 4F 00 49 00 4E 00 02 FF 00 00 ; 664B
    hex    32 05 43 00 52 00 45 00 44 00 49 00 54 00 FF FF ; 665B

; 666Bh - "1COIN 3CREDIT"

    hex    13 09 31 05 43 00 4F 00 49 00 4E 00 02 FF 00 00 ; 666B
    hex    33 05 43 00 52 00 45 00 44 00 49 00 54 00 FF FF ; 667B

; 668Bh - "1COIN 6CREDIT"

    hex    13 09 31 05 43 00 4F 00 49 00 4E 00 02 FF 00 00 ; 668B
    hex    36 05 43 00 52 00 45 00 44 00 49 00 54 00 FF FF ; 669B

; 66ABh - "(c)(p)1984 TEHKAN LTD."

    hex    1C 07 3A 05 3B 05 31 05 39 05 38 05 34 05 00 00 ; 66AB
    hex    54 0F 45 0F 48 0F 4B 0F 41 0F 4E 0F 00 00 4C 05 ; 66BB
    hex    54 05 44 05 2E 05 FF FF                         ; 66CB

    add    a,b             ; 66D3 80
    add    a,c             ; 66D4 81
    add    a,d             ; 66D5 82
    add    a,e             ; 66D6 83
    dec    d               ; 66D7 15
    dec    d               ; 66D8 15
    dec    d               ; 66D9 15
    dec    d               ; 66DA 15
    sub    b               ; 66DB 90
    sub    c               ; 66DC 91
    sub    d               ; 66DD 92
    sub    e               ; 66DE 93
    dec    d               ; 66DF 15
    dec    d               ; 66E0 15
    dec    d               ; 66E1 15
    dec    d               ; 66E2 15
    sub    h               ; 66E3 94
    sub    l               ; 66E4 95
    sub    (hl)            ; 66E5 96
    sub    a               ; 66E6 97
    dec    d               ; 66E7 15
    dec    d               ; 66E8 15
    dec    d               ; 66E9 15
    dec    d               ; 66EA 15
    sbc    a,b             ; 66EB 98
    sbc    a,c             ; 66EC 99
    sbc    a,d             ; 66ED 9A
    sbc    a,e             ; 66EE 9B
    dec    d               ; 66EF 15
    dec    d               ; 66F0 15
    dec    d               ; 66F1 15
    dec    d               ; 66F2 15
    sbc    a,h             ; 66F3 9C
    sbc    a,l             ; 66F4 9D
    sbc    a,(hl)          ; 66F5 9E
    sbc    a,a             ; 66F6 9F
    dec    d               ; 66F7 15
    dec    d               ; 66F8 15
    dec    d               ; 66F9 15
    dec    d               ; 66FA 15
    push   af              ; 66FB F5
    push   hl              ; 66FC E5
    ld     hl,$0000        ; 66FD 21 00 00
    ld     ($809f),hl      ; 6700 22 9F 80
    ld     ($80a1),hl      ; 6703 22 A1 80
    ld     ($80a3),hl      ; 6706 22 A3 80
    ld     a,($676d)       ; 6709 3A 6D 67
    ld     ($8500),a       ; 670C 32 00 85
    ld     a,($676e)       ; 670F 3A 6E 67
    ld     ($8501),a       ; 6712 32 01 85
    pop    hl              ; 6715 E1
    pop    af              ; 6716 F1
    ret                    ; 6717 C9
    push   af              ; 6718 F5
    push   bc              ; 6719 C5
    push   de              ; 671A D5
    push   hl              ; 671B E5
    push   ix              ; 671C DD E5
    push   iy              ; 671E FD E5
    ld     ix,$809f        ; 6720 DD 21 9F 80
    ld     a,(ix+$00)      ; 6724 DD 7E 00
    cp     $0e             ; 6727 FE 0E
    jr     c,$6761         ; 6729 38 36
    ld     iy,$676d        ; 672B FD 21 6D 67
    ld     de,($80a0)      ; 672F ED 5B A0 80
    or     a               ; 6733 B7
    add    iy,de           ; 6734 FD 19
    ld     a,(iy+$00)      ; 6736 FD 7E 00
    cp     $ff             ; 6739 FE FF
    jr     nz,$6749        ; 673B 20 0C
    ld     iy,$676d        ; 673D FD 21 6D 67
    ld     (ix+$01),$00    ; 6741 DD 36 01 00
    ld     (ix+$02),$00    ; 6745 DD 36 02 00
    ld     a,(iy+$00)      ; 6749 FD 7E 00
    ld     ($8500),a       ; 674C 32 00 85
    ld     a,(iy+$01)      ; 674F FD 7E 01
    ld     ($8501),a       ; 6752 32 01 85
    ld     (ix+$00),$00    ; 6755 DD 36 00 00
    ld     hl,($80a0)      ; 6759 2A A0 80
    inc    hl              ; 675C 23
    inc    hl              ; 675D 23
    ld     ($80a0),hl      ; 675E 22 A0 80
    inc    (ix+$00)        ; 6761 DD 34 00
    pop    iy              ; 6764 FD E1
    pop    ix              ; 6766 DD E1
    pop    hl              ; 6768 E1
    pop    de              ; 6769 D1
    pop    bc              ; 676A C1
    pop    af              ; 676B F1
    ret                    ; 676C C9
    ld     sp,$3200        ; 676D 31 00 32
    nop                    ; 6770 00
    ld     sp,$3200        ; 6771 31 00 32
    add    a,b             ; 6774 80
    ld     sp,$3300        ; 6775 31 00 33
    nop                    ; 6778 00
    ld     sp,$3300        ; 6779 31 00 33
    nop                    ; 677C 00
    rst    38h             ; 677D FF
    rst    38h             ; 677E FF

    rst    38h             ; 677F FF
    rst    38h             ; 6780 FF
    ld     ix,$8510        ; 6781 DD 21 10 85
    ld     a,($8902)       ; 6785 3A 02 89
    cp     $ff             ; 6788 FE FF
    jr     z,$67ca         ; 678A 28 3E
    ld     a,$ff           ; 678C 3E FF
    ld     ($8902),a       ; 678E 32 02 89
    ld     (ix+$00),$18    ; 6791 DD 36 00 18
    ld     (ix+$01),$0b    ; 6795 DD 36 01 0B
    ld     (ix+$04),$00    ; 6799 DD 36 04 00
    ld     (ix+$05),$00    ; 679D DD 36 05 00
    ld     (ix+$06),$00    ; 67A1 DD 36 06 00
    ld     (ix+$07),$00    ; 67A5 DD 36 07 00
    ld     a,($819b)       ; 67A9 3A 9B 81
    inc    a               ; 67AC 3C
    ld     ($819b),a       ; 67AD 32 9B 81
    cp     $08             ; 67B0 FE 08
    jr     c,$67b6         ; 67B2 38 02
    ld     a,$07           ; 67B4 3E 07
    ld     c,a             ; 67B6 4F
    dec    c               ; 67B7 0D
    ld     b,$10           ; 67B8 06 10
    ld     a,c             ; 67BA 79
    cp     $00             ; 67BB FE 00
    jr     z,$67c6         ; 67BD 28 07
    ld     a,b             ; 67BF 78
    add    a,$10           ; 67C0 C6 10
    ld     b,a             ; 67C2 47
    dec    c               ; 67C3 0D
    jr     $67ba           ; 67C4 18 F4
    ld     a,b             ; 67C6 78
    ld     ($8903),a       ; 67C7 32 03 89
    ld     a,($8513)       ; 67CA 3A 13 85
    cp     $f0             ; 67CD FE F0
    jp     z,$67fe         ; 67CF CA FE 67
    ld     a,($8512)       ; 67D2 3A 12 85
    cp     $78             ; 67D5 FE 78
    jr     z,$67ef         ; 67D7 28 16
    jr     nc,$67e3        ; 67D9 30 08
    inc    a               ; 67DB 3C
    cp     $78             ; 67DC FE 78
    jr     z,$67e9         ; 67DE 28 09
    inc    a               ; 67E0 3C
    jr     $67e9           ; 67E1 18 06
    dec    a               ; 67E3 3D
    cp     $78             ; 67E4 FE 78
    jr     z,$67e9         ; 67E6 28 01
    dec    a               ; 67E8 3D
    ld     ($8512),a       ; 67E9 32 12 85
    jp     $687a           ; 67EC C3 7A 68
    ld     a,($8513)       ; 67EF 3A 13 85
    inc    a               ; 67F2 3C
    cp     $f0             ; 67F3 FE F0
    jr     z,$67f8         ; 67F5 28 01
    inc    a               ; 67F7 3C
    ld     ($8513),a       ; 67F8 32 13 85
    jp     $687a           ; 67FB C3 7A 68
    ld     a,($8904)       ; 67FE 3A 04 89
    cp     $01             ; 6801 FE 01
    jr     z,$6829         ; 6803 28 24
    ld     a,($8903)       ; 6805 3A 03 89
    ld     b,a             ; 6808 47
    ld     a,($8512)       ; 6809 3A 12 85
    cp     b               ; 680C B8
    jr     z,$6817         ; 680D 28 08
    dec    a               ; 680F 3D
    cp     b               ; 6810 B8
    jr     z,$6817         ; 6811 28 04
    dec    a               ; 6813 3D
    cp     b               ; 6814 B8
    jr     nz,$6823        ; 6815 20 0C
    ld     b,a             ; 6817 47
    ld     a,$01           ; 6818 3E 01
    ld     ($8904),a       ; 681A 32 04 89
    ld     a,$30           ; 681D 3E 30
    ld     ($8905),a       ; 681F 32 05 89
    ld     a,b             ; 6822 78
    ld     ($8512),a       ; 6823 32 12 85
    jp     $687a           ; 6826 C3 7A 68
    ld     a,($8905)       ; 6829 3A 05 89
    dec    a               ; 682C 3D
    ld     ($8905),a       ; 682D 32 05 89
    cp     $00             ; 6830 FE 00
    jp     nz,$687a        ; 6832 C2 7A 68
    ld     a,$1f           ; 6835 3E 1F
    call   $00fc           ; 6837 CD FC 00
    ld     hl,$0000        ; 683A 21 00 00
    ld     ($8510),hl      ; 683D 22 10 85
    ld     b,(ix+$02)      ; 6840 DD 46 02
    ld     c,(ix+$03)      ; 6843 DD 4E 03
    call   $00de           ; 6846 CD DE 00
    push   hl              ; 6849 E5
    pop    iy              ; 684A FD E1
    ld     (iy+$00),$0e    ; 684C FD 36 00 0E
    ld     (iy+$01),$0f    ; 6850 FD 36 01 0F
    ld     (iy-$20),$0c    ; 6854 FD 36 E0 0C
    ld     (iy-$1f),$0d    ; 6858 FD 36 E1 0D
    ld     de,$0400        ; 685C 11 00 04
    or     a               ; 685F B7
    add    iy,de           ; 6860 FD 19
    ld     (iy+$00),$00    ; 6862 FD 36 00 00
    ld     (iy+$01),$00    ; 6866 FD 36 01 00
    ld     (iy-$20),$00    ; 686A FD 36 E0 00
    ld     (iy-$1f),$00    ; 686E FD 36 E1 00
    ld     a,$00           ; 6872 3E 00
    ld     ($8902),a       ; 6874 32 02 89
    ld     ($8088),a       ; 6877 32 88 80
    ret                    ; 687A C9

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 687B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 688B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 689B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 68AB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 68BB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 68CB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 68DB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 68EB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 68FB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 690B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 691B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 692B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 693B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 694B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 695B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 696B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 697B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 698B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 699B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 69AB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 69BB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 69CB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 69DB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 69EB
    hex    00 00 00 00 00                                  ; 69FB

    push   bc              ; 6A00 C5
    push   de              ; 6A01 D5
    push   hl              ; 6A02 E5
    call   $00cf           ; 6A03 CD CF 00
    djnz   $6a72           ; 6A06 10 6A
    ld     (bc),a          ; 6A08 02
    jp     $64db           ; 6A09 C3 DB 64
    nop                    ; 6A0C 00
    nop                    ; 6A0D 00
    nop                    ; 6A0E 00
    nop                    ; 6A0F 00

; 6A10h - address list

    word   $6A20           ; 6A10 20 6A
    word   $6A60           ; 6A12 60 6A

    hex    00 00 00 00 00 00 00 00 00 00 00 00             ; 6A14

    hex    0C 04 22 14 20 14 26 14 24 14 2A 14 28 14 2E 14 ; 6A20
    hex    2C 14 32 14 30 14 36 14 34 14 3A 14 38 14 3E 14 ; 6A30
    hex    3C 14 42 14 40 14 46 14 44 14 4A 14 48 14 4E 14 ; 6A40
    hex    4C 14 FF FF                                     ; 6A50

    hex    00 00 00 00 00 00 00 00 00 00 00 00             ; 6A54

    hex    0D 04 23 14 21 14 27 14 25 14 2B 14 29 14 2F 14 ; 6A60
    hex    2D 14 33 14 31 14 37 14 35 14 3B 14 39 14 3F 14 ; 6A70
    hex    3D 14 43 14 41 14 47 14 45 14 4B 14 49 14 4F 14 ; 6A80
    hex    4D 14 FF FF                                     ; 6A90

; 6A94h - zeros

; 6D00h - ?

    hex    18 08 06 10 1E 00 02 05 05 15 02 11 03 01 04 11 ; 6D00
    hex    05 01 05 11 04 05 03 15 0A 05 04 15 04 05 05 15 ; 6D10
    hex    06 05 03 01 42 09 01 08 01 00 45 06 02 16 3A 02 ; 6D20
    hex    02 16 28 02 02 15 27 05 15 00 02 15 70 05 02 16 ; 6D30
    hex    33 06 02 10 0E 0A 30 00 02 10 05 00 02 10 10 00 ; 6D40
    hex    02 10 05 00 02 10 60 00 0A 01 0A 02 02 16 21 06 ; 6D50
    hex    02 10 10 01 2D 0A 40 09 02 16 10 06 02 16 02 06 ; 6D60
    hex    02 16 35 06 70 00 02 14 42 04 30 05 25 00 53 09 ; 6D70
    hex    53 0A 0A 00 98 00 30 02 02 16 20 06 05 05 10 06 ; 6D80
    hex    1C 05 02 15 24 08 02 10 05 00 02 10 10 00 02 10 ; 6D90
    hex    05 00 02 10 10 00 02 10 05 00 02 10 10 00 05 01 ; 6DA0
    hex    02 11 20 01 02 11 40 01 42 08 27 02 02 16 35 06 ; 6DB0
    hex    30 08 60 01 02 10 20 04 10 06 10 05 18 04 02 16 ; 6DC0
    hex    30 06 02 15 30 05 FF FF                         ; 6DD0

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 6DD8
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 6DE8
    hex    00 00 00 00 00 00 00 00                         ; 6DF8

    hex    01 15 02 05 02 15 02 05 02 15 02 05 02 15 06 05 ; 6E00
    hex    1E 05 0E 02 02 10 1E 00 03 05 03 15 03 05 10 05 ; 6E10
    hex    01 00 10 16 10 06 10 16 05 06 05 16 05 06 05 16 ; 6E20
    hex    30 05 10 00 03 10 28 00 08 02 05 16 05 06 05 16 ; 6E30
    hex    05 02 05 16 02 06 02 16 02 06 02 16 02 06 02 16 ; 6E40
    hex    05 06 05 16 05 06 02 16 02 06 02 16 02 06 02 16 ; 6E50
    hex    02 06 02 16 02 06 02 16 05 06 05 16 08 00 02 15 ; 6E60
    hex    02 05 02 15 02 05 02 15 02 05 02 15 02 15 02 05 ; 6E70
    hex    80 05 50 00 45 02 40 00 02 10 53 00 05 15 05 05 ; 6E80
    hex    02 15 05 06 02 16 02 06 03 16 03 06 03 16 03 06 ; 6E90
    hex    03 16 03 06 03 16 03 06 03 16 18 06 32 08 01 00 ; 6EA0
    hex    02 10 2A 04 02 15 35 05 1C 00 05 16 05 06 05 16 ; 6EB0
    hex    05 02 03 12 10 00 03 02 02 05 02 05 02 05 18 01 ; 6EC0
    hex    50 00 02 10 13 00 02 15 05 00 3D 09 60 00 30 01 ; 6ED0
    hex    FF FF                                           ; 6EE0

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 6EE2
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00       ; 6EF2

    hex    3A 06 02 15 02 05 02 15 02 05 02 15 02 05 02 15 ; 6F00
    hex    02 05 02 15 02 05 02 15 02 05 02 15 02 05 02 15 ; 6F10
    hex    02 05 02 15 02 05 40 01 30 00 40 02 02 10 10 04 ; 6F20
    hex    18 05 02 15 20 01 10 02 02 10 20 04 02 10 05 05 ; 6F30
    hex    05 15 05 05 05 15 10 05 20 02 20 08 02 10 20 04 ; 6F40
    hex    02 10 10 01 20 00 02 10 02 00 02 10 05 00 02 10 ; 6F50
    hex    02 00 02 10 05 00 02 10 02 00 02 10 05 00 02 10 ; 6F60
    hex    02 00 02 10 05 00 15 0A 15 08 18 01 44 00 0C 01 ; 6F70
    hex    28 02 22 0A 10 08 02 14 16 04 13 06 02 12 10 02 ; 6F80
    hex    10 01 02 00 10 01 0E 00 02 14 02 06 02 16 02 06 ; 6F90
    hex    02 16 02 06 02 16 12 06 40 00 44 01 10 00 02 10 ; 6FA0
    hex    40 04 02 16 0A 06 33 08 02 10 20 04 1E 05 02 10 ; 6FB0
    hex    05 06 05 16 05 06 05 16 05 06 05 16 05 06 05 16 ; 6FC0
    hex    05 06 05 16 05 06 05 16 10 06 08 00 05 15 05 05 ; 6FD0
    hex    05 15 05 05 05 15 05 05 10 08 10 0A 10 00 10 01 ; 6FE0
    hex    FF FF                                           ; 6FF0

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00       ; 6FF2

; 7000h - ?

    hex    08 08 08 08 08 08 08 08 08 08 08 08 08 08       ; 7000

; 700Eh - list of addresses
;
; Probable sprite stuff.

    word   $702C           ; 700E 2C 70
    word   $7058           ; 7010 58 70
    word   $7084           ; 7012 84 70
    word   $7094           ; 7014 94 70
    word   $70C0           ; 7016 C0 70
    word   $70D0           ; 7018 D0 70
    word   $70C0           ; 701A C0 70
    word   $70F8           ; 701C F8 70
    word   $7108           ; 701E 08 71
    word   $7118           ; 7020 18 71
    word   $7144           ; 7022 44 71
    word   $7154           ; 7024 54 71
    word   $716C           ; 7026 6C 71
    word   $717C           ; 7028 7C 71
    word   $71A8           ; 702A A8 71

    hex    09 06 12 0B 10 0B 12 1B 10 1B 12 1B 10 1B 12 1B ; 702C
    hex    10 1B 12 1B 10 1B 12 1B 10 1B 12 1B 10 1B 12 1B ; 703C
    hex    10 1B 12 1B 10 1B 1E 0B 1C 0B FF FF             ; 704C

    hex    0A 06 13 0B 11 0B 13 1B 11 1B 13 1B 11 1B 13 1B ; 7058
    hex    11 1B 13 1B 11 1B 13 1B 11 1B 13 1B 11 1B 13 1B ; 7068
    hex    11 1B 13 1B 11 1B 1F 0B 1D 0B FF FF             ; 7078

    hex    0B 06 16 1B 14 1B 10 FF 00 00 1E 1B 1C 1B FF FF ; 7084

    hex    0C 06 17 1B 15 1B 00 00 59 00 4F 00 55 00 3D 00 ; 7094
    hex    56 00 45 00 00 00 47 00 4F 00 54 00 54 00 45 00 ; 70A4
    hex    4E 00 00 00 00 00 1F 1B 1D 1B FF FF             ; 70B4

    hex    0D 06 16 1B 14 1B 10 FF 00 00 1E 1B 1C 1B FF FF ; 70C0

    hex    0E 06 17 1B 15 1B 04 FF 00 00 46 00 49 00 52 00 ; 70D0
    hex    45 00 00 00 42 00 4F 00 4D 00 42 00 53 00 2E 00 ; 70E0
    hex    00 00 1F 1B 1D 1B FF FF                         ; 70F0

    hex    0F 06 16 1B 14 1B 10 FF 00 00 1E 1B 1C 1B FF FF ; 70F8

    hex    10 06 17 1B 15 1B 10 FF 00 00 1F 1B 1D 1B FF FF ; 7108

    hex    11 06 16 1B 14 1B 00 00 53 05 50 05 45 05 43 05 ; 7118
    hex    49 05 41 05 4C 05 00 00 42 05 4F 05 4E 05 55 05 ; 7128
    hex    53 05 00 00 00 00 1E 1B 1C 1B FF FF             ; 7138

    hex    12 06 17 1B 15 1B 10 FF 00 00 1F 1B 1D 1B FF FF ; 7144

    hex    13 06 16 1B 14 1B 0A FF 00 00 3E 05 3C 05 04 FF ; 7154
    hex    00 00 1E 1B 1C 1B FF FF                         ; 7164

    hex    14 06 17 1B 15 1B 10 FF 00 00 1F 1B 1D 1B FF FF ; 716C

    hex    15 06 16 0B 14 0B 1A 1B 18 1B 1A 1B 18 1B 1A 1B ; 717C
    hex    18 1B 1A 1B 18 1B 1A 1B 18 1B 1A 1B 18 1B 1A 1B ; 718C
    hex    18 1B 1A 1B 18 1B 1A 0B 18 0B FF FF             ; 719C

    hex    16 06 17 0B 15 0B 1B 1B 19 1B 1B 1B 19 1B 1B 1B ; 71A8
    hex    19 1B 1B 1B 19 1B 1B 1B 19 1B 1B 1B 19 1B 1B 1B ; 71B8
    hex    19 1B 1B 1B 19 1B 1B 0B 19 0B FF FF             ; 71C8

; 71D4h - list of addresses
;
; Probably sprite stuff.

    word   $71E4           ; 71D4 E4 71
    word   $71EC           ; 71D6 EC 71
    word   $71FC           ; 71D8 FC 71
    word   $7204           ; 71DA 04 72
    word   $7214           ; 71DC 14 72
    word   $721C           ; 71DE 1C 72
    word   $722C           ; 71E0 2C 72
    word   $7234           ; 71E2 34 72

    hex    0E 09 32 0F 30 0F FF FF                         ; 71E4

    hex    13 0B 00 00 31 0F 30 0F 30 0F 30 0F 30 0F FF FF ; 71EC

    hex    0E 09 32 0F 31 0F FF FF                         ; 71FC

    hex    13 0B 00 00 32 0F 30 0F 30 0F 30 0F 30 0F FF FF ; 7204

    hex    0E 09 32 0F 32 0F FF FF                         ; 7214

    hex    13 0B 00 00 33 0F 30 0F 30 0F 30 0F 30 0F FF FF ; 721C

    hex    0E 09 32 0F 33 0F FF FF                         ; 722C

    hex    13 0B 00 00 35 0F 30 0F 30 0F 30 0F 30 0F FF FF ; 7234

; 7244h - not referenced ?
 
    hex    13 0B 82 89 0F 06 00                            ; 7244

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 724B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 725B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 726B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 727B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 728B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 729B
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 72AB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 72BB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 72CB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 72DB
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; 72EB
    hex    00 00 00 00 00 ; 72FB

; 7300h - platform design for level 1

    word   $730A           ; 7300 0A 73
    word   $731C           ; 7302 1C 73
    word   $7328           ; 7304 28 73
    word   $7338           ; 7306 38 73
    word   $7344           ; 7308 44 73

    hex    08 11 60 09 62 09 63 09 62 09 63 09 62 09 61 09 ; 730A
    hex    FF FF 0B 08 60 09 62 09 63 09 61 09 FF FF 14 0E ; 731A
    hex    60 09 62 09 63 09 62 09 63 09 61 09 FF FF 17 05 ; 732A
    hex    60 09 62 09 63 09 61 09 FF FF 1A 12 60 09 62 09 ; 733A
    hex    63 09 62 09 63 09 62 09 63 09 62 09 61 09 FF FF ; 734A

; 735Ah - platform design for level 17

    word   $736E           ; 735A 6E 73
    word   $7382           ; 735C 82 73
    word   $738E           ; 735E 8E 73
    word   $739A           ; 7360 9A 73
    word   $73A6           ; 7362 A6 73
    word   $73B2           ; 7364 B2 73
    word   $73BE           ; 7366 BE 73
    word   $73CA           ; 7368 CA 73
    word   $73D6           ; 736A D6 73
    word   $73E2           ; 736C E2 73

    hex    08 0C 60 09 62 09 63 09 62 09 63 09 62 09 63 09 ; 736E
    hex    61 09 FF FF 0C 08 64 09 0E FF 00 00 64 09 FF FF ; 737E
    hex    0D 08 66 09 0E FF 00 00 66 09 FF FF 0E 08 67 09 ; 738E
    hex    0E FF 00 00 67 09 FF FF 0F 08 66 09 0E FF 00 00 ; 739E
    hex    66 09 FF FF 10 08 67 09 0E FF 00 00 67 09 FF FF ; 73AE
    hex    11 08 66 09 0E FF 00 00 66 09 FF FF 12 08 67 09 ; 73BE
    hex    0E FF 00 00 67 09 FF FF 13 08 65 09 0E FF 00 00 ; 73CE
    hex    65 09 FF FF 17 0C 60 09 62 09 63 09 62 09 63 09 ; 73DE
    hex    62 09 63 09 61 09 FF FF ; 73EE

; 73F6h - platform design for level 6

    word   $7408           ; 73F6 08 74
    word   $7416           ; 73F8 16 74
    word   $742C           ; 73FA 2C 74
    word   $743C           ; 73FC 3C 74
    word   $7442           ; 73FE 42 74
    word   $7448           ; 7400 48 74
    word   $744E           ; 7402 4E 74
    word   $7454           ; 7404 54 74
    word   $745A           ; 7406 5A 74

    hex    08 0F 60 09 62 09 63 09 62 09 61 09 FF FF 0B 02 ; 7408
    hex    6C 09 62 09 63 09 62 09 63 09 62 09 63 09 62 09 ; 7418
    hex    61 09 FF FF 0E 14 68 09 62 09 63 09 62 09 63 09 ; 7428
    hex    61 09 FF FF 0F 14 66 09 FF FF 10 14 67 09 FF FF ; 7438
    hex    11 14 66 09 FF FF 12 14 67 09 FF FF 13 14 66 09 ; 7448
    hex    FF FF 14 08 60 09 62 09 63 09 62 09 63 09 62 09 ; 7458
    hex    63 09 62 09 63 09 62 09 63 09 62 09 6B 09 FF FF ; 7468

; 7478h - platform design for level 8

    word   $7490           ; 7478 90 74
    word   $749C           ; 747A 9C 74
    word   $74A8           ; 747C A8 74
    word   $74B4           ; 747E B4 74
    word   $74C0           ; 7480 C0 74
    word   $74CC           ; 7482 CC 74
    word   $74EC           ; 7484 EC 74
    word   $750C           ; 7486 0C 75
    word   $7518           ; 7488 18 75
    word   $7524           ; 748A 24 75
    word   $7530           ; 748C 30 75
    word   $753C           ; 748E 3C 75

    hex    06 0B 64 09 08 FF 00 00 64 09 FF FF 07 0B 66 09 ; 7490
    hex    08 FF 00 00 66 09 FF FF 08 0B 67 09 08 FF 00 00 ; 74A0
    hex    67 09 FF FF 09 0B 66 09 08 FF 00 00 66 09 FF FF ; 74B0
    hex    0A 0B 67 09 08 FF 00 00 67 09 FF FF 0B 06 60 09 ; 74C0
    hex    62 09 63 09 62 09 63 09 6B 09 08 FF 00 00 6A 09 ; 74D0
    hex    62 09 63 09 62 09 63 09 61 09 FF FF 14 06 60 09 ; 74E0
    hex    62 09 63 09 62 09 63 09 69 09 08 FF 00 00 68 09 ; 74F0
    hex    62 09 63 09 62 09 63 09 61 09 FF FF 15 0B 66 09 ; 7500
    hex    08 FF 00 00 66 09 FF FF 16 0B 67 09 08 FF 00 00 ; 7510
    hex    67 09 FF FF 17 0B 66 09 08 FF 00 00 66 09 FF FF ; 7520
    hex    18 0B 67 09 08 FF 00 00 67 09 FF FF 19 0B 65 09 ; 7530
    hex    08 FF 00 00 65 09 FF FF ; 7540

; 7548h - platform design for level 18

    word   $7550           ; 7548 50 75
    word   $7574           ; 754A 74 75
    word   $7590           ; 754C 90 75
    word   $75B4           ; 754E B4 75

    hex    08 05 60 09 62 09 63 09 61 09 05 FF 00 00 60 09 ; 7550
    hex    62 09 63 09 61 09 05 FF 00 00 60 09 62 09 63 09 ; 7560
    hex    61 09 FF FF 0E 09 60 09 62 09 63 09 62 09 61 09 ; 7570
    hex    04 FF 00 00 60 09 62 09 63 09 62 09 61 09 FF FF ; 7580
    hex    14 05 60 09 62 09 63 09 61 09 05 FF 00 00 60 09 ; 7590
    hex    62 09 63 09 61 09 05 FF 00 00 60 09 62 09 63 09 ; 75A0
    hex    61 09 FF FF 1A 09 60 09 62 09 63 09 62 09 61 09 ; 75B0
    hex    04 FF 00 00 60 09 62 09 63 09 62 09 61 09 FF FF ; 75C0

; 75D0h - platform design for level 7

    word   $75D8           ; 75D0 D8 75
    word   $75E4           ; 75D2 E4 75
    word   $7604           ; 75D4 04 76
    word   $7624           ; 75D6 24 76

    hex    08 0E 60 09 62 09 63 09 61 09 FF FF 0E 08 60 09 ; 75D8
    hex    62 09 63 09 62 09 63 09 61 09 04 FF 00 00 60 09 ; 75E8
    hex    62 09 63 09 62 09 63 09 61 09 FF FF 11 08 60 09 ; 75F8
    hex    62 09 63 09 62 09 63 09 61 09 04 FF 00 00 60 09 ; 7608
    hex    62 09 63 09 62 09 63 09 61 09 FF FF 17 0E 60 09 ; 7618
    hex    62 09 63 09 61 09 FF FF ; 7628

; 7630h - platform design for level 4

    word   $7634           ; 7630 34 76
    word   $7650           ; 7632 50 76

    hex    08 06 60 09 62 09 63 09 62 09 61 09 0A FF 00 00 ; 7634
    hex    60 09 62 09 63 09 62 09 61 09 FF FF 17 06 60 09 ; 7644
    hex    62 09 63 09 62 09 61 09 0A FF 00 00 60 09 62 09 ; 7654
    hex    63 09 62 09 61 09 FF FF ; 7664

; 766Ch - platform design for level 3

    word   $7672           ; 766C 72 76
    word   $768E           ; 766E 8E 76
    word   $769A           ; 7670 9A 76

    hex    08 09 60 09 62 09 63 09 62 09 61 09 04 FF 00 00 ; 7672
    hex    60 09 62 09 63 09 62 09 61 09 FF FF 14 0E 60 09 ; 7682
    hex    62 09 63 09 61 09 FF FF 17 05 60 09 62 09 63 09 ; 7692
    hex    61 09 0E FF 00 00 60 09 62 09 63 09 61 09 FF FF ; 76A2

; 76B2h - platform design for level 13

    word   $76CC           ; 76B2 CC 76
    word   $76F0           ; 76B4 F0 76
    word   $76FC           ; 76B6 FC 76
    word   $7708           ; 76B8 08 77
    word   $7714           ; 76BA 14 77
    word   $7720           ; 76BC 20 77
    word   $772C           ; 76BE 2C 77
    word   $7738           ; 76C0 38 77
    word   $7744           ; 76C2 44 77
    word   $7750           ; 76C4 50 77
    word   $775C           ; 76C6 5C 77
    word   $7768           ; 76C8 68 77
    word   $7774           ; 76CA 74 77

    hex    08 08 68 09 62 09 63 09 62 09 63 09 62 09 63 09 ; 76CC
    hex    62 09 63 09 62 09 63 09 62 09 63 09 62 09 63 09 ; 76DC
    hex    69 09 FF FF 09 08 66 09 0E FF 00 00 66 09 FF FF ; 76EC
    hex    0A 08 67 09 0E FF 00 00 67 09 FF FF 0B 08 66 09 ; 76FC
    hex    0E FF 00 00 66 09 FF FF 0C 08 67 09 0E FF 00 00 ; 770C
    hex    67 09 FF FF 0D 08 66 09 0E FF 00 00 66 09 FF FF ; 771C
    hex    0E 08 67 09 0E FF 00 00 67 09 FF FF 0F 08 66 09 ; 772C
    hex    0E FF 00 00 66 09 FF FF 10 08 67 09 0E FF 00 00 ; 773C
    hex    67 09 FF FF 11 08 66 09 0E FF 00 00 66 09 FF FF ; 774C
    hex    12 08 67 09 0E FF 00 00 67 09 FF FF 13 08 65 09 ; 775C
    hex    0E FF 00 00 65 09 FF FF 14 0E 60 09 62 09 63 09 ; 776C
    hex    61 09 FF FF                                     ; 777C

; 7780h - platform design for level 9

    word   $77A0           ; 7780 A0 77
    word   $77AA           ; 7782 AA 77
    word   $77B0           ; 7784 B0 77
    word   $77B6           ; 7786 B6 77
    word   $77C2           ; 7788 C2 77
    word   $77D4           ; 778A D4 77
    word   $77DA           ; 778C DA 77
    word   $77E0           ; 778E E0 77
    word   $77EC           ; 7790 EC 77
    word   $7804           ; 7792 04 78
    word   $7810           ; 7794 10 78
    word   $7816           ; 7796 16 78
    word   $781C           ; 7798 1C 78
    word   $783A           ; 779A 3A 78
    word   $7840           ; 779C 40 78
    word   $7846           ; 779E 46 78

    hex    05 0E 68 09 62 09 61 09 FF FF 06 0E 66 09 FF FF ; 77A0
    hex    07 0E 67 09 FF FF 08 0E 6A 09 62 09 63 09 61 09 ; 77B0
    hex    FF FF 0B 0B 60 09 62 09 63 09 6E 09 63 09 62 09 ; 77C0
    hex    61 09 FF FF 0C 0E 66 09 FF FF 0D 0E 67 09 FF FF ; 77D0
    hex    0E 0E 6A 09 62 09 63 09 61 09 FF FF 11 0E 68 09 ; 77E0
    hex    62 09 63 09 62 09 63 09 62 09 63 09 62 09 63 09 ; 77F0
    hex    61 09 FF FF 14 0E 6A 09 62 09 63 09 61 09 FF FF ; 7800
    hex    12 0E 66 09 FF FF 13 0E 67 09 FF FF 17 05 60 09 ; 7810
    hex    62 09 63 09 62 09 63 09 62 09 63 09 62 09 63 09 ; 7820
    hex    6E 09 63 09 62 09 61 09 FF FF 18 0E 66 09 FF FF ; 7830
    hex    19 0E 67 09 FF FF 1A 0E 6A 09 62 09 63 09 61 09 ; 7840
    hex    FF FF                                           ; 7850

; 7852h - platform design for level 2

    word   $7858           ; 7852 58 78
    word   $7878           ; 7854 78 78
    word   $7894           ; 7856 94 78

    hex    0B 02 6C 09 62 09 63 09 62 09 63 09 61 09 10 FF ; 7858
    hex    00 00 60 09 62 09 63 09 62 09 63 09 6D 09 FF FF ; 7868
    hex    11 06 60 09 62 09 63 09 62 09 61 09 0A FF 00 00 ; 7878
    hex    60 09 62 09 63 09 62 09 61 09 FF FF 17 09 60 09 ; 7888
    hex    62 09 63 09 62 09 61 09 04 FF 00 00 60 09 62 09 ; 7898
    hex    63 09 62 09 61 09 FF FF                         ; 78A8

; 78B0h - platform design for level 11

    word   $78D0           ; 78B0 D0 78
    word   $78F2           ; 78B2 F2 78
    word   $78FE           ; 78B4 FE 78
    word   $790A           ; 78B6 0A 79
    word   $7916           ; 78B8 16 79
    word   $7922           ; 78BA 22 79
    word   $792E           ; 78BC 2E 79
    word   $793A           ; 78BE 3A 79
    word   $7946           ; 78C0 46 79
    word   $7952           ; 78C2 52 79
    word   $795E           ; 78C4 5E 79
    word   $796A           ; 78C6 6A 79
    word   $7976           ; 78C8 76 79
    word   $7982           ; 78CA 82 79
    word   $798E           ; 78CC 8E 79
    word   $799A           ; 78CE 9A 79

    hex    08 08 68 09 62 09 63 09 62 09 63 09 62 09 63 09 ; 78D0
    hex    62 09 61 09 03 FF 00 00 60 09 62 09 63 09 69 09 ; 78E0
    hex    FF FF 09 08 66 09 0E FF 00 00 66 09 FF FF 0A 08 ; 78F0
    hex    67 09 0E FF 00 00 67 09 FF FF 0B 08 66 09 0E FF ; 7900
    hex    00 00 66 09 FF FF 0C 08 67 09 0E FF 00 00 67 09 ; 7910
    hex    FF FF 0D 08 66 09 0E FF 00 00 66 09 FF FF 0E 08 ; 7920
    hex    67 09 0E FF 00 00 67 09 FF FF 0F 08 66 09 0E FF ; 7930
    hex    00 00 66 09 FF FF 10 08 67 09 0E FF 00 00 67 09 ; 7940
    hex    FF FF 11 08 66 09 0E FF 00 00 66 09 FF FF 12 08 ; 7950
    hex    67 09 0E FF 00 00 67 09 FF FF 13 08 66 09 0E FF ; 7960
    hex    00 00 66 09 FF FF 14 08 67 09 0E FF 00 00 67 09 ; 7970
    hex    FF FF 15 08 66 09 0E FF 00 00 66 09 FF FF 16 08 ; 7980
    hex    67 09 0E FF 00 00 67 09 FF FF 17 08 6A 09 62 09 ; 7990
    hex    63 09 61 09 03 FF 00 00 60 09 62 09 63 09 62 09 ; 79A0
    hex    63 09 62 09 63 09 62 09 6B 09 FF FF             ; 79B0

; 79BCh - platform design for level 16

    word   $79E8           ; 79BC E8 79
    word   $7A00           ; 79BE 00 7A
    word   $7A06           ; 79C0 06 7A
    word   $7A0C           ; 79C2 0C 7A
    word   $7A18           ; 79C4 18 7A
    word   $7A24           ; 79C6 24 7A
    word   $7A30           ; 79C8 30 7A
    word   $7A3C           ; 79CA 3C 7A
    word   $7A42           ; 79CC 42 7A
    word   $7A48           ; 79CE 48 7A
    word   $7A60           ; 79D0 60 7A
    word   $7A6C           ; 79D2 6C 7A
    word   $7A78           ; 79D4 78 7A
    word   $7A90           ; 79D6 90 7A
    word   $7A96           ; 79D8 96 7A
    word   $7A9C           ; 79DA 9C 7A
    word   $7AA8           ; 79DC A8 7A
    word   $7AB4           ; 79DE B4 7A
    word   $7AC0           ; 79E0 C0 7A
    word   $7ACC           ; 79E2 CC 7A
    word   $7AD2           ; 79E4 D2 7A
    word   $7AD8           ; 79E6 D8 7A

    hex    05 08 60 09 62 09 63 09 62 09 63 09 62 09 63 09 ; 79E8
    hex    62 09 63 09 69 09 FF FF 06 11 66 09 FF FF 07 11 ; 79F8
    hex    67 09 FF FF 08 11 66 09 08 FF 00 00 64 09 FF FF ; 7A08
    hex    09 11 67 09 08 FF 00 00 66 09 FF FF 0A 11 66 09 ; 7A18
    hex    08 FF 00 00 67 09 FF FF 0B 11 65 09 08 FF 00 00 ; 7A28
    hex    66 09 FF FF 0C 1A 67 09 FF FF 0D 1A 66 09 FF FF ; 7A38
    hex    0E 05 68 09 62 09 63 09 62 09 63 09 62 09 61 09 ; 7A48
    hex    0E FF 00 00 67 09 FF FF 0F 05 66 09 14 FF 00 00 ; 7A58
    hex    66 09 FF FF 10 05 67 09 14 FF 00 00 67 09 FF FF ; 7A68
    hex    11 05 66 09 0E FF 00 00 60 09 62 09 63 09 62 09 ; 7A78
    hex    63 09 62 09 6B 09 FF FF 12 05 67 09 FF FF 13 05 ; 7A88
    hex    66 09 FF FF 14 05 67 09 08 FF 00 00 64 09 FF FF ; 7A98
    hex    15 05 66 09 08 FF 00 00 66 09 FF FF 16 05 67 09 ; 7AA8
    hex    08 FF 00 00 67 09 FF FF 17 05 65 09 08 FF 00 00 ; 7AB8
    hex    66 09 FF FF 18 0E 67 09 FF FF 19 0E 66 09 FF FF ; 7AC8
    hex    1A 0E 6A 09 62 09 63 09 62 09 63 09 62 09 63 09 ; 7AD8
    hex    62 09 63 09 61 09 FF FF                         ; 7AE8

; 7AF0h - platform design for level 13

    word   $7AFC           ; 7AF0 FC 7A
    word   $7B0C           ; 7AF2 0C 7B
    word   $7B1C           ; 7AF4 1C 7B
    word   $7B2C           ; 7AF6 2C 7B
    word   $7B3C           ; 7AF8 3C 7B
    word   $7B4C           ; 7AFA 4C 7B

    hex    08 18 60 09 62 09 63 09 62 09 63 09 6D 09 FF FF ; 7AFC
    hex    0B 02 6C 09 62 09 63 09 62 09 63 09 61 09 FF FF ; 7B0C
    hex    0E 18 60 09 62 09 63 09 62 09 63 09 6D 09 FF FF ; 7B1C
    hex    11 02 6C 09 62 09 63 09 62 09 63 09 61 09 FF FF ; 7B2C
    hex    14 18 60 09 62 09 63 09 62 09 63 09 6D 09 FF FF ; 7B3C
    hex    17 02 6C 09 62 09 63 09 62 09 63 09 61 09 FF FF ; 7B4C

; 7B5Ch - platform design for level 14

    word   $7B7C           ; 7B5C 7C 7B
    word   $7B98           ; 7B5E 98 7B
    word   $7BA4           ; 7B60 A4 7B
    word   $7BB0           ; 7B62 B0 7B
    word   $7BC8           ; 7B64 C8 7B
    word   $7BD4           ; 7B66 D4 7B
    word   $7BE0           ; 7B68 E0 7B
    word   $7BEC           ; 7B6A EC 7B
    word   $7BF8           ; 7B6C F8 7B
    word   $7C04           ; 7B6E 04 7C
    word   $7C10           ; 7B70 10 7C
    word   $7C1C           ; 7B72 1C 7C
    word   $7C28           ; 7B74 28 7C
    word   $7C40           ; 7B76 40 7C
    word   $7C4C           ; 7B78 4C 7C
    word   $7C58           ; 7B7A 58 7C

    hex    05 09 60 09 62 09 63 09 62 09 61 09 04 FF 00 00 ; 7B7C
    hex    60 09 62 09 63 09 62 09 61 09 FF FF 09 05 64 09 ; 7B8C
    hex    14 FF 00 00 64 09 FF FF 0A 05 66 09 14 FF 00 00 ; 7B9C
    hex    66 09 FF FF 0B 05 67 09 08 FF 00 00 60 09 62 09 ; 7BAC
    hex    63 09 61 09 08 FF 00 00 67 09 FF FF 0C 05 66 09 ; 7BBC
    hex    14 FF 00 00 66 09 FF FF 0D 05 65 09 14 FF 00 00 ; 7BCC
    hex    65 09 FF FF 0E 0B 64 09 08 FF 00 00 64 09 FF FF ; 7BDC
    hex    0F 0B 66 09 08 FF 00 00 66 09 FF FF 10 0B 67 09 ; 7BEC
    hex    08 FF 00 00 67 09 FF FF 11 0B 65 09 08 FF 00 00 ; 7BFC
    hex    65 09 FF FF 12 05 64 09 14 FF 00 00 64 09 FF FF ; 7C0C
    hex    13 05 66 09 14 FF 00 00 66 09 FF FF 14 05 67 09 ; 7C1C
    hex    08 FF 00 00 60 09 62 09 63 09 61 09 08 FF 00 00 ; 7C2C
    hex    67 09 FF FF 15 05 66 09 14 FF 00 00 66 09 FF FF ; 7C3C
    hex    16 05 65 09 14 FF 00 00 65 09 FF FF 1A 09 60 09 ; 7C4C
    hex    62 09 63 09 62 09 61 09 04 FF 00 00 60 09 62 09 ; 7C5C
    hex    63 09 62 09 61 09 FF FF                         ; 7C6C

; 7C74h - jump table, 16 entries

    word   $7c94
    word   $7c9e 
    word   $7ca8 
    word   $7cb2 
    word   $7cbc 
    word   $7cc0 
    word   $7ccb 
    word   $7cd5 
    word   $7cdf 
    word   $7cea 
    word   $7cf4 
    word   $7cfe 
    word   $7d09
    word   $7d13
    word   $7d1d 
    word   $7d28

; 7C94h - 

    call   $7d32           ; 7C94 CD 32 7D
    call   $00cf           ; 7C97 CD CF 00
    word   $7300           ; 7C9A 00 73
    dec    b               ; 7C9C 05
    ret                    ; 7C9D C9

    call   $7d32           ; 7C9E CD 32 7D
    call   $00cf           ; 7CA1 CD CF 00
    word   $735A           ; 7CA4 5A 73
    ld     a,(bc)          ; 7CA6 0A
    ret                    ; 7CA7 C9

    call   $7d32           ; 7CA8 CD 32 7D
    call   $00cf           ; 7CAB CD CF 00
    word   $73F6           ; 7CAE F6 73
    add    hl,bc           ; 7CB0 09
    ret                    ; 7CB1 C9

    call   $7d32           ; 7CB2 CD 32 7D
    call   $00cf           ; 7CB5 CD CF 00
    word   $7478           ; 7CB8 78 74
    inc    c               ; 7CBA 0C
    ret                    ; 7CBB C9

    call   $7d32           ; 7CBC CD 32 7D
    ret                    ; 7CBF C9

    call   $7d32           ; 7CC0 CD 32 7D
    call   $00cf           ; 7CC3 CD CF 00
    word   $7548           ; 7CC6 48 75
    inc    b               ; 7CC8 04
    nop                    ; 7CC9 00
    ret                    ; 7CCA C9

    call   $7d32           ; 7CCB CD 32 7D
    call   $00cf           ; 7CCE CD CF 00
    word   $75D0           ; 7CD1 D0 75
    inc    b               ; 7CD3 04
    ret                    ; 7CD4 C9

    call   $7d32           ; 7CD5 CD 32 7D
    call   $00cf           ; 7CD8 CD CF 00
    word   $7630           ; 7CDB 30 76
    ld     (bc),a          ; 7CDD 02
    ret                    ; 7CDE C9

    call   $7d32           ; 7CDF CD 32 7D
    call   $00cf           ; 7CE2 CD CF 00
    word   $766C           ; 7CE5 6C 76
    inc    bc              ; 7CE7 03
    nop                    ; 7CE8 00
    ret                    ; 7CE9 C9

    call   $7d32           ; 7CEA CD 32 7D
    call   $00cf           ; 7CED CD CF 00
    word   $76B2           ; 7CF0 B2 76
    dec    c               ; 7CF2 0D
    ret                    ; 7CF3 C9

    call   $7d32           ; 7CF4 CD 32 7D
    call   $00cf           ; 7CF7 CD CF 00
    word   $7780           ; 7CFA 80 77
    djnz   $7cc7           ; 7CFC 10 C9

    call   $7d32           ; 7CFE CD 32 7D
    call   $00cf           ; 7D01 CD CF 00
    word   $7852           ; 7D04 52 78
    inc    bc              ; 7D06 03
    nop                    ; 7D07 00
    ret                    ; 7D08 C9

    call   $7d32           ; 7D09 CD 32 7D
    call   $00cf           ; 7D0C CD CF 00
    word   $78B0           ; 7D0F B0 78
    djnz   $7cdc           ; 7D11 10 C9

    call   $7d32           ; 7D13 CD 32 7D
    call   $00cf           ; 7D16 CD CF 00
    word   $79BC           ; 7D19 BC 79
    ld     d,$c9           ; 7D1B 16 C9

    call   $7d32           ; 7D1D CD 32 7D
    call   $00cf           ; 7D20 CD CF 00
    word   $7AF0           ; 7D23 F0 7A
    ld     b,$00           ; 7D25 06 00
    ret                    ; 7D27 C9

    call   $7d32           ; 7D28 CD 32 7D
    call   $00cf           ; 7D2B CD CF 00
    word   $7B5C           ; 7D2E 5C 7B
    djnz   $7cfb           ; 7D30 10 C9

; 7D32h - ?

    ld     a,$09           ; 7D32 3E 09
    call   $4ff0           ; 7D34 CD F0 4F
    ret                    ; 7D37 C9

; 7D38h - zeros

; 7E04h - ?

    jp     p,$00ff         ; 7E04 F2 FF 00

; 7E07h - ?

;	
;	8000h - RAM
;

; 800Ah - state ?
;
; Most of time set to values like DEh, E0h and E2h. Changes very
; often when enemies are spawned, more slowly the rest of the
; time and not at all during bonus screen or when you got a P.


; 8013h - copy of DIP switches 1
;
; abccddee
;       00 - coin A 1 -> 1
;       01 - coin A 1 -> 2
;       10 - coin A 1 -> 3
;       11 - coin A 1 -> 6
;     00   - coin B 1 -> 1
;     01   - coin B 1 -> 1
;     10   - coin B 1 -> 2
;     11   - coin B 1 -> 3
;   00     - lives 3
;   01     - lives 4
;   10     - lives 5
;   11     - lives 2
;  0       - cabinet cocktail
;  1       - cabinet upright	// FIXME isn't that the reverse ?
; 0        - demo sounds off
; 1        - demo sounds on

; 8014h - copy of DIP switches 2
; 
; abbccddd
;      000 - initial high score 10k
;      001 - initial high score 100k
;      010 - initial high score 30k
;      011 - initial high score 50k
;      100 - initial high score 100k
;      101 - initial high score 50k
;      110 - initial high score 100k
;      111 - initial high score 50k
;    00    - bird speed easy
;    01    - bird speed medium
;    10    - bird speed hard
;    11    - bird speed hardest
;  00      - enemies number & speed medium
;  01      - enemies number & speed easy
;  10      - enemies number & speed hard
;  11      - enemies number & speed hardest
; 0        - special coin easy
; 1        - special coin hard

; 8015h - port ?
;
; 1 when dropping a coin, 0 otherwise.

; 8016h - ?
; 
; Always 1 ?

; 8018h - move right
;
; When not game over, set to FFh when the joystick is pushed
; right, 0 otherwise.

; 8019h - move right
;
; When not game over and the joystick is pushed right, counts from
; 0 to FFh and wraps around. Set to 0 otherwise.

; 801Ah - move left
;
; When not game over, set to FFh when the joystick is pushed
; left, 0 otherwise.

; 801Bh - move left
;
; When not game over and the joystick is pushed left, counts from
; 0 to FFh and wraps around. Set to 0 otherwise.

; 801Ch - move up
;
; When not game over, set to FFh when the joystick is pushed
; up, 0 otherwise.

; 801Dh - move up
;
; When not game over and the joystick is pushed up, counts from
; 0 to FFh and wraps around. Set to 0 otherwise.

; 801Eh - move down
;
; When not game over, set to FFh when the joystick is pushed
; down, 0 otherwise.

; 801Fh - move down
;
; When not game over and the joystick is pushed down, counts from
; 0 to FFh and wraps around. Set to 0 otherwise.
; Has an influence over how 81A9h is initialised.

; 8020h - button
;
; When not game over, set to FFh when the button is pressed 0
; otherwise.

; 8021h - button
;
; When not game over and the button is pressed, counts from
; 0 to FFh and wraps around. Set to 0 otherwise.

; 8022h - fast counter

; 8023h - fast counter

; 8024h - fast counter

; 8025h - fast counter

; 8026h - ?
; Always 0 ?

; 8027h - ?
; Always 0 ?

; 8028h - bombjack movement
; Always 0, 1, 2 or 3.

; 8029h - bombjack movement
; Sprite frame ?

; 802AH - ?
; Always 0 ?

; 802Bh - bombjack x

; 802Ch - bombjack y

; 802Dh - bombjack x
; Alternates very fast between 0 and 80h when moving horizontally.

; 802Eh - flight counter
; Counts very fast when bombjack is flying

; 802Fh - ?
; Always 0 ?

; 8030h..3035h - movement
; Related to Bombjack's moves

; word 8036h - got P counter
; When Bombjack gets P, counts down from about 130h to 0, at
; which the effect stops.

; 8038h - ?
; Always 0 ?

; 8039h - ?
; Always 0 ?

; 803Ah..803Eh - jump
; Changes when Bombjack jumps.

; 803Fh - landing counter
; When Bombjack lands, counts down from about 6 to 0. Bombjack
; can't move until the counter has reached 0.

; 8040h - ?
; Always 0 ?

; 8041h - floor
; 08h when Bombjack stands on ground
; 00h when Bombjack is in the air
; 02h when Bombjack bumps his head against a platform
; 01h when Bombjack bumps against a wall

; 8042h..8043h - flight countdown

; 8044h - ?
; Briefly changes when pressing button. Otherwise always 2.

; 8045h - ?
; Always 0 ?

; 8046h - button
; Briefly changes to 2 or 3 when the button is pressed. Otherwise 0.

; 8047h - button
; Briefly changes to 2 or 3 when the button is pressed. Otherwise 0.

; 8048h..804Ah - ?
; Always 0 ?

; 804Bh - ground counter
; Counts up when Bombjack is on ground. 0 when flying.

; 804Ch..8059h - ?
; Always 0 ?

; word 805Ah - fall counter
; Increased when Bombjack is falling

; word 805Ch - vertical movement
; Changes when Bombjack is moving vertically

; 805Dh - horizontal movement
; Changes when Bombjack is moving horizontally

; 805Fh - ?
; Always 0 ?

; 8060h..8063h - movement
; Changes when Bombjack moves

; 8064h..806Fh - ?
; Always 0 ?

; 8070h - 0 while game over, 1 while playing

; 8071h - always 3 ? [dubious: Level#] (seems to be -> 819b -> 81a8)]]

; 8072h..807Bh - ?
; Smallish values that never seem to change during a game.

; 807Ch - always 0 ?

; 8081h	- 1 while "push ? player button", 0 otherwise

; 8089h - walking
; 13h when Bombjack is walking, 93h otherwise.

; 808Ch	- 0 most of the time incl HOF and demos,
; 	  2 while "1 coin 1 credit" and briefly between levels ?

; 8091h - counter
; Stops only when "START" ?

; 8092h - counter
; Stops only when "START" ?

; 80A0h - victory
; Changes during Bombjack's little victory dance.

; 80A1h..80BFh - ?
; Always 0 ?

; 80C4h - counter
; +8 every ~2s

; 80D2h - max enemies
; (Robots or spawned)

; 80D3h - enemy count
; (Robots or spawned).

; 80DAh - counter
; Stops counting after the last robot is spawned

; 80DCh - counter
; Stops counting after the last robot is transformed

; 80E8h	- ?
; Always 0 ? Briefly set to 1 at start of demo 2.

; 819Bh	- 2 while playing, 3 in "insert coin" screen, sometimes 4 ?

; 81A0h	- level number
; Controls the background and platform set. Initial value at
; 11.040d (controls demo 1 too !)

; 81A2h - number of bombs left
; Initial value is 24.

; 81A8h - level number - 1
; Forcing does not seem to have any effect.

; 81A9h - level number - 1
; Controls which enemies are spawned. Used as an index into the
; table at DBB8h.

; 81ABh	- number of "fire bombs gotten"
; Reset to zero at level start.

; 81ACh	- bonus multiplier - 1

; 81AFh - level number
; In BCD. Controls the number displayed at the bottom of the
; screen. Initial value at 11.0417 (controls demo 1 too !)

; 81B0h - number of transformed robots
; Reset to zero at level start. Wraps around from 7 to zero. This
; is probably an index into the last 8 bytes of the records at
; DBB8h.

; 81B9h	- level number
; 0 when not playing, even during demos. Set to 81A0h + 1 at
; level start. Forcing does not seem to have any effect

; 81D2h - level number player 1

; 81C8h - level number player 1
; In BCD. Set to 81AFh + 1 at level start.

; 81E1h - level number player 2
; In BCD.

; 81E8h - ?
; Set to 81AFh + 1 at level start ?

; 8268h - ?
; Start of a table ?


;
;	9000h - video RAM
;

; 9000h..93FFh - video RAM

; 9400h..97FFh - colour RAM

; 9C00h..9CFFh - palette RAM

;
;	Memory mapped registers
;

; 9820h..9827h - sprites (MMIOP, write only)

; 9A00h - number of small sprites for video controller (MMIOP, write only)

; 9E00h - background image selector (MMIOP, write only)

; B000h - IN0 (MMIOP, read only)
; B000h - interrupt enable (MMIOP, write only)

; B001h - IN1 (MMIOP, read only)

; B002h - IN2 (MMIOP, read only)

; B003h - watchdog reset ? (MMIOP, read only)

; B004h - DIP switches 1 (MMIOP, read only)
; Copied into 8013h
; B004h - flip screen (MMIOP, write only)

; B005h - DIP switches 1 (MMIOP, read only)
; Copied into 8014h
; B800h - command to soundboard (MMIOP, write only)

;
;	C000h - ROM
;

; C000h - address list

    word   $C060           ; C000 60 C0
    word   $C07C           ; C002 7C C0
    word   $C094           ; C004 94 C0
    word   $C0AC           ; C006 AC C0
    word   $C0C4           ; C008 C4 C0
    word   $C0DC           ; C00A DC C0
    word   $C0F4           ; C00C F4 C0
    word   $C10C           ; C00E 0C C1
    word   $C124           ; C010 24 C1
    word   $C13C           ; C012 3C C1
    word   $C154           ; C014 54 C1
    word   $C16E           ; C016 6E C1

; "FANTASTIC SCORE"

    hex    03 08 46 0F 41 0F 4E 0F 54 0F 41 0F 53 0F 54 0F ; C018
    hex    49 0F 43 0F 00 00 53 0F 43 0F 4F 0F 52 0F 45 0F ; C028
    hex    3F 0F FF FF                                     ; C038

; "RECORD YOUR NAME"

    hex    05 08 52 05 45 05 43 05 4F 05 52 05 44 05 00 00 ; C03C
    hex    59 05 4F 05 55 05 52 05 00 00 4E 05 41 05 4D 05 ; C04C
    hex    45 05 FF FF                                     ; C05C

; "BEST PLAYERS"

    hex    04 0A 42 06 45 06 53 06 54 06 00 00 50 06 4C 06 ; C060
    hex    41 06 59 06 45 06 52 06 53 06 FF FF             ; C070

; "1ST ROUND"

    hex    07 04 31 05 53 05 54 05 0F FF 00 00 52 05 4F 05 ; C07C
    hex    55 05 4E 05 44 05 FF FF                         ; C08C

; "2ND ROUND"

    hex    09 04 32 05 4E 05 44 05 0F FF 00 00 52 05 4F 05 ; C094
    hex    55 05 4E 05 44 05 FF FF                         ; C0A4

; "3RD ROUND"

    hex    0B 04 33 05 52 05 44 05 0F FF 00 00 52 05 4F 05 ; C0AC
    hex    55 05 4E 05 44 05 FF FF                         ; C0BC

; "4TH ROUND"

    hex    0D 04 34 05 54 05 48 05 0F FF 00 00 52 05 4F 05 ; C0C4
    hex    55 05 4E 05 44 05 FF FF                         ; C0D4

; "5TH ROUND"

    hex    0F 04 35 05 54 05 48 05 0F FF 00 00 52 05 4F 05 ; C0DC
    hex    55 05 4E 05 44 05 FF FF                         ; C0EC

; "6TH ROUND"

    hex    11 04 36 05 54 05 48 05 0F FF 00 00 52 05 4F 05 ; C0F4
    hex    55 05 4E 05 44 05 FF FF                         ; C104

; "7TH ROUND"

    hex    13 04 37 05 54 05 48 05 0F FF 00 00 52 05 4F 05 ; C10C
    hex    55 05 4E 05 44 05 FF FF                         ; C11C

; "8TH ROUND"

    hex    15 04 38 05 54 05 48 05 0F FF 00 00 52 05 4F 05 ; C124
    hex    55 05 4E 05 44 05 FF FF                         ; C134

; "9TH ROUND"

    hex    17 04 39 05 54 05 48 05 0F FF 00 00 52 05 4F 05 ; C13C
    hex    55 05 4E 05 44 05 FF FF                         ; C14C

; "10TH ROUND"

    hex    19 03 31 05 30 05 54 05 48 05 0F FF 00 00 52 05 ; C154
    hex    4F 05 55 05 4E 05 44 05 FF FF                   ; C164

; "(c)(p)1984 TEHKAN LTD."

    hex    1C 07 3A 05 3B 05 31 05 39 05 38 05 34 05 00 00 ; C16E
    hex    54 0F 45 0F 48 0F 4B 0F 41 0F 4E 0F 00 00 4C 05 ; C17E
    hex    54 05 44 05 2E 05 FF FF                         ; C18E

; ?

    xor    d               ; C196 AA
    pop    bc              ; C197 C1
    or     b               ; C198 B0
    pop    bc              ; C199 C1
    or     (hl)            ; C19A B6
    pop    bc              ; C19B C1
    cp     h               ; C19C BC
    pop    bc              ; C19D C1
    jp     nz,$c8c1        ; C19E C2 C1 C8
    pop    bc              ; C1A1 C1
    adc    a,$c1           ; C1A2 CE C1
    call   nc,$dac1        ; C1A4 D4 C1 DA
    pop    bc              ; C1A7 C1
    ret    po              ; C1A8 E0
    pop    bc              ; C1A9 C1
    rlca                   ; C1AA 07
    ex     af,af'          ; C1AB 08
    nop                    ; C1AC 00
    add    a,c             ; C1AD 81
    rrca                   ; C1AE 0F
    ex     af,af'          ; C1AF 08
    add    hl,bc           ; C1B0 09
    ex     af,af'          ; C1B1 08
    inc    b               ; C1B2 04
    add    a,c             ; C1B3 81
    dec    b               ; C1B4 05
    ex     af,af'          ; C1B5 08
    dec    bc              ; C1B6 0B
    ex     af,af'          ; C1B7 08
    ex     af,af'          ; C1B8 08
    add    a,c             ; C1B9 81
    dec    b               ; C1BA 05
    ex     af,af'          ; C1BB 08
    dec    c               ; C1BC 0D
    ex     af,af'          ; C1BD 08
    inc    c               ; C1BE 0C
    add    a,c             ; C1BF 81
    dec    b               ; C1C0 05
    ex     af,af'          ; C1C1 08
    rrca                   ; C1C2 0F
    ex     af,af'          ; C1C3 08
    djnz   $c147           ; C1C4 10 81
    dec    b               ; C1C6 05
    ex     af,af'          ; C1C7 08
    ld     de,$1408        ; C1C8 11 08 14
    add    a,c             ; C1CB 81
    dec    b               ; C1CC 05
    ex     af,af'          ; C1CD 08
    inc    de              ; C1CE 13
    ex     af,af'          ; C1CF 08
    jr     $c153           ; C1D0 18 81
    dec    b               ; C1D2 05
    ex     af,af'          ; C1D3 08
    dec    d               ; C1D4 15
    ex     af,af'          ; C1D5 08
    inc    e               ; C1D6 1C
    add    a,c             ; C1D7 81
    dec    b               ; C1D8 05
    ex     af,af'          ; C1D9 08
    rla                    ; C1DA 17
    ex     af,af'          ; C1DB 08
    jr     nz,$c15f        ; C1DC 20 81
    dec    b               ; C1DE 05
    ex     af,af'          ; C1DF 08
    add    hl,de           ; C1E0 19
    ex     af,af'          ; C1E1 08
    inc    h               ; C1E2 24
    add    a,c             ; C1E3 81
    dec    b               ; C1E4 05
    ex     af,af'          ; C1E5 08
    ld     ($3c81),a       ; C1E6 32 81 3C
    add    a,c             ; C1E9 81
    ld     b,(hl)          ; C1EA 46
    add    a,c             ; C1EB 81
    ld     d,b             ; C1EC 50
    add    a,c             ; C1ED 81
    ld     e,d             ; C1EE 5A
    add    a,c             ; C1EF 81
    ld     h,h             ; C1F0 64
    add    a,c             ; C1F1 81
    ld     l,(hl)          ; C1F2 6E
    add    a,c             ; C1F3 81
    ld     a,b             ; C1F4 78
    add    a,c             ; C1F5 81
    add    a,d             ; C1F6 82
    add    a,c             ; C1F7 81
    adc    a,h             ; C1F8 8C
    add    a,c             ; C1F9 81
    ld     c,$c2           ; C1FA 0E C2
    inc    d               ; C1FC 14
    jp     nz,$c21a        ; C1FD C2 1A C2
    jr     nz,$c1c4        ; C200 20 C2
    ld     h,$c2           ; C202 26 C2
    inc    l               ; C204 2C
    jp     nz,$c232        ; C205 C2 32 C2
    jr     c,$c1cc         ; C208 38 C2
    ld     a,$c2           ; C20A 3E C2
    ld     b,h             ; C20C 44
    jp     nz,$1b07        ; C20D C2 07 1B
    jr     z,$c193         ; C210 28 81
    dec    b               ; C212 05
    ld     (bc),a          ; C213 02
    add    hl,bc           ; C214 09
    dec    de              ; C215 1B
    add    hl,hl           ; C216 29
    add    a,c             ; C217 81
    dec    b               ; C218 05
    ld     (bc),a          ; C219 02
    dec    bc              ; C21A 0B
    dec    de              ; C21B 1B
    ld     hl,($0581)      ; C21C 2A 81 05
    ld     (bc),a          ; C21F 02
    dec    c               ; C220 0D
    dec    de              ; C221 1B
    dec    hl              ; C222 2B
    add    a,c             ; C223 81
    dec    b               ; C224 05
    ld     (bc),a          ; C225 02
    rrca                   ; C226 0F
    dec    de              ; C227 1B
    inc    l               ; C228 2C
    add    a,c             ; C229 81
    dec    b               ; C22A 05
    ld     (bc),a          ; C22B 02
    ld     de,$2d1b        ; C22C 11 1B 2D
    add    a,c             ; C22F 81
    dec    b               ; C230 05
    ld     (bc),a          ; C231 02
    inc    de              ; C232 13
    dec    de              ; C233 1B
    ld     l,$81           ; C234 2E 81
    dec    b               ; C236 05
    ld     (bc),a          ; C237 02
    dec    d               ; C238 15
    dec    de              ; C239 1B
    cpl                    ; C23A 2F
    add    a,c             ; C23B 81
    dec    b               ; C23C 05
    ld     (bc),a          ; C23D 02
    rla                    ; C23E 17
    dec    de              ; C23F 1B
    jr     nc,$c1c3        ; C240 30 81
    dec    b               ; C242 05
    ld     (bc),a          ; C243 02
    add    hl,de           ; C244 19
    dec    de              ; C245 1B
    ld     sp,$0581        ; C246 31 81 05
    ld     (bc),a          ; C249 02
    jr     $c20c           ; C24A 18 C0
    inc    a               ; C24C 3C
    ret    nz              ; C24D C0
    ld     h,h             ; C24E 64
    jp     nz,$c27c        ; C24F C2 7C C2
    sub    h               ; C252 94
    jp     nz,$c2ac        ; C253 C2 AC C2
    call   nz,$dcc2        ; C256 C4 C2 DC
    jp     nz,$c2f4        ; C259 C2 F4 C2
    inc    c               ; C25C 0C
    jp     $c324           ; C25D C3 24 C3
    inc    a               ; C260 3C
    jp     $c16e           ; C261 C3 6E C1
    rlca                   ; C264 07
    inc    b               ; C265 04
    ld     sp,$5302        ; C266 31 02 53
    ld     (bc),a          ; C269 02
    ld     d,h             ; C26A 54
    ld     (bc),a          ; C26B 02
    rrca                   ; C26C 0F
    rst    38h             ; C26D FF
    nop                    ; C26E 00
    nop                    ; C26F 00
    ld     d,d             ; C270 52
    ld     bc,$014f        ; C271 01 4F 01
    ld     d,l             ; C274 55
    ld     bc,$014e        ; C275 01 4E 01
    ld     b,h             ; C278 44
    ld     bc,$ffff        ; C279 01 FF FF

; Blocks of 24 bytes... hmm...

    add    hl,bc           ; C27C 09
    inc    b               ; C27D 04
    ld     ($4e00),a       ; C27E 32 00 4E
    nop                    ; C281 00
    ld     b,h             ; C282 44
    nop                    ; C283 00
    rrca                   ; C284 0F
    rst    38h             ; C285 FF
    nop                    ; C286 00
    nop                    ; C287 00
    ld     d,d             ; C288 52
    ld     bc,$014f        ; C289 01 4F 01
    ld     d,l             ; C28C 55
    ld     bc,$014e        ; C28D 01 4E 01
    ld     b,h             ; C290 44
    ld     bc,$ffff        ; C291 01 FF FF

    dec    bc              ; C294 0B
    inc    b               ; C295 04
    inc    sp              ; C296 33
    nop                    ; C297 00
    ld     d,d             ; C298 52
    nop                    ; C299 00
    ld     b,h             ; C29A 44
    nop                    ; C29B 00
    rrca                   ; C29C 0F
    rst    38h             ; C29D FF
    nop                    ; C29E 00
    nop                    ; C29F 00
    ld     d,d             ; C2A0 52
    ld     bc,$014f        ; C2A1 01 4F 01
    ld     d,l             ; C2A4 55
    ld     bc,$014e        ; C2A5 01 4E 01
    ld     b,h             ; C2A8 44
    ld     bc,$ffff        ; C2A9 01 FF FF

    dec    c               ; C2AC 0D
    inc    b               ; C2AD 04
    inc    (hl)            ; C2AE 34
    nop                    ; C2AF 00
    ld     d,h             ; C2B0 54
    nop                    ; C2B1 00
    ld     c,b             ; C2B2 48
    nop                    ; C2B3 00
    rrca                   ; C2B4 0F
    rst    38h             ; C2B5 FF
    nop                    ; C2B6 00
    nop                    ; C2B7 00
    ld     d,d             ; C2B8 52
    ld     bc,$014f        ; C2B9 01 4F 01
    ld     d,l             ; C2BC 55
    ld     bc,$014e        ; C2BD 01 4E 01
    ld     b,h             ; C2C0 44
    ld     bc,$ffff        ; C2C1 01 FF FF

    rrca                   ; C2C4 0F
    inc    b               ; C2C5 04
    dec    (hl)            ; C2C6 35
    nop                    ; C2C7 00
    ld     d,h             ; C2C8 54
    nop                    ; C2C9 00
    ld     c,b             ; C2CA 48
    nop                    ; C2CB 00
    rrca                   ; C2CC 0F
    rst    38h             ; C2CD FF
    nop                    ; C2CE 00
    nop                    ; C2CF 00
    ld     d,d             ; C2D0 52
    ld     bc,$014f        ; C2D1 01 4F 01
    ld     d,l             ; C2D4 55
    ld     bc,$014e        ; C2D5 01 4E 01
    ld     b,h             ; C2D8 44
    ld     bc,$ffff        ; C2D9 01 FF FF

    ld     de,$3604        ; C2DC 11 04 36
    nop                    ; C2DF 00
    ld     d,h             ; C2E0 54
    nop                    ; C2E1 00
    ld     c,b             ; C2E2 48
    nop                    ; C2E3 00
    rrca                   ; C2E4 0F
    rst    38h             ; C2E5 FF
    nop                    ; C2E6 00
    nop                    ; C2E7 00
    ld     d,d             ; C2E8 52
    ld     bc,$014f        ; C2E9 01 4F 01
    ld     d,l             ; C2EC 55
    ld     bc,$014e        ; C2ED 01 4E 01
    ld     b,h             ; C2F0 44
    ld     bc,$ffff        ; C2F1 01 FF FF

    inc    de              ; C2F4 13
    inc    b               ; C2F5 04
    scf                    ; C2F6 37
    nop                    ; C2F7 00
    ld     d,h             ; C2F8 54
    nop                    ; C2F9 00
    ld     c,b             ; C2FA 48
    nop                    ; C2FB 00
    rrca                   ; C2FC 0F
    rst    38h             ; C2FD FF
    nop                    ; C2FE 00
    nop                    ; C2FF 00
    ld     d,d             ; C300 52
    ld     bc,$014f        ; C301 01 4F 01
    ld     d,l             ; C304 55
    ld     bc,$014e        ; C305 01 4E 01
    ld     b,h             ; C308 44
    ld     bc,$ffff        ; C309 01 FF FF

    dec    d               ; C30C 15
    inc    b               ; C30D 04
    jr     c,$c310         ; C30E 38 00
    ld     d,h             ; C310 54
    nop                    ; C311 00
    ld     c,b             ; C312 48
    nop                    ; C313 00
    rrca                   ; C314 0F
    rst    38h             ; C315 FF
    nop                    ; C316 00
    nop                    ; C317 00
    ld     d,d             ; C318 52
    ld     bc,$014f        ; C319 01 4F 01
    ld     d,l             ; C31C 55
    ld     bc,$014e        ; C31D 01 4E 01
    ld     b,h             ; C320 44
    ld     bc,$ffff        ; C321 01 FF FF

    rla                    ; C324 17
    inc    b               ; C325 04
    add    hl,sp           ; C326 39
    nop                    ; C327 00
    ld     d,h             ; C328 54
    nop                    ; C329 00
    ld     c,b             ; C32A 48
    nop                    ; C32B 00
    rrca                   ; C32C 0F
    rst    38h             ; C32D FF
    nop                    ; C32E 00
    nop                    ; C32F 00
    ld     d,d             ; C330 52
    ld     bc,$014f        ; C331 01 4F 01
    ld     d,l             ; C334 55
    ld     bc,$014e        ; C335 01 4E 01
    ld     b,h             ; C338 44
    ld     bc,$ffff        ; C339 01 FF FF

    add    hl,de           ; C33C 19
    inc    bc              ; C33D 03
    ld     sp,$3000        ; C33E 31 00 30
    nop                    ; C341 00
    ld     d,h             ; C342 54
    nop                    ; C343 00
    ld     c,b             ; C344 48
    nop                    ; C345 00
    rrca                   ; C346 0F
    rst    38h             ; C347 FF
    nop                    ; C348 00
    nop                    ; C349 00
    ld     d,d             ; C34A 52
    ld     bc,$014f        ; C34B 01 4F 01
    ld     d,l             ; C34E 55
    ld     bc,$014e        ; C34F 01 4E 01
    ld     b,h             ; C352 44
    ld     bc,$ffff        ; C353 01 FF FF

    nop                    ; C356 00
    nop                    ; C357 00
    nop                    ; C358 00
    jr     nz,$c37c        ; C359 20 21
    ld     c,$fc           ; C35B 0E

    word   $C3FC           ; C35C FC C3
    word   $C404           ; C35E 04 C4

    sbc    a,$f1           ; C360 DE F1
    nop                    ; C362 00
    jr     nz,$c365        ; C363 20 00
    ld     b,b             ; C365 40
    ld     a,h             ; C366 7C
    ld     c,$44           ; C367 0E

    word   $C444           ; C368 44 C4
    word   $C44C           ; C36A 4C C4

    add    a,e             ; C36C 83
    pop    af              ; C36D F1
    nop                    ; C36E 00
    ld     b,b             ; C36F 40
    nop                    ; C370 00
    ld     h,b             ; C371 60
    ld     hl,$8c0c        ; C372 21 0C

    word   $C48C           ; C374 8C C4
    word   $C494           ; C376 94 C4

    sbc    a,$f3           ; C378 DE F3
    nop                    ; C37A 00
    ld     h,b             ; C37B 60
    nop                    ; C37C 00
    add    a,b             ; C37D 80
    ld     d,a             ; C37E 57
    inc    c               ; C37F 0C

    word   $C4D4           ; C380 D4 C4
    word   $C4DC           ; C382 DC C4

    call   nz,$f3a8        ; C384 A8 F3
    nop                    ; C386 00
    ret    nz              ; C387 C0
    nop                    ; C388 00
    ret    po              ; C389 E0
    cpl                    ; C38A 2F
    dec    bc              ; C38B 0B

    word   $C51C           ; C38C 1C C5
    word   $C524           ; C38E 24 C5

    ret    po              ; C390 E0
    call   p,$ffff         ; C391 F4 FF FF

    nop                    ; C394 00
    add    a,b             ; C395 80
    nop                    ; C396 00
    add    a,h             ; C397 84

    word   $C420           ; C398 20 C4
    word   $C428           ; C39A 28 C4

    nop                    ; C39C 00
    add    a,h             ; C39D 84
    nop                    ; C39E 00
    adc    a,b             ; C39F 88

    word   $C468           ; C3A0 68 C4
    word   $C470           ; C3A2 70 C4

    nop                    ; C3A4 00
    adc    a,b             ; C3A5 88
    nop                    ; C3A6 00
    adc    a,h             ; C3A7 8C

    word   $C4B0           ; C3A8 B0 C4
    word   $C4B8           ; C3AA B8 C4

    nop                    ; C3AC 00
    adc    a,h             ; C3AD 8C
    nop                    ; C3AE 00
    sub    b               ; C3AF 90

    word   $C4F8           ; C3B0 F8 C4
    word   $C500           ; C3B2 00 C5

    nop                    ; C3B4 00
    sub    b               ; C3B5 90
    nop                    ; C3B6 00
    sbc    a,b             ; C3B7 98

    word   $C540           ; C3B8 40 C5
    word   $C548           ; C3BA 48 C5

    rst    38h             ; C3BC FF
    rst    38h             ; C3BD FF

; "PUSH BUTTON FOR CHECK"

    hex    01 03 50 01 55 01 53 01 48 01 00 00 42 01 55 01 ; C3BE
    hex    54 01 54 01 4F 01 4E 01 00 00 46 01 4F 01 52 01 ; C3CE
    hex    00 00 00 00 43 08 48 08 45 08 43 08 4B 08 FF FF ; C3DE

; "ROM 0"

    hex    03 03 52 03 4F 03 4D 03 00 00 30 03 FF FF       ; C3EE

; "OK"

    hex    03 0A 4F 03 4B 03 FF FF                         ; C3FC

; "ERROR"

    hex    03 0A 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C404

; "RAM 0"

    hex    03 11 52 03 41 03 4D 03 00 00 30 03 FF FF       ; C412

; "OK"

    hex    03 18 4F 03 4B 03 FF FF                         ; C420

; "ERROR"

    hex    03 18 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C428

; "ROM 1"

    hex    04 03 52 03 4F 03 4D 03 00 00 31 03 FF FF       ; C436

: "OK"

    hex    04 0A 4F 03 4B 03 FF FF                         ; C444

; "ERROR"

    hex    04 0A 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C44C

; "RAM 1"

    hex    04 11 52 03 41 03 4D 03 00 00 31 03 FF FF       ; C45A

; "OK"

    hex    04 18 4F 03 4B 03 FF FF                         ; C468

; "ERROR"

    hex    04 18 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C470

; "ROM 2"

    hex    05 03 52 03 4F 03 4D 03 00 00 32 03 FF FF       ; C47E

; "OK"

    hex    05 0A 4F 03 4B 03 FF FF                         ; C48C

; "ERROR"

    hex    05 0A 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C494

; "RAM 2"

    hex    05 11 52 03 41 03 4D 03 00 00 32 03 FF FF       ; C4A2

; "OK"

    hex    05 18 4F 03 4B 03 FF FF                         ; C4B0

; "ERROR"

    hex    05 18 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C4B8

; "ROM 3"

    hex    06 03 52 03 4F 03 4D 03 00 00 33 03 FF FF       ; C4C6

; "OK";

    hex    06 0A 4F 03 4B 03 FF FF                         ; C4D4

; "ERROR"

    hex    06 0A 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C4DC

; "RAM 3"

    hex    06 11 52 03 41 03 4D 03 00 00 33 03 FF FF       ; C4EA

; "OK"

    hex    06 18 4F 03 4B 03 FF FF                         ; C4F8

; "ERROR"

    hex    06 18 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C500

; "ROM 4"

    hex    07 03 52 03 4F 03 4D 03 00 00 34 03 FF FF       ; C50E

; "OK"

    hex    07 0A 4F 03 4B 03 FF FF                         ; C51C

; "ERROR"

    hex    07 0A 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C524

; "RAM 4"

    hex    07 11 52 03 41 03 4D 03 00 00 34 03 FF FF       ; C532

; "OK"

    hex    07 18 4F 03 4B 03 FF FF                         ; C540

; "ERROR"

    hex    07 18 45 02 52 02 52 02 4F 02 52 02 FF FF       ; C548

; "COLOR RGBW"

    hex    08 03 43 01 4F 01 4C 01 4F 01 52 01 28 01 52 01 ; C556
    hex    47 01 42 01 57 01 29 01 FF FF                   ; C566

; "1P"

    hex    16 03 31 03 50 03 FF FF                         ; C570
 
; "2P"

    hex    18 03 32 03 50 03 FF FF                         ; C578

; "COIN"

    hex    1A 03 43 03 4F 03 49 03 4E 03 FF FF             ; C580

; "DIP SW 1"

    hex    1C 03 44 03 49 03 50 03 00 00 53 03 57 03 28 03 ; C58C
    hex    31 03 29 03 FF FF                               ; C59C

; "DIP SW 2"

    hex    1E 03 44 03 49 03 50 03 00 00 53 03 57 03 28 03 ; C5A2
    hex    32 03 29 03 FF FF                               ; C5B2

    cp     (hl)            ; C5B8 BE
    jp     $c3ee           ; C5B9 C3 EE C3
    ld     (hl),$c4        ; C5BC 36 C4
    ld     a,(hl)          ; C5BE 7E
    call   nz,$c4c6        ; C5BF C4 C6 C4
    ld     c,$c5           ; C5C2 0E C5
    ld     (de),a          ; C5C4 12
    call   nz,$c45a        ; C5C5 C4 5A C4
    and    d               ; C5C8 A2
    call   nz,$c4ea        ; C5C9 C4 EA C4
    ld     ($56c5),a       ; C5CC 32 C5 56
    push   bc              ; C5CF C5
    add    a,b             ; C5D0 80
    push   bc              ; C5D1 C5
    ld     (hl),b          ; C5D2 70
    push   bc              ; C5D3 C5
    ld     a,b             ; C5D4 78
    push   bc              ; C5D5 C5
    adc    a,h             ; C5D6 8C
    push   bc              ; C5D7 C5
    and    d               ; C5D8 A2
    push   bc              ; C5D9 C5
    call   po,$f8c5        ; C5DA E4 C5 F8
    push   bc              ; C5DD C5
    inc    c               ; C5DE 0C
    add    a,$20           ; C5DF C6 20
    add    a,$34           ; C5E1 C6 34
    add    a,$16           ; C5E3 C6 16
    ld     c,$30           ; C5E5 0E 30
    dec    b               ; C5E7 05
    jr     nc,$c5ef        ; C5E8 30 05
    jr     nc,$c5f1        ; C5EA 30 05
    jr     nc,$c5f3        ; C5EC 30 05
    jr     nc,$c5f5        ; C5EE 30 05
    jr     nc,$c5f4        ; C5F0 30 02
    jr     nc,$c5f6        ; C5F2 30 02
    jr     nc,$c5f8        ; C5F4 30 02
    rst    38h             ; C5F6 FF
    rst    38h             ; C5F7 FF

    jr     $c608           ; C5F8 18 0E
    jr     nc,$c601        ; C5FA 30 05
    jr     nc,$c603        ; C5FC 30 05
    jr     nc,$c605        ; C5FE 30 05
    jr     nc,$c607        ; C600 30 05
    jr     nc,$c609        ; C602 30 05
    jr     nc,$c608        ; C604 30 02
    jr     nc,$c60a        ; C606 30 02
    jr     nc,$c60c        ; C608 30 02
    rst    38h             ; C60A FF
    rst    38h             ; C60B FF

    ld     a,(de)          ; C60C 1A
    ld     c,$30           ; C60D 0E 30
    dec    b               ; C60F 05
    jr     nc,$c617        ; C610 30 05
    jr     nc,$c619        ; C612 30 05
    jr     nc,$c61b        ; C614 30 05
    nop                    ; C616 00
    dec    b               ; C617 05
    nop                    ; C618 00
    dec    b               ; C619 05
    nop                    ; C61A 00
    dec    b               ; C61B 05
    nop                    ; C61C 00
    dec    b               ; C61D 05
    rst    38h             ; C61E FF
    rst    38h             ; C61F FF

    inc    e               ; C620 1C
    ld     c,$30           ; C621 0E 30
    dec    b               ; C623 05
    jr     nc,$c62b        ; C624 30 05
    jr     nc,$c62d        ; C626 30 05
    jr     nc,$c62f        ; C628 30 05
    jr     nc,$c631        ; C62A 30 05
    jr     nc,$c633        ; C62C 30 05
    jr     nc,$c635        ; C62E 30 05
    jr     nc,$c637        ; C630 30 05
    rst    38h             ; C632 FF
    rst    38h             ; C633 FF

    ld     e,$0e           ; C634 1E 0E
    jr     nc,$c63d        ; C636 30 05
    jr     nc,$c63f        ; C638 30 05
    jr     nc,$c641        ; C63A 30 05
    jr     nc,$c643        ; C63C 30 05
    jr     nc,$c645        ; C63E 30 05
    jr     nc,$c647        ; C640 30 05
    jr     nc,$c649        ; C642 30 05
    jr     nc,$c64b        ; C644 30 05
    rst    38h             ; C646 FF
    rst    38h             ; C647 FF

    ld     bc,$4313        ; C648 01 13 43
    ex     af,af'          ; C64B 08
    ld     c,b             ; C64C 48
    ex     af,af'          ; C64D 08
    ld     b,c             ; C64E 41
    ex     af,af'          ; C64F 08
    ld     c,(hl)          ; C650 4E
    ex     af,af'          ; C651 08
    ld     b,a             ; C652 47
    ex     af,af'          ; C653 08
    ld     b,l             ; C654 45
    ex     af,af'          ; C655 08
    nop                    ; C656 00
    nop                    ; C657 00
    ld     b,h             ; C658 44
    ex     af,af'          ; C659 08
    ld     c,c             ; C65A 49
    ex     af,af'          ; C65B 08
    ld     d,e             ; C65C 53
    ex     af,af'          ; C65D 08
    ld     d,b             ; C65E 50
    ex     af,af'          ; C65F 08
    rst    38h             ; C660 FF
    rst    38h             ; C661 FF

    nop                    ; C662 00
    nop                    ; C663 00
    rrca                   ; C664 0F
    nop                    ; C665 00
    dec    c               ; C666 0D
    nop                    ; C667 00
    dec    bc              ; C668 0B
    nop                    ; C669 00
    add    hl,bc           ; C66A 09
    nop                    ; C66B 00
    rlca                   ; C66C 07
    nop                    ; C66D 00
    dec    b               ; C66E 05
    nop                    ; C66F 00
    inc    bc              ; C670 03
    nop                    ; C671 00
    nop                    ; C672 00
    nop                    ; C673 00
    ret    p               ; C674 F0
    nop                    ; C675 00
    ret    nc              ; C676 D0
    nop                    ; C677 00
    or     b               ; C678 B0
    nop                    ; C679 00
    sub    b               ; C67A 90
    nop                    ; C67B 00
    ld     (hl),b          ; C67C 70
    nop                    ; C67D 00
    ld     d,b             ; C67E 50
    nop                    ; C67F 00
    jr     nc,$c682        ; C680 30 00
    nop                    ; C682 00
    nop                    ; C683 00
    nop                    ; C684 00
    rrca                   ; C685 0F
    nop                    ; C686 00
    dec    c               ; C687 0D
    nop                    ; C688 00
    dec    bc              ; C689 0B
    nop                    ; C68A 00
    add    hl,bc           ; C68B 09
    nop                    ; C68C 00
    rlca                   ; C68D 07
    nop                    ; C68E 00
    dec    b               ; C68F 05
    nop                    ; C690 00
    inc    bc              ; C691 03
    nop                    ; C692 00
    nop                    ; C693 00
    rst    38h             ; C694 FF
    rrca                   ; C695 0F
    db     $dd             ; C696 DD
    dec    c               ; C697 0D
    cp     e               ; C698 BB
    dec    bc              ; C699 0B
    sbc    a,c             ; C69A 99
    add    hl,bc           ; C69B 09
    ld     (hl),a          ; C69C 77
    rlca                   ; C69D 07
    ld     d,l             ; C69E 55
    dec    b               ; C69F 05
    inc    sp              ; C6A0 33
    inc    bc              ; C6A1 03
    or     d               ; C6A2 B2
    add    a,$de           ; C6A3 C6 DE
    add    a,$0a           ; C6A5 C6 0A
    rst    00h             ; C6A7 C7
    ld     (hl),$c7        ; C6A8 36 C7
    ld     h,d             ; C6AA 62
    rst    00h             ; C6AB C7
    adc    a,(hl)          ; C6AC 8E
    rst    00h             ; C6AD C7
    cp     d               ; C6AE BA
    rst    00h             ; C6AF C7
    and    $c7             ; C6B0 E6 C7
    ld     a,(bc)          ; C6B2 0A
    inc    bc              ; C6B3 03
    inc    bc              ; C6B4 03
    rst    38h             ; C6B5 FF
    dec    b               ; C6B6 05
    inc    c               ; C6B7 0C
    nop                    ; C6B8 00
    nop                    ; C6B9 00
    inc    bc              ; C6BA 03
    rst    38h             ; C6BB FF
    ld     b,$0c           ; C6BC 06 0C
    nop                    ; C6BE 00
    nop                    ; C6BF 00
    inc    bc              ; C6C0 03
    rst    38h             ; C6C1 FF
    rlca                   ; C6C2 07
    inc    c               ; C6C3 0C
    nop                    ; C6C4 00
    nop                    ; C6C5 00
    inc    bc              ; C6C6 03
    rst    38h             ; C6C7 FF
    ex     af,af'          ; C6C8 08
    inc    c               ; C6C9 0C
    nop                    ; C6CA 00
    nop                    ; C6CB 00
    inc    bc              ; C6CC 03
    rst    38h             ; C6CD FF
    add    hl,bc           ; C6CE 09
    inc    c               ; C6CF 0C
    nop                    ; C6D0 00
    nop                    ; C6D1 00
    inc    bc              ; C6D2 03
    rst    38h             ; C6D3 FF
    ld     a,(bc)          ; C6D4 0A
    inc    c               ; C6D5 0C
    nop                    ; C6D6 00
    nop                    ; C6D7 00
    inc    bc              ; C6D8 03
    rst    38h             ; C6D9 FF
    dec    bc              ; C6DA 0B
    inc    c               ; C6DB 0C
    rst    38h             ; C6DC FF
    rst    38h             ; C6DD FF

    dec    bc              ; C6DE 0B
    inc    bc              ; C6DF 03
    inc    bc              ; C6E0 03
    rst    38h             ; C6E1 FF
    dec    b               ; C6E2 05
    inc    c               ; C6E3 0C
    nop                    ; C6E4 00
    nop                    ; C6E5 00
    inc    bc              ; C6E6 03
    rst    38h             ; C6E7 FF
    ld     b,$0c           ; C6E8 06 0C
    nop                    ; C6EA 00
    nop                    ; C6EB 00
    inc    bc              ; C6EC 03
    rst    38h             ; C6ED FF
    rlca                   ; C6EE 07
    inc    c               ; C6EF 0C
    nop                    ; C6F0 00
    nop                    ; C6F1 00
    inc    bc              ; C6F2 03
    rst    38h             ; C6F3 FF
    ex     af,af'          ; C6F4 08
    inc    c               ; C6F5 0C
    nop                    ; C6F6 00
    nop                    ; C6F7 00
    inc    bc              ; C6F8 03
    rst    38h             ; C6F9 FF
    add    hl,bc           ; C6FA 09
    inc    c               ; C6FB 0C
    nop                    ; C6FC 00
    nop                    ; C6FD 00
    inc    bc              ; C6FE 03
    rst    38h             ; C6FF FF
    ld     a,(bc)          ; C700 0A
    inc    c               ; C701 0C
    nop                    ; C702 00
    nop                    ; C703 00
    inc    bc              ; C704 03
    rst    38h             ; C705 FF
    dec    bc              ; C706 0B
    inc    c               ; C707 0C
    rst    38h             ; C708 FF
    rst    38h             ; C709 FF
    dec    c               ; C70A 0D
    inc    bc              ; C70B 03
    inc    bc              ; C70C 03
    rst    38h             ; C70D FF
    dec    b               ; C70E 05
    dec    c               ; C70F 0D
    nop                    ; C710 00
    nop                    ; C711 00
    inc    bc              ; C712 03
    rst    38h             ; C713 FF
    ld     b,$0d           ; C714 06 0D
    nop                    ; C716 00
    nop                    ; C717 00
    inc    bc              ; C718 03
    rst    38h             ; C719 FF
    rlca                   ; C71A 07
    dec    c               ; C71B 0D
    nop                    ; C71C 00
    nop                    ; C71D 00
    inc    bc              ; C71E 03
    rst    38h             ; C71F FF
    ex     af,af'          ; C720 08
    dec    c               ; C721 0D
    nop                    ; C722 00
    nop                    ; C723 00
    inc    bc              ; C724 03
    rst    38h             ; C725 FF
    add    hl,bc           ; C726 09
    dec    c               ; C727 0D
    nop                    ; C728 00
    nop                    ; C729 00
    inc    bc              ; C72A 03
    rst    38h             ; C72B FF
    ld     a,(bc)          ; C72C 0A
    dec    c               ; C72D 0D
    nop                    ; C72E 00
    nop                    ; C72F 00
    inc    bc              ; C730 03
    rst    38h             ; C731 FF
    dec    bc              ; C732 0B
    dec    c               ; C733 0D
    rst    38h             ; C734 FF
    rst    38h             ; C735 FF

    ld     c,$03           ; C736 0E 03
    inc    bc              ; C738 03
    rst    38h             ; C739 FF
    dec    b               ; C73A 05
    dec    c               ; C73B 0D
    nop                    ; C73C 00
    nop                    ; C73D 00
    inc    bc              ; C73E 03
    rst    38h             ; C73F FF
    ld     b,$0d           ; C740 06 0D
    nop                    ; C742 00
    nop                    ; C743 00
    inc    bc              ; C744 03
    rst    38h             ; C745 FF
    rlca                   ; C746 07
    dec    c               ; C747 0D
    nop                    ; C748 00
    nop                    ; C749 00
    inc    bc              ; C74A 03
    rst    38h             ; C74B FF
    ex     af,af'          ; C74C 08
    dec    c               ; C74D 0D
    nop                    ; C74E 00
    nop                    ; C74F 00
    inc    bc              ; C750 03
    rst    38h             ; C751 FF
    add    hl,bc           ; C752 09
    dec    c               ; C753 0D
    nop                    ; C754 00
    nop                    ; C755 00
    inc    bc              ; C756 03
    rst    38h             ; C757 FF
    ld     a,(bc)          ; C758 0A
    dec    c               ; C759 0D
    nop                    ; C75A 00
    nop                    ; C75B 00
    inc    bc              ; C75C 03
    rst    38h             ; C75D FF
    dec    bc              ; C75E 0B
    dec    c               ; C75F 0D
    rst    38h             ; C760 FF
    rst    38h             ; C761 FF

    djnz   $c767           ; C762 10 03
    inc    bc              ; C764 03
    rst    38h             ; C765 FF
    dec    b               ; C766 05
    ld     c,$00           ; C767 0E 00
    nop                    ; C769 00
    inc    bc              ; C76A 03
    rst    38h             ; C76B FF
    ld     b,$0e           ; C76C 06 0E
    nop                    ; C76E 00
    nop                    ; C76F 00
    inc    bc              ; C770 03
    rst    38h             ; C771 FF
    rlca                   ; C772 07
    ld     c,$00           ; C773 0E 00
    nop                    ; C775 00
    inc    bc              ; C776 03
    rst    38h             ; C777 FF
    ex     af,af'          ; C778 08
    ld     c,$00           ; C779 0E 00
    nop                    ; C77B 00
    inc    bc              ; C77C 03
    rst    38h             ; C77D FF
    add    hl,bc           ; C77E 09
    ld     c,$00           ; C77F 0E 00
    nop                    ; C781 00
    inc    bc              ; C782 03
    rst    38h             ; C783 FF
    ld     a,(bc)          ; C784 0A
    ld     c,$00           ; C785 0E 00
    nop                    ; C787 00
    inc    bc              ; C788 03
    rst    38h             ; C789 FF
    dec    bc              ; C78A 0B
    ld     c,$ff           ; C78B 0E FF
    rst    38h             ; C78D FF

    ld     de,$0303        ; C78E 11 03 03
    rst    38h             ; C791 FF
    dec    b               ; C792 05
    ld     c,$00           ; C793 0E 00
    nop                    ; C795 00
    inc    bc              ; C796 03
    rst    38h             ; C797 FF
    ld     b,$0e           ; C798 06 0E
    nop                    ; C79A 00
    nop                    ; C79B 00
    inc    bc              ; C79C 03
    rst    38h             ; C79D FF
    rlca                   ; C79E 07
    ld     c,$00           ; C79F 0E 00
    nop                    ; C7A1 00
    inc    bc              ; C7A2 03
    rst    38h             ; C7A3 FF
    ex     af,af'          ; C7A4 08
    ld     c,$00           ; C7A5 0E 00
    nop                    ; C7A7 00
    inc    bc              ; C7A8 03
    rst    38h             ; C7A9 FF
    add    hl,bc           ; C7AA 09
    ld     c,$00           ; C7AB 0E 00
    nop                    ; C7AD 00
    inc    bc              ; C7AE 03
    rst    38h             ; C7AF FF
    ld     a,(bc)          ; C7B0 0A
    ld     c,$00           ; C7B1 0E 00
    nop                    ; C7B3 00
    inc    bc              ; C7B4 03
    rst    38h             ; C7B5 FF
    dec    bc              ; C7B6 0B
    ld     c,$ff           ; C7B7 0E FF
    rst    38h             ; C7B9 FF

    inc    de              ; C7BA 13
    inc    bc              ; C7BB 03
    inc    bc              ; C7BC 03
    rst    38h             ; C7BD FF
    dec    b               ; C7BE 05
    rrca                   ; C7BF 0F
    nop                    ; C7C0 00
    nop                    ; C7C1 00
    inc    bc              ; C7C2 03
    rst    38h             ; C7C3 FF
    ld     b,$0f           ; C7C4 06 0F
    nop                    ; C7C6 00
    nop                    ; C7C7 00
    inc    bc              ; C7C8 03
    rst    38h             ; C7C9 FF
    rlca                   ; C7CA 07
    rrca                   ; C7CB 0F
    nop                    ; C7CC 00
    nop                    ; C7CD 00
    inc    bc              ; C7CE 03
    rst    38h             ; C7CF FF
    ex     af,af'          ; C7D0 08
    rrca                   ; C7D1 0F
    nop                    ; C7D2 00
    nop                    ; C7D3 00
    inc    bc              ; C7D4 03
    rst    38h             ; C7D5 FF
    add    hl,bc           ; C7D6 09
    rrca                   ; C7D7 0F
    nop                    ; C7D8 00
    nop                    ; C7D9 00
    inc    bc              ; C7DA 03
    rst    38h             ; C7DB FF
    ld     a,(bc)          ; C7DC 0A
    rrca                   ; C7DD 0F
    nop                    ; C7DE 00
    nop                    ; C7DF 00
    inc    bc              ; C7E0 03
    rst    38h             ; C7E1 FF
    dec    bc              ; C7E2 0B
    rrca                   ; C7E3 0F
    rst    38h             ; C7E4 FF
    rst    38h             ; C7E5 FF

    inc    d               ; C7E6 14
    inc    bc              ; C7E7 03
    inc    bc              ; C7E8 03
    rst    38h             ; C7E9 FF
    dec    b               ; C7EA 05
    rrca                   ; C7EB 0F
    nop                    ; C7EC 00
    nop                    ; C7ED 00
    inc    bc              ; C7EE 03
    rst    38h             ; C7EF FF
    ld     b,$0f           ; C7F0 06 0F
    nop                    ; C7F2 00
    nop                    ; C7F3 00
    inc    bc              ; C7F4 03
    rst    38h             ; C7F5 FF
    rlca                   ; C7F6 07
    rrca                   ; C7F7 0F
    nop                    ; C7F8 00
    nop                    ; C7F9 00
    inc    bc              ; C7FA 03
    rst    38h             ; C7FB FF
    ex     af,af'          ; C7FC 08
    rrca                   ; C7FD 0F
    nop                    ; C7FE 00
    nop                    ; C7FF 00
    inc    bc              ; C800 03
    rst    38h             ; C801 FF
    add    hl,bc           ; C802 09
    rrca                   ; C803 0F
    nop                    ; C804 00
    nop                    ; C805 00
    inc    bc              ; C806 03
    rst    38h             ; C807 FF
    ld     a,(bc)          ; C808 0A
    rrca                   ; C809 0F
    nop                    ; C80A 00
    nop                    ; C80B 00
    inc    bc              ; C80C 03
    rst    38h             ; C80D FF
    dec    bc              ; C80E 0B
    rrca                   ; C80F 0F
    rst    38h             ; C810 FF
    rst    38h             ; C811 FF

    inc    e               ; C812 1C
    inc    c               ; C813 0C
    ld     b,e             ; C814 43
    ld     b,$52           ; C815 06 52
    ld     b,$45           ; C817 06 45
    ld     b,$44           ; C819 06 44
    ld     b,$49           ; C81B 06 49
    ld     b,$54           ; C81D 06 54
    ld     b,$ff           ; C81F 06 FF
    rst    38h             ; C821 FF

    inc    e               ; C822 1C
    ld     (de),a          ; C823 12
    rla                    ; C824 17
    add    a,b             ; C825 80
    dec    b               ; C826 05
    ld     (bc),a          ; C827 02

; C828h - Bomb data vector ?

    word   $C848           ; C828 48 C8
    word   $C860           ; C82A 60 C8
    word   $C878           ; C82C 78 C8
    word   $C890           ; C82E 90 C8
    word   $C8A8           ; C830 A8 C8
    word   $C8C0           ; C832 C0 C8
    word   $C8D8           ; C834 D8 C8
    word   $C8F0           ; C836 F0 C8
    word   $C908           ; C838 08 C9
    word   $C920           ; C83A 20 C9
    word   $C938           ; C83C 38 C9
    word   $C950           ; C83E 50 C9
    word   $C968           ; C840 68 C9
    word   $C980           ; C842 80 C9
    word   $C998           ; C844 98 C9
    word   $C9B0           ; C846 B0 C9

; C848h - Bomb data for level 1 ?

    hex    42 52 62 72 83 84 85 86 03 04 05 06 10 20 30 77 ; C848
    hex    67 57 38 28 18 80 70 60                         ; C858

; C860h - Bomb data for level 2 ?

    hex    32 42 52 73 74 75 76 77 25 24 23 13 14 15 16 17 ; C860
    hex    63 64 65 71 51 41 31 11                         ; C870

    hex    23 13 03 04 05 06 07 87 86 85 84 83 73 63 66 56 ; C878
    hex    36 26 51 41 10 00 70 80                         ; C888

    hex    51 52 53 68 67 66 85 75 65 25 15 05 28 27 26 31 ; C890
    hex    32 33 10 11 12 70 71 72                         ; C8A0

    hex    71 72 73 75 76 77 51 52 53 35 36 37 11 12 13 15 ; C8A8
    hex    16 17 55 56 57 31 32 33                         ; C8B8

    hex    81 71 61 21 11 01 23 33 53 63 85 75 65 25 15 05 ; C8C0
    hex    27 37 57 67 78 88 18 08                         ; C8D0

    hex    56 57 58 54 64 74 36 37 38 34 24 14 11 21 31 51 ; C8D8
    hex    61 71 06 07 08 86 87 88                         ; C8E8

    hex    56 66 76 36 26 16 64 74 84 24 14 04 61 71 81 21 ; C8F0
    hex    11 01 08 18 28 68 78 88                         ; C900

    hex    06 16 26 35 34 33 53 54 55 66 76 86 51 61 70 80 ; C908
    hex    31 21 10 00 28 38 58 68                         ; C918

    hex    51 61 71 02 03 04 15 16 17 31 21 11 82 83 84 75 ; C920
    hex    76 77 55 54 53 33 34 35                         ; C930

    hex    23 22 21 11 12 13 16 26 36 54 64 74 66 67 68 82 ; C938
    hex    72 62 86 87 88 70 60 50                         ; C948

    hex    01 11 21 14 24 34 16 26 36 56 66 76 54 64 74 61 ; C950
    hex    71 81 52 42 32 50 40 30                         ; C960

    hex    51 52 53 64 65 66 22 23 24 35 36 37 70 71 72 86 ; C968
    hex    87 88 10 11 12 06 07 08                         ; C978

    hex    72 73 74 14 15 16 26 27 28 43 42 41 22 12 02 57 ; C980
    hex    67 77 84 83 82 60 61 62                         ; C990

    hex    00 01 02 13 14 15 74 75 76 86 87 88 28 18 08 60 ; C998
    hex    70 80 73 83 82 52 42 32                         ; C9A8

    hex    55 56 57 24 23 22 85 84 83 70 60 50 30 20 10 03 ; C9B0
    hex    04 05 62 63 64 37 36 35                         ; C9C0

    jr     $c9fa           ; C9C8 18 30
    ld     c,b             ; C9CA 48
    ld     h,b             ; C9CB 60
    ld     a,b             ; C9CC 78
    sub    b               ; C9CD 90
    xor    b               ; C9CE A8
    ret    nz              ; C9CF C0
    ret    c               ; C9D0 D8

; "SIDE-ONE"

    hex    00 03 53 00 49 00 44 00 45 00 2D 00 4F 00 4E 00 ; C9D1
    hex    45 00 FF FF                                     ; C9E1

; "HI-SCORE"

    hex    1E 16 48 00 49 00 2D 00 53 00 43 00 4F 00 52 00 ; C9E5
    hex    45 00 FF FF                                     ; C9F5

; "SIDE-TWO" (when it's player 1's turn)

    hex    00 15 53 00 49 00 44 00 45 00 2D 00 54 00 57 00 ; C9F9
    hex    4F 00 FF FF                                     ; CA09

; "SIDE-ONE"

    hex    00 03 53 0F 49 0F 44 0F 45 0F 2D 0F 4F 0F 4E 0F ; CA0D
    hex    45 0F FF FF                                     ; CA1D

; "HI-SCORE"

    hex    1E 16 48 0F 49 0F 2D 0F 53 0F 43 0F 4F 0F 52 0F ; CA21
    hex    45 0F FF FF                                     ; CA31

; "SIDE-TW0" (sic) (when it's player 2's turn)

    hex    00 15 53 0F 49 0F 44 0F 45 0F 2D 0F 54 0F 57 0F ; CA35
    hex    30 0F FF FF                                     ; CA45

    ld     bc,$b503        ; CA49 01 03 B5
    add    a,c             ; CA4C 81
    dec    b               ; CA4D 05
    ex     af,af'          ; CA4E 08
    ld     bc,$9c03        ; CA4F 01 03 9C
    add    a,c             ; CA52 81
    dec    b               ; CA53 05
    ex     af,af'          ; CA54 08
    rra                    ; CA55 1F
    ld     d,$e2           ; CA56 16 E2
    add    a,b             ; CA58 80
    dec    b               ; CA59 05
    ex     af,af'          ; CA5A 08
    ld     bc,$ce15        ; CA5B 01 15 CE
    add    a,c             ; CA5E 81
    dec    b               ; CA5F 05
    ex     af,af'          ; CA60 08
    ld     bc,$9c15        ; CA61 01 15 9C
    add    a,c             ; CA64 81
    dec    b               ; CA65 05
    ex     af,af'          ; CA66 08

    word   $C9D1           ; CA67 D1 C9
    word   $C9E5           ; CA69 E5 C9
    word   $C9F9           ; CA6B F9 C9
    word   $C9D1           ; CA6D D1 C9
    word   $C9E5           ; CA6F E5 C9
    word   $CA97           ; CA71 97 CA
    word   $CA9F           ; CA73 9F CA
    word   $CA49           ; CA75 49 CA
    word   $CA55           ; CA77 55 CA
    word   $CA5B           ; CA79 5B CA
    word   $CA49           ; CA7B 49 CA
    word   $CA55           ; CA7D 55 CA
    word   $CA97           ; CA7F 97 CA
    word   $CA9F           ; CA81 9F CA
    word   $CA0D           ; CA83 0D CA
    word   $C9E5           ; CA85 E5 C9
    word   $C9F9           ; CA87 F9 C9
    word   $CA0D           ; CA89 0D CA
    word   $C9E5           ; CA8B E5 C9
    word   $CA97           ; CA8D 97 CA
    word   $CA9F           ; CA8F 9F CA
    word   $C9D1           ; CA91 D1 C9
    word   $C9E5           ; CA93 E5 C9
    word   $CA35           ; CA95 35 CA

    jp     z,$1500         ; CA97 00 15
    ex     af,af'          ; CA99 08
    rst    38h             ; CA9A FF
    inc    h               ; CA9B 24
    nop                    ; CA9C 00
    rst    38h             ; CA9D FF
    rst    38h             ; CA9E FF

    ld     bc,$0815        ; CA9F 01 15 08
    rst    38h             ; CAA2 FF
    inc    h               ; CAA3 24
    nop                    ; CAA4 00
    rst    38h             ; CAA5 FF
    rst    38h             ; CAA6 FF

    ld     e,$10           ; CAA7 1E 10
    ld     d,d             ; CAA9 52
    ld     bc,$014f        ; CAAA 01 4F 01
    ld     d,l             ; CAAD 55
    ld     bc,$014e        ; CAAE 01 4E 01
    ld     b,h             ; CAB1 44
    ld     bc,$ffff        ; CAB2 01 FF FF

    rra                    ; CAB5 1F
    ld     de,$81b9        ; CAB6 11 B9 81
    inc    bc              ; CAB9 03
    ld     (bc),a          ; CABA 02
    rra                    ; CABB 1F
    ld     de,$81d2        ; CABC 11 D2 81
    inc    bc              ; CABF 03
    ld     (bc),a          ; CAC0 02
    rra                    ; CAC1 1F
    ld     de,$81af        ; CAC2 11 AF 81
    inc    bc              ; CAC5 03
    ld     (bc),a          ; CAC6 02
    rra                    ; CAC7 1F
    djnz   $caca           ; CAC8 10 00
    nop                    ; CACA 00
    dec    l               ; CACB 2D
    inc    bc              ; CACC 03
    rst    38h             ; CACD FF
    rst    38h             ; CACE FF

    rra                    ; CACF 1F
    djnz   $caff           ; CAD0 10 2D
    inc    bc              ; CAD2 03
    rst    38h             ; CAD3 FF
    rst    38h             ; CAD4 FF

    rra                    ; CAD5 1F
    inc    de              ; CAD6 13
    dec    l               ; CAD7 2D
    inc    bc              ; CAD8 03
    rst    38h             ; CAD9 FF
    rst    38h             ; CADA FF

    word   $CAFB           ; CADB FB CA
    word   $CB03           ; CADD 03 CB
    word   $CB0B           ; CADF 0B CB
    word   $CB17           ; CAE1 17 CB
    word   $CB23           ; CAE3 23 CB
    word   $CB33           ; CAE5 33 CB
    word   $CB43           ; CAE7 43 CB
    word   $CB57           ; CAE9 57 CB
    word   $CB6B           ; CAEB 6B CB
    word   $CB83           ; CAED 83 CB
    word   $CB9B           ; CAEF 9B CB
    word   $CBB7           ; CAF1 B7 CB
    word   $CBD3           ; CAF3 D3 CB
    word   $CBF3           ; CAF5 F3 CB
    word   $CC13           ; CAF7 13 CC
    word   $CC33           ; CAF9 33 CC

    hex    1E 02 0E FF 24 00 FF FF                         ; CAFB

    hex    1F 02 0E FF 24 00 FF FF                         ; CB03

    hex    1E 02 0E 00 0C 00 0C FF 24 00 FF FF             ; CB0B

    hex    1F 02 0F 00 0D 00 0C FF 24 00 FF FF             ; CB17

    hex    1E 02 0E 00 0C 00 0E 00 0C 00 0A FF 24 00 FF FF ; CB23

    hex    1F 02 0F 00 0D 00 0F 00 0D 00 0A FF 24 00 FF FF ; CB33

    hex    1E 02 0E 00 0C 00 0E 00 0C 00 0E 00 0C 00 08 FF ; CB43
    hex    24 00 FF FF                                     ; CB53

    hex    1F 02 0F 00 0D 00 0F 00 0D 00 0F 00 0D 00 08 FF ; CB57
    hex    24 00 FF FF                                     ; CB67

    hex    1E 02 0E 00 0C 00 0E 00 0C 00 0E 00 0C 00 0E 00 ; CB6B
    hex    0C 00 06 FF 24 00 FF FF                         ; CB7B

    hex    1F 02 0F 00 0D 00 0F 00 0D 00 0F 00 0D 00 0F 00 ; CB83
    hex    0D 00 06 FF 24 00 FF FF                         ; CB93

    hex    1E 02 0E 00 0C 00 0E 00 0C 00 0E 00 0C 00 0E 00 ; CB9B
    hex    0C 00 0E 00 0C 00 04 FF 24 00 FF FF             ; CBAB

    hex    1F 02 0F 00 0D 00 0F 00 0D 00 0F 00 0D 00 0F 00 ; CBB7
    hex    0D 00 0F 00 0D 00 04 FF 24 00 FF FF             ; CBC7

    hex    1E 02 0E 00 0C 00 0E 00 0C 00 0E 00 0C 00 0E 00 ; CBD3
    hex    0C 00 0E 00 0C 00 0E 00 0C 00 02 FF 24 00 FF FF ; CBE3

    hex    1F 02 0F 00 0D 00 0F 00 0D 00 0F 00 0D 00 0F 00 ; CBF3
    hex    0D 00 0F 00 0D 00 0F 00 0D 00 02 FF 24 00 FF FF ; CC03

    hex    1E 02 0E 00 0C 00 0E 00 0C 00 0E 00 0C 00 0E 00 ; CC13
    hex    0C 00 0E 00 0C 00 0E 00 0C 00 0E 00 0C 00 FF FF ; CC23

    hex    1F 02 0F 00 0D 00 0F 00 0D 00 0F 00 0D 00 0F 00 ; CC33
    hex    0D 00 0F 00 0D 00 0F 00 0D 00 0F 00 0D 00 FF FF ; CC43

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CC53
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CC63
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CC73
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CC83
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CC93
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CCA3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CCB3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CCC3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CCD3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CCE3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CCF3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD03
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD13
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD23
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD33
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD43
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD53
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD63
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD73
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD83
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CD93
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CDA3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CDB3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CDC3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CDD3
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CDE3
    hex    00 00 00 00 00 00                               ; CDF3

    adc    a,$00           ; CDF9 CE 00
    ld     l,l             ; CDFB 6D
    nop                    ; CDFC 00
    ld     l,(hl)          ; CDFD 6E
    nop                    ; CDFE 00
    ld     l,a             ; CDFF 6F
    jr     $ce0a           ; CE00 18 08
    ld     b,$10           ; CE02 06 10
    ld     e,$00           ; CE04 1E 00
    ld     (bc),a          ; CE06 02
    dec    b               ; CE07 05
    dec    b               ; CE08 05
    dec    d               ; CE09 15
    ld     (bc),a          ; CE0A 02
    ld     de,$0103        ; CE0B 11 03 01
    inc    b               ; CE0E 04
    ld     de,$0105        ; CE0F 11 05 01
    dec    b               ; CE12 05
    ld     de,$0504        ; CE13 11 04 05
    inc    bc              ; CE16 03
    dec    d               ; CE17 15
    ld     a,(bc)          ; CE18 0A
    dec    b               ; CE19 05
    inc    b               ; CE1A 04
    dec    d               ; CE1B 15
    inc    b               ; CE1C 04
    dec    b               ; CE1D 05
    dec    b               ; CE1E 05
    dec    d               ; CE1F 15
    ld     b,$05           ; CE20 06 05
    inc    bc              ; CE22 03
    ld     bc,$0942        ; CE23 01 42 09
    ld     bc,$0108        ; CE26 01 08 01
    nop                    ; CE29 00
    ld     b,$10           ; CE2A 06 10
    inc    bc              ; CE2C 03
    nop                    ; CE2D 00
    inc    c               ; CE2E 0C
    inc    b               ; CE2F 04
    inc    c               ; CE30 0C
    ld     b,$0c           ; CE31 06 0C
    ld     (bc),a          ; CE33 02
    ld     b,$12           ; CE34 06 12
    dec    b               ; CE36 05
    ld     (bc),a          ; CE37 02
    inc    b               ; CE38 04
    ld     (de),a          ; CE39 12
    inc    b               ; CE3A 04
    ld     (bc),a          ; CE3B 02
    rlca                   ; CE3C 07
    ld     (de),a          ; CE3D 12
    inc    bc              ; CE3E 03
    ld     (bc),a          ; CE3F 02
    inc    c               ; CE40 0C
    ld     (de),a          ; CE41 12
    dec    b               ; CE42 05
    ld     (bc),a          ; CE43 02
    dec    bc              ; CE44 0B
    ld     a,(bc)          ; CE45 0A
    inc    c               ; CE46 0C
    nop                    ; CE47 00
    ex     af,af'          ; CE48 08
    djnz   $ce4f           ; CE49 10 04
    inc    b               ; CE4B 04
    inc    b               ; CE4C 04
    nop                    ; CE4D 00
    ld     bc,$0f08        ; CE4E 01 08 0F
    add    hl,bc           ; CE51 09
    inc    bc              ; CE52 03
    ld     bc,$0011        ; CE53 01 11 00
    inc    bc              ; CE56 03
    djnz   $ce5a           ; CE57 10 01
    ld     (de),a          ; CE59 12
    ld     (bc),a          ; CE5A 02
    ld     d,$37           ; CE5B 16 37
    ld     b,$06           ; CE5D 06 06
    ld     d,$06           ; CE5F 16 06
    ld     b,$05           ; CE61 06 05
    ld     d,$02           ; CE63 16 02
    ld     (bc),a          ; CE65 02
    inc    sp              ; CE66 33
    nop                    ; CE67 00
    inc    bc              ; CE68 03
    djnz   $ce70           ; CE69 10 05
    ld     de,$0101        ; CE6B 11 01 01
    ex     af,af'          ; CE6E 08
    ld     de,$0104        ; CE6F 11 04 01
    add    hl,bc           ; CE72 09
    ld     de,$1901        ; CE73 11 01 19
    ld     hl,$0c09        ; CE76 21 09 0C
    nop                    ; CE79 00
    inc    b               ; CE7A 04
    djnz   $ce7f           ; CE7B 10 02
    ld     (de),a          ; CE7D 12
    rla                    ; CE7E 17
    ld     b,$02           ; CE7F 06 02
    inc    b               ; CE81 04
    dec    b               ; CE82 05
    dec    b               ; CE83 05
    ld     bc,$0501        ; CE84 01 01 05
    add    hl,bc           ; CE87 09
    inc    d               ; CE88 14
    add    hl,de           ; CE89 19
    ld     (bc),a          ; CE8A 02
    add    hl,bc           ; CE8B 09
    inc    bc              ; CE8C 03
    ex     af,af'          ; CE8D 08
    ld     (bc),a          ; CE8E 02
    ld     a,(bc)          ; CE8F 0A
    ld     bc,$0702        ; CE90 01 02 07
    nop                    ; CE93 00
    inc    b               ; CE94 04
    djnz   $ce9f           ; CE95 10 08
    nop                    ; CE97 00
    ld     b,$10           ; CE98 06 10
    inc    b               ; CE9A 04
    jr     $cea8           ; CE9B 18 0B
    ex     af,af'          ; CE9D 08
    inc    bc              ; CE9E 03
    nop                    ; CE9F 00
    dec    b               ; CEA0 05
    djnz   $ceab           ; CEA1 10 08
    nop                    ; CEA3 00
    ld     bc,$0b10        ; CEA4 01 10 0B
    jr     $ceb3           ; CEA7 18 0A
    ex     af,af'          ; CEA9 08
    dec    b               ; CEAA 05
    nop                    ; CEAB 00
    dec    b               ; CEAC 05
    djnz   $ceb6           ; CEAD 10 07
    nop                    ; CEAF 00
    ld     a,(bc)          ; CEB0 0A
    jr     $cebb           ; CEB1 18 08
    ex     af,af'          ; CEB3 08
    rlca                   ; CEB4 07
    nop                    ; CEB5 00
    inc    b               ; CEB6 04
    djnz   $cebf           ; CEB7 10 06
    nop                    ; CEB9 00
    inc    bc              ; CEBA 03
    ex     af,af'          ; CEBB 08
    dec    bc              ; CEBC 0B
    jr     $cec8           ; CEBD 18 09
    ex     af,af'          ; CEBF 08
    inc    c               ; CEC0 0C
    nop                    ; CEC1 00
    jr     nz,$cec4        ; CEC2 20 00
    inc    c               ; CEC4 0C
    ld     a,(bc)          ; CEC5 0A
    ld     a,(bc)          ; CEC6 0A
    add    hl,bc           ; CEC7 09
    ld     l,e             ; CEC8 6B
    nop                    ; CEC9 00
    ld     (de),a          ; CECA 12
    ld     (bc),a          ; CECB 02
    dec    b               ; CECC 05
    ld     (de),a          ; CECD 12
    ld     a,(bc)          ; CECE 0A
    ld     b,$03           ; CECF 06 03
    inc    b               ; CED1 04
    inc    bc              ; CED2 03
    dec    b               ; CED3 05
    inc    b               ; CED4 04
    inc    b               ; CED5 04
    inc    c               ; CED6 0C
    ld     b,$04           ; CED7 06 04
    ld     d,$09           ; CED9 16 09
    ld     (de),a          ; CEDB 12
    inc    b               ; CEDC 04
    djnz   $cefc           ; CEDD 10 1D
    ld     bc,$0001        ; CEDF 01 01 00
    ld     bc,$1108        ; CEE2 01 08 11
    ld     a,(bc)          ; CEE5 0A
    ld     bc,$2108        ; CEE6 01 08 21
    nop                    ; CEE9 00
    ld     (bc),a          ; CEEA 02
    ld     bc,$1103        ; CEEB 01 03 11
    ld     bc,$0210        ; CEEE 01 10 02
    ld     (de),a          ; CEF1 12
    ld     b,$02           ; CEF2 06 02
    ld     b,$12           ; CEF4 06 12
    ld     c,$02           ; CEF6 0E 02
    ld     b,$12           ; CEF8 06 12
    inc    b               ; CEFA 04
    ld     (bc),a          ; CEFB 02
    dec    b               ; CEFC 05
    ld     (de),a          ; CEFD 12
    ld     bc,$0c1a        ; CEFE 01 1A 0C
    ld     a,(bc)          ; CF01 0A
    ld     d,a             ; CF02 57
    nop                    ; CF03 00
    dec    b               ; CF04 05
    djnz   $cf0a           ; CF05 10 03
    nop                    ; CF07 00
    jr     c,$cf0e         ; CF08 38 04
    dec    d               ; CF0A 15
    dec    b               ; CF0B 05
    ld     e,$01           ; CF0C 1E 01
    dec    de              ; CF0E 1B
    add    hl,bc           ; CF0F 09
    dec    b               ; CF10 05
    ex     af,af'          ; CF11 08
    dec    c               ; CF12 0D
    nop                    ; CF13 00
    inc    c               ; CF14 0C
    ld     bc,$1108        ; CF15 01 08 11
    dec    b               ; CF18 05
    ld     bc,$1106        ; CF19 01 06 11
    inc    b               ; CF1C 04
    ld     bc,$1107        ; CF1D 01 07 11
    inc    b               ; CF20 04
    ld     bc,$1107        ; CF21 01 07 11
    ld     b,$01           ; CF24 06 01
    inc    bc              ; CF26 03
    add    hl,bc           ; CF27 09
    dec    b               ; CF28 05
    nop                    ; CF29 00
    dec    b               ; CF2A 05
    ld     (bc),a          ; CF2B 02
    ld     a,(bc)          ; CF2C 0A
    nop                    ; CF2D 00
    inc    sp              ; CF2E 33
    ld     (bc),a          ; CF2F 02
    ld     d,a             ; CF30 57
    ld     a,(bc)          ; CF31 0A
    dec    b               ; CF32 05
    ld     (bc),a          ; CF33 02
    ld     (bc),a          ; CF34 02
    ld     (de),a          ; CF35 12
    dec    b               ; CF36 05
    ld     d,$3f           ; CF37 16 3F
    ld     b,$04           ; CF39 06 04
    inc    b               ; CF3B 04
    rlca                   ; CF3C 07
    dec    b               ; CF3D 05
    ld     a,(de)          ; CF3E 1A
    ld     bc,$00a0        ; CF3F 01 A0 00
    ld     (de),a          ; CF42 12
    ld     bc,$0015        ; CF43 01 15 00
    dec    d               ; CF46 15
    ld     b,$10           ; CF47 06 10
    nop                    ; CF49 00
    dec    b               ; CF4A 05
    dec    b               ; CF4B 05
    inc    bc              ; CF4C 03
    dec    d               ; CF4D 15
    inc    bc              ; CF4E 03
    dec    b               ; CF4F 05
    dec    b               ; CF50 05
    dec    d               ; CF51 15
    inc    bc              ; CF52 03
    dec    b               ; CF53 05
    inc    bc              ; CF54 03
    dec    d               ; CF55 15
    and    b               ; CF56 A0
    dec    b               ; CF57 05
    inc    bc              ; CF58 03
    djnz   $cfb5           ; CF59 10 5A
    dec    b               ; CF5B 05
    inc    bc              ; CF5C 03
    ld     d,$03           ; CF5D 16 03
    ld     b,$03           ; CF5F 06 03
    ld     d,$03           ; CF61 16 03
    ld     b,$03           ; CF63 06 03
    ld     d,$03           ; CF65 16 03
    ld     b,$03           ; CF67 06 03
    ld     d,$03           ; CF69 16 03
    ld     d,$03           ; CF6B 16 03
    ld     b,$ff           ; CF6D 06 FF
    rst    38h             ; CF6F FF

    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CF70
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CF80
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CF90
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CFA0
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CFB0
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CFC0
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CFD0
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CFE0
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; CFF0

    word   $D008           ; D000 08 D0
    word   $D06E           ; D002 6E D0
    word   $D084           ; D004 84 D0
    word   $D08C           ; D006 8C D0

; "PUSH"

    hex    0F 0E 50 0F 55 0F 53 0F 48 0F FF FF             ; D008

; "1 PLAYER BUTTON ONLY"

    hex    11 06 31 05 00 00 50 05 4C 05 41 05 59 05 45 05 ; D014
    hex    52 05 00 00 42 05 55 05 54 05 54 05 4F 05 4E 05 ; D024
    hex    00 00 4F 05 4E 05 4C 05 59 05 FF FF             ; D034

; "1 OR 2 PLAYERS BUTTON"

    hex    11 06 31 05 00 00 4F 05 52 05 00 00 32 05 00 00 ; D040
    hex    50 05 4C 05 41 05 59 05 45 05 52 05 53 05 00 00 ; D050
    hex    42 05 55 05 54 05 54 05 4F 05 4E 05 FF FF       ; D060

; "PRESENTED"

    hex    14 0B 50 05 52 05 45 05 53 05 45 05 4E 05 54 05 ; D06E
    hex    45 05 44 05 FF FF                               ; D07E

; "BY"

    hex    16 0F 42 05 59 05 FF FF                         ; D084

; "TEHKAN LTD."

    hex    18 0B 54 01 45 01 48 01 4B 01 41 01 4E 01 00 00 ; D08C
    hex    4C 05 54 05 44 05 2E 05 FF FF                   ; D09C

    word   $D0D6           ; D0A6 D6 D0
    word   $D0F0           ; D0A8 F0 D0
    word   $D0D6           ; D0AA D6 D0
    word   $D100           ; D0AC 00 D1
    word   $D0D6           ; D0AE D6 D0
    word   $D110           ; D0B0 10 D1
    word   $D120           ; D0B2 20 D1
    word   $D12E           ; D0B4 2E D1
    word   $D120           ; D0B6 20 D1
    word   $D13E           ; D0B8 3E D1
    word   $D14E           ; D0BA 4E D1
    word   $D16A           ; D0BC 6A D1
    word   $D186           ; D0BE 86 D1
    word   $D1A6           ; D0C0 A6 D1
    word   $D14E           ; D0C2 4E D1
    word   $D16A           ; D0C4 6A D1
    word   $D196           ; D0C6 96 D1
    word   $D1B6           ; D0C8 B6 D1
    word   $D1C6           ; D0CA C6 D1
    word   $D1E2           ; D0CC E2 D1
    word   $D1FE           ; D0CE FE D1
    word   $D21A           ; D0D0 1A D2
    word   $D22A           ; D0D2 2A D2
    word   $D23A           ; D0D4 3A D2

; "EVERY BONUS"

    hex    18 05 45 00 56 00 45 00 52 00 59 00 00 00 42 00 ; D0D6
    hex    4F 00 4E 00 55 00 53 00 FF FF                   ; D0E6

; "5\0040 Pts"

    hex    18 12 35 05 04 FF 30 05 00 00 3E 05 3C 05 FF FF ; D0F0

; "1\0050 Pts"

    hex    18 11 31 05 05 FF 30 05 00 00 3E 05 3C 05 FF FF ; D100

; "3\0040 Pts"

    hex    18 12 33 05 04 FF 30 05 00 00 3E 05 3C 05 FF FF ; D110

; "BONUS"

    hex    18 08 42 00 4F 00 4E 00 55 00 53 00 FF FF       ; D120

; "5\0040 Pts"

    hex    18 0F 35 05 04 FF 30 05 00 00 3E 05 3C 05 FF FF ; D12E

; "1\0050 Pts"

    hex    18 0E 31 05 05 FF 30 05 00 00 3E 05 3C 05 FF FF ; D13E

; "FIRST  BONUS""

    hex    17 05 46 00 49 00 52 00 53 00 54 00 00 00 00 00 ; D14E
    hex    42 00 4F 00 4E 00 55 00 53 00 FF FF             ; D15E

; "SECOND  BONUS"

    hex    19 05 53 00 45 00 43 00 4F 00 4E 00 44 00 00 00 ; D16A
    hex    42 00 4F 00 4E 00 55 00 53 00 FF FF             ; D17A

; "5\0040 Pts"

    hex    17 13 35 05 04 FF 30 05 00 00 3E 05 3C 05 FF FF ; D186

; "1\0050 Pts"

    hex    17 12 31 05 05 FF 30 05 00 00 3E 05 3C 05 FF FF ; D196

; "1\0050 Pts"

    hex    19 12 31 05 05 FF 30 05 00 00 3E 05 3C 05 FF FF ; D1A6

; "3\0050 Pts"

    hex    19 12 33 05 05 FF 30 05 00 00 3E 05 3C 05 FF FF ; D1B6

; "FIRST  BONUS"

    hex    16 05 46 00 49 00 52 00 53 00 54 00 00 00 00 00 ; D1C6
    hex    42 00 4F 00 4E 00 55 00 53 00 FF FF             ; D1D6

; "SECOND  BONUS"

    hex    18 05 53 00 45 00 43 00 4F 00 4E 00 44 00 00 00 ; D1E2
    hex    42 00 4F 00 4E 00 55 00 53 00 FF FF             ; D1F2

; "THIRD  BONUS"

    hex    1A 05 54 00 48 00 49 00 52 00 44 00 00 00 00 00 ; D1FE
    hex    42 00 4F 00 4E 00 55 00 53 00 FF FF             ; D20E

; "5\0040 Pts"

    hex    16 13 35 05 04 FF 30 05 00 00 3E 05 3C 05 FF FF ; D21A

; "1\0050 Pts"

    hex    18 12 31 05 05 FF 30 05 00 00 3E 05 3C 05 FF FF ; D22A

; "3\0050 Pts"

    hex    1A 12 33 05 05 FF 30 05 00 00 3E 05 3C 05 FF FF ; D23A

; "YOU ARE LUCKY"

    hex    0A 09 59 0F 4F 0F 55 0F 00 00 41 0F 52 0F 45 0F ; D24A
    hex    00 00 4C 0F 55 0F 43 0F 4B 0F 59 0F FF FF       ; D25A

; "YOU CAN ENJOY YALP EROM ENO" :-)

    hex    59 05 4F 05 55 05 00 0A 43 05 41 05 4E 05 00 0A ; D268
    hex    45 05 4E 05 4A 05 4F 05 59 05 FF 59 05 41 05 4C ; D278
    hex    05 50 05 00 0A 45 05 52 05 4F 05 4D 05 00 0A 45 ; D288
    hex    05 4E 05 4F 05 FF                               ; D298

; D29Eh - zeros

; D500h - initial palette
;
; Filling with zeros causes display to turn black except for
; dynamic colours.

    hex    00 00 00                                        ; D500
    hex    FF 0F CE 08 FF 0F 0F 00 A0 0F FF 00 00 00 2A 00 ; D504
    hex    50 00 A0 00 88 08 66 06 44 04 F0 00 00 00 0F 00 ; D514
    hex    00 00 44 04 88 08 CC 0C FF 0F 0F 00 00 00 0F 00 ; D524
    hex    00 00 44 04 88 08 CC 0C FF 0F F0 0F 00 00 0F 00 ; D534
    hex    00 00 55 05 88 08 BB 0B DD 0D FF 0F 00 00 00 00 ; D544
    hex    00 00 0D 00 DD 00 AA 0A 00 00 FF 0F 00 00 0F 00 ; D554
    hex    00 00 44 04 88 08 CC 0C FF 0F 8F 00 00 00 88 00 ; D564
    hex    FF 0F FF 0F F6 06 FF 0F FF 00 00 0F 00 00 00 0F ; D574
    hex    6F 00 FF 00 F0 00 F0 0F FF 0F AF 0C 00 00 88 08 ; D584
    hex    AA 08 CC 0C 80 00 A0 00 C0 00 0A 0F 00 00 88 08 ; D594
    hex    88 08 00 00 00 00 00 00 00 00 0A 0F 00 00 00 00 ; D5A4
    hex    00 00 0D 00 DD 00 AA 0A 00 00 FF 0F 00 00 44 04 ; D5B4
    hex    66 06 88 08 AA 0A FF 0F CC 0C 47 0E 00 00 00 00 ; D5C4
    hex    00 00 08 00 0C 00 0F 00 00 00 FF 0F 00 00 07 00 ; D5D4
    hex    17 00 27 00 37 00 47 00 57 00 67 00 00 00 77 00 ; D5E4
    hex    77 01 77 02 77 03 77 04 77 05 77 06 00 00 3F 00 ; D5F4
    hex    5F 00 7F 00 9F 00 BF 00 DF 00 FF 00 00 00 40 00 ; D604
    hex    60 00 80 00 A0 00 C0 00 E0 00 F0 00 00 00 3F 00 ; D614
    hex    5F 00 7F 00 9F 00 BF 00 DF 00 FF 00 00 00 55 00 ; D624
    hex    77 00 99 00 BB 00 DD 00 FF 00 FF 04 00 00 00 08 ; D634
    hex    00 0C 00 0F 80 0F A0 0F C0 0F F0 0F 00 00 00 0F ; D644
    hex    C0 00 F0 00 F8 00 F0 00 FF 00 FF 0F 00 00 00 0F ; D654
    hex    5C 00 8F 00 FF 00 8F 00 FF 00 FF 0F 00 00 00 0F ; D664
    hex    0F 00 4F 04 8F 08 4F 04 CF 0C FF 0F 00 00 00 00 ; D674
    hex    FF 00 08 00 0C 00 0F 00 00 00 FF 0F 00 00 00 00 ; D684
    hex    FF 00 08 00 0C 00 0F 00 00 00 FF 0F 00 00 00 00 ; D694
    hex    FF 00 08 00 0C 00 0F 00 00 00 FF 0F 00 00 00 00 ; D6A4
    hex    FF 00 08 00 0C 00 0F 00 00 00 FF 0F 00 00 00 00 ; D6B4
    hex    FF 00 08 00 0C 00 0F 00 00 00 FF 0F 00 00 00 00 ; D6C4
    hex    00 00 00 00 00 00 00 00 CC 0C FF 0F 00 00 00 00 ; D6D4
    hex    00 00 00 00 00 00 00 00 CC 0C FF 0F 00 00 00 00 ; D6E4
    hex    00 00 00 00 00 00 00 00 CC 0C FF 0F 00 00 58 0B ; D6F4
    hex    45 00 67 02 89 04 AB 06 CD 08 EF 0A 00 00 00 0C ; D704
    hex    00 0B 00 0A 00 09 00 08 00 07 00 06 00 00 00 0C ; D714
    hex    45 00 67 02 89 04 AB 06 CD 08 EF 0A 00 00 88 0C ; D724
    hex    50 00 A0 00 88 08 66 06 44 04 22 02 00 00 00 0C ; D734
    hex    00 0B 00 0A 00 09 00 08 88 08 AA 0A 00 00 44 04 ; D744
    hex    44 04 00 00 30 03 50 05 70 07 05 07 00 00 44 04 ; D754
    hex    44 04 00 00 33 00 55 00 77 00 05 07 00 00 44 04 ; D764
    hex    44 04 00 00 33 03 55 05 77 07 05 07 00 00 05 00 ; D774
    hex    50 00 80 00 28 00 4A 00 06 00 45 00 00 00 27 00 ; D784
    hex    08 0C 40 00 CC 0C 0D 00 80 00 CC 00 00 00 0F 00 ; D794
    hex    55 05 77 07 99 09 BB 0B DD 0D 00 0F 00 00 88 0C ; D7A4
    hex    2A 00 AA 0A 88 08 66 06 44 04 22 02 00 00 60 0C ; D7B4
    hex    55 05 77 07 99 09 BB 0B DD 0D A0 00 00 00 00 07 ; D7C4
    hex    37 00 77 00 10 01 30 03 50 05 57 06 00 00 00 07 ; D7D4
    hex    37 00 77 00 11 00 33 00 55 00 57 06 00 00 00 07 ; D7E4
    hex    37 00 77 00 11 01 33 03 55 05 57 06 00 00 00 00 ; D7F4
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; D804
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 55 00 ; D814
    hex    77 00 99 00 00 00 00 00 00 00 00 00 00 00 44 04 ; D824
    hex    66 06 88 08 00 00 00 00 00 00 00 00 00 00 00 00 ; D834
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ; D844
    hex    00 00 00 00 00 00 00 00 00 00 00 00 00 00 55 00 ; D854
    hex    66 00 77 00 88 00 88 00 88 00 88 00 00 00 44 04 ; D864
    hex    55 05 66 06 77 07 77 07 77 07 77 07             ; D874

; Codes of 8x8 graphics used to represent the bombs in their
; various states (alight or not).
;
; CA
; DB
;
; E is the colour value.

    hex    CC CD CE CF 15 94 95 96 97 0D CC CD CE CF 15 94 95 96 97 0D ; D880
    hex    CC CD CE CF 15 94 95 96 97 0D CC CD CE CF 15 94 95 96 97 0D ; D894
    hex    CC CD CE CF 15 94 95 96 97 0D CC CD CE CF 15 94 95 96 97 0D ; D8A8
    hex    D8 D9 DA DB 15 90 91 92 93 0D DC DD DE DF 15 90 91 92 93 0D ; D8BC

; ?

    hex    04 00 00 01 C0 00 00 02 ; D8D0
    hex    05 00 00 01 C0 00 00 02 ; D8D8
    hex    06 00 C0 00 80 00 00 02 ; D8E0
    hex    07 00 C0 00 80 00 00 01 ; D8E8
    hex    07 00                   ; D8F0

    hex    80 00 60 00 00 02 04 00 80 00 40 00 00 01 05 00 ; D8F2
    hex    60 00 60 00 00 01 06 00 60 00 40 00 00 01 07 00 ; D902
    hex    60 00 40 00 00 01 07 00 20 00 40 00 00 01 04 00 ; D912
    hex    80 00 30 00 00 01 05 00 80 00 30 00 00 01 06 00 ; D922
    hex    60 00 30 00 00 01 07 00 60 00 30 00 00 01 07 00 ; D932
    hex    20 00 20 00 00 01 04 00 60 00 20 00 00 01 05 00 ; D942
    hex    60 00 20 00 00 01 06 00 40 00 20 00 00 01 07 00 ; D952
    hex    40 00 20 00 00 01 07 00 20 00 20 00 00 01 04 00 ; D962
    hex    40 00 20 00 00 01 05 00 40 00 20 00 00 01 06 00 ; D972
    hex    40 00 20 00 00 01 07 00 40 00 20 00 00 01 07 00 ; D982
    hex    20 00 20 00 00 01 04 00 20 00 20 00 00 01 05 00 ; D992
    hex    20 00 20 00 00 01 06 00 20 00 20 00 00 01 07 00 ; D9A2
    hex    20 00 20 00 00 01 07 00 20 00 20 00 00 01 07 00 ; D9B2
    hex    20 00 20 00 00 01 07 00 20 00 20 00 00 01 07 00 ; D9C2
    hex    20 00 20 00 00 01                               ; D9D2
    
; D9D8h - 20 24-byte records
;
; Every second byte is 00h.

    hex 01 00 20 00 44 00 80 00 02 00 00 00 01 00 30 00 55 00 80 00 02 00 00 00
    hex 01 00 38 00 66 00 80 00 03 00 00 00 01 00 40 00 55 00 70 00 03 00 00 00
    hex 01 00 40 00 66 00 70 00 04 00 00 00 01 00 40 00 77 00 70 00 04 00 00 00
    hex 01 00 48 00 66 00 60 00 05 00 00 00 01 00 48 00 77 00 60 00 05 00 00 00
    hex 01 00 48 00 88 00 60 00 06 00 00 00 02 00 40 00 55 00 80 00 06 00 00 00
    hex 02 00 40 00 66 00 80 00 07 00 00 00 02 00 40 00 77 00 80 00 07 00 00 00
    hex 02 00 48 00 66 00 70 00 08 00 00 00 02 00 48 00 77 00 70 00 08 00 00 00
    hex 02 00 48 00 88 00 70 00 08 00 00 00 02 00 50 00 77 00 60 00 08 00 00 00
    hex 02 00 50 00 88 00 60 00 08 00 00 00 02 00 50 00 99 00 60 00 08 00 00 00
    hex 02 00 58 00 88 00 60 00 08 00 00 00 02 00 50 00 77 00 80 00 08 00 00 00
    hex 02 00 50 00 88 00 80 00 08 00 00 00 02 00 58 00 77 00 80 00 08 00 00 00
    hex 02 00 58 00 88 00 70 00 08 00 00 00 02 00 58 00 99 00 70 00 08 00 00 00
    hex 02 00 60 00 88 00 70 00 08 00 00 00 02 00 60 00 99 00 60 00 08 00 00 00
    hex 02 00 60 00 A2 00 60 00 08 00 00 00 02 00 68 00 99 00 60 00 08 00 00 00
    hex 02 00 68 00 A2 00 50 00 08 00 00 00 02 00 68 00 AA 00 50 00 08 00 00 00
    hex 02 00 70 00 A2 00 50 00 08 00 00 00 02 00 70 00 AA 00 48 00 08 00 00 00
    hex 02 00 70 00 B3 00 48 00 08 00 00 00 02 00 78 00 AA 00 48 00 08 00 00 00
    hex 02 00 78 00 B3 00 40 00 08 00 00 00 02 00 78 00 BB 00 40 00 08 00 00 00
    hex 02 00 80 00 B3 00 40 00 08 00 00 00 02 00 80 00 BB 00 38 00 08 00 00 00
    hex 02 00 80 00 C4 00 38 00 08 00 00 00 02 00 80 00 BB 00 38 00 08 00 00 00

; DBB8h - 32 20-byte records
;
; The last 8 bytes are the code of the enemies that will be
; spawned (see CHARS for the list). The enemies are spawned in
; the same order. If less than 8 enemies are spawned, the first
; generation will use E.G. the 4 first, the second generation
; the 4 last and the third generation the 4 first again.
;
; The other fields need to be figured out.

    hex    80 00 00 01 01 00 00 02 01 00 00 04 B3 B3 B3 B4 B3 B3 B3 B4 ; DBB8
    hex    91 00 C0 00 02 00 80 01 01 00 00 03 B1 B1 B1 B4 B1 B1 B1 B4 ; DBCC
    hex    A2 00 C0 00 03 00 00 01 01 00 00 02 B2 B2 B2 B4 B2 B2 B2 B4 ; DBE0
    hex    AE 00 80 00 04 00 C0 00 01 00 80 01 B0 B0 B0 B4 B0 B0 B0 B4 ; DBF4
    hex    C0 00 80 00 05 00 C0 00 04 00 00 03 B4 B4 B4 B4 B4 B4 B4 B4 ; DC08
    hex    D1 00 80 00 06 00 C0 00 03 00 80 01 B2 B2 B1 B1 B2 B2 B3 B3 ; DC1C
    hex    80 01 80 00 07 00 C0 00 03 00 80 01 B0 B1 B2 B3 B4 B3 B2 B1 ; DC30
    hex    EE 00 80 00 08 00 C0 00 03 00 80 01 B0 B0 B1 B1 B0 B0 B1 B1 ; DC44
    hex    00 01 80 00 09 00 C0 00 03 00 80 01 B3 B3 B4 B4 B3 B3 B4 B4 ; DC58
    hex    00 01 80 00 0A 00 C0 00 03 00 80 01 B2 B2 B2 B2 B2 B2 B2 B2 ; DC6C
    hex    00 01 80 00 0B 00 C0 00 04 00 80 01 B4 B3 B4 B3 B0 B1 B0 B1 ; DC80
    hex    00 01 80 00 0C 00 C0 00 04 00 80 01 B1 B2 B1 B2 B1 B2 B1 B2 ; DC94
    hex    00 01 80 00 0D 00 C0 00 04 00 80 01 B3 B0 B3 B0 B3 B0 B3 B0 ; DCA8
    hex    00 01 80 00 0E 00 C0 00 05 00 80 01 B4 B4 B4 B1 B4 B4 B4 B1 ; DCBC
    hex    00 02 80 00 0F 00 C0 00 05 00 80 01 B0 B0 B0 B0 B0 B0 B0 B0 ; DCD0
    hex    00 01 80 00 10 00 C0 00 05 00 80 01 B1 B1 B1 B1 B1 B1 B1 B1 ; DCE4
    hex    80 01 80 00 11 00 C0 00 05 00 80 01 B2 B2 B2 B2 B2 B2 B2 B2 ; DCF8
    hex    80 01 80 00 12 00 C0 00 05 00 80 01 B3 B3 B3 B3 B3 B3 B3 B3 ; DD0C
    hex    80 01 80 00 13 00 C0 00 06 00 80 01 B4 B4 B4 B4 B4 B4 B4 B4 ; DD20
    hex    80 01 80 00 14 00 C0 00 06 00 80 01 B0 B0 B4 B4 B2 B2 B4 B4 ; DD34
    hex    80 01 80 00 15 00 C0 00 06 00 80 01 B0 B0 B0 B0 B1 B1 B1 B1 ; DD48
    hex    80 01 80 00 16 00 C0 00 06 00 80 01 B1 B1 B1 B1 B2 B2 B2 B2 ; DD5C
    hex    80 01 80 00 17 00 C0 00 06 00 80 01 B2 B2 B2 B2 B3 B3 B3 B3 ; DD70
    hex    80 01 80 00 18 00 C0 00 07 00 80 01 B3 B3 B3 B3 B4 B4 B4 B4 ; DD84
    hex    00 02 80 00 19 00 C0 00 07 00 80 01 B1 B3 B1 B3 B1 B3 B1 B3 ; DD98
    hex    00 02 80 00 1A 00 C0 00 07 00 80 01 B4 B4 B4 B4 B0 B0 B0 B0 ; DDAC
    hex    00 02 80 00 1B 00 C0 00 07 00 80 01 B3 B3 B3 B3 B1 B1 B1 B1 ; DDC0
    hex    00 02 80 00 1C 00 C0 00 08 00 80 01 B2 B2 B2 B2 B4 B4 B4 B4 ; DDD4
    hex    00 02 80 00 1D 00 C0 00 08 00 80 01 B0 B0 B0 B0 B3 B3 B3 B3 ; DDE8
    hex    00 02 80 00 1E 00 C0 00 09 00 80 01 B4 B3 B4 B2 B4 B1 B4 B0 ; DDFC
    hex    00 02 80 00 1F 00 C0 00 09 00 80 01 B0 B1 B2 B3 B4 B0 B1 B2 ; DE10
    hex    00 02 80 00 20 00 C0 00 09 00 80 01 B3 B4 B0 B1 B2 B3 B4 B0 ; DE24

; DE38h - level meta data
;
; 64 6-byte records (ABCDEF).
;
; A = platform design number
; B = platform colour (0=yel/red, 1=green, 2=yel/red, 3=yellow, 4=blue)
; C = repeats of 2, 0, 3, 4, 1
; D = bomb design number (always same as A except for levels 5, 10, ...)
; E = backdrop number (0=pyramid, 1=acropole, 2=castle, 3=city, 4=lights)
; F = music (0, 1, 2)
;
; The fact that there are only 64 entries must be why backdrops
; resync at level 65. I don't know why platform colours desync
; again at level 77, though.

    hex 00 00 02 00 00 00  ; Level 1
    hex 0B 01 00 0B 01 01
    hex 08 02 03 08 02 02
    hex 07 03 04 07 03 00
    hex 04 04 01 04 04 01
    hex 02 00 02 02 00 02
    hex 06 01 00 06 01 00
    hex 03 02 03 03 02 01
    hex 0A 03 04 0A 03 02
    hex 04 04 01 00 04 00
    hex 0C 00 02 0C 00 01
    hex 0E 01 00 0E 01 02
    hex 09 02 03 09 02 00
    hex 0F 03 04 0F 03 01
    hex 04 04 01 04 04 02
    hex 0D 00 02 0D 00 00
    hex 01 01 00 01 01 01
    hex 05 02 03 05 02 02  ; Level 18
    hex 0B 03 04 0B 03 00  ; Level 19
    hex 04 04 01 0D 04 01  ; Level 20
    hex 0E 00 02 0E 00 02
    hex 06 01 00 06 01 00
    hex 07 02 03 07 02 01
    hex 09 03 04 09 03 02
    hex 04 04 01 0F 04 00  ; Level 25
    hex 05 00 02 05 00 01
    hex 03 01 00 03 01 02
    hex 01 02 03 01 02 00
    hex 0C 03 04 0C 03 01
    hex 04 04 01 00 04 02
    hex 02 00 02 02 00 00
    hex 08 01 00 08 01 01
    hex 0F 02 03 0F 02 02
    hex 0D 03 04 0D 03 00
    hex 04 04 01 06 04 01
    hex 0A 00 02 0A 00 02
    hex 09 01 00 09 01 00
    hex 01 02 03 01 02 01
    hex 02 03 04 02 03 02
    hex 04 04 01 02 04 00
    hex 03 00 02 03 00 01
    hex 05 01 00 05 01 02
    hex 06 02 03 06 02 00
    hex 07 00 04 07 03 01  ; Level 44 - B returns to 0
    hex 04 01 01 0E 04 02
    hex 08 02 02 08 00 00
    hex 09 03 00 09 01 01
    hex 0A 04 03 0A 02 02
    hex 0B 00 04 0B 03 00
    hex 04 01 01 0A 04 01
    hex 0C 02 02 0C 00 02
    hex 0D 03 00 0D 01 00
    hex 0E 04 03 0E 02 01
    hex 0F 00 04 0F 03 02
    hex 04 01 01 06 04 00
    hex 00 02 02 00 00 01
    hex 01 03 00 01 01 02
    hex 02 04 03 02 02 00
    hex 03 00 04 03 03 01
    hex 04 01 01 08 04 02
    hex 05 02 02 05 00 00
    hex 06 03 00 06 01 01
    hex 07 04 03 07 02 02
    hex 08 00 04 08 03 00

hex ; DFB8h - zeros

; DFF2h - ?

    rlc    c               ; DFF2 CB 01
    pop    hl              ; DFF4 E1
    pop    hl              ; DFF5 E1
    pop    hl              ; DFF6 E1
    pop    hl              ; DFF7 E1
    ex     af,af'          ; DFF8 08
    add    a,b             ; DFF9 80
    ld     (bc),a          ; DFFA 02
    ret    po              ; DFFB E0
    ld     (bc),a          ; DFFC 02
    ex     af,af'          ; DFFD 08
    nop                    ; DFFE 00
    nop                    ; DFFF 00


