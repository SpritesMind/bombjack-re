;==============================================================
; Bombjack Sound Selector Screen hack v1.0 - 4 Apr' 2011
;
; This hack add a sound selector screen on Bombjack game.
; It will replace the test screen because
; 1/ it uses a lot of ROM space I could replace for my need
; 2/ it will report ROM error
; 3/ it takes too much time to test ROM & RAM !
;
; This hack was tested on MAME, using 'bombjack' rom (not tested with 'bombjack2')
; I don't know if it works on real hardware.
; I think it would but...theory and reality could be very different!
;
; This hack could be updated and assembled using WLA-DX :
; http://www.smspower.org/Development/WLA-DX
;
; I used LunarIPS to create/apply the hack :
; http://fusoya.eludevisibility.org/lips/index.html
;
; I made this hack to dump sound in perfect quality.
; You could use MAME_VGM for this :
; http://www.smspower.org/forums/viewtopic.php?t=11965
; (browse the forum for others VGM tools)
;
; KanedaFr - http://arhackde.spritesmind.net/
;==============================================================

; WLA-DX banking setup
.memorymap
defaultslot 0
slotsize $8000
slot 0 $0000
.endme

.rombankmap
bankstotal 1
banksize $8000
banks 1
.endro

; useful func
.define j_drawText $cf
.define j_drawNumbers $D8
.define j_AddSound $FC 
.define NMI_func $01C0
.define updateInputs $0382
.define initPals $0618
.define PalEffect2 $062F 
.define updateSprites $0958
.define flushPals $09AB
.define waitNMI $09EE
.define playSound $1BFD
.define resetSprites $4BD9
.define j_updateSound $6012
.define drawLogo $64D8
.define loadLogoPal	$6524

; useful var
.define playMode $8070
.define joyRight $8018
.define joyRightCounter	$8019
.define joyLeft	$801A
.define joyLeftCounter $801B
.define joyUp $801C
.define joyUpCounter $801D
.define joyDown $801E
.define joyDownCounter $801F
.define buttonPressTime $8020
.define currentSound	$8036 ; used like invincibleCounter in original game
.define nmiDelay $806E
.define refreshFlag $808C
.define z80StackSize	$8700 
.define z80Stack	$8701
.define	INO_Int $B000
.define soundlatch $B800


.bank 0 slot 0

; nmi hack
.org $6d
	jp nmiSoundFunc

; I replace the test screen by our own sound test screen
.org $03C7
testScreen:
	jp selector
	
;-------------------------------
; nmiFunc hook
; I put it there to avoid change everytime the address at $6E
; yes, I'm lazy ;)
;-------------------------------
nmiSoundFunc:
	push af
	ld a, (playMode)
	cp 3
	jp z, soundNMI
	
	pop af
	jp NMI_func
	
soundNMI:
;	push af
	push hl
	
	xor a
	ld	(INO_Int), a	; disable int
	
	ld      hl, (nmiDelay)
	ld      a, l
	or      h
	jr      z, locret_9ED
	dec     hl
	ld      (nmiDelay), hl
	
locret_9ED:
	ld	a, 1
	ld	(INO_Int), a

	pop hl
	pop	af
	retn
	
;-------------------------------
; our sound selector
;-------------------------------
selector:
	call drawLogo
	call updateSprites
	
	call initPals
	call loadLogoPal

	; PalEffect2 needs 'some' call to compute correct color values
	; I use 10h because it works, not because it is the correct value ;)
	ld b, 10h
	
logoPalUpdate:
	call PalEffect2
	djnz logoPalUpdate
	
	
	ld a, 1
	ld (refreshFlag), a
	call flushPals
	
	call	j_drawText
	.dw testStrings
	.db 4
	
	; this mode doesn't exist in original game
	; I add it to detect nmi while sound test
	ld a, 3
	ld (playMode), a
	
	ld      a, 1
	ld      (INO_Int), a
	
	ld	a, 10h
	ld (currentSound), a
	
loop1:
	call 	j_updateSound
	call 	updateInputs

	ld a, (joyUp)
	cp $FF
	jp nz, continue
	
	ld a, 1
	call	setSound
	call 	j_updateSound
	
	xor	 a
	call resetSprites
	call updateSprites
	call refresh
	
	xor	 a
	ld (playMode), a
	ld (INO_Int), a
	ret
	
continue:	
	ld	a, (buttonPressTime)
	cp	3
	jp	nz, testNext
	
resetSound:
	ld a, 1
	call	setSound
	call 	j_updateSound
	
waitClean:
	call refresh

	ld  a, (currentSound)
	call	setSound
	
	jp loop1

testNext:
	ld a, (joyRightCounter)
	cp 1
	jp nz, testPrev

	ld  a, (currentSound)
	inc a
	cp	2Fh
	jp nz, validSound
	ld a, 10h 
	jp validSound

testPrev:
	ld a, (joyLeftCounter)
	cp 1
	jp nz, noDir

	ld  a, (currentSound)
	cp	10h
	jp z, goLastSound
	dec a
	jp validSound

goLastSound:
	ld a, 2Eh

validSound:
	ld (currentSound), a
	call refresh
	
drawCurrentSound:	
	call	j_drawNumbers
	.dw strCurrentSound
	.db    1

noDir:
noSound:	
	jp loop1

;-------------------------------
; wait for 1 nmi
;-------------------------------
refresh:
	ld bc, $1
	call waitNMI
	ret
	
	
;-------------------------------
; setSound forcing playMode
;-------------------------------
setSound:
	; to be sure to hear sound whatever dipswithc value is
	; force playMode to 1 (game) and get back to 3 (soundTest)
	push af
	ld a,1
	ld (playMode), a
	pop af
	
	call	j_AddSound
	
	ld a,3
	ld (playMode), a
	ret

;-------------------------------
; Strings
;-------------------------------
strCurrentSound:
	.dw valCurrentSound
valCurrentSound:	
		.db 22			; y
		.db 17			; x
		.dw currentSound		; varToPrint
		.db 0			; pal
		.db 2			; length
		
testStrings:	 
	.dw strLeftRight
	.dw strUp
	.dw strButton
	.dw strROM0	
strLeftRight:
		.db    17		
		.db    6
		.db  'L'
		.db    5
		.db  'E'
		.db    5
		.db  'F'
		.db    5
		.db  'T'
		.db    5
		.db    0
		.db    0
		.db  'R'
		.db    5
		.db  'I'
		.db    5
		.db  'G'
		.db    5
		.db  'H'
		.db    5
		.db  'T'
		.db    5
		.db    0
		.db    0
		.db  'T'
		.db    5
		.db  'O'
		.db    5
		.db    0
		.db    0
		.db  'S'
		.db    2
		.db  'E'
		.db    2
		.db  'L'
		.db    2
		.db  'E'
		.db    2
		.db  'C'
		.db    2
		.db  'T'
		.db    2
		.db 0FFh
		.db 0FFh

strUp:		
		.db    18		
		.db    6
		.db  'U'
		.db    5
		.db  'P'
		.db    5
		.db    0
		.db    0
		.db  'T'
		.db    5
		.db  'O'
		.db    5
		.db    0
		.db    0
		.db  'Q'
		.db    2
		.db  'U'
		.db    2
		.db  'I'
		.db    2
		.db  'T'
		.db    2
		.db 0FFh
		.db 0FFh
		
strButton:
		.db    19		
		.db    6
		.db  'B'
		.db    5
		.db  'U'
		.db    5
		.db  'T'
		.db    5
		.db  'T'
		.db    5
		.db  'O'
		.db    5
		.db  'N'
		.db    5
		.db    0
		.db    0
		.db  'T'
		.db    5
		.db  'O'
		.db    5
		.db    0
		.db    0
		.db  'P'
		.db    2
		.db  'L'
		.db    2
		.db  'A'
		.db    2
		.db  'Y'
		.db    2
		.db 0FFh
		.db 0FFh
strROM0:	
		.db    22			
		.db    11
		.db  'S'
		.db    5
		.db  'O'
		.db    5
		.db  'U'
		.db    5
		.db  'N'
		.db    5
		.db  'D'
		.db    5
		.db    0
		.db    0
		.db  '1'
		.db    0
		.db  '0'
		.db    0
		.db  'H'
		.db    0
		.db 0FFh
		.db 0FFh