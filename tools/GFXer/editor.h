#ifndef _INC_EDITOR_
#define _INC_EDITOR_



extern SDL_Texture *texture;
extern SDL_Texture *palTexture;

extern unsigned char currentZoom;

int editor_init( );
void editor_update( );
void editor_exit( );

void editor_tile_next();
void editor_tile_previous();
void editor_tile_export();
void editor_setPixelAt(Sint32 x, Sint32 y, unsigned char doubleClick);

void editor_pal_next();
void editor_pal_previous();
void editor_selectColorAt(Sint32 x, Sint32 y, unsigned char doubleClick);

void editor_flip();
void editor_zoom_inc();
void editor_about();

#endif
