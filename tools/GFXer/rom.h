#ifndef _INC_ROM_
#define _INC_ROM_


#define ROM_CHARS		0
#define ROM_TILES		1
#define ROM_SPRITES		2
#define ROM_LSPRITES	3

extern unsigned char *romData; //exposed to editor.c
extern unsigned char romLoadedType;
extern unsigned char tileWidth, tileHeight;
extern unsigned int tileCount;

int rom_init( );
void rom_exit( );

int rom_load(unsigned char type);
int rom_save( );
void rom_unload();


#endif
