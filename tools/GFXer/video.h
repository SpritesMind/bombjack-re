#ifndef _INC_VIDEO_
#define _INC_VIDEO_

#define TOOL_HEIGHT		24

#define WIN_WIDTH       32*10
#define WIN_HEIGHT      32*10 + TOOL_HEIGHT

#define PAL_SQUARE_SIZE	32

extern SDL_Renderer* renderer;
extern SDL_Renderer* palRenderer;

int video_init( );
void video_update( );
void video_exit( );

int video_isPalWindow(Uint32 windowID);
int video_isMainWindow(Uint32 windowID);

SDL_Surface *video_getSurface();


#endif
