#include "gfxer.h"

int events_poll( ){
	SDL_Event event;
	while (SDL_PollEvent(&event) != 0){
		SDL_Scancode scanCode = event.key.keysym.scancode;
		switch(event.type) {
			//case 512:
			case SDL_QUIT:
				return 1;
				break;
			case SDL_KEYDOWN:
				if ((event.key.keysym.mod & KMOD_CTRL)) {
                        if(scanCode == SDL_SCANCODE_S)
							rom_save( );
				
				}
				else if(scanCode == SDL_SCANCODE_X)
					editor_tile_export( );
				else if (scanCode == SDL_SCANCODE_LEFT)
					editor_tile_previous();
				else if (scanCode == SDL_SCANCODE_RIGHT)
					editor_tile_next();
					
				else if (scanCode == SDL_SCANCODE_UP)
					editor_pal_previous();
				else if (scanCode == SDL_SCANCODE_DOWN)
					editor_pal_next();
				
				else if (scanCode == SDL_SCANCODE_C)
					rom_load(ROM_CHARS);
				else if (scanCode == SDL_SCANCODE_T)
					rom_load(ROM_TILES);
				else if (scanCode == SDL_SCANCODE_S)
					rom_load(ROM_SPRITES);
				else if (scanCode == SDL_SCANCODE_L)
					rom_load(ROM_LSPRITES);
					
				else if (scanCode == SDL_SCANCODE_Z)
					editor_zoom_inc();
				editor_update( );
				break;
			case SDL_MOUSEWHEEL:
			case SDL_MOUSEMOTION:
				break;
			case SDL_MOUSEBUTTONDOWN:
				if ( (event.button.button == SDL_BUTTON_LEFT) && (event.button.state == SDL_PRESSED) ) {
					if (video_isMainWindow(event.button.windowID) )
						editor_setPixelAt(event.button.x, event.button.y, (event.button.clicks > 1) );
					else if (video_isPalWindow(event.button.windowID) )
						editor_selectColorAt(event.button.x, event.button.y, (event.button.clicks > 1) );
					
				}
				break;
			default:
				//printf("event %d\n",event.type);
				break;
		}
	}
	
	return 0;
}
