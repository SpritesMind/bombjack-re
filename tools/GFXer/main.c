#include "gfxer.h"

int main(int argc, char *args[]) {

	if (!video_init())	return 1;
	if (!rom_init())	goto quit;
	if (!editor_init())	goto quit;


    while (!events_poll( ))
    {            
        video_update( );
    }
  
  
quit:
	editor_exit();
	rom_exit();
    video_exit();

    SDL_Quit();

    return 0;
}
