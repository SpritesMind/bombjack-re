#include <stdio.h>
#include <stdlib.h>
#include "gfxer.h"


#define PAL_COUNT	16
#define MAX_ZOOM	10

SDL_Texture *texture;
SDL_Texture *palTexture;

static unsigned int currentIdx;
static unsigned char currentPalIdx = 0;
static unsigned char currentColorIdx = 0;
unsigned char currentZoom = 1;

static unsigned char previousMode;


static SDL_Surface *surface;

/* the Bombjack palettes */
const unsigned char pals[PAL_COUNT][8][3]={
{
	//0
	{0,0,0},
	{0,0,0},
	{255,255,255},
	{238,204,136},
	{255,255,255},
	{255,0,0},
	{0,170,255},
	{255,255,0},
},

{
	//1
	{0,0,0},
	{170,34,0},
	{0,85,0},
	{0,170,0},
	{136,136,136},
	{102,102,102},
	{68,68,68},
	{0,255,0}
},

{
	//2
	{0,0,0},
	{255,0,0},
	{0,0,0},
	{68,68,68},
	{136,136,136},
	{204,204,204},
	{255,255,255},
	{255,0,0},
},

{
	//3
	{0,0,0},
	{255,0,0},
	{0,0,0},
	{68,68,68},
	{136,136,136},
	{204,204,204},
	{255,255,255},
	{0,255,255}
},

{
	//4
	{0,0,0},
	{255,0,0},
	{0,0,0},
	{85,85,85},
	{136,136,136},
	{187,187,187},
	{221,221,221},
	{255,255,255},
},

{
	//5
	{0,0,0},
	{0,0,0},
	{0,0,0},
	{221,0,0},
	{221,221,0},
	{170,170,170},
	{0,0,0},
	{255,255,255}
},

{
	//6
	{0,0,0},
	{255,0,0},
	{0,0,0},
	{68,68,68},
	{136,136,136},
	{204,204,204},
	{255,255,255},
	{255,136,0},
},

{
	//7
	{0,0,0},
	{136,136,0},
	{255,255,255},
	{255,255,255},
	{102,255,102},
	{255,255,255},
	{255,255,0},
	{0,0,255}
},

{
	//8
	{0,0,0},
	{0,0,255},
	{255,102,0},
	{255,255,0},
	{0,255,0},
	{0,255,255},
	{255,255,255},
	{255,170,204},
},

{
	//9	
	{0,0,0},
	{136,136,136},
	{170,170,136},
	{204,204,204},
	{0,136,0},
	{0,170,0},
	{0,204,0},
	{170,0,255}
},

{
	//10
	{0,0,0},
	{136,136,136},
	{136,136,136},
	{0,0,0},
	{0,0,0},
	{0,0,0},
	{0,0,0},
	{170,0,255},
},

{
	//11
	{0,0,0},
	{0,0,0},
	{0,0,0},
	{221,0,0},
	{221,221,0},
	{170,170,170},
	{0,0,0},
	{255,255,255}
},

{
	//12
	{0,0,0},
	{255,0,0},
	{221,0,0},
	{187,0,0},
	{153,0,0},
	{119,0,0},
	{85,0,0},
	{51,0,0},
},

{
	//13
	{0,0,0},
	{0,255,0},
	{0,221,0},
	{0,187,0},
	{0,153,0},
	{0,119,0},
	{0,85,0},
	{0,51,0}
},

{
	//14
	{0,0,0},
	{0,0,255},
	{0,0,221},
	{0,0,187},
	{0,0,153},
	{0,0,119},
	{0,0,85},
	{0,0,51},
},

{
	//15
	{0,0,0},
	{255,255,255},
	{221,221,221},
	{187,187,187},
	{153,153,153},
	{119,119,119},
	{85,85,85},
	{51,51,51}
}

};
/*
static void screenshot(SDL_Renderer *renderer, unsigned char *filename)
{
    const Uint32 format = SDL_PIXELFORMAT_ARGB8888;
    const int width = 640;
    const int height = 400;
 
    SDL_Surface *surface = SDL_CreateRGBSurfaceWithFormat(0, width, height, 32, format);
    SDL_RenderReadPixels(renderer, NULL, format, surface->pixels, surface->pitch);
    SDL_SaveBMP(surface, "screenshot.bmp");
    SDL_FreeSurface(surface);
}
*/

static void set_char_pixel(unsigned char x, unsigned char y, unsigned char colorIdx){
	
	unsigned char color1 = (colorIdx>>2)&0x01;
    unsigned char color2 = (colorIdx>>1)&0x01;
    unsigned char color3 = (colorIdx)&0x01;
    unsigned char pixel, mask;
    unsigned char decal;
    unsigned int offset;

	decal = 7 - x;
    mask = 1;
    mask <<=decal;
    mask = ~mask;

	offset = currentIdx*8 + y;

    pixel  = romData[0x0000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= (color1)<<decal; /* put the current pixel = color1 */
    romData[0x0000 + offset] = pixel; /* put the char */

    pixel  = romData[0x1000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= color2<<decal; /* put the current pixel = color2 */
    romData[0x1000 + offset] = pixel; /* put the char */

    pixel  = romData[0x2000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= color3<<decal; /* put the current pixel = color3 */
    romData[0x2000 + offset] = pixel; /* put the char */
}

static void set_sprite_pixel(unsigned char x, unsigned char y, unsigned char colorIdx){
	
	unsigned char color1 = (colorIdx>>2)&0x01;
    unsigned char color2 = (colorIdx>>1)&0x01;
    unsigned char color3 = (colorIdx)&0x01;
    unsigned char pixel, mask;
    unsigned char decal;
    unsigned int offset;

    decal = 7 - (x%8);
    mask = 1;
    mask <<=decal;
    mask = ~mask;

	offset = currentIdx*32 + y%8 + 16*(y/8) + 8*(x/8);
    pixel  = romData[0x0000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= (color1)<<decal; /* put the current pixel = color1 */
    romData[0x0000 + offset] = pixel; /* put the char */

    pixel  = romData[0x2000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= color2<<decal; /* put the current pixel = color2 */
    romData[0x2000 + offset] = pixel; /* put the char */

    pixel  = romData[0x4000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= color3<<decal; /* put the current pixel = color3 */
    romData[0x4000 + offset] = pixel; /* put the char */
}

static void set_lsprite_pixel(unsigned char x, unsigned char y, unsigned char colorIdx){
	
	unsigned char color1 = (colorIdx>>2)&0x01;
    unsigned char color2 = (colorIdx>>1)&0x01;
    unsigned char color3 = (colorIdx)&0x01;
    unsigned char pixel, mask;
    unsigned char decal;
    unsigned int offset;

    decal = 7 - (x%8);
    mask = 1;
    mask <<=decal;
    mask = ~mask;

	offset = currentIdx*128 + y%8 + 16*(y/8) + 32*(y/16) + 8*(x/8) + 16*(x/16);
    pixel  = romData[0x1000 + 0x0000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= (color1)<<decal; /* put the current pixel = color1 */
    romData[0x1000 + 0x0000 + offset] = pixel; /* put the char */

    pixel  = romData[0x1000 + 0x2000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= color2<<decal; /* put the current pixel = color2 */
    romData[0x1000 + 0x2000 + offset] = pixel; /* put the char */

    pixel  = romData[0x1000 + 0x4000 + offset]; /* get the char */
    pixel  &= mask; /* put the current pixel = 0 */
    pixel |= color3<<decal; /* put the current pixel = color3 */
    romData[0x1000 + 0x4000 + offset] = pixel; /* put the char */
}


static void setPixel(unsigned char x, unsigned char y)
{
	switch(romLoadedType)
	{
		case ROM_CHARS:
			set_char_pixel(x,y, currentColorIdx);
			break;
		case ROM_TILES:
		case ROM_SPRITES:
			set_sprite_pixel(x,y, currentColorIdx);
			break;
		case ROM_LSPRITES:
			set_lsprite_pixel(x,y, currentColorIdx);
			break;
	}
}


static void draw_pix(unsigned char x, unsigned char y, unsigned char colorIdx, unsigned char zoom){
	unsigned long color = SDL_MapRGBA(surface->format, 
										pals[currentPalIdx][colorIdx&0x0F][0],
										pals[currentPalIdx][colorIdx&0x0F][1],
										pals[currentPalIdx][colorIdx&0x0F][2],
										255  );

	//see http://sdl.beuc.net/sdl.wiki/Pixel_Access
	//Uint8 *pixels = (Uint8 *)surface->pixels;									
	//pixels += (x*zoom*surface->format->BytesPerPixel) + (y*zoom*surface->pitch);
	//32bits access
	//* (Uint32*) pixels = color;

	SDL_Rect dest = (SDL_Rect) {
                x*zoom,
                y*zoom,
                zoom,
                zoom
        };
	
	SDL_FillRect(surface,
                 &dest,
                 color);

}



static void draw_char(unsigned char zoom){

	unsigned char i, j, pixel;

    for(i=0;i<8;i++)
    {
        for(j=0;j<8;j++)
        {
            pixel  = ((romData[0x0000 + currentIdx*8 +  i]) >> (7-j))&0x01;
            pixel <<= 1;
            pixel |= ((romData[0x1000 + currentIdx*8 +  i]) >> (7-j))&0x01;
            pixel <<= 1;
            pixel |= ((romData[0x2000 + currentIdx*8 +  i]) >> (7-j))&0x01;

            draw_pix(j, i, pixel, zoom);
        }
    }	
}

static void draw_sprite(unsigned char zoom){

	unsigned char i, j, decal, pixel;

    for(i=0;i<16;i++)
    {
        for(j=0;j<16;j++)
        {
            decal = 7 - (j%8);
            pixel  = ((romData[0x0000 + currentIdx*32 + i%8 + 16*(i/8) + 8*(j/8)]) >> decal )&0x01;
            pixel <<= 1;
            pixel |= ((romData[0x2000 + currentIdx*32 + i%8 + 16*(i/8) + 8*(j/8)]) >> decal )&0x01;
            pixel <<= 1;
            pixel |= ((romData[0x4000 + currentIdx*32 + i%8 + 16*(i/8) + 8*(j/8)]) >> decal )&0x01;

            draw_pix(j, i, pixel, zoom);
        }
    }
    
	
}
static void draw_large_sprite(unsigned char zoom){

	unsigned char i, j, decal, pixel;
    
    for(i=0;i<32;i++)
    {
        for(j=0;j<32;j++)
        {
            decal = 7 - (j%8);
            pixel  = ((romData[0x1000+ 0x0000 + currentIdx*128 + i%8 + 16*(i/8) + 32*(i/16) + 8*(j/8) + 16*(j/16)]) >> decal )&0x01;
            pixel <<= 1;
            pixel |= ((romData[0x1000+ 0x2000 + currentIdx*128 + i%8 + 16*(i/8) + 32*(i/16) + 8*(j/8) + 16*(j/16)]) >> decal )&0x01;
            pixel <<= 1;
            pixel |= ((romData[0x1000+ 0x4000 + currentIdx*128 + i%8 + 16*(i/8) + 32*(i/16) + 8*(j/8) + 16*(j/16)]) >> decal )&0x01;

            draw_pix(j, i, pixel, zoom);
        }
    }
    
	
}


static void draw_on_surface(unsigned char zoom)
{
	if (surface)	SDL_FreeSurface(surface);

	//32x32, 32bits
	surface = SDL_CreateRGBSurface(0, 32*zoom, 32*zoom, 32, 0, 0, 0, 0);
	
	SDL_LockSurface(surface);
	
	switch(romLoadedType)
	{
		case ROM_CHARS:
			draw_char(zoom);
			break;
		case ROM_TILES:
		case ROM_SPRITES:
			draw_sprite(zoom);
			break;
		case ROM_LSPRITES:
			draw_large_sprite(zoom);
			break;
	}
	
	
	SDL_UnlockSurface(surface);
}

static void draw_tiles(unsigned char zoom )
{
	if (texture)	SDL_DestroyTexture(texture);
	
	draw_on_surface(zoom);
	
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);surface = NULL;
}

static void draw_pal( )
{
	unsigned char i;
	
	if (palTexture)	SDL_DestroyTexture(palTexture);
	if (surface)	SDL_FreeSurface(surface);
	
	//8x1, 32bits
	surface = SDL_CreateRGBSurface(0, 8, 1, 32, 0, 0, 0, 0);
	
	SDL_LockSurface(surface);
	for(i=0; i<8; i++)
		draw_pix(i, 0, i, 1);
	
	SDL_UnlockSurface(surface);
	
	palTexture = SDL_CreateTextureFromSurface(palRenderer, surface);
	SDL_FreeSurface(surface);surface = NULL;
}

static void toolbarClick(Sint32 x, Sint32 y, unsigned char doubleClick)
{
	unsigned char tool = x/TOOL_HEIGHT;
	switch(tool)
	{
		case 0: 
			rom_load(ROM_CHARS);
			break;
		case 1:
			rom_load(ROM_TILES);
			break;
		case 2:
			rom_load(ROM_SPRITES);
			break;
		case 3:
			rom_load(ROM_LSPRITES);
			break;		
		case 4:
			rom_save( );
			break;
		case 5:
			editor_tile_export( );
			return;
			break;
		case 6:
			editor_tile_previous();
			break;
		case 7:
			editor_tile_next();
			break;
		case 8:
			editor_flip();
			break;
		case 9:
			editor_zoom_inc();
			break;
		case 10:
			editor_about();
			return;
		default:
			break;
			
	}
	editor_update();
}


int editor_init()
{
	currentIdx = 0;
	currentPalIdx = 0;
	currentColorIdx = 0;
	
	currentZoom = 1;
	
	previousMode = 0xFF;
	editor_update();
	
	return TRUE;
}


void editor_update( )
{
	if (previousMode != romLoadedType)
		currentIdx = 0;
	previousMode = romLoadedType;
	draw_tiles(currentZoom);
	draw_pal( );
}

void editor_exit( ) {
	if (texture)	SDL_DestroyTexture(texture);
	if (palTexture)	SDL_DestroyTexture(palTexture);
	if (surface)	SDL_FreeSurface(surface);
	
	texture = NULL;
	palTexture = NULL;
	surface = NULL;
}




void editor_tile_export( ){
	draw_on_surface(1);
	
	SDL_SaveBMP(surface, "tile.bmp");
	
	SDL_FreeSurface(surface);surface = NULL;	
	
	
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Export", "Current exported to tile.bmp", NULL);
}

void editor_tile_next( ){
	if (currentIdx < (tileCount-1))	currentIdx++;
	draw_tiles(currentZoom);
}

void editor_tile_previous( ){
	if (currentIdx)	currentIdx--;
	draw_tiles(currentZoom);
}

void editor_setPixelAt(Sint32 x, Sint32 y, unsigned char doubleClick)
{
	unsigned int pixelX, pixelY;
	if (x<0)	return;
	if (y<0)	return;
	
	if (y < TOOL_HEIGHT)
	{
		toolbarClick(x,y, doubleClick);
		return;
	}
	y -= TOOL_HEIGHT;
	
	pixelX = x / currentZoom;
	pixelY = y / currentZoom;
	
	if (pixelX >= tileWidth)	return;
	if (pixelY >= tileHeight)	return;
		
	setPixel(pixelX, pixelY);
	draw_tiles(currentZoom);
}

void editor_pal_next( ){
	if (currentPalIdx < (PAL_COUNT-1))	currentPalIdx++;
	draw_pal();
	draw_tiles(currentZoom);
}

void editor_pal_previous( ){
	if (currentPalIdx)	currentPalIdx--;
	draw_pal();
	draw_tiles(currentZoom);
}

void editor_selectColorAt(Sint32 x, Sint32 y, unsigned char doubleClick)
{
	if (x<0)	return;
	if (y<0)	return;
	
	currentColorIdx = x/PAL_SQUARE_SIZE;
}


void editor_flip()
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Flip", "Not implemented yet", NULL);
}
void editor_zoom_inc()
{
	currentZoom++;
	currentZoom %= (MAX_ZOOM+1); //1 based
	if (currentZoom == 0) currentZoom++;
	
	//not needed (see events.c)
	//draw_tiles();
}

void editor_about()
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "About", "BombJack GFXer\nBy Kaneda\nv2.0", NULL);
}
