#include "gfxer.h"

#define FPS 	60

extern const char icon[];
extern int icon_size;


extern const char toolbar[];
extern int toolbar_size;


SDL_Renderer* renderer = NULL;
SDL_Renderer* palRenderer = NULL;

static int prev_time = 0;
static int cur_time = 0;

static SDL_Window* window = NULL;
static SDL_Window* palWindow = NULL;
static SDL_Texture* toolbarTexture = NULL;


static SDL_Surface *getIcon( )
{
	int i;
	SDL_RWops *rw = SDL_RWFromConstMem(icon, icon_size);
	if (rw == NULL)
	{
		printf("Asset not find :%s", SDL_GetError());
		return NULL;
	}
	
	//rw is free by loadBmp_RW with the ",1"
	return SDL_LoadBMP_RW(rw, 1);
}

static SDL_Surface *getToolbar( )
{
	int i;
	SDL_RWops *rw = SDL_RWFromConstMem(toolbar, toolbar_size);
	if (rw == NULL)
	{
		printf("Asset not find :%s", SDL_GetError());
		return NULL;
	}
	
	//rw is free by loadBmp_RW with the ",1"
	return SDL_LoadBMP_RW(rw, 1);
}



int video_isMainWindow(Uint32 windowID)
{
	return (windowID == SDL_GetWindowID(window));
}
int video_isPalWindow(Uint32 windowID)
{
	return (windowID == SDL_GetWindowID(palWindow));
}
   
SDL_Surface *video_getSurface()
{
	if (window == NULL)	return NULL;
	
	return SDL_GetWindowSurface(window);
}
int video_init( ) {
	SDL_Surface *icon_surface;
	SDL_Surface *toolbar_surface;
	
	if(SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER) != 0) {
        printf("error initializing SDL: %s\n", SDL_GetError());
        return FALSE;
    }

    Uint32 renderer_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;

    window = SDL_CreateWindow(
            "BombJack GFXer",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            WIN_WIDTH,
            WIN_HEIGHT,
            0
    );
    SDL_SetWindowTitle(window, "BombJack GFXer");
    renderer = SDL_CreateRenderer(window, -1, renderer_flags);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255); //set clear color
    
    toolbar_surface = getToolbar();
    toolbarTexture = SDL_CreateTextureFromSurface(renderer, toolbar_surface);

    icon_surface = getIcon();
    if (icon_surface != NULL)	SDL_SetWindowIcon(window, icon_surface);
    
    palWindow = SDL_CreateWindow(
            "Palettes",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            8*PAL_SQUARE_SIZE,
            PAL_SQUARE_SIZE,
            0
    );
    palRenderer = SDL_CreateRenderer(palWindow, -1, renderer_flags);
    SDL_SetRenderDrawColor(palRenderer, 255, 255, 255, 255); //set clear color

    cur_time = SDL_GetTicks();

	return TRUE;
}


void video_update( ){
	SDL_Rect dest = (SDL_Rect) {
                0,
                TOOL_HEIGHT,
                32*currentZoom,
                32*currentZoom
        };
    SDL_Rect toolbarRect = (SDL_Rect) {
                0,
                0,
                TOOL_HEIGHT*11,
                TOOL_HEIGHT
        };
        
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, toolbarTexture, NULL, &toolbarRect );
	SDL_RenderCopy(renderer, texture, NULL, &dest );	
    SDL_RenderPresent(renderer);

	SDL_RenderClear(palRenderer);
	SDL_RenderCopy(palRenderer, palTexture, NULL, NULL); //&dest );
    SDL_RenderPresent(palRenderer);

    if(cur_time < FPS)
       SDL_Delay(cur_time - (Uint32)prev_time);
            
    prev_time = cur_time;
    cur_time = SDL_GetTicks();
}

void video_exit( ){
	
	SDL_DestroyRenderer(palRenderer);
    SDL_DestroyWindow(palWindow);	
    
	SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);	
}
