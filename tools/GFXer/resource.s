	.data
	.align 2
	.global _icon
_icon:
     .incbin "GFXer.bmp"
     
	.global _icon_size
_icon_size:
    .int _icon_size-_icon

	.align 2
	.global _toolbar
_toolbar:
     .incbin "toolbar.bmp"
     
	.global _toolbar_size
_toolbar_size:
    .int _toolbar_size-_toolbar
