#include <stdio.h>
#include <stdlib.h>
#include "gfxer.h"

unsigned char *romData;
unsigned char romLoadedType = 0xFF;
unsigned char tileWidth, tileHeight;
unsigned int tileCount;

static int loadAt(unsigned int addr, char *filename)
{
	long size;
	FILE *f = fopen(filename, "rb");
	if (f == NULL)	
	{
		printf("Error : can't load %s\n", filename);
		return FALSE;
	}

	fseek(f, 0, SEEK_END);
	size = ftell(f);
	fseek(f, 0, SEEK_SET);
	
	//printf("Load 0x%0lx bytes from %s\n", size, filename);
	fread(romData + addr, size, 1, f);
	
	fclose(f);
	
	return TRUE;
}
static int saveAt(unsigned int addr, unsigned int size, char *filename)
{
	FILE *f = fopen(filename, "wb");
	if (f == NULL)	
	{
		printf("Error : can't write to %s\n", filename);
		return FALSE;
	}
	
	printf("Write 0x%0x bytes to %s\n", size, filename);
	fwrite(romData + addr, size, 1, f);
	fclose(f);
	
	return TRUE;
}


static int load_chars(){
	//0x3000 bytes
	romData = (unsigned char *)	malloc(0x3000);
	if (romData == NULL)
	{
		printf("Can't allocate %d bytes\n", 0x3000);
		return FALSE;
	}
	
	
	if (!loadAt(0x0000, "03_e08t.bin"))	{rom_unload(); return FALSE;}
	if (!loadAt(0x1000, "04_h08t.bin"))	{rom_unload(); return FALSE;}
	if (!loadAt(0x2000, "05_k08t.bin"))	{rom_unload(); return FALSE;}
	
	romLoadedType = ROM_CHARS;
	tileWidth = tileHeight = 8;
	tileCount = 0x200; //0x3000/(3*8); 
	return TRUE;

}
static int save_chars(){
	//0x3000 bytes
	if (romData == NULL)
	{
		printf("No data to save\n");
		return FALSE;
	}
	
	if (!saveAt(0x0000, 0x1000, "03_e08t.bin"))	return FALSE;
	if (!saveAt(0x1000, 0x1000, "04_h08t.bin"))	return FALSE;
	if (!saveAt(0x2000, 0x1000, "05_k08t.bin"))	return FALSE;
	
	
	SDL_ShowSimpleMessageBox(0, "Save", "CHAR saved", NULL);
	
	return TRUE;
}

static int load_tiles( ){
	//0x6000 bytes
	romData = (unsigned char *)	malloc(0x6000);
	if (romData == NULL)
	{
		printf("Can't allocate %d bytes\n", 0x6000);
		return FALSE;
	}
	
	if (!loadAt(0x0000, "06_l08t.bin"))	{rom_unload(); return FALSE;}
	if (!loadAt(0x2000, "07_n08t.bin"))	{rom_unload(); return FALSE;}
	if (!loadAt(0x4000, "08_r08t.bin"))	{rom_unload(); return FALSE;}
	
	romLoadedType = ROM_TILES;	
	tileWidth = tileHeight = 16; 
	tileCount = 0x6000/(3*16*2); //0x100

	return TRUE;
}
static int save_tiles( ){
	//0x6000 bytes
	if (romData == NULL)
	{
		printf("No data to save\n");
		return FALSE;
	}
	
	if (!saveAt(0x0000, 0x2000, "06_l08t.bin"))	return FALSE;
	if (!saveAt(0x2000, 0x2000, "07_n08t.bin"))	return FALSE;
	if (!saveAt(0x4000, 0x2000, "08_r08t.bin"))	return FALSE;
	
	return TRUE;
}



static int load_sprites(){
	//0x6000 bytes	
	romData = (unsigned char *)	malloc(0x6000);
	if (romData == NULL)
	{
		printf("Can't allocate %d bytes\n", 0x6000);
		return FALSE;
	}
	
	if (!loadAt(0x0000, "16_m07b.bin"))	{rom_unload(); return FALSE;}
	if (!loadAt(0x2000, "15_l07b.bin"))	{rom_unload(); return FALSE;}
	if (!loadAt(0x4000, "14_j07b.bin"))	{rom_unload(); return FALSE;}
	
	romLoadedType = ROM_SPRITES;
	
	tileWidth = tileHeight = 16;
	tileCount = 0x80; //0x6000/(3*16*2); 
	return TRUE;
}
static int save_sprites(){
	//0x6000 bytes	
	if (romData == NULL)
	{
		printf("No data to save\n");
		return FALSE;
	}

	if (!saveAt(0x0000, 0x2000, "16_m07b.bin"))	return FALSE;
	if (!saveAt(0x2000, 0x2000, "15_l07b.bin"))	return FALSE;
	if (!saveAt(0x4000, 0x2000, "14_j07b.bin"))	return FALSE;

	return TRUE;
}

static int load_large_sprites(){
	if (!load_sprites())	return FALSE;
	
	romLoadedType = ROM_LSPRITES;
	
	tileWidth = tileHeight = 32;
	tileCount = 0x20; //0x6000/(3*32*4); 
	return TRUE;
}
static int save_large_sprites(){
	return save_sprites();
}


///////// PUBLIC


void rom_unload( ) {
	if (romData)	free(romData );
	
	romData = NULL;
	
	romLoadedType = 0xFF;
}

int rom_load( unsigned char type ){
	if (type == romLoadedType)	return TRUE;

	rom_unload();
	switch(type)
	{
		case ROM_CHARS:
			return load_chars();
			break;
		case ROM_TILES:
			return load_tiles();
			break;
		case ROM_SPRITES:
			return load_sprites();
			break;
		case ROM_LSPRITES:
			return load_large_sprites();
			break;
	}
	
	return FALSE;
}

int rom_save(  ){
	if (0xFF == romLoadedType)	return TRUE;

	switch(romLoadedType)
	{
		case ROM_CHARS:
			return save_chars();
		case ROM_TILES:
			return save_tiles( );
		case ROM_SPRITES:
			return save_sprites();
		case ROM_LSPRITES:
			return save_large_sprites();
			break;
	}
	
	return FALSE;
}

int rom_init( )
{
	return rom_load(ROM_CHARS);
}

void rom_exit( )
{
	rom_unload();
}
